//
//  AddApplyLeave+Action.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddApplyLeaveVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        let compId = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            if isUpdate == "1" {
                requestServer(param: ["edit_type":"leave", "leave_id": leaveApprovalDetailModel?.leave_id ?? "", "start_date": "\(buttonFromYear.titleLabel?.text! ?? "")-\(buttonFromMonth.titleLabel?.text! ?? "")-\(buttonFromDay.titleLabel?.text! ?? "")","end_date": "\(buttonUntilYear.titleLabel?.text! ?? "")-\(buttonUntilMonth.titleLabel?.text! ?? "")-\(buttonUntilDay.titleLabel?.text! ?? "")", "reason": textViewReason.text!, "employee_id": userID,  "company_id": compId,"remarks": textViewReason.text!, "leave_type":self.categoryList[self.buttonSelectCategory.tag].leave_type_id])
            } else {
                requestServerAddLeave(param: ["add_type": "leave" , "from_date": "\(buttonFromYear.titleLabel?.text! ?? "")-\(buttonFromMonth.titleLabel?.text! ?? "")-\(buttonFromDay.titleLabel?.text! ?? "")","to_date": "\(buttonUntilYear.titleLabel?.text! ?? "")-\(buttonUntilMonth.titleLabel?.text! ?? "")-\(buttonUntilDay.titleLabel?.text! ?? "")", "reason": textViewReason.text!, "attachment_file": chooseImage, "company_id":compId, "employee_id": userID, "leave_type_id": self.categoryList[self.buttonSelectCategory.tag].leave_type_id , "remarks": textViewReason.text!])
            }
        }
    }
    
    func isValidate() -> Bool {
        if buttonFromDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please enter start date.")
            return false
        } else if buttonFromMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please enter start month.")
            return false
        } else if buttonFromYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please enter start year.")
            return false
        } else if buttonUntilDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please enter until day.")
            return false
        } else if buttonUntilMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please enter until month.")
            return false
        }  else if buttonUntilMonth.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please enter until year.")
            return false
        } else if buttonSelectCategory.titleLabel?.text == "Select Category" {
            DisplayBanner.show(message: "Please select category.")
            return false
        }else if chooseImage.jpegData(compressionQuality: 0.5)?.count == 0 || chooseImage.jpegData(compressionQuality: 0.5)?.count == nil {
            DisplayBanner.show(message: "Please add evidence.")
            return false
        }
        return true
    }
    
    
    @IBAction func buttonActionFromDay(_ sender: UIButton) {
        Console.log("buttonActionFromDay")
        chooseDayFrom.show()
    }
    @IBAction func buttonActionFromMonth(_ sender: UIButton) {
        Console.log("buttonActionFromMonth")
        chooseMonthFrom.show()
    }
    @IBAction func buttonActionFromYear(_ sender: UIButton) {
        Console.log("buttonActionFromYear")
        chooseYearFrom.show()
    }
    
    @IBAction func buttonActionUntilDay(_ sender: UIButton) {
        Console.log("buttonActionUntilDay")
        chooseDayUntil.show()
    }
    @IBAction func buttonActionUntilMonth(_ sender: UIButton) {
        Console.log("buttonActionUntilMonth")
        chooseMonthUntil.show()
    }
    @IBAction func buttonActionUntilYear(_ sender: UIButton) {
        Console.log("buttonActionUntilYear")
        chooseYearUntil.show()
    }
    
    @IBAction func buttonActionSelectCategory(_ sender: UIButton) {
        Console.log("buttonActionSelectCategory")
        selectCategory.show()
    }
    
    @IBAction func buttonActionAddEvidence(_ sender: UIButton) {
        Console.log("buttonActionAddEvidence")
        self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
            if selected == 1 {
                self.openCamera()
            } else if selected == 2 {
                self.openGallery()
            }
        }
        
    }
}
