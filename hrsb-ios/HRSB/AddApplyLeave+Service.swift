//
//  AddApplyLeave+Service.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddApplyLeaveVC {
    func requestServer(param: [String: Any]) {
        let endPoint = MethodName.updateLeave
        ApiManager.request(path: endPoint, parameters: param, methodType: .post) { (result) in
            switch result {
                case .success(let data):
                    self.handleSuccess(data: data)
                case .failure(let error):
                    self.handleFailure(error: error)
                case .noDataFound(_):
                    break
            }
        }
    }
    func requestServerForCategory(param: [String: Any]) {
        let endPoint = MethodName.leaveCategory
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: param, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleCategorySuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            Console.log("progress \(progress)")
        }
        
    }
    
    
    func handleCategorySuccess(data: Any) {
        if let data = data as? [String: Any] {
            do {
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    let list = try decoder.decode(LeaveCategoryModel.self, from: data)
                    var names = [String]()
                    for cat in list.all_leave_types!  {
                        names.append("\(cat.days_per_year ?? "")  "+(cat.type_name ?? ""))
                        self.categoryList.append(cat)
                    }
                    setupChooseCategoryDropDown(arrStr: names)
                    
                } catch {
                    Console.log(error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleSuccess(data: Any) {
        if let data = data as? [String: Any] {
            let message = data["message"]as? String ?? ""
            DisplayBanner.show(message: message)
            do {
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    if let arrController = self.navigationController?.viewControllers {
                        for vc in arrController {
                            if vc is ApplyLeaveViewController {
                                self.navigationController?.popToViewController(vc, animated: true)
                                break
                            }
                        }
                    }
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func requestServerAddLeave(param: [String: Any]) {
        let endPoint = MethodName.addLeave
        var params = param
        let imgInfo = ["attachment_file": params["attachment_file"] as! UIImage]
        params.removeValue(forKey: "attachment_file")
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params as! Dictionary<String, String>, imageInfo: imgInfo, unReachable: {
            Loader.hide()
            print("unreachable")
        }) { (response, code) in
            Loader.hide()
            if code == 200 {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func handleSuccessAddLeave(data: Any) {
        if let data = data as? [String: Any] {
            let message = data["message"]as? String ?? ""
            DisplayBanner.show(message: message)
            do {
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.navigationController?.popViewController(animated: true)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
