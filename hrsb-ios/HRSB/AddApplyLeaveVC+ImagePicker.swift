//
//  AddApplyLeaveVC+ImagePicker.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddApplyLeaveVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        if let image = info[.originalImage] as? UIImage{
            chooseImage = image
            print("Image Data:- \(image)")
            buttonAddEvidence.setImage(image, for: .normal)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
