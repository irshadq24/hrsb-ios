//
//  AddApplyLeaveVC.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher

class AddApplyLeaveVC: UIViewController {

    @IBOutlet weak var buttonFromDay: UIButton!
    @IBOutlet weak var buttonFromMonth: UIButton!
    @IBOutlet weak var buttonFromYear: UIButton!
    
    @IBOutlet weak var buttonUntilDay: UIButton!
    @IBOutlet weak var buttonUntilMonth: UIButton!
    @IBOutlet weak var buttonUntilYear: UIButton!
   
    @IBOutlet weak var buttonSelectCategory: UIButton!
    @IBOutlet weak var buttonAddEvidence: UIButton!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var textViewReason: GrowingTextView!
    var leaveApprovalDetailModel: LeaveApprovalDetailModel?
    var isUpdate = ""
    var categoryList = [LeaveCategory]()
    var dayValueFrom = ""
    var monthValueFrom = ""
    var yearValueFrom = ""
    
    var dayValueUntil = ""
    var monthValueUntil = ""
    var yearValueUntil = ""
    
    let chooseDayFrom = DropDown()
    let chooseMonthFrom = DropDown()
    let chooseYearFrom = DropDown()
    
    
    let selectCategory = DropDown()
    
    let chooseDayUntil = DropDown()
    let chooseMonthUntil = DropDown()
    let chooseYearUntil = DropDown()
    
    
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDayFrom,
            self.chooseMonthFrom,
            self.chooseYearFrom,
            self.selectCategory,
            self.chooseDayUntil,
            self.chooseMonthUntil,
            self.chooseYearUntil
        ]
    }()
    
    
    var chooseImage = UIImage()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
       
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServerForCategory(param: ["user_id": userID])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isUpdate == "1" {
            configureView()
        }
    }
    
    
    func configureView() {
        if let date = leaveApprovalDetailModel?.from_date?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonFromDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonFromMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonFromYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        if let date = leaveApprovalDetailModel?.to_date?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonUntilDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonUntilMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonUntilYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        
        buttonSelectCategory.setTitle(leaveApprovalDetailModel?.title, for: .normal)
        textViewReason.text = leaveApprovalDetailModel?.reason
        let url = URL(string: leaveApprovalDetailModel?.avidence_file_download ?? "")
        buttonAddEvidence.kf.setImage(with: url, for: .normal)
        
    }
    
    func setup() {
        buttonSubmit.makeRoundCorner(25)
        buttonAddEvidence.makeRoundCorner(5)
        imagePicker.delegate = self
    }
    
    func setupDropDowns() {
        setupChooseDayFromDropDown()
        setupChooseMonthFromDropDown()
        setupChooseYearFromDropDown()
        
        setupChooseDayUntilDropDown()
        setupChooseMonthUntilDropDown()
        setupChooseYearUntilDropDown()
        
    }
    ////
    
    func setupChooseDayUntilDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDayUntil.anchorView = buttonUntilDay
        chooseDayUntil.bottomOffset = CGPoint(x: 0, y: buttonUntilDay.bounds.height)
        chooseDayUntil.dataSource = arrayDay
        // Action triggered on selection
        chooseDayUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonUntilDay.setTitle(item, for: .normal)
            self?.dayValueUntil = item
        }
    }
    func setupChooseMonthUntilDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonthUntil.anchorView = buttonUntilMonth
        chooseMonthUntil.bottomOffset = CGPoint(x: 0, y: buttonUntilMonth.bounds.height)
        chooseMonthUntil.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonthUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonUntilMonth.setTitle(item, for: .normal)
            self?.monthValueUntil = item
        }
    }
    func setupChooseYearUntilDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYearUntil.anchorView = buttonUntilYear
        chooseYearUntil.bottomOffset = CGPoint(x: 0, y: buttonUntilYear.bounds.height)
        chooseYearUntil.dataSource = arrayYear
        // Action triggered on selection
        chooseYearUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonUntilYear.setTitle(item, for: .normal)
            self?.yearValueUntil = item
        }
    }
    
    
    
    /////
    
    func setupChooseDayFromDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDayFrom.anchorView = buttonFromDay
        chooseDayFrom.bottomOffset = CGPoint(x: 0, y: buttonFromDay.bounds.height)
        chooseDayFrom.dataSource = arrayDay
        // Action triggered on selection
        chooseDayFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonFromDay.setTitle(item, for: .normal)
            self?.dayValueFrom = item
        }
    }
    func setupChooseMonthFromDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonthFrom.anchorView = buttonFromMonth
        chooseMonthFrom.bottomOffset = CGPoint(x: 0, y: buttonFromMonth.bounds.height)
        chooseMonthFrom.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonthFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonFromMonth.setTitle(item, for: .normal)
            self?.monthValueFrom = item
        }
    }
    func setupChooseYearFromDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYearFrom.anchorView = buttonFromYear
        chooseYearFrom.bottomOffset = CGPoint(x: 0, y: buttonFromYear.bounds.height)
        chooseYearFrom.dataSource = arrayYear
        // Action triggered on selection
        chooseYearFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonFromYear.setTitle(item, for: .normal)
            self?.yearValueFrom = item
        }
    }
    
    func setupChooseCategoryDropDown(arrStr: [String]) {
        selectCategory.anchorView = buttonSelectCategory
        selectCategory.bottomOffset = CGPoint(x: 0, y: buttonSelectCategory.bounds.height)
        selectCategory.dataSource = arrStr
        // Action triggered on selection
        selectCategory.selectionAction = { [weak self] (index, item) in
            let seprator = item.components(separatedBy: "  ")
            self?.buttonSelectCategory.setTitle(seprator[1], for: .normal)
            self?.buttonSelectCategory.tag = index
            
        }
    }
    

    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }
    
    
}
