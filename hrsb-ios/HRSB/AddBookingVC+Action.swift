//
//  AddBookingVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddBookingVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            Console.log("Success")
            if isUpdate == "isUpdate" {
                requestServerUpdateBooking(param: ["add_type": "update", "booking_id": vehicleBookingData?.booking_id ?? "", "title": textViewTitle.text!, "date_from": "\(buttonStartYear.titleLabel?.text! ?? "")-\(buttonStartMonth.titleLabel?.text! ?? "")-\(buttonStartDay.titleLabel?.text! ?? "")", "date_to": "\(buttonToYear.titleLabel?.text! ?? "")-\(buttonToMonth.titleLabel?.text! ?? "")-\(buttonToDay.titleLabel?.text! ?? "")", "purpose": textViewBookingPurpose.text!, "project": textViewProject.text!])
            } else {
                requestServer(param: ["add_type": "booking", "user_id": userID, "vehicle_id": vehicle_id, "title": textViewTitle.text!, "date_from": "\(buttonStartYear.titleLabel?.text! ?? "")-\(buttonStartMonth.titleLabel?.text! ?? "")-\(buttonStartDay.titleLabel?.text! ?? "")", "date_to": "\(buttonToYear.titleLabel?.text! ?? "")-\(buttonToMonth.titleLabel?.text! ?? "")-\(buttonToDay.titleLabel?.text! ?? "")", "purpose": textViewBookingPurpose.text!, "project": textViewProject.text!])
            }
        }
    }
    
    func isValidate() -> Bool {
        if (textViewTitle.text.isEmpty) {
            DisplayBanner.show(message: "Please write title.")
            return false
        } else if buttonStartDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please select start day.")
            return false
        } else if buttonStartMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select start month.")
            return false
        } else if buttonStartYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select start year.")
            return false
        } else if buttonToDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please select end day.")
            return false
        } else if buttonToDay.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select start end month.")
            return false
        } else if buttonToDay.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select start end year.")
            return false
        } else if (textViewBookingPurpose.text.isEmpty) {
            DisplayBanner.show(message: "Please write booking purpose.")
            return false
        } else if (textViewProject.text.isEmpty) {
            DisplayBanner.show(message: "Please write project detail.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionStartDay(_ sender: UIButton) {
        Console.log("buttonActionStartDay")
        chooseDayFrom.show()
        
    }
    @IBAction func buttonActionStartMonth(_ sender: UIButton) {
        Console.log("buttonActionStartMonth")
        chooseMonthFrom.show()
        
    }
    @IBAction func buttonActionStartYear(_ sender: UIButton) {
        Console.log("buttonActionStartYear")
        chooseYearFrom.show()
        
    }
    @IBAction func buttonActionToDay(_ sender: UIButton) {
        Console.log("buttonActionToDay")
        chooseDayUntil.show()
        
    }
    @IBAction func buttonActionToMonth(_ sender: UIButton) {
        Console.log("buttonActionToMonth")
        chooseMonthUntil.show()
        
    }
    @IBAction func buttonActionToYear(_ sender: UIButton) {
        Console.log("buttonActionToYear")
        chooseYearUntil.show()
        
    }
    
    
    
    
}
