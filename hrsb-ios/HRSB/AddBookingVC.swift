
//
//  AddBookingVC.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class AddBookingVC: UIViewController {

    @IBOutlet weak var buttonStartYear: UIButton!
    @IBOutlet weak var buttonStartMonth: UIButton!
    @IBOutlet weak var buttonStartDay: UIButton!
    
    @IBOutlet weak var buttonToYear: UIButton!
    @IBOutlet weak var buttonToMonth: UIButton!
    @IBOutlet weak var buttonToDay: UIButton!
    
    @IBOutlet weak var textViewTitle: GrowingTextView!
    @IBOutlet weak var textViewBookingPurpose: GrowingTextView!
    @IBOutlet weak var textViewProject: GrowingTextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    var vehicleBookingData: VehicleBookingDetail?
    var isUpdate = ""
    let chooseDayFrom = DropDown()
    let chooseMonthFrom = DropDown()
    let chooseYearFrom = DropDown()
   
    let chooseDayUntil = DropDown()
    let chooseMonthUntil = DropDown()
    let chooseYearUntil = DropDown()
    
    var vehicle_id = ""
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDayFrom,
            self.chooseMonthFrom,
            self.chooseYearFrom,
            self.chooseDayUntil,
            self.chooseMonthUntil,
            self.chooseYearUntil
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
       // Console.log("Data Update:- \(vehicleBookingData)")
        if isUpdate == "isUpdate" {
            setUpUpdateDate(update: vehicleBookingData)
        }
       
    }
    
    func setUpUpdateDate(update: VehicleBookingDetail?) {
        textViewTitle.text = update?.title
        textViewBookingPurpose.text = update?.purpose
        textViewProject.text = update?.project
        
        if let date = update?.date_from?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonStartDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonStartMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonStartYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        if let date = update?.date_to?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonToDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonToMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonToYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        buttonSubmit.setTitle("Update", for: .normal)
    }
    
    func setupDropDowns() {
        setupChooseDayFromDropDown()
        setupChooseMonthFromDropDown()
        setupChooseYearFromDropDown()
        setupChooseDayUntilDropDown()
        setupChooseMonthUntilDropDown()
        setupChooseYearUntilDropDown()
    }
    
    
    func setup() {
        buttonSubmit.makeRoundCorner(25)
    }
    
    func setupChooseDayUntilDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDayUntil.anchorView = buttonToDay
        chooseDayUntil.bottomOffset = CGPoint(x: 0, y: buttonToDay.bounds.height)
        chooseDayUntil.dataSource = arrayDay
        // Action triggered on selection
        chooseDayUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonToDay.setTitle(item, for: .normal)
            self?.buttonToDay.setTitle(item, for: .normal)
        }
    }
    func setupChooseMonthUntilDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonthUntil.anchorView = buttonToMonth
        chooseMonthUntil.bottomOffset = CGPoint(x: 0, y: buttonToMonth.bounds.height)
        chooseMonthUntil.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonthUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonToMonth.setTitle(item, for: .normal)
            self?.buttonToMonth.setTitle(item, for: .normal)
        }
    }
    func setupChooseYearUntilDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYearUntil.anchorView = buttonToYear
        chooseYearUntil.bottomOffset = CGPoint(x: 0, y: buttonToYear.bounds.height)
        chooseYearUntil.dataSource = arrayYear
        // Action triggered on selection
        chooseYearUntil.selectionAction = { [weak self] (index, item) in
            self?.buttonToYear.setTitle(item, for: .normal)
            self?.buttonToYear.setTitle(item, for: .normal)
        }
    }
    
    
    
    /////
    
    func setupChooseDayFromDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDayFrom.anchorView = buttonStartDay
        chooseDayFrom.bottomOffset = CGPoint(x: 0, y: buttonStartDay.bounds.height)
        chooseDayFrom.dataSource = arrayDay
        // Action triggered on selection
        chooseDayFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonStartDay.setTitle(item, for: .normal)
            self?.buttonStartDay.setTitle(item, for: .normal)
        }
    }
    func setupChooseMonthFromDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonthFrom.anchorView = buttonStartMonth
        chooseMonthFrom.bottomOffset = CGPoint(x: 0, y: buttonStartMonth.bounds.height)
        chooseMonthFrom.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonthFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonStartMonth.setTitle(item, for: .normal)
            self?.buttonStartMonth.setTitle(item, for: .normal)
        }
    }
    func setupChooseYearFromDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYearFrom.anchorView = buttonStartYear
        chooseYearFrom.bottomOffset = CGPoint(x: 0, y: buttonStartYear.bounds.height)
        chooseYearFrom.dataSource = arrayYear
        // Action triggered on selection
        chooseYearFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonStartYear.setTitle(item, for: .normal)
            self?.buttonStartYear.setTitle(item, for: .normal)
        }
    }


}
