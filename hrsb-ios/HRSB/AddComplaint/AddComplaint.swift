//
//  AddComplaint.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct AddComplaint: Codable {
    let status: String?
    let message: String?
    let data: AddComplaintDetail?
}

struct AddComplaintDetail: Codable {
    let complaint_from: String?
    let company_id: String?
    let title: String?
    let description: String?
    let complaint_date: String?
    let complaint_against: String?
}

