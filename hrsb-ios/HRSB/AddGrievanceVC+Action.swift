//
//  AddGrievanceVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddGrievanceVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        let compId = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            if isEdit == "1" {
                requestServerEditCompaint(param: ["edit_type": "complaint", "description": textViewDescription.text!,"complaint_date": "\(yearValue)-\(monthValue)-\(dayValue)", "title": textFieldComplaintTitle.text!, "employee_id": userID ,"company_id": compId,"complaint_against": buttonSelectEmployee.titleLabel?.text ?? "", "complaint_id": editDetail?.complaint_id ?? ""])
            } else {
                requestServerAddComplaint(param: ["add_type": "complaint", "description": textViewDescription.text!,"complaint_date": "\(yearValue)-\(monthValue)-\(dayValue)", "title": textFieldComplaintTitle.text!, "employee_id": userID,"company_id": compId,"complaint_against": buttonSelectEmployee.titleLabel?.text ?? ""])
            }
            
        }
    }
    
    
    func isValidate() -> Bool {
        if textFieldComplaintTitle.text == "" {
            DisplayBanner.show(message: "Please add complaint title.")
            return false
        }else if buttonDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please select Day.")
            return false
        }else if buttonMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select Month.")
            return false
        }else if buttonYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select Year.")
            return false
        } else if buttonSelectEmployee.titleLabel?.text == "Select employee(s) name" {
            DisplayBanner.show(message: "Please select employee name.")
            return false
        } else if textViewDescription.text == "" {
            DisplayBanner.show(message: "Please write description.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionDay(_ sender: UIButton) {
        Console.log("buttonActionDay")
        chooseDay.show()
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton) {
        Console.log("buttonActionMonth")
        chooseMonth.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton) {
        Console.log("buttonActionYear")
        chooseYear.show()
    }
    
    @IBAction func buttonActionSelectEmployee(_ sender: UIButton) {
        Console.log("buttonActionSelectEmployee")
        selectEmployee.show()
    }
    
}
