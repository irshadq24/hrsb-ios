//
//  AddGrievanceVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddGrievanceVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.employeeList
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.employeeList = try decoder.decode(EmployeeList.self, from: data)
                    Console.log("EmployeeList:- \(self.employeeList)")
                    setupChooseEmployeeDropDown()
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func requestServerAddComplaint(param: [String: Any]?) {
        let endPoint = MethodName.addComplaint
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessAddComplaint(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessAddComplaint(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.addComplaint = try decoder.decode(AddComplaint.self, from: data)
                    Console.log("AddComplaint:- \(self.addComplaint)")
                    setupChooseEmployeeDropDown()
                    navigationController?.popViewController(animated: true)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func requestServerEditCompaint(param: [String: Any]?) {
        let endPoint = MethodName.editComplaint
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessEditCompaint(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessEditCompaint(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
//                   self.addComplaint = try decoder.decode(AddComplaint.self, from: data)
//                    Console.log("AddComplaint:- \(self.addComplaint)")
                    if let arrController = self.navigationController?.viewControllers {
                        for vc in arrController {
                            if vc is GrievanceViewController {
                                self.navigationController?.popToViewController(vc, animated: true)
                                break
                            }
                        }
                    }
                    DisplayBanner.show(message: "Record updated successfully.")
                   // setupChooseEmployeeDropDown()
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}
