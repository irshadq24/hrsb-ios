//
//  AddGrievanceVC.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class AddGrievanceVC: UIViewController {
    @IBOutlet weak var textFieldComplaintTitle: UITextField!
    @IBOutlet weak var buttonDay: UIButton!
    @IBOutlet weak var buttonMonth: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    @IBOutlet weak var buttonSelectEmployee: UIButton!
    @IBOutlet weak var textViewDescription: GrowingTextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    var employeeList: EmployeeList?
    var addComplaint: AddComplaint?
    var editDetail: GrievanceArrayList?
    var isEdit = ""
    
    var dayValue = ""
    var monthValue = ""
    var yearValue = ""
    
    let chooseDay = DropDown()
    let chooseMonth = DropDown()
    let chooseYear = DropDown()
    let selectEmployee = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDay,
            self.chooseMonth,
            self.chooseYear,
            self.selectEmployee
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID])
    }
    
    func setup() {
        buttonSubmit.makeRoundCorner(25)
        Console.log("Edit Detail:- \(editDetail)")
        if isEdit == "1" {
            textFieldComplaintTitle.text = editDetail?.title
            textViewDescription.text = editDetail?.description
            buttonSelectEmployee.setTitle(editDetail?.complaint_against, for: .normal)
            if let date = editDetail?.complaint_date?.getDateInstance(validFormat: "yyyy-MM-dd") {
                buttonDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
                buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
                buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
            }
        }
        
    }

    func setupDropDowns() {
        setupChooseDayDropDown()
        setupChooseMonthDropDown()
        setupChooseYearDropDown()
    }
    
    func setupChooseDayDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDay.anchorView = buttonDay
        chooseDay.bottomOffset = CGPoint(x: 0, y: buttonDay.bounds.height)
        chooseDay.dataSource = arrayDay
        // Action triggered on selection
        chooseDay.selectionAction = { [weak self] (index, item) in
            self?.buttonDay.setTitle(item, for: .normal)
            self?.dayValue = item
        }
    }
    func setupChooseMonthDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonth.anchorView = buttonMonth
        chooseMonth.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonth.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonth.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.monthValue = item
        }
    }
    func setupChooseYearDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYear.anchorView = buttonYear
        chooseYear.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYear.dataSource = arrayYear
        // Action triggered on selection
        chooseYear.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.yearValue = item
        }
    }
    
    func setupChooseEmployeeDropDown() {
        var empList = [String]()
        for emp in (employeeList?.data)! {
            empList.append(emp.employee_name ?? "")
        }
        
        selectEmployee.anchorView = buttonSelectEmployee
        selectEmployee.bottomOffset = CGPoint(x: 0, y: buttonSelectEmployee.bounds.height)
        selectEmployee.dataSource = empList
        // Action triggered on selection
        selectEmployee.selectionAction = { [weak self] (index, item) in
            self?.buttonSelectEmployee.setTitle(item, for: .normal)
           
        }
    }
    
}
