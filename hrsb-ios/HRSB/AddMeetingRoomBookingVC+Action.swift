//
//  AddMeetingRoomBookingVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddMeetingRoomBookingVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        Console.log("buttonActionSubmit")
        if isValidate() {
             Console.log("Success")
            let data = allDropdown?.all_facilities?[sender.tag]
            if isEdit == "isEdit" {
                requestServerUpdateMeeting(param: ["edit_type": "facility_booking", "facility": data?.facility_id ?? "","date_from": "\(yearValue)-\(monthValue)-\(dayValue)", "date_to": "\(yearValue)-\(monthValue)-\(dayValue)", "purpose": textViewBookingPurpose.text! , "employee_id": userID, "subsidiary": buttonSubsidiries.titleLabel?.text! ?? "", "branch": buttonBranch.titleLabel?.text! ?? "", "remarks": textViewRemark.text!, "time_slot": Array(self.timeSlot), "facilities_room_id": bookingDetailData?.facilities_room_id ?? ""])
            } else {
                requestServer(param: ["add_type": "facility_booking", "facility": data?.facility_id ?? "" , "date_from": "\(yearValue)-\(monthValue)-\(dayValue)", "date_to": "\(yearValue)-\(monthValue)-\(dayValue)", "purpose": textViewBookingPurpose.text! , "employee_id": userID, "subsidiary": buttonSubsidiries.titleLabel?.text! ?? "", "branch": buttonBranch.titleLabel?.text! ?? "", "remarks": textViewRemark.text!, "time_slot": Array(self.timeSlot)])
            }
            
        }
    }
    
    
    func isValidate() -> Bool {
        if buttonSubsidiries.titleLabel?.text == "Select subsidiaries" {
            DisplayBanner.show(message: "Please select subsidiaries.")
            return false
        } else if buttonBranch.titleLabel?.text == "Select branch" {
            DisplayBanner.show(message: "Please select branch.")
            return false
        } else if buttonMeetingRoom.titleLabel?.text == "Select meeting room" {
            DisplayBanner.show(message: "Please select meeting room.")
            return false
        } else if buttonDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please enter day.")
            return false
        } else if buttonMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please enter month.")
            return false
        } else if buttonYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please enter year.")
            return false
        } else if buttonSelectSlot.titleLabel?.text! == "" {
            DisplayBanner.show(message: "Please enter time slot.")
            return false
        } else if textViewBookingPurpose.text.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            DisplayBanner.show(message: "Please write booking purpose.")
            return false
        } else if textViewRemark.text == "" {
            DisplayBanner.show(message: "Please write remark.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionDay(_ sender: UIButton) {
        Console.log("buttonActionDay")
        chooseDay.show()
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton) {
        Console.log("buttonActionMonth")
        chooseMonth.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton) {
        Console.log("buttonActionYear")
        chooseYear.show()
    }
    
    @IBAction func buttonActionSelectSubsidiries(_ sender: UIButton) {
        Console.log("buttonActionSelectSubsidiries")
        selectSubsidiries.show()
    }
    
    @IBAction func buttonActionSelectBranch(_ sender: UIButton) {
        Console.log("buttonActionSelectBranch")
        selectBranch.show()
    }
    
    @IBAction func buttonActionSelectMeetingRoom(_ sender: UIButton) {
        Console.log("buttonActionSelectMeetingRoom")
        selectMeetingRoom.show()
    }
    
    
    @IBAction func buttonActionSelectTimeSlot(_ sender: UIButton) {
        Console.log("buttonActionSelectTimeSlot")
        selectTimeSlot.show()
    }
    
    
    @objc func buttonActionDeleteInterest(_ sender: UIButton) {
        print("buttonActionDeleteInterest")
        interestGroup.remove(at: sender.tag)
        collectionViewTimeSlot.reloadData()
        
    }
    
}
