//
//  AddMeetingRoomBookingVC+CollectionView.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension AddMeetingRoomBookingVC: UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interestGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewTimeSlot.dequeueReusableCell(withReuseIdentifier: "TimeSlotCelll", for: indexPath)as! TimeSlotCelll
        cell.labelTime.text = interestGroup[indexPath.row]
        cell.buttonClose.tag = indexPath.item
        cell.buttonClose.addTarget(self, action: #selector(buttonActionDeleteInterest(_:)), for: .touchUpInside)
        return cell
    }
}

extension AddMeetingRoomBookingVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = interestGroup[indexPath.row].size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
        return CGSize(width: size.width + 72.0, height: 50)
    }
}

