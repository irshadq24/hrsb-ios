//
//  AddMeetingRoomBookingVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension AddMeetingRoomBookingVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.addMeetingBooking
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let message = data["message"]as? String ?? ""
                DisplayBanner.show(message: message)
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    navigationController?.popViewController(animated: true)
//                    self.vehicleListModel = try decoder.decode(VehicleListModel.self, from: data)
//                    Console.log("VehicleListModel:- \(self.vehicleListModel)")
//                    tble.reloadData()
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    func requestServerDropdown() {
        let endPoint = MethodName.dropdown
        ApiManager.request(path:endPoint, parameters: nil, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessDropdown(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessDropdown(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.allDropdown = try decoder.decode(AllDropdown.self, from: data)
                    Console.log("AllDropdown:- \(self.allDropdown)")
                    setupDropDowns()
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func requestServerUpdateMeeting(param: [String: Any]?) {
        let endPoint = MethodName.updateMeetingBooking
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessUpdateMeeting(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessUpdateMeeting(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let message = data["message"]as? String ?? ""
                DisplayBanner.show(message: message)
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    if let nav = navigationController?.viewControllers {
                        for vc in nav {
                            if vc.isKind(of: MeetingBookingRoomVC.self) {
                                navigationController?.popToViewController(vc, animated: true)
                                return
                            }
                        }
                    }
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
