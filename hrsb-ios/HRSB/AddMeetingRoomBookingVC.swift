//
//  AddMeetingRoomBookingVC.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class AddMeetingRoomBookingVC: UIViewController {

    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonSubsidiries: UIButton!
    @IBOutlet weak var buttonBranch: UIButton!
    @IBOutlet weak var buttonMeetingRoom: UIButton!
    @IBOutlet weak var buttonDay: UIButton!
    @IBOutlet weak var buttonMonth: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    
   // @IBOutlet weak var textFeildSlot: UITextField!
   // @IBOutlet weak var textFieldBookingPurposr: UITextField!
    @IBOutlet weak var textViewRemark: GrowingTextView!
    @IBOutlet weak var buttonSelectSlot: UIButton!
    @IBOutlet weak var collectionViewTimeSlot: UICollectionView!
    @IBOutlet weak var textViewBookingPurpose: GrowingTextView!
    var dayValue = ""
    var monthValue = ""
    var yearValue = ""
    var isEdit = ""
    var interestGroup = [String]()
    var timeSlot = Set<String>()
    let chooseDay = DropDown()
    let chooseMonth = DropDown()
    let chooseYear = DropDown()
    let selectSubsidiries = DropDown()
    let selectBranch = DropDown()
    let selectMeetingRoom = DropDown()
    let selectTimeSlot = DropDown()
    var bookingDetailData: BookingDetailData?
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDay,
            self.chooseMonth,
            self.chooseYear,
            self.selectSubsidiries,
            self.selectBranch,
            self.selectMeetingRoom,
            self.selectTimeSlot
        ]
    }()
    var allDropdown: AllDropdown?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
        collectionViewTimeSlot.delegate = self
        collectionViewTimeSlot.dataSource = self
        requestServerDropdown()
    }
    func setup() {
        buttonSubmit.makeRoundCorner(25)
        if isEdit == "isEdit" {
            setupEditDetail(editDeatil: bookingDetailData)
        }
    }
    
    func setupEditDetail(editDeatil: BookingDetailData?) {
        buttonSubsidiries.setTitle(editDeatil?.subsidiary, for: .normal)
        buttonBranch.setTitle(editDeatil?.branch, for: .normal)
        buttonMeetingRoom.setTitle(editDeatil?.facility, for: .normal)
        if let date = editDeatil?.date_from?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        buttonSelectSlot.setTitle(editDeatil?.time_slot, for: .normal)
        textViewBookingPurpose.text = editDeatil?.purpose
        textViewRemark.text = editDeatil?.remarks
        buttonSubmit.setTitle("Update", for: .normal)
        let date = editDeatil?.time_slot
        if let slot = date?.split(separator: ","), slot.count > 0 {
            for time in slot {
                print(String(time).count)
                if (String(time).trimmingCharacters(in: .whitespacesAndNewlines)) != ""{
                    interestGroup.append(String(time))
                }
                
            }
        }
    }

    func setupDropDowns() {
        setupChooseDayDropDown()
        setupChooseMonthDropDown()
        setupChooseYearDropDown()
        setupChooseSubsidiriesDropDown()
        setupChooseBranchDropDown()
        setupChooseMeetingRoomDropDown()
        setupChooseTimeSlotDropDown()
    }
    
    func setupChooseDayDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDay.anchorView = buttonDay
        chooseDay.bottomOffset = CGPoint(x: 0, y: buttonDay.bounds.height)
        chooseDay.dataSource = arrayDay
        // Action triggered on selection
        chooseDay.selectionAction = { [weak self] (index, item) in
            self?.buttonDay.setTitle(item, for: .normal)
            self?.dayValue = item
        }
    }
    func setupChooseMonthDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonth.anchorView = buttonMonth
        chooseMonth.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonth.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonth.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.monthValue = item
        }
    }
    func setupChooseYearDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYear.anchorView = buttonYear
        chooseYear.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYear.dataSource = arrayYear
        // Action triggered on selection
        chooseYear.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.yearValue = item
        }
    }
    
    func setupChooseSubsidiriesDropDown() {
        selectSubsidiries.anchorView = buttonSubsidiries
        selectSubsidiries.bottomOffset = CGPoint(x: 0, y: buttonSubsidiries.bounds.height)
        selectSubsidiries.dataSource = [
            "HRBC Holding Sdn Bhd",
            "HRBC Engineering & Construction",
            "HRBC Plant Services",
            "HRBC Training",
            "HRBC Hydropower"
        ]
        // Action triggered on selection
        selectSubsidiries.selectionAction = { [weak self] (index, item) in
            self?.buttonSubsidiries.setTitle(item, for: .normal)
            
        }
    }
    func setupChooseBranchDropDown() {
        selectBranch.anchorView = buttonBranch
        selectBranch.bottomOffset = CGPoint(x: 0, y: buttonBranch.bounds.height)
        selectBranch.dataSource = [
            "Main Office",
            "Melaka",
            "Terengganu",
            "Melaka"
            
            
           // self?.interestGroup.append(item)
            //self?.collectionViewInterestPost.reloadData()
            
        ]
        // Action triggered on selection
        selectBranch.selectionAction = { [weak self] (index, item) in
            self?.buttonBranch.setTitle(item, for: .normal)
            
        }
    }
    func setupChooseMeetingRoomDropDown() {
        selectMeetingRoom.anchorView = buttonMeetingRoom
        selectMeetingRoom.bottomOffset = CGPoint(x: 0, y: buttonMeetingRoom.bounds.height)
        selectMeetingRoom.dataSource = [
            "Meeting Room 1",
            "Meeting Room 2",
            "Meeting Room 3",
            "Meeting Room 4",
            "Meeting Room 5",
        ]
        // Action triggered on selection
        selectMeetingRoom.selectionAction = { [weak self] (index, item) in
            self?.buttonMeetingRoom.setTitle(item, for: .normal)
            
        }
    }
    func setupChooseTimeSlotDropDown() {
        var list = [String]()
        for obj in allDropdown?.all_timeslots ?? [] {
            list.append((obj.time_from ?? "")+"-"+(obj.time_to ?? ""))
        }
        selectTimeSlot.anchorView = buttonSelectSlot
        selectTimeSlot.bottomOffset = CGPoint(x: 0, y: buttonSelectSlot.bounds.height)
        selectTimeSlot.direction = .bottom
        
        selectTimeSlot.dataSource = list
        
        
        // Action triggered on selection
        selectTimeSlot.selectionAction = { [weak self] (index, item) in
            self?.buttonSelectSlot.setTitle(item, for: .normal)
            self?.interestGroup.append(item)
            self?.timeSlot.insert("\(index)")
            self?.collectionViewTimeSlot.reloadData()
        }
    }
}

