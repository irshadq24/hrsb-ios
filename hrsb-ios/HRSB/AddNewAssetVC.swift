//
//  AddNewAssetVC.swift
//  HRSB
//
//  Created by Salman on 07/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class AddNewAssetVC: UIViewController {

    @IBOutlet weak var txtAssetCode: UITextField!
    @IBOutlet weak var txtAssetName: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtMenufacturer: UITextField!
    @IBOutlet weak var txtSerialNum: UITextField!
    @IBOutlet weak var txtAssetNote: UITextField!
    @IBOutlet weak var txtInvoiceNum: UITextField!
    
    @IBOutlet weak var btnMenufacturer: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnEmployee: UIButton!
    @IBOutlet weak var btnWorking: UIButton!
    
    @IBOutlet weak var btnPurchaseDay: UIButton!
    @IBOutlet weak var btnPurchaseMonth: UIButton!
    @IBOutlet weak var btnPurchaseYear: UIButton!
    
    @IBOutlet weak var btnWarrantyDay: UIButton!
    @IBOutlet weak var btnWarrantyMonth: UIButton!
    @IBOutlet weak var btnWarrantyYear: UIButton!
    
    @IBOutlet weak var btnAddLicense: UIButton!
    @IBOutlet  var txtAddLicense: [UITextField]!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet  var viewLicense: [UIView]!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAssetImage: UIButton!
    
    let picker = SGImagePicker(enableEditing: false)
    var assetImage: UIImage?
    
    let dropDown = DropDown()
    var arrCategory = [[String: Any]]()
    var arrEmployee = [[String: Any]]()
    var arrComapny = [[String: Any]]()
    
    var arrMenufacturer = [[String: Any]]()
    var arrLicence = [[String: Any]]()
    
    var selectedCompany = [String: Any]()
    var selectedEmployee = [String: Any]()
    var selectedCategory = [String: Any]()
    var selectedMenufacturer = [String: Any]()
    var isForEdit = false
    var assetDetail = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        getAllCategory()
        getAllEmployee()
        getAllCompany()
        getAssetMenufacturer()
        fetchAssetLicence()
        for viewLcs in viewLicense {
            viewLcs.isHidden = true
            var contFrame = viewContainer.frame
            contFrame.size.height = contFrame.size.height-70
            viewContainer.frame = contFrame
        }
        btnSubmit.makeRoundCorner(25)
        btnWorking.setTitle("NO", for: .normal)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification , object:nil)
        lblTitle.text = "Add New Asset"
        if isForEdit {
            lblTitle.text = "Update Asset"
            txtAssetName.text = assetDetail["name"] as? String ?? ""
            btnCategory.setTitle(assetDetail["category_name"] as? String ?? "", for: .normal)
            selectedCategory["assets_category_id"] = assetDetail["category_id"] as? String ?? ""
            txtAssetCode.text = assetDetail["company_asset_code"] as? String ?? ""
            btnCompany.setTitle(assetDetail["company_name"] as? String ?? "", for: .normal)
            selectedCompany["company_id"] = assetDetail["company_id"] as? String ?? ""
            btnEmployee.setTitle(assetDetail["employee_name"] as? String ?? "", for: .normal)
            selectedEmployee["user_id"] = assetDetail["employee_id"] as? String ?? ""
            if assetDetail["is_working"] as? String ?? "" == "1" {
                btnWorking.tag = 1
                btnWorking.setTitle("YES", for: .normal)
            }
            if let purchaseDate = assetDetail["purchase_date"] as? String, let date = purchaseDate.getDateInstance(validFormat: "yyyy-MM-dd") {
                self.btnPurchaseDay.setTitle("\(date.day() ?? 0)", for: .normal)
                self.btnPurchaseMonth.setTitle("\(date.month() ?? 0)", for: .normal)
                self.btnPurchaseYear.setTitle("\(date.year() ?? 0)", for: .normal)
            }
            if let warrantyDate = assetDetail["warranty_end_date"] as? String, let date = warrantyDate.getDateInstance(validFormat: "yyyy-MM-dd") {
                self.btnWarrantyDay.setTitle("\(date.day() ?? 0)", for: .normal)
                self.btnWarrantyMonth.setTitle("\(date.month() ?? 0)", for: .normal)
                self.btnWarrantyYear.setTitle("\(date.year() ?? 0)", for: .normal)
            }
            txtInvoiceNum.text = assetDetail["invoice_number"] as? String ?? ""
            btnMenufacturer.setTitle(assetDetail["manufacturer_name"] as? String ?? "", for: .normal)
            selectedMenufacturer["manufacturer_id"] = assetDetail["manufacturer_id"] as? String ?? ""
            txtSerialNum.text = assetDetail["serial_number"] as? String ?? ""
            txtAssetNote.text = assetDetail["asset_note"] as? String ?? ""
            
            let strUrl = (assetDetail["asset_image_path"] as? String ?? "")+(assetDetail["asset_image"] as? String ?? "")
            btnAssetImage.imageView?.kf.setImage(with: URL(string: strUrl), completionHandler: { image, _, _, _ in
                self.assetImage = image
                self.btnAssetImage.setImage(image, for: .normal)
            })
            print("detail",assetDetail)
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        var contFrame = viewContainer.frame
        contFrame.size.height = contFrame.size.height+70
        tblView.contentSize = contFrame.size
        viewContainer.frame = contFrame
    }
    
    
    func getAllEmployee() {
        //let endPoint = MethodName.employeeList
        let endPoint = MethodName.getNormalEmployee
        ApiManager.request(path: endPoint, parameters: ["user_id":getUserDefault(Constant.UserDefaultsKey.UserId)], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrEmployee = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    func getAssetMenufacturer() {
        let endPoint = MethodName.getAssetMenufacturer
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrMenufacturer = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    func getAllCategory() {
        let endPoint = MethodName.getAssetCategory
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrCategory = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    
    
    func getAllCompany() {
        let endPoint = MethodName.dropdown
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["get_all_companies"] as? [[String: Any]] {
                    self?.arrComapny = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapPurchaseDate(_ sender: Any) {
        ViewDatePicker.show { (selectedDate) in
            guard let _ = selectedDate else {
                return
            }
            self.btnPurchaseDay.setTitle("\(selectedDate!.day() ?? 0)", for: .normal)
            self.btnPurchaseMonth.setTitle("\(selectedDate!.month() ?? 0)", for: .normal)
            self.btnPurchaseYear.setTitle("\(selectedDate!.year() ?? 0)", for: .normal)
            print(selectedDate!)
        }
        
    }
    
    @IBAction func tapWarrantyDate(_ sender: Any) {
        ViewDatePicker.show { (selectedDate) in
            guard let _ = selectedDate else {
                return
            }
            self.btnWarrantyDay.setTitle("\(selectedDate!.day() ?? 0)", for: .normal)
            self.btnWarrantyMonth.setTitle("\(selectedDate!.month() ?? 0)", for: .normal)
            self.btnWarrantyYear.setTitle("\(selectedDate!.year() ?? 0)", for: .normal)
            print(selectedDate!)
        }
    }
    
    @IBAction func tapSelectCategory(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrCategory {
            let name = sub["category_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            sender.setTitle(item, for: .normal)
            self?.selectedCategory = self?.arrCategory[index] ?? [:]
        }
        dropDown.show()
    }
    @IBAction func tapCompany(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrComapny {
            let name = sub["name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCompany = self?.arrComapny[index] ?? [:]
            sender.setTitle(item, for: .normal)
        }
        dropDown.show()
    }
    
    @IBAction func tapEmployee(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrEmployee {
            let name = sub["employee_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedEmployee = self?.arrEmployee[index] ?? [:]
            sender.setTitle(item, for: .normal)
        }
        dropDown.show()
    }
    
    @IBAction func tapMenufacturer(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrMenufacturer {
            let name = sub["manufacturer_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedMenufacturer = self?.arrMenufacturer[index] ?? [:]
            sender.setTitle(item, for: .normal)
        }
        dropDown.show()
    }
    
    @IBAction func tapIsWorking(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.dataSource = ["YES", "NO"]
        dropDown.selectionAction = { [weak self] (index, item) in
            sender.setTitle(item, for: .normal)
            sender.tag = index+1
        }
        dropDown.show()
    }
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func tapAddAsset(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.assetImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.assetImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
        }
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        var license = ""
        for txtLicense in txtAddLicense {
            if txtLicense.text != "" {
                license = license+",\(txtLicense.text!)"
            }
        }
        if assetImage == nil {
            self.showAlert(title: "", message: "Please enter all details")
            return
        }
        var params = ["type":"add_asset", "assets_category_id":self.selectedCategory["assets_category_id"] as? String ?? "", "name":txtAssetName.text!, "company_asset_code": txtAssetCode.text!, "is_working": "\(btnWorking.tag)", "company_id": self.selectedCompany["company_id"] as? String ?? "", "employee_id":self.selectedEmployee["user_id"] as? String ?? "", "purchase_date":"\(btnPurchaseYear.titleLabel?.text! ?? "")-\(btnPurchaseMonth.titleLabel?.text! ?? "")-\(btnPurchaseDay.titleLabel?.text! ?? "")", "invoice_number": txtInvoiceNum.text!, "manufacturer": self.selectedMenufacturer["manufacturer_id"] as? String ?? "", "serial_number": txtSerialNum.text!, "warranty_end_date": "\(btnWarrantyYear.titleLabel?.text! ?? "")-\(btnWarrantyMonth.titleLabel?.text! ?? "")-\(btnWarrantyDay.titleLabel?.text! ?? "")", "asset_note": txtAssetNote.text!, "license_tag": license]
        
        for (_, item) in params.enumerated() {
            if item.value == "" {
                self.showAlert(title: "", message: "Please enter all details")
                return
            }
        }
        var endPoint = MethodName.addAsset
        if isForEdit {
            endPoint = MethodName.updateAsset
            params["asset_id"] = assetDetail["assets_id"] as? String ?? ""
            params["type"] = "update_asset"
        }
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["asset_image": self.assetImage!], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
    }
    
    @IBAction func tapDeleteLicense(_ sender: UIButton) {
        txtAddLicense[sender.tag].text = ""
        sender.superview?.isHidden = true
        var contFrame = viewContainer.frame
        contFrame.size.height = contFrame.size.height-70
        viewContainer.frame = contFrame
        tblView.contentSize = contFrame.size
    }
    
    @IBAction func tapAddLicense(_ sender: Any) {
        var tag = 0
        for (index,viewLcs) in viewLicense.enumerated() {
            if viewLcs.isHidden {
                //viewLcs.isHidden = false
                tag = index
                break
            }
        }
        var arrLic = [String]()
        for lic in arrLicence {
            arrLic.append(lic["license_name"] as? String ?? "")
        }
        if arrLic.count == 0 {
            return
        }
        print(self.txtAddLicense.count)
        self.showAlertWithActions(msg: "Please select licence", titles: arrLic) { (selected) in
            print(selected)
            print(arrLic.count)
            self.txtAddLicense?[tag].text = arrLic[selected-1]
            self.txtAddLicense?[tag].isEnabled = false
            self.viewLicense[tag].isHidden = false
            var contFrame = self.viewContainer.frame
            contFrame.size.height = contFrame.size.height+70
            self.tblView.contentSize = contFrame.size
            self.viewContainer.frame = contFrame
        }
    }
    
    func fetchAssetLicence() {
        let endPoint = MethodName.assetDropdown
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: [:], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let detail = dict["data"] as? [[String: String]] {
                    self?.arrLicence = detail
                }
            case .failure(let error):
                break
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
   

    
}




