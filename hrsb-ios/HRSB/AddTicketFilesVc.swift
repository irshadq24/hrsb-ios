//
//  AddTicketFilesVc.swift
//  HRSB
//
//  Created by Salman on 05/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddTicketFilesVc: UIViewController {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descrptionTextView: UITextView!
    let picker = SGImagePicker(enableEditing: false)
    var selectedImage: UIImage?
    var ticketId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.makeRoundCorner(25)
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func attatchmentBtnTap(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    self.selectedImage = image
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    self.selectedImage = image
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
        }
    }
    
    @IBAction func submitBtnTap(_ sender: Any) {
        if selectedImage == nil {
            self.showAlert(title: "", message: "Please select image.")
            return
        }
        if titleTextField.text == "" || descrptionTextView.text == "" {
            self.showAlert(title: "", message: "Please enter details.")
            return
        }
        let params = ["add_type":"file_attachment", "file_title":titleTextField.text!, "file_description":descrptionTextView.text!, "ticket_id":ticketId, "user_id":getUserDefault("UserId")]
        
        let endPoint = MethodName.addAttachment
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["attachment_file": self.selectedImage!], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
        
    }
    
}
