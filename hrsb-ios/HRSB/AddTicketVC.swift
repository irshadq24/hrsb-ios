//
//  AddTicketVC.swift
//  HRSB
//
//  Created by Salman on 03/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import Kingfisher

class CommonCellModel {
    var key = ""
    var value = ""
    var title = ""
    var text = ""
    var isForDropDown = false
    var dropDownDataSource = [String]()
    var index = 0
    var instance: AddTicketVC!
    init(title: String, text: String, value: String = "" ,isForDropDown: Bool = false, dropDownDataSource:[String] = [String](), index:Int = 0, key: String) {
        self.title = title
        self.text = text
        self.isForDropDown = isForDropDown
        self.dropDownDataSource = dropDownDataSource
        self.key = key
        self.index = index
        self.value = value
        if index == 0  {
            self.value = getUserDefault(Constant.UserDefaultsKey.Name)
        }
        if index == 1 {
            self.value = getUserDefault(Constant.UserDefaultsKey.CompanyId)
        }
        if index == 2 {
            self.value = getUserDefault(Constant.UserDefaultsKey.UserId)
        }
        
        
    }
}

class AddTicketVC: UIViewController {
    
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var arrTicket = [CommonCellModel]()
    let picker = SGImagePicker(enableEditing: false)
    var isForEdit = false
    var ticketModel: TicketModel?
    var pickedImage: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureData()
        btnSubmit.makeRoundCorner(25)
    }
    
    func configureData() {
        getCategory()
        getEmployee()
        if isForEdit {
            btnAttachment.imageView?.kf.setImage(with: URL(string: ticketModel?.attachment ?? ""), completionHandler: { image, _, _, _ in
                self.pickedImage = image
                self.btnAttachment.setImage(image, for: .normal)
            })
            lblTitle.text = "Update Ticket"
            arrTicket.append(CommonCellModel(title: "Employee Name", text: ticketModel?.empName ?? "",  value: ticketModel?.empName ?? "", index: arrTicket.count, key: "name"))
            arrTicket.append(CommonCellModel(title: "Company", text: ticketModel?.company ?? "", value: ticketModel?.company ?? "",index: arrTicket.count, key: "company"))
            arrTicket.append(CommonCellModel(title: "Staff ID", text: ticketModel?.ticketCode ?? "", value: ticketModel?.ticketCode ?? "",index: arrTicket.count, key: "employee_id"))
            arrTicket.append(CommonCellModel(title: "Subject", text: ticketModel?.subject ?? "", value: ticketModel?.subject ?? "",index: arrTicket.count, key: "subject"))
            arrTicket.append(CommonCellModel(title: "Category", text: ticketModel?.category ?? "", value: ticketModel?.category ?? "", isForDropDown: true,  dropDownDataSource: ["hardware", "software"], index: arrTicket.count, key: "category"))
            arrTicket.append(CommonCellModel(title: "Sub Category", text: ticketModel?.subCategory ?? "", value: ticketModel?.subCategory ?? "",isForDropDown: true,  dropDownDataSource: [String](), index: arrTicket.count, key: "sub_category"))
            
            if getUserDefault(Constant.UserDefaultsKey.depId) == "5" {
                arrTicket.append(CommonCellModel(title: "Priority", text: ticketModel?.priority ?? "", value: ticketModel?.priority ?? "",isForDropDown: true,  dropDownDataSource: ["Low","Medium", "High", "Critical"], index: arrTicket.count, key: "ticket_priority"))
            }
            
            arrTicket.append(CommonCellModel(title: "Ticket description", text: ticketModel?.description ?? "", value: ticketModel?.description ?? "", index: arrTicket.count, key: "description"))
            for data in arrTicket {
                data.instance = self
            }
            return
        }
        lblTitle.text = "Create Ticket"
        arrTicket.append(CommonCellModel(title: "Employee Name", text: getUserDefault(Constant.UserDefaultsKey.Name), index: arrTicket.count, key: "name"))
        arrTicket.append(CommonCellModel(title: "Company", text: getUserDefault(Constant.UserDefaultsKey.companyName), index: arrTicket.count, key: "company"))
        arrTicket.append(CommonCellModel(title: "Staff ID", text: getUserDefault(Constant.UserDefaultsKey.Name), index: arrTicket.count, key: "employee_id"))
        arrTicket.append(CommonCellModel(title: "Subject", text: "Enter subject", index: arrTicket.count, key: "subject"))
        arrTicket.append(CommonCellModel(title: "Category", text: "Select category",isForDropDown: true,  dropDownDataSource: ["hardware", "software"], index: arrTicket.count, key: "category"))
        arrTicket.append(CommonCellModel(title: "Sub Category", text: "Select sub category",isForDropDown: true,  dropDownDataSource: [String](), index: arrTicket.count, key: "sub_category"))
        if getUserDefault(Constant.UserDefaultsKey.depId) == "5" {
            arrTicket.append(CommonCellModel(title: "Priority", text: "Select level of priority",isForDropDown: true,  dropDownDataSource: ["Low","Medium", "High", "Critical"], index: arrTicket.count, key: "ticket_priority"))
        }
        arrTicket.append(CommonCellModel(title: "Ticket description", text: "Enter description", index: arrTicket.count, key: "description"))
        for data in arrTicket {
            data.instance = self
        }
        
    }
    
    func requestServerDropdown() { //Get all company
        let endPoint = MethodName.dropdown
        ApiManager.request(path:endPoint, parameters: nil, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                break
            case .failure(let error):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    func getCategory() {
        let endPoint = MethodName.getCategory
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    print("read", subCate)
                    var arrSubCat = [String]()
                    for sub in subCate {
                        let name = sub["category_name"] as? String ?? ""
                        arrSubCat.append(name)
                    }
                    self?.arrTicket[4].dropDownDataSource = arrSubCat
                    self?.tblView.reloadData()
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    func getEmployee() {
        let endPoint = MethodName.getEmployee
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    print("read", subCate)
                    var arrSubCat = [String]()
                    for emp in dict {
                        print(emp)
                    }
                    //self?.arrTicket[4].dropDownDataSource = arrSubCat
                    self?.tblView.reloadData()
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        if pickedImage == nil {
            self.showAlert(title: "", message: "Please enter attachment file.")
            return
        }
        for model in arrTicket {
            if model.value == "" {
                self.showAlert(title: "", message: "Please enter all the details.")
                return
            }
        }
        var params = [String: Any]()
        for data in arrTicket {
            params[data.key] = data.value
        }
        var endPoint = MethodName.addTicket
        if isForEdit {
            endPoint = MethodName.updateTicket
            params["edit_type"] = "ticket"
            params["ticket_for"] = ["1"]
            params["ticket_id"] = ticketModel?.ticketId ?? ""
        } else {
            params["add_type"] = "ticket"
            params["ticket_for"] = ["1"]
            params["ticket_priority"] = "1"
        }
        print(params)
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["ticket_file": self.pickedImage!], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
        
//        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { (result) in
//            switch result {
//            case .success(let data):
//                let dict = data as? [String: Any]
//                if let ticketDetail = dict?["data"] as? [String: Any] {
//                    print("read", ticketDetail)
//                }else {
//                    self.showAlert(title: "", message: dict?["message"]as? String ?? "")
//                }
//            case .failure(let error):
//                self.showAlert(title: "", message: error.debugDescription)
//                break
//
//            case .noDataFound(_):
//                break
//            }
//        }) { (progress) in
//            print(progress)
//        }
        
//        ApiManager.request(path: endPoint, parameters: params, methodType: .post) { [weak self](result) in
//            switch result {
//            case .success(let data):
//                if let dict = data as? [String: Any], let ticketDetail = dict["data"] as? [String: Any] {
//                    print("read", ticketDetail)
//                }
//            case .failure(let error):
//                self?.showAlert(title: "", message: error.debugDescription)
//            case .noDataFound(_):
//                break
//            }
//        }
    }
    
    @IBAction func tapAddAttachment(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    sender.imageView?.contentMode = .scaleAspectFill
                    self.pickedImage = image
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    sender.imageView?.contentMode = .scaleAspectFill
                    self.pickedImage = image

                })
            }
        }
    }
}



extension AddTicketVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCommonInput", for: indexPath) as! CellCommonInput
        cell.info = arrTicket[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTicket.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
