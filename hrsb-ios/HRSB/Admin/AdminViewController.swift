//
//  AdminViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class AdminViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    let reuseIdentifier = "AdminCell"
    
    var items = ["Facility Management", "Track and Manage Corporate Vehicle","Vehicle Booking Approval"]
    var itemsImage = ["environmentdocumentation", "track_vehicle_icon", "approval_icon"]
    
    
    @IBOutlet weak var view_white: UIView!
    
    
    @IBOutlet var view_Corner: UIView!
    
    @IBOutlet var view_addfacilitybooking: UIView!
    
    @IBOutlet weak var scroll_addfacilitybooking: UIScrollView!
    
    
    @IBOutlet var view_plus: UIView!
    
    @IBOutlet weak var plus: UIButton!
   
    
    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var tble: UITableView!
    
    
    @IBOutlet weak var submit: UIButton!
    
    @IBOutlet var view_facilitybooking: UIView!
    
    @IBOutlet weak var scroll_facilitybooking: UIScrollView!
    
    
    var appBarViewController = MDCAppBarViewController()

    @IBOutlet weak var backfacilitybooking: UIButton!
    
    
    @IBOutlet weak var closeaddfacilitybooking: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//
//
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)


        
        tble.dataSource = self
        tble.delegate = self
        
        
        self.view.addSubview(view_plus)
        
        view_plus.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)

        
        submit.layer.cornerRadius = 25
        
        
        scroll_addfacilitybooking.contentSize = CGSize(width: 414, height: 900)

        
        self.setheight()

        
        addSlideMenuButton()
        addSlideMenuButton1()
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    
    
    
    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
       // present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! AdminCell
        
        
        
        
        cell.imageViewItem.image = UIImage(named: itemsImage[indexPath.row])
        cell.l1.text = items[indexPath.row]
        
        
//        cell.l2.text = "date"
//        cell.l3.text = "9:00"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.FacilityManagementVC)
            navigationController?.pushViewController(HrVc, animated: true)
        }else if indexPath.row == 1{
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CorporateVehicleVC)
            navigationController?.pushViewController(HrVc, animated: true)
        } else if indexPath.row == 2 {
            let vc = VehicleBookingApprovalVC.instantiate(fromAppStoryboard: .main)
            navigationController?.pushViewController(vc, animated: true)
        }
    
    }



    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    

    
    @IBAction func Back_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)

        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)


    }
    
    
    
    
    @IBAction func BackfaciltyBooking_Action(_ sender: Any) {
        
        
        view_facilitybooking.removeFromSuperview()
        
        
        view_Corner.removeFromSuperview()
        
        
    }
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (70 * items.count))

        
        
        
        
    }

   
    
    
    @IBAction func plus_Action(_ sender: Any) {
        
        
        self.view.addSubview(view_addfacilitybooking)
        
        
        
        
        
        view_addfacilitybooking.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: self.view.frame.height)

        
    }
    
    
    
    
    
    
    
    
    @IBAction func submit_Acton(_ sender: Any) {
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func closeaddfacilitybooking_Acton(_ sender: Any) {
        
        
        
        view_addfacilitybooking.removeFromSuperview()
        
    }
    

}
