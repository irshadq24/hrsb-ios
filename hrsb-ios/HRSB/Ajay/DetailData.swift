//
//  DetailData.swift
//  HRSB
//
//  Created by Zakir Khan on 25/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct DetailData: Codable {
    let response: ResponseDetail?
}
struct ResponseDetail: Codable{
    let status: Bool
    let message: String?
    let is_lineup_declare: Int?
    let data: [DataList]?
}
struct DataList: Codable {
    let id: Int?
    let series_name: String
    let modified: String?
    let is_lineup: Int?
    let team_name: String?
    let player_record: PlayerRecord?
}
struct PlayerRecord: Codable {
    let bowling_style: String?
    let born: String?
    let batting_style: String?
    let id: Int?
    let batting_firstClassAverage: String?
    let country: String?
    let batting_listAStrikeRate: String?
}
