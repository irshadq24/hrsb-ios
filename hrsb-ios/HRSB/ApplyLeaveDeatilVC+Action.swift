//
//  ApplyLeaveDeatilVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 03/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension ApplyLeaveDeatilVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let vc =  AddApplyLeaveVC.instantiate(fromAppStoryboard: .main)
        vc.leaveApprovalDetailModel = leaveApprovalDetailModel
        vc.isUpdate = "1"
       navigationController?.pushViewController(vc, animated: true)
        
    }
}


