//
//  ApplyLeaveDeatilVC.swift
//  HRSB
//
//  Created by Air 3 on 03/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ApplyLeaveDeatilVC: UIViewController {

    
    @IBOutlet weak var labelDateTitle: UILabel!
    @IBOutlet weak var labelTimeTitle: UILabel!
    @IBOutlet weak var labelDateFrom: UILabel!
    @IBOutlet weak var labelDateTo: UILabel!
    @IBOutlet weak var labelLeaveCategory: UILabel!
    @IBOutlet weak var labelProjectName: UILabel!
    @IBOutlet weak var labelReplacementDate: UILabel!
    @IBOutlet weak var labelReason: UILabel!
    @IBOutlet weak var textViewImageLink: UITextView!
    @IBOutlet weak var labelLeaveType: UILabel!
    @IBOutlet weak var viewMainCard: UIView!
    @IBOutlet weak var buttonEdit: UIButton!
    var leaveId = ""
    var leaveApprovalDetailModel: LeaveApprovalDetailModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         buttonEdit.isHidden = true
         viewMainCard.isHidden = true
         let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
         requestServer(param: ["user_id": userID, "leave_id": leaveId])
    }
    
    func configureSetupView(leaveApprovalDetailModel: LeaveApprovalDetailModel) {
        labelDateFrom.text = leaveApprovalDetailModel.from_date
        labelDateTo.text = leaveApprovalDetailModel.to_date
        labelLeaveCategory.text = leaveApprovalDetailModel.leave_type
        labelProjectName.text = leaveApprovalDetailModel.title
        labelReplacementDate.text = "--"
        labelReason.text = leaveApprovalDetailModel.reason
        textViewImageLink.text = leaveApprovalDetailModel.avidence_file_download
        if let date = leaveApprovalDetailModel.created_at?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            labelDateTitle.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            labelTimeTitle.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        if leaveApprovalDetailModel.status == "pending" || leaveApprovalDetailModel.status == "Rejected"{
            buttonEdit.isHidden = false
        } else {
            buttonEdit.isHidden = true
        }
    }

}
