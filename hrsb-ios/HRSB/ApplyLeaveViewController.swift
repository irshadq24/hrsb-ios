//
//  ApplyLeaveViewController.swift
//  HRSB
//
//  Created by BestWeb on 01/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class ApplyLeaveViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    
    @IBOutlet var scroll_leaveappdetails: UIScrollView!
    
    @IBOutlet var lbl_reason_detail: UILabel!
    
    @IBOutlet var lbl_project_detail: UILabel!
    
    
    @IBOutlet var lbl_dateFrom_detail: UILabel!
    
    
    @IBOutlet var lbl_dateuntil_detail: UILabel!
    
    
    @IBOutlet var lbl_replacement_detail: UILabel!
    
    @IBOutlet var lbl_leavecate_detail: UILabel!
    var leaveListModel: LeaveListModel?
    
    
    struct Leave {
        
        let leavecategories: String
        let leavetypeid: String
        let typename: String
        let userid: String
        
    }
    
    var LeaveList = [Leave]()

    @IBOutlet var closeleaveappdtailsbtn: UIButton!
    
    
    
    @IBOutlet var view_leaveappdetails: UIView!
    
    @IBOutlet weak var day1btn: UIButton!
    
    
    @IBOutlet weak var month1btn: UIButton!
    
    
    @IBOutlet weak var year1btn: UIButton!
    
    @IBOutlet weak var day2btn: UIButton!
    
    @IBOutlet weak var month2btn: UIButton!
    @IBOutlet weak var SelectCateBtn: UIButton!
    
    
    @IBOutlet weak var year2btn: UIButton!
    
    @IBOutlet weak var lbl_reason: UILabel!
    @IBOutlet weak var lbl_leaveCate: UILabel!
    
    @IBOutlet weak var lbl_dateuntil: UILabel!
    
    
    @IBOutlet weak var lbl_datefrom: UILabel!
    
    @IBOutlet weak var lbl_addEvi: UILabel!
    

    
    
    
    
    @IBOutlet weak var txt_leaveCategory: UITextField!
    
    @IBOutlet weak var txt_year2: UITextField!
    @IBOutlet weak var txt_month2: UITextField!
    
    
    @IBOutlet weak var txt_day1: UITextField!
    
    @IBOutlet weak var txt_month1: UITextField!
    
    
    @IBOutlet weak var txt_reason: MDCTextField!
    
    @IBOutlet weak var txt_year1: UITextField!
    
    @IBOutlet weak var AddEviBtn: UIButton!
    
    
    
    @IBOutlet weak var txt_day2: UITextField!
    
    
    
    @IBOutlet var view_leaveApplication: UIView!
    
    
    @IBOutlet var view_corner: UIView!
    
    
    @IBOutlet weak var addbtn: UIButton!
    
    @IBOutlet weak var Scroll_leaveApplication: UIScrollView!
    
    
    @IBOutlet weak var view_white: UIView!
    
    
    var appBarViewController = MDCAppBarViewController()
    
    let reuseIdentifier = "ApplyLeaveCell"
    
    var items = ["Replacement Leave", "Medical Leave", "Replacement Leave", "Emergency Leave", "Medical Leave"]

    
    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var closeleaveApp_view: UIButton!
    
    
    @IBOutlet weak var tble: UITableView!
    
    @IBOutlet weak var submit: UIButton!
    
    
    
    @IBOutlet weak var view_one: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        
//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
//
//
//        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
//
//
//
//
//        view_one.frame = CGRect(x: 0, y: self.view_white.frame.origin.y + self.view_white.frame.height, width: self.view.frame.width, height: 60)
//
//
//
//
//        let searchItemImage = UIImage(named: "bell.png")
//        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
//        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
//                                         style: .plain,
//                                         target: self,
//                                         action: #selector(self.NotifyAction))
//
//
//        let tuneItemImage = UIImage(named: "Bell1")
//        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
//        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
//
//        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
//        imageView.contentMode = .scaleAspectFit
//
//        let image = UIImage(named: "toplogo.png")
//        imageView.image = image
//
//        self.appBarViewController.headerStackView.addSubview(imageView)
//
//
//
//
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
//
//
//
//
//
//        self.view.addSubview(view_corner)
//
//        view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
//
//
//
//        submit.layer.cornerRadius = 20
//
//        self.setheight()
//
//
//        addSlideMenuButton()
//
//        addSlideMenuButton1()
//
//        self.LeaveList(userid : "6")
//
//
//
//
//        scroll_leaveappdetails.contentSize = CGSize(width: view.frame.width, height: 1000)

        tble.dataSource = self
        tble.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    
    @IBAction func Back_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)

        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: true, completion: nil)
        
    }

    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = view_one.frame.origin.y + view_one.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }
    
    
    
    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
       // present(presentedVC, animated: false, completion: nil)
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaveListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! ApplyLeaveCell
        let data = leaveListModel?.data?[indexPath.row]
        cell.labelStatus.text = "Status: \(data?.status ?? "")"
        cell.l1.text = data?.leave_type
        if let date = data?.created_at?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            cell.l3.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
             cell.l2.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        self.view .addSubview(view_leaveappdetails)
//
//        view_leaveappdetails.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
//
//
//
//
//        self.LeaveListDetail(userid : "5", leaveid : "1")
            let id  = leaveListModel?.data?[indexPath.row]
            let vc = ApplyLeaveDeatilVC.instantiate(fromAppStoryboard: .main)
            vc.leaveId = id?.leave_id ?? ""
            navigationController?.pushViewController(vc, animated: true)
    }

    
    
    
    
    
    @IBAction func add_ACtion(_ sender: Any) {
        
        
        self.view .addSubview(view_leaveApplication)
        
        view_leaveApplication.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)

        
        lbl_datefrom.frame = CGRect(x: 15, y: 30, width: view.frame.width - 30, height: 16)
        
        
        
        
        
        txt_day1.frame = CGRect(x: 15, y: lbl_datefrom.frame.origin.y + lbl_datefrom.frame.height + 10, width: 80, height: 30)
        
        txt_month1.frame = CGRect(x: view.frame.width/2 - 40, y: lbl_datefrom.frame.origin.y + lbl_datefrom.frame.height + 10, width: 80, height: 30)
        
        txt_year1.frame = CGRect(x: view.frame.width - 110, y: lbl_datefrom.frame.origin.y + lbl_datefrom.frame.height + 10, width: 80, height: 30)

        
        
        
        
        lbl_dateuntil.frame = CGRect(x: 15, y: txt_day1.frame.origin.y + txt_day1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
        
        txt_day2.frame = CGRect(x: 15, y: lbl_dateuntil.frame.origin.y + lbl_dateuntil.frame.height + 10, width: 80, height: 30)

        txt_month2.frame = CGRect(x: view.frame.width/2 - 40, y: lbl_dateuntil.frame.origin.y + lbl_dateuntil.frame.height + 10, width: 80, height: 30)

        txt_year2.frame = CGRect(x: view.frame.width - 110, y: lbl_dateuntil.frame.origin.y + lbl_dateuntil.frame.height + 10, width: 80, height: 30)


        
        lbl_leaveCate.frame = CGRect(x: 15, y: txt_day2.frame.origin.y + txt_day2.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
        txt_leaveCategory.frame = CGRect(x: 15, y: lbl_leaveCate.frame.origin.y + lbl_leaveCate.frame.height + 10, width: view.frame.width - 30, height: 30)


        
        lbl_reason.frame = CGRect(x: 15, y: txt_leaveCategory.frame.origin.y + txt_leaveCategory.frame.height + 15, width: view.frame.width - 30, height: 16)

        
        
        txt_reason.frame = CGRect(x: 15, y: lbl_reason.frame.origin.y + lbl_reason.frame.height + 10, width: view.frame.width - 30, height: 30)

        
        
        lbl_addEvi.frame = CGRect(x: 15, y: txt_reason.frame.origin.y + txt_reason.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
      //  day1btn.frame= CGRect(x: , y: <#T##CGFloat#>, width: <#T##CGFloat#>, height: <#T##CGFloat#>)
        
        
        

    }
    
    
    
    @IBAction func submit_Action(_ sender: Any) {
    }
    
    
    
    
    @IBAction func CloseLeaveApp_Action(_ sender: Any) {
        
        
        view_leaveApplication.removeFromSuperview()
        
    }
    
    
    
    
    
    
    
    
    @IBAction func AddEvi_Action(_ sender: Any) {
        
        
        
        
    }
    
    
    
    
    @IBAction func Day1_Action(_ sender: Any) {
    }
    
    
    
    
    @IBAction func SelctCate_Action(_ sender: Any) {
    }
    
    
    @IBAction func Month1_Action(_ sender: Any) {
    }
    
    
    
    
    
    
    @IBAction func Year1_Action(_ sender: Any) {
    }
    
    
    
    @IBAction func Day2_Action(_ sender: Any) {
    }
    
    @IBAction func Month2_Action(_ sender: Any) {
    }
    
    
    
    @IBAction func Year2_ACtion(_ sender: Any) {
    }
    
    
    
    private func LeaveList(userid : String){
        
        
        //        let url = URL(string: "http://hrsbhr.bestweb.my/api/memo/announcement_list")
        //        Alamofire.request(url!, method: .post, parameters: nil).responseJSON { response in
        //            print(response)
        //            print("res", response)
        
        
        ServiceHelper.sharedInstance.LeaveList(userid: userid){ (result, error) in
            
            
            
            if let resultValue = result as? [String : Any]{
                
                print("resultValue", resultValue)
                
                
                                let data = resultValue["leave_categories"] as! [[String: Any]]
                                print("da", data)
                
                
                
                
                                for dataItem in data{
                
                
                                    print("summary", dataItem["type_name"] as! String)
                                    
                                    
                                    self.LeaveList.append(Leave(leavecategories: dataItem["leave_categories"] as! String, leavetypeid: dataItem["leave_type_id"] as! String, typename: dataItem["type_name"] as! String, userid: dataItem["user_id"] as! String))
                                    
                                    
                                    
                //
                                   self.tble.reloadData()
                //
                //
                                }
                
            }
        }
    }
    

    
    
    
    
    
    
    @IBAction func CloseleaveApp_Action(_ sender: Any) {
        
        
        view_leaveappdetails.removeFromSuperview()
        
    }
    
    
    @IBAction func buttonActionAddApplyLeave(_ sender: Any) {
        Console.log("buttonActionAddApplyLeave")
        let vc =  AddApplyLeaveVC.instantiate(fromAppStoryboard: .main)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    private func LeaveListDetail(userid : String, leaveid : String){
        
        
        //        let url = URL(string: "http://hrsbhr.bestweb.my/api/memo/announcement_list")
        //        Alamofire.request(url!, method: .post, parameters: nil).responseJSON { response in
        //            print(response)
        //            print("res", response)
        
        
        ServiceHelper.sharedInstance.LeaveListDetail(userid: userid, leaveid: leaveid){ (result, error) in
            
            
            if let resultValue = result as? [String : Any]{
                
                print("resultValue", resultValue)
                
                
                if let title = resultValue["title"] as? String{
                    
                    
                    let name = resultValue["complaint_against"] as? String
                    let date = resultValue["from_date"] as? String
                    let From = resultValue["type"] as? String
                    let desc = resultValue["description"] as? String
                    
                    
                    print("respo", date)

                    
                    self.lbl_dateFrom_detail.text = date
                    
                    
                    
                }
            }
            
            
        }
    }
    
    
}
