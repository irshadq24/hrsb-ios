//
//  ApprovalLeaveDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension ApprovalLeaveDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionUpdate(_ sender: UIButton) {
        Console.log("buttonActionUpdate")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            requestServerLeaveAprrovalReject(param: ["user_id": userID, "leave_id": leaveApprovalDetailModel?.leave_id ?? "", "add_type": buttonSelectStatus.titleLabel?.text! ?? ""])
        }
        
    }
    
    func isValidate() -> Bool {
        if buttonSelectStatus.titleLabel?.text == "Select status" {
            DisplayBanner.show(message: "Please select status.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionSelectStatus(_ sender: UIButton) {
        Console.log("buttonActionSelectStatus")
        chooseStatus.show()
    }
}
