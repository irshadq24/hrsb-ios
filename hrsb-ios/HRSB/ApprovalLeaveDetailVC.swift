//
//  ApprovalLeaveDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class ApprovalLeaveDetailVC: UIViewController {

    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonUpdate: UIButton!
    @IBOutlet weak var labelDateTitle: UILabel!
    @IBOutlet weak var labelTimeTitle: UILabel!
    @IBOutlet weak var buttonSelectStatus: UIButton!
    @IBOutlet weak var labelEmployeeId: UILabel!
    @IBOutlet weak var labelEmployeeName: UILabel!
    @IBOutlet weak var labelDateFrom: UILabel!
    @IBOutlet weak var labelDateTo: UILabel!
    @IBOutlet weak var labelLeaveCategory: UILabel!
    @IBOutlet weak var labelProjectName: UILabel!
    @IBOutlet weak var labelReplacementDate: UILabel!
    @IBOutlet weak var labelReason: UILabel!
    @IBOutlet weak var textViewImageLink: UITextView!
    @IBOutlet weak var labelLeaveType: UILabel!
    @IBOutlet weak var viewcard: UIView!
    @IBOutlet weak var viewMainCard: UIView!
    var leaveId = ""
    var leaveApprovalDetailModel: LeaveApprovalDetailModel?
    let chooseStatus = DropDown()
    var statusValue = ""
    var currentStatus = ""
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseStatus,
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
        viewMainCard.isHidden = true
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID, "leave_id": leaveId])
    }
    
    func setup() {
        buttonUpdate.isHidden = true
        if currentStatus == "pending" {
            buttonUpdate.isHidden = false
        }
        buttonUpdate.makeRoundCorner(25)
    }
    
    func configureSetupView(leaveApprovalDetailModel: LeaveApprovalDetailModel) {
        labelDateFrom.text = leaveApprovalDetailModel.from_date
        labelDateTo.text = leaveApprovalDetailModel.to_date
        labelEmployeeName.text = leaveApprovalDetailModel.full_name
        labelLeaveCategory.text = leaveApprovalDetailModel.reason
        labelProjectName.text = leaveApprovalDetailModel.title
        labelReplacementDate.text = "--"
        labelReason.text = leaveApprovalDetailModel.reason
        labelEmployeeId.text = leaveApprovalDetailModel.employee_id
        textViewImageLink.text = leaveApprovalDetailModel.avidence_file_download
        if let date = leaveApprovalDetailModel.created_at?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            labelDateTitle.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            labelTimeTitle.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        viewcard.isHidden = false
        stackHeight.constant = 180
        if leaveApprovalDetailModel.status == "first level approved" || leaveApprovalDetailModel.status == "approved"{
            viewcard.isHidden = true
            stackHeight.constant = 65
        }
    }
    
    func setupDropDowns() {
        setupChooseStatusDropDown()
    }
    
    func setupChooseStatusDropDown() {
        chooseStatus.anchorView = buttonSelectStatus
        chooseStatus.bottomOffset = CGPoint(x: 0, y: buttonSelectStatus.bounds.height)
        chooseStatus.dataSource = [
            "approve",
            "reject"
        ]
        // Action triggered on selection
        chooseStatus.selectionAction = { [weak self] (index, item) in
            self?.buttonSelectStatus.setTitle(item, for: .normal)
            self?.statusValue = item
        }
    }


}
