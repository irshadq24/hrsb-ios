//
//  AssetDetailVC.swift
//  HRSB
//
//  Created by Salman on 16/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AssetDetailVC: UIViewController {

    @IBOutlet weak var lblWarrantyDate: UILabel!
    @IBOutlet weak var lblPurchaseDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblAssetCode: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblEmp: UILabel!
    @IBOutlet weak var lblIsWorking: UILabel!
    @IBOutlet weak var lblInvoiceNum: UILabel!
    @IBOutlet weak var lblSerialNum: UILabel!
    @IBOutlet weak var lblAssetNote: UILabel!
    @IBOutlet weak var imgViewAsset: UIImageView!
    var model: AssetModel!
    var arrLicence = [[String: String]]()
    var assetImagePath = ""
    var assetDetail = [String: Any]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchAssetDetail()
        getAssetUpdateList()
    }
    
    @IBAction func tapDownload(_ sender: Any) {
        if let url = URL(string: assetImagePath)  {
            self.downloadImage(from: url)
        }
    }
    
    @IBAction func tapEdit(_ sender: Any) {
        let addAsset = self.storyboard?.instantiateViewController(withIdentifier: "AddNewAssetVC") as! AddNewAssetVC
        addAsset.isForEdit = true
        addAsset.assetDetail = assetDetail
        self.navigationController?.pushViewController(addAsset, animated: true)
    }
    
    
    @IBAction func tapUpdateAsset(_ sender: Any) {
        let viewAddLic = ViewAddLicence.fromNib(xibName: "ViewAddLicence") as!  ViewAddLicence
        viewAddLic.assetId = self.model.assetId
        viewAddLic.delegate = self
        viewAddLic.frame = self.view.frame
        self.view.addSubview(viewAddLic)
    }
    
    func setupData(info: [String: Any]) {
        assetDetail = info
        
        lblEmp.text = info["employee_name"] as? String ?? ""
        lblTitle.text = info["name"] as? String ?? ""
        lblCompany.text = info["company_name"] as? String ?? ""
        lblCategory.text = info["category_name"] as? String ?? ""
        lblAssetCode.text = info["company_asset_code"] as? String ?? ""
        lblAssetNote.text = info["asset_note"] as? String ?? ""
        if let working = info["is_working"] as? String, working == "1" {
            lblIsWorking.text = "YES"
        }else{
            lblIsWorking.text = "NO"
        }
        lblSerialNum.text = info["serial_number"] as? String ?? ""
        lblInvoiceNum.text = info["invoice_number"] as? String ?? ""
        lblPurchaseDate.text = info["purchase_date"] as? String ?? ""
        lblWarrantyDate.text = info["warranty_end_date"] as? String ?? ""
        imgViewAsset.makeRoundCorner(5)
        imgViewAsset.makeBorder(1, color: UIColor.lightGray)
        assetImagePath = (info["asset_image_path"] as? String ?? "")+(info["asset_image"] as? String ?? "")
        imgViewAsset.kf.setImage(with: URL(string: (info["asset_image_path"] as? String ?? "")+(info["asset_image"] as? String ?? "")))
        tblView.reloadData()
    }
    
    func fetchAssetDetail() {
        let endPoint = MethodName.getAssetDetail
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["assets_id": self.model.assetId], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let detail = dict["data"] as? [String: Any] {
                    self?.setupData(info: detail)
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func getAssetUpdateList() {
        let endPoint = MethodName.getAssetUpdateList
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["asset_id": self.model.assetId], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let detail = dict["data"] as? [[String: String]] {
                    self?.arrLicence = detail
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    
    @IBAction func tapDelete(_ sender: Any) {
        self.showOkCancelAlertWithAction("Are you sure, to delete this asset?") { (isOk) in
            if isOk {
                let endPoint = MethodName.deleteAsset
                ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "delete_asset", "asset_id": self.model.assetId], methodType: .post, result: { [weak self](result) in
                    switch result {
                    case .success(let data):
                        if let dict = data as? [String: Any] {
                            if let status = dict["status"] as? String, status == "1" {
                                self?.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                                    self?.navigationController?.popViewController(animated: true)
                                })
                            } else {
                                self?.showAlert(title: "", message: dict["message"] as? String ?? "")
                            }
                        }
                        print(data)
                    case .failure(let error):
                        self?.showAlert(title: "", message: error.debugDescription)
                    case .noDataFound(_):
                        break
                    }
                }) { (progress) in
                    print(progress)
                }
            }
        }
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension AssetDetailVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCategory", for: indexPath) as! CellCategory
        cell.lblCetegory.text = arrLicence[indexPath.row]["category_name"]
        cell.lblType.text = arrLicence[indexPath.row]["type_name"]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLicence.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
}

extension AssetDetailVC: AddLicenceDelegate {
    func didAddLicencence(info: [String : String]) {
        arrLicence.append(info)
        tblView.reloadData()
    }
}
