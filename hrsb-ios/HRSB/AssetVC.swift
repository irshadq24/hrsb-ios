//
//  AssetVC.swift
//  HRSB
//
//  Created by Salman on 06/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AssetModel {
    var assetId = ""
    var assetName = ""
    var companyName =  ""
    var empName = ""
    var categoryName = ""
    var workingStatus = ""
    
    init(info: [String: Any]) {
        assetId = info["asset_id"] as? String ?? ""
        assetName = info["asset_name"] as? String ?? ""
        companyName = info["company_name"] as? String ?? ""
        empName = info["employee_name"] as? String ?? ""
        categoryName = info["category_name"] as? String ?? ""
        workingStatus = info["working_status"] as? String ?? ""
    }
}


class AssetVC: UIViewController {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var arrAsset = [AssetModel]()
    var isFilterEnable = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnAdd.isHidden = true
        if getUserDefault(Constant.UserDefaultsKey.depId) == "5" {
            btnAdd.isHidden = false
        }
        if isFilterEnable {
            tblView.reloadData()
        }
        else {
            fetchAsset()
        }
        
    }

    
    func fetchAsset() {
        let endPoint = MethodName.fetchAsset
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["user_id": getUserDefault("UserId")], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let assets = dict["data"] as? [[String: Any]] {
                    self?.arrAsset.removeAll()
                    for asset in assets {
                        self?.arrAsset.append(AssetModel(info: asset))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapAdd(_ sender: Any) {
        self.isFilterEnable = false
        let asset = storyboard?.instantiateViewController(withIdentifier: "AddNewAssetVC") as! AddNewAssetVC
        navigationController?.pushViewController(asset, animated: true)
    }
    
    @IBAction func tapFilter(_ sender: Any) {
        let asset = storyboard?.instantiateViewController(withIdentifier: "FilterAssetVC") as! FilterAssetVC
        asset.assetVC = self
        navigationController?.pushViewController(asset, animated: true)
    }
    
    
}

extension AssetVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAsset", for: indexPath) as! CellAsset
        cell.info = arrAsset[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAsset.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = storyboard?.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
        detail.model = arrAsset[indexPath.row]
        navigationController?.pushViewController(detail, animated: true)
        //let AssetModel
    }
}
