//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import MaterialComponents
class BaseViewController: UIViewController, SlideMenuDelegate, SlideMenuDelegate1 {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
//        let topViewController : UIViewController = self.navigationController!.topViewController!
//        print("View Controller is : \(topViewController) \n", terminator: "")
//        switch(index){
//        case 0:
//            print("Home\n", terminator: "")
//
//            self.openViewControllerBasedOnIdentifier("Home")
//
//            break
//        case 1:
//            print("Play\n", terminator: "")
//
//
//
//            self.openViewControllerBasedOnIdentifier("HrVc")
//
//            break
//        case 2:
//            print("Play\n", terminator: "")
//
//            self.openViewControllerBasedOnIdentifier("TestViewController")
//
//            break
//        default:
//            print("default\n", terminator: "")
//        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        }
        
          else if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
                print("Same VC")
            }
        
        else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func addSlideMenuButton(){
        

        
////
//        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
//      //  btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
//
//        let image = UIImage(named: "menu.png")?.withRenderingMode(.alwaysOriginal)
//
//        btnShowMenu.setImage(image, for:  UIControl.State())
//
//        btnShowMenu.frame = CGRect(x: 16, y: view.frame.origin.y + 36, width: 24, height: 24)
//        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
//
//        self.view.addSubview(btnShowMenu)
//
        
        
        
                let menuItemImage = UIImage(named: "menu.png")
                /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        
        
                let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        
                let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                               style: .plain,
                                               target: self,
                                               action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)))
                self.navigationItem.leftBarButtonItem = menuItem


        
//
//        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    
    
    
    
    

    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
    
    
    
    
    
    
    
    
    func addSlideMenuButton1(){
        
        
        
        
        
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        //   btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        
     //   btnShowMenu.setImage(UIImage(named: "profile.png"), for:  UIControl.State())
        
        
        let image = UIImage(named: "profile.png")?.withRenderingMode(.alwaysOriginal)
        btnShowMenu.setImage(image, for:  UIControl.State())
        let url = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.profilePic)
        ApiManager.getImage(url) { (image) in
            if let img = image?.withRenderingMode(.alwaysOriginal) {
                btnShowMenu.setImage(img, for:  UIControl.State())
            }
            
            btnShowMenu.imageView?.contentMode = .scaleAspectFill
            btnShowMenu.imageView?.makeRounded()
        }
       btnShowMenu.frame = CGRect(x: view.frame.width - 40, y: 36, width: 24, height: 24)
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2688:
                print("iPhone XS MAX")
                btnShowMenu.frame = CGRect(x: view.frame.width - 40, y: 60, width: 24, height: 24)
            case 2436:
                print("iPhone X, XS")
                btnShowMenu.frame = CGRect(x: view.frame.width - 40, y: 60, width: 24, height: 24)
            case 1792:
                print("iPhone XR")
                btnShowMenu.frame = CGRect(x: view.frame.width - 40, y: 60, width: 24, height: 24)
            default:
                break
            }
        }
        
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed1(_:)), for: UIControl.Event.touchUpInside)
        self.view.addSubview(btnShowMenu)


        
        
        

        
        
        
        
        
        //        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        //        self.navigationItem.leftBarButtonItem = customBarItem;
    }

    
    
    @objc func onSlideMenuButtonPressed1(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        
        
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//
//        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.ProfileVC)
//        //    presentedVC.view.backgroundColor = UIColor.green
//        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
//        //   let nvc = UINavigationController(rootViewController: presentedVC)
//
//        presentedVC.view.frame = CGRect(x: 40, y: 40, width: 300, height: 500)
//
//
//        present(presentedVC, animated: false, completion: nil)

        
        
        
        let menuVC : ProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        menuVC.btnMenu1 = sender
        menuVC.delegate1 = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x:UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x:0 , y: 0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
        
        
        
        
    }
 
    
    
    
//    if UIDevice().userInterfaceIdiom == .phone {
//    switch UIScreen.main.nativeBounds.height {
//    case 1136:
//    print("iPhone 5 or 5S or 5C")
//
//    case 1334:
//    print("iPhone 6/6S/7/8")
//
//    case 1920, 2208:
//    print("iPhone 6+/6S+/7+/8+")
//
//    case 2436:
//    print("iPhone X, XS")
//
//    case 2688:
//    print("iPhone XS Max")
//
//    case 1792:
//    print("iPhone XR")
//
//    default:
//    print("Unknown")
//    }
//    }
//
//
    
    
    
    
    
    
    
    
    
    func slideMenuItemSelectedAtIndex1(_ index: Int32) {
//        let topViewController : UIViewController = self.navigationController!.topViewController!
//        print("View Controller is : \(topViewController) \n", terminator: "")
//        switch(index){
//        case 0:
//            print("Home\n", terminator: "")
//
//            self.openViewControllerBasedOnIdentifier1("ProfileViewController")
//
//            break
//        case 1:
//            print("Play\n", terminator: "")
//
//
//
//            self.openViewControllerBasedOnIdentifier1("HumanResourceViewController")
//
//            break
//        case 2:
//            print("Play\n", terminator: "")
//
//            self.openViewControllerBasedOnIdentifier1("TestViewController")
//
//            break
//        default:
//            print("default\n", terminator: "")
//        }
    }
    
    func openViewControllerBasedOnIdentifier1(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        }
            
        else if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        }
            
        else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }

    
    
    
    
    
    
}
