//
//  BookingDetail.swift
//  HRSB
//
//  Created by Air 3 on 27/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct BookingDetail: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [BookingDetailData]?
    
}

struct BookingDetailData: Codable {
    let facilities_room_id: String?
    let employee_name: String?
    let subsidiary: String?
    let facility: String?
    let branch: String?
    let date_from: String?
    let time_slot: String?
    let purpose: String?
    let remarks: String?
    let created_date: String?
    
}
