//
//  CalendarModel.swift
//  HRSB
//
//  Created by Zakir Khan on 25/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct CalendarModel: Codable {
    let title: String?
    let all_holidays: [AllHolidays]?
    let all_upcoming_birthday: [AllUpcomingBirthday]?
    let all_training: [AllTraining]?
}

struct AllHolidays: Codable {
    let holiday_id: String?
    let company_id: String?
    let event_name: String?
    let description: String?
    let start_date: String?
    let end_date: String?
    let is_publish: String?
    let created_at: String?
}

struct AllUpcomingBirthday: Codable {
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let date_of_birth: String?
    let next_birthday: String?
}

struct AllTraining: Codable {
    let training_id: String?
    let training_title: String?
    let company_id: String?
    let employee_id: String?
    let training_type_id: String?
    let start_date: String?
    let finish_date: String?
    let training_cost: String?
    
    let training_status: String?
    let description: String?
    let training_file: String?
    let employee_status: String?
    let performance: String?
    let remarks: String?
    let created_at: String?
    
    let training_duration: String?
    let training_venue: String?
    let applying_desc: String?
    let date_of_application: String?
}
