//
//  CalendarViewController+Service.swift
//  HRSB
//
//  Created by Zakir Khan on 25/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation



extension CalendarViewController {
    func requestServer() {
        let endPoint = MethodName.calendarData
        ApiManager.request(path:endPoint, parameters: nil, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.calendarModel = try decoder.decode(CalendarModel.self, from: data)
                    Console.log("CalendarModel:- \(self.calendarModel)")
                    tableViewCalendar.reloadData()

                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
