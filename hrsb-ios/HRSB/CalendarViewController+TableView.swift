//
//  CalendarViewController+TableView.swift
//  HRSB
//
//  Created by Air 3 on 02/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CalendarViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calendarModel?.all_training?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewCalendar.dequeueReusableCell(withIdentifier: "CalendarCell", for: indexPath)as! CalendarCell
        let data = calendarModel?.all_training?[indexPath.row]
        cell.l1.text = data?.description    //"Open house raya"
        cell.l2.text = "9:30"
        cell.l3.text = "15:00"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
