//
//  CalendarViewController.swift
//  HRSB
//
//  Created by BestWeb on 20/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarViewController: UIViewController{

    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var tableViewCalendar: UITableView!
    var calendarModel: CalendarModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        requestServer()
    }
    
    func setup() {
        tableViewCalendar.delegate = self
        tableViewCalendar.dataSource = self
        calendar.delegate = self
        calendar.dataSource = self
    }
    

    
    
}
extension CalendarViewController: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 1
    }
}
