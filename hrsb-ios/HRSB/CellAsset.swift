//
//  CellAsset.swift
//  HRSB
//
//  Created by Salman on 06/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellAsset: UITableViewCell {
    
    @IBOutlet weak var lblEmp: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAssetName: UILabel!
    @IBOutlet weak var lblWorkingStatus: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    var info: AssetModel? {
        didSet {
            lblEmp.text = "Employee: \(info?.empName ?? "")"
            lblCategory.text = info?.categoryName ?? ""
            lblWorkingStatus.text = "Is Working? : \(info?.workingStatus ?? "")"
            lblAssetName.text = info?.assetName
            lblCompanyName.text = info?.companyName
        }
    }
    
    
}
