//
//  CellComment.swift
//  HRSB
//
//  Created by Salman on 05/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellComment: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    
//    "comment_id": "5",
//    "ticket_id": "5",
//    "employee_name": "Admin Admin",
//    "employee_designation": "Software Developer",
//    "ticket_comment": "An other postman comment for this ticket",
//    "created_at": "2019-08-18 03:01:07"
//
    var info: CommentModel? {
        didSet {
            self.lblName.text = info?.empName
            self.lblComment.text = info?.comment
        }
    }

    @IBAction func tapDelete(_ sender: Any) {
        kAppDelegate.window?.rootViewController?.showOkCancelAlertWithAction("Are you sure, you want to delete comment?", handler: { (isOk) in
            if isOk{
                let params = ["edit_type":"delete_comment", "comment_id": self.info?.id ?? ""]
                let endPoint = MethodName.deleteComment
                ApiManager.request(path: endPoint, parameters: params, methodType: .post) { [weak self](result) in
                    switch result {
                    case .success(_):
                        if let indexPath = self?.info?.instance.tblView.indexPath(for: self!) {
                            self?.info?.instance.arrTicketModel.remove(at: indexPath.row)
                            self?.info?.instance.tblView.deleteRows(at: [indexPath], with: .automatic)
                        }
                        break
                    case .failure(let error):
                        break
                    case .noDataFound(_):
                        break
                    }
                }
            }
        })
    }
}
