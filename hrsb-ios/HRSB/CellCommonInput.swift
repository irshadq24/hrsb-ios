//
//  CellCommonInput.swift
//  HRSB
//
//  Created by Salman on 04/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class CellCommonInput: UITableViewCell {

    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnDropDown: UIButton!
    
    let dropDown = DropDown()
    
    var info: CommonCellModel? {
        didSet {
            guard let model = info else {
                return
            }
            lblTitle.text = model.title
            txtInput.placeholder = model.text
            txtInput.isEnabled = true
            if model.index == 0 || model.index == 1 || model.index == 2 {
                txtInput.text = model.text
                txtInput.isEnabled = false
            }else {
                txtInput.text = model.value
            }
            if model.index == 4 || model.index ==  5 || model.index == 6 {
                btnDropDown.setTitle(model.text, for: .normal)
            }
            txtInput.isHidden = model.isForDropDown
            viewDropdown.isHidden = !model.isForDropDown
            
            dropDown.dataSource = model.dropDownDataSource
            
            dropDown.anchorView = btnDropDown
            dropDown.bottomOffset = CGPoint(x: 0, y: btnDropDown.bounds.height)
            
            // Action triggered on selection
            dropDown.selectionAction = { [weak self] (index, item) in
                self?.info?.value = item
                if model.index == 4 {
                    self?.info?.text = item
                    self?.info?.value = "\(index+1)"
                    self?.getSubCategory(mainCat: "\(index+1)")
                }
                if model.index == 5 {
                    self?.info?.value = "\(index+1)"
                }
                if model.index == 6 {
                    self?.info?.value = "\(index+1)"
                }
                self?.btnDropDown.setTitle(item, for: .normal)
            }
        }
    }

    func getSubCategory(mainCat: String) {
        let endPoint = MethodName.getSubCategory
        ApiManager.request(path: endPoint, parameters: ["category": mainCat], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    print("read", subCate)
                    var arrSubCat = [String]()
                    for sub in subCate {
                        let name = sub["category_name"] as? String ?? ""
                        arrSubCat.append(name)
                    }
                    let instance = self?.info?.instance
                    instance?.arrTicket[5].dropDownDataSource = arrSubCat
                    instance?.tblView.reloadData()
                }
            case .failure(_):
                break
                
            case .noDataFound(_):
                break
            }
        }
    }
    
    override func awakeFromNib() {
        txtInput.addTarget(self, action: #selector(self.editingDidChange(_:)), for: .editingChanged)
    }
    
    @objc func editingDidChange(_ sender: UITextField) {
        self.info?.text = sender.text ?? ""
        self.info?.value = sender.text ?? ""
    }
    
    @IBAction func tapDropDown(_ sender: Any) {
        dropDown.show()
    }
}


