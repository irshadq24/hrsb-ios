//
//  CellEditPurposal.swift
//  HRSB
//
//  Created by Salman on 07/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class CellEditPurposal: UITableViewCell {
    
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnDropDown: UIButton!
    let dropDown = DropDown()
    
    
    
    var info: CommonCellDataSource? {
        didSet {
            guard let model = info else {
                return
            }
            txtInput.text = model.displayText
            lblTitle.text = model.title
            txtInput.isHidden = model.isForDropDown
            viewDropdown.isHidden = !model.isForDropDown
            dropDown.dataSource = model.dropDownDataSource
            dropDown.anchorView = btnDropDown
            dropDown.bottomOffset = CGPoint(x: 0, y: btnDropDown.bounds.height)
            btnDropDown.setTitle("\(model.displayText)", for: .normal)
            if model.displayText == "" {
                btnDropDown.setTitleColor(UIColor.lightGray, for: .normal)
            } else {
                btnDropDown.setTitleColor(UIColor.black, for: .normal)
            }
            // Action triggered on selection
            dropDown.selectionAction = { [weak self] (index, item) in
                self?.info?.apiValue = item
                self?.info?.dropdownSelectedIndex = index
                self?.btnDropDown.setTitle(item, for: .normal)
                self?.btnDropDown.setTitleColor(UIColor.black, for: .normal)
            }
        }
    }
    
    
    
    override func awakeFromNib() {
        txtInput.addTarget(self, action: #selector(self.editingDidChange(_:)), for: .editingChanged)
    }
    
    @objc func editingDidChange(_ sender: UITextField) {
        self.info?.displayText = sender.text ?? ""
        self.info?.apiValue = sender.text ?? ""
    }
    
    @IBAction func tapDropDown(_ sender: Any) {
        dropDown.show()
    }
}
