//
//  CellProposal.swift
//  HRSB
//
//  Created by Salman on 01/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellProposal: UITableViewCell {

    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblClosingDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateReceived: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    
    var info: ProposalTrackingModel? {
        didSet {
            lblSubject.text = info?.company ?? ""
            lblType.text = "Type: \(info?.tender_type ?? "")"
            lblStatus.text = "Status: \(info?.status ?? "")"
            if let receive = info?.tender_date_receive.toString(dateFormat: "MMMM"), let closing = info?.closing_submitted_date.toString(dateFormat: "MMMM"){
                lblClosingDate.text = "Closing Date: \(closing)"
                lblDateReceived.text = "Date Receied:  \(receive)"
            }
        }
    }
}
