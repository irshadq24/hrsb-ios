//
//  CellReference.swift
//  HRSB
//
//  Created by Salman on 06/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellReference: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    var info: ReferenceModel? {
        didSet {
            if let time = info?.date.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
                lblDate.text = time.toString(dateFormat: "dd-MM-yyyy")
                lblTime.text = time.toString(dateFormat: "hh:mm a")
                lblName.text = info?.referenceTitle
            }
        }
    }

}
