//
//  CellReviewDate.swift
//  HRSB
//
//  Created by Salman on 11/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellReviewDate: UITableViewCell {

    @IBOutlet weak var btnReviewDate: UIButton!
    @IBOutlet weak var btnSubmissionDate: UIButton!
    @IBOutlet weak var lblTitle: UILabel!


    var info:DateModel? {
        didSet {
            lblTitle.text = info?.title
            if let review = info?.reviewDate {
                btnReviewDate.setTitle(review.toString(dateFormat: "yyyy-MM-dd") , for: .normal)
                btnReviewDate.setTitleColor(UIColor.black, for: .normal)
            } else {
                btnReviewDate.setTitle(info?.title ?? "" , for: .normal)
                btnReviewDate.setTitleColor(UIColor.lightGray, for: .normal)
            }
            if let submission = info?.submissionDate {
                btnSubmissionDate.setTitle(submission.toString(dateFormat: "yyyy-MM-dd") , for: .normal)
                btnSubmissionDate.setTitleColor(UIColor.black, for: .normal)
            }else {
                btnSubmissionDate.setTitle(info?.title ?? "" , for: .normal)
                btnSubmissionDate.setTitleColor(UIColor.lightGray, for: .normal)
            }
        }
    }
    
    
    @IBAction func tapReviewDate(_ sender: UIButton) {
        ViewDatePicker.show { (selectedDate) in
            guard let _ = selectedDate else {
                return
            }
            self.info?.reviewDate = selectedDate
            sender.setTitle(selectedDate!.toString(dateFormat: "yyyy-MM-dd"), for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            print(selectedDate!)
        }
    }
    
    @IBAction func tapProposalDate(_ sender: UIButton) {
        ViewDatePicker.show { (selectedDate) in
            guard let _ = selectedDate else {
                return
            }
            self.info?.submissionDate = selectedDate
            sender.setTitle(selectedDate!.toString(dateFormat: "yyyy-MM-dd"), for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            print(selectedDate!)
        }
    }
    
}
