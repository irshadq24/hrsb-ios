//
//  ClaimDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension ClaimDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionEdit(_ sender: UIButton) {
        Console.log("buttonActionEdit")
        let selectId  = expenseDetail?.data?.id
        let vc = EditClaimVC.instantiate(fromAppStoryboard: .main)
        vc.id = selectId ?? ""
        vc.expenseDetail = self.expenseDetail
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tapDelete(_ sender: Any) {
        self.showOkCancelAlertWithAction("Are you sure you want to delete this claim?") { (isOk) in
            print("Is ok")
            if isOk {
                self.requestDeleteExpence()
            }
        }
    }
    
}

