//
//  ClaimDetailVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


extension ClaimDetailVC {
    func requestDeleteExpence() {
        let params = ["expense_id": id]
        let endPoint = MethodName.deleteExpense
        ApiManager.request(path:endPoint, parameters: params, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let _):
                self?.navigationController?.popViewController(animated: true)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.expenseDetail
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                   self.expenseDetail = try decoder.decode(Expense.self, from: data)
                    let data = expenseDetail?.data
                    labelRemarks.text = data?.remarks
                    
                    labelClaimAmount.text = (data!.amount ?? "")+" (RM)"
                    textViewImageLink.text = data?.billcopy_download
                    lblProjectCode.text = (data!.project_code ?? "")
                    if let date = data?.created_at?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss")  {
                        labelDate.text = date.toString(dateFormat: "dd-MM-yyyy")
                    }
                    
                    let expense_type_id =  data?.expense_type_id
                    let name = data?.all_expense_types?.filter({ (types) -> Bool in
                        return types.expense_type_id == expense_type_id
                    })
                    labelCategory.text = ""
                    if let expance = name, expance.count > 0 {
                        labelCategory.text = expance[0].name ?? ""
                    }
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}
