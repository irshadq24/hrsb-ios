//
//  ClaimDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ClaimDetailVC: UIViewController {

    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelRemarks: UILabel!
    @IBOutlet weak var labelClaimAmount: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textViewImageLink: UITextView!
    @IBOutlet weak var lblProjectCode: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    var expenseDetail: Expense?
    var id = ""
    var pending = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if pending == "2" || pending == "3" || pending == "7"{
            btnDelete.isHidden = true
            btnDelete.isHidden = true
        }
        
        let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
        requestServer(param: ["user_id": userID, "id" : id])
    }
}
