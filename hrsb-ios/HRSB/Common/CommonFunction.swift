//
//  CommonFunction.swift
//  HRSB
//
//  Created by BestWeb on 20/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit




func setUserDefault(_ key:String,value:String){
    let defaults = UserDefaults.standard
    defaults.setValue(value, forKey: key)
}

func getUserDefault(_ key:String) ->String{
    let defaults = UserDefaults.standard
    let val = defaults.value(forKey: key)
    if val != nil{
        return val! as! String
    }
    else{
        return ""
    }
}


struct CommonFunction {
    

    
    static let shared = CommonFunction()
    
    
    
    
    func getViewControllerWithIdentifier(identifier : String) -> UIViewController
    {
        return UIStoryboard(name: Constant.StoryBoardIdentifiers.Main, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }

    func trimString(text : String?) -> String
    {
        if text != nil
        {
            return text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        return ""
        
    }


    func saveDetailsToUserDefault(detailDict : [String : Any])
    {
        UserDefaults.standard.setValuesForKeys(detailDict)
        UserDefaults.standard.synchronize()
    }

    
    func getStringForKeyFromUserDefaults(key : String) -> String
    {
        if let value = UserDefaults.standard.string(forKey: key)
        {
            return value
        }
        return ""
    }
    
    func getBoolValueForKey(key : String) -> Bool
    {
        return UserDefaults.standard.bool(forKey: key)
    }
    
    func getFloatForKey(key : String) -> Float
    {
        return UserDefaults.standard.float(forKey: key)
    }
    
    func getIntForKey(key : String) -> Int
    {
        return UserDefaults.standard.integer(forKey: key)
    }

    
    
}



