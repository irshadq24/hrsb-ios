//
//  Constant.swift
//  HRSB
//
//  Created by BestWeb on 20/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct Constant {
    
    
    
    struct StoryBoardIdentifiers
    {
        static let Main : String = "Main"
    }

    struct ViewControllerIdentifiers
    {
        static let Login = "LoginViewController"
        static let Home = "HomeViewController"
        static let HrVc = "HumanResourceViewController"
        static let RecruitVC = "RecruitmentViewController"
        static let EmployeVC = "EmployeeViewController"
        static let StaffRelationVc = "StaffRelationViewController"
        static let TrainingDeveVC = "TrainingandDevelopmentViewController"
        static let CompenstaionVC = "CompensationViewController"
        static let AdminVC = "AdminViewController"
        static let MemoVC = "MemoViewController"
        static let GrievanceVC = "GrievanceViewController"
        static let DomesticInqVC = "DomesticInqViewController"
        static let CalendarVC = "CalendarViewController"
        static let AttendanceVC = "AttendenceViewController"
        static let OperationVC = "OperationListVC"
        static let NotifyVC = "NotificationViewController"
        static let ProfileVC = "ProfileViewController"
        static let ClaimVC = "ClaimProcessViewController"
        static let ExecutionVC = "ExecutionProcessViewController"
        static let BudgetaryVC = "BudgetaryViewController"
        static let HSEQ = "HSEQViewController"
        static let InFoVC = "InformationTechnologyViewController"
        static let ProcureVC = "ProcurementVC"
        static let SettingVC = "SettingsViewController"
        static let MyActivity = "MyActivityViewController"
        static let IMSVC = "IMSViewController"
        static let HSEVC = "HSEViewController"
        static let ChangePwdVC = "ChangePasswordViewController"
        static let CorporateVehicleVC = "CorporateVehicleViewController"
        static let FacilityManagementVC = "FacilityManagementViewController"
        static let QualityDocVC = "QualityDocViewController"
        static let IncidentListingSummaryVC = "IncidentListingSummaryViewController"
        static let ApplyLeaveVC = "ApplyLeaveViewController"
        static let ForgotPwd = "ForgotPasswordViewController"
        static let VC = "ViewController"
        
    }
    
    
    struct TableViewCellIdentifier {

        
        
        
        
    }
    
    struct ErrorMessage
    {
        static let Alert = "Alert"
        static let Error = "Error"
    }

    
    struct ServiceApi
    {
//        static let DomainUrl = "http://13.250.181.252/api/" //"http://18.139.70.23/api/"
        //        static let DomainUrl = "http://hrsbhr.bestweb.my/api/"
                static let DomainUrl = "http://13.250.181.252/api/"
//        static let DomainUrl = "http://13.58.75.119/api/"
        
        static let SubDomain = "admin/"
        static let Login = "login"
        static let GenerateOTP = ""
        static let Job = "Job/"
        static let JobList = "job_list"
        static let ForgotPwd = "forgot_password"
        static let JobRead = "read"
        static let Memo = "memo/"
        static let Memoannouncementlist = "announcement_list"
        static let ChangePwd = "change_pass"
        static let complaints = "complaints/"
        static let complaintsList = "complaint_list"
        static let complaintsread = "complaint_read"
        static let timesheet = "timesheet/"
        static let leave = "leave"
        static let leavedetail = "leave_detail"
        static let addcomplaint = "add_complaint"
        static let warning = "warning/"
        static let warninglist = "warning_list"
        static let read = "read"

        //operataion module
        static let OperationProjectList = "operation/operation_project_list/"
        static let OperationProjectDropdown = "operation/all_drop_downs"
        static let OperationAddProject = "operation/add_operation_project/"
        static let OperationEditProject = "operation/edit_operation_project/"
        static let OperationProjectDetails = "operation/read_operation_project_detail/"
        static let OperationAllEmployee = "operation/all_employee_list"
        static let OperationAssignEmployee = "operation/list_of_assigned_team/"
        static let OperationSaveAssignWork = "operation/save_assigned_work_team/"
         static let OperationTypeTaskEmp = "operation/type_of_task_drop_downs/"
        static let OperationUpdateAllTaskDetails = "operation/update_all_task_details"
        
        static let OperationSatisfactoryFrom = "Satisfactory_form/add_satisfactory/"
         static let OperationReadSatisfactoryFrom = "Satisfactory_form/read_satisfactory_form"
        static let OperationDeleteClosure = "Project_closure/delete_closure_file"
        static let OperationDeleteExecution = "operation/delete_execution_file"
        static let OperationProjectClosure = "Project_closure/project_closure_list/"
        static let OperationProjectExecution = "operation/project_excution_list"
         static let OperationUploadClosure = "Project_closure/project_closure_file_upload/"
        static let OperationUploadExecution = "operation/project_execution_file_upload"
        static let OperationFinalizedAssign = "operation/list_of_assigned_team_task"
        
        //Audit
        static let GetAuditList = "Internal_audit/get_audit_plan"
        static let GetAuditDetail = "Internal_audit/get_audit_plan_details"
          static let DeleteAudit = "Internal_audit//delete_audit_plan"
          static let GetEmpAudit = "employees/employees_list"
          static let AddAudit = "/Internal_audit/add_audit_plan"
          static let EditAudit = "/Internal_audit/edit_audit_plan"
          static let GetAuditReport = "audit_finding/audit_finding_report_list"
          static let GetAuditReportChart = "audit_finding/audit_finding_report_chart/"
        
    }
    
    struct UserDefaultsKey
    {
        static let MobileNumber = "mobileNumber"
        static let Token = "token"
        static let Email = "Email"
        static let Name = "Name"
        static let ULogin = "ULogin"
        static let UserId = "UserId"
        static let CompanyId = "CompanyId"
        static let Designation = "designation"
        static let EmpId = "EmpId"
        static let userRoleId = "UserRoleId"
        static let profilePic = "profile_picture_view"
        static let companyName = "company"
        static let empCode = "empCode"
        static let depId = "depId"
    }
    
    
    struct SharedConstants {
        
        let Username: String
        let Email: String
    
        
        
    }
    

    
    
}















