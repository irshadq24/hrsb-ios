//
//  ServiceHelper.swift
//  HRSB
//
//  Created by BestWeb on 24/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import Alamofire
class ServiceHelper: NSObject {
    
    
    static let sharedInstance = ServiceHelper()
    
    //MARK:-- Login
    func checkLogin(userName : String, password : String, completionHandler : @escaping( Any?, Error?) -> Void)
    {
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.SubDomain + Constant.ServiceApi.Login
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["username":userName, "password":password, "user_id": "0"] as [String : Any])
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
            case .success:
                if let token = response.response?.allHeaderFields["user"] as? String
                {
                    CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.Token : token])
                    completionHandler(response.result.value, nil)
                }
                else
                {
                    completionHandler(nil,self.getLocalErrorWithCode(errorCode: 98, errorMessage: "Unable to connect with server."))
                }
            case .failure(let error):
                let message : String
                let errorCode : Int
                if let httpStatusCode = response.response?.statusCode {
                    errorCode = httpStatusCode
                    switch(httpStatusCode) {
                    case 400:
                        message = "Username or password not provided."
                        break
                    case 401:
                        message = "Incorrect password."
                        break
                    case 422:
                        message = "This account is inactive, Would you like to activate?"
                        break
                    default:
                        message = "Unable to connect with server."
                        break
                    }
                } else {
                    errorCode = 98
                    message = error.localizedDescription
                }
                completionHandler(nil, self.getLocalErrorWithCode(errorCode: errorCode, errorMessage: message))
            }
        }
    }
    
    
    
    
    
    
    
    func generateOTP(mobileNumber : String, completionHandler : @escaping( Any?, Error?) -> Void)
    {
        let completeURL =  URL(string:Constant.ServiceApi.DomainUrl + Constant.ServiceApi.SubDomain + Constant.ServiceApi.GenerateOTP + "/" + mobileNumber)!
        var request = URLRequest(url: completeURL, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request).responseString { (response) in
            switch response.result {
            case .success:
                print(response.response?.allHeaderFields["OTP"] ?? "No OTP")
                completionHandler(true, nil)
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    // Private Function
    
    //MARK: - api main functions

    private func getRequestWithUrl(urlString : String, postData : [String : Any]) -> URLRequest
    {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
        request.httpMethod = "POST" //set http method as POST
        request.httpBody =  try? JSONSerialization.data(withJSONObject: postData, options: .prettyPrinted)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
        
    }
    private func getNewRequestWithUrl(urlString : String, postData : [String : Any]) -> URLRequest
    {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
        request.httpMethod = "POST" //set http method as POST
        request.httpBody = postData.percentEscaped().data(using: .utf8)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        return request
        
    }
    private func getNewGETRequestWithUrl(urlString : String, getData : [String : Any]) -> URLRequest
       {
           let url = URL(string: urlString)!
           var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
           request.httpMethod = "GET" //set http method as POST
           request.httpBody = getData.percentEscaped().data(using: .utf8)
         //  request.httpBody = postData.percentEscaped().data(using: .utf8)
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
           
           return request
           
       }
    
    private func getRequestWithUrl(urlString : String, method : String = "GET") -> URLRequest
    {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
        request.httpMethod = method //set http method as POST
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.Token), forHTTPHeaderField: "token")
        
        return request
        
    }
    
    
    private func getNewDELETERequestWithUrl(urlString : String, getData : [String : Any]) -> URLRequest
            {
                let url = URL(string: urlString)!
                var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 120.0)
                request.httpMethod = "DELETE" //set http method as POST
                request.httpBody = getData.percentEscaped().data(using: .utf8)
              //  request.httpBody = postData.percentEscaped().data(using: .utf8)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                
                return request
                
            }
    
    private func getLocalErrorWithCode(errorCode : Int, errorMessage : String) -> Error
    {
        return NSError(domain: "HRSB", code: errorCode, userInfo: [NSLocalizedDescriptionKey : errorMessage]) as Error
    }
    
    
    
    
    
    
    
    
    
    func checkLogins(userName : String, password : String  ,completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.SubDomain + Constant.ServiceApi.Login
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["username": userName, "password" : password ] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("response:::", response)
                
                completionHandler(response.result.value, nil)
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
    }
    
    
    func HumanJobList(completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.Job + Constant.ServiceApi.JobList
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: [:] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    func MemoList(userid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.Memo + Constant.ServiceApi.Memoannouncementlist
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    
    func GrievanceList(userid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.complaints + Constant.ServiceApi.complaintsList
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    
    func GrievanceRead(userid : String,complaintid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.complaints + Constant.ServiceApi.complaintsread
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid, "complaint_id":complaintid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    
    
    func HumanJobRead(jobid : String, completionHandler : @escaping( Any?, Error?) -> Void){
        
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.Job + Constant.ServiceApi.JobRead
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["job_id": jobid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("response:::", response)
                
                completionHandler(response.result.value, nil)
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
        
    }
    
    
    //    func loginRest(login: String, password: String, deviceId: String){
    //        let urlStr = "http://hrsbhr.bestweb.my/api/admin/login"
    //        let params = ["username":login, "password":password, "usei_id":deviceId]
    //        _ = try! JSONSerialization.data(withJSONObject: params)
    //
    //        let headers: HTTPHeaders = ["Content-Type": "application/json"]
    //
    //        Alamofire.request(urlStr, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
    //            switch response.result {
    //            case .success:
    //                print("SUKCES with \(response)")
    //            case .failure(let error):
    //                print("ERROR with '\(error)")
    //
    //
    //            }
    //        }
    //
    //    }
    
    
    
    
    
    
    
    
    
    
    
    func ForgotPassword(email : String,completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.SubDomain + Constant.ServiceApi.ForgotPwd
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["email": email] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("response:::", response)
                
                completionHandler(response.result.value, nil)
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
    }
    
    
    
    
    func ChangePwd(Newpwd : String,ConfirmNewPwd: String,userid: String,type: String,completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.SubDomain + Constant.ServiceApi.ChangePwd
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["new_password": Newpwd, "new_password_confirm": ConfirmNewPwd, "user_id": userid, "type":type] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                print("response:::", response)
                
                completionHandler(response.result.value, nil)
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
    }
    
    
    
    
    
    func LeaveList(userid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.timesheet + Constant.ServiceApi.leave
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    
    
    func LeaveListDetail(userid : String,leaveid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.timesheet + Constant.ServiceApi.leavedetail
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid, "leave_id" : leaveid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    
    
    func AddGrievance(addtype : String,employeeid : String,companyid : String,title : String,description : String,complaintdate : String,complaintagainst : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.complaints + Constant.ServiceApi.addcomplaint
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["add_type": addtype,"employee_id": employeeid,"company_id": companyid,"title": title,"description": description,"complaint_date": complaintdate,"complaint_against": complaintagainst,] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    func WarningList(userid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.warning + Constant.ServiceApi.warninglist
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    
    
    
    func WarningRead(userid : String, warningid : String,completionHandler : @escaping( Any?, Error?) -> Void) {
        
        let completeURL =  Constant.ServiceApi.DomainUrl + Constant.ServiceApi.warning + Constant.ServiceApi.read
        
        print("requrl", completeURL)
        
        let request = self.getRequestWithUrl(urlString: completeURL, postData: ["user_id": userid, "warning_id" : warningid] as  [String: Any])
        
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success:
                //  print("response:::", response)
                
                
                
                completionHandler(response.result.value, nil)
                
                
                print("response:::", response.result.value)
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }
    //Operation Module api call
    
    func getOperationProjectList(type:String, completionHandler : @escaping( Any?, Error?) -> Void){
        let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationProjectList
        
        print("requrl", completeURL)
        let reqBody = ["response_list":type,"type":"operation_project_list","user_id":UseridStr ] as  [String: Any]
        let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
        print("reqBody", reqBody)
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)

        }
        
        
    }
    func getOperationProjectDetail(project_id:String, completionHandler : @escaping( Any?, Error?) -> Void){
     
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationProjectDetails
        
        print("requrl", completeURL)
        let reqBody = ["type":"read_operation_project","project_id":project_id ] as  [String: Any]
        let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
        print("reqBody", reqBody)
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)

        }
        
        
    }
    func getOperationAllEmployee( completionHandler : @escaping( Any?, Error?) -> Void){
       
          let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationAllEmployee
          
          print("requrl", completeURL)
          let reqBody = ["type":"all_employee_list"] as  [String: Any]
          let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
          print("reqBody", reqBody)
          print("req", request)
          
          Alamofire.request(request).responseJSON { (response) in
              completionHandler(response.result.value, nil)

          }
          
          
      }
    func getOperationAssignEmployee(reqBody:[String: Any], completionHandler : @escaping( Any?, Error?) -> Void){
          
             let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationAssignEmployee
             
             print("requrl", completeURL)
       // let reqBody = ["type":type,"project_id":project_id] as  [String: Any]
             let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
             print("reqBody", reqBody)
             print("req", request)
             
             Alamofire.request(request).responseJSON { (response) in
                 completionHandler(response.result.value, nil)

             }
             
             
         }
    func getOperationTaskTypeEmp(type:String,project_id:String,user_id:String,response_list:String,completionHandler : @escaping( Any?, Error?) -> Void){
             
                let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationTypeTaskEmp
                
                print("requrl", completeURL)
        let reqBody = ["type":type,"project_id":project_id,"user_id":user_id,"response_list":response_list] as  [String: Any]
                let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
                print("reqBody", reqBody)
                print("req", request)
                
                Alamofire.request(request).responseJSON { (response) in
                    completionHandler(response.result.value, nil)

                }
                
                
            }
    func getOperationUpdateAllTaskDetails(type:String,project_id:String,task_details:[String], completionHandler : @escaping( Any?, Error?) -> Void){
         
            let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationUpdateAllTaskDetails
            
            print("requrl", completeURL)
        let tasks = task_details.joined(separator: ",")
    let reqBody = ["type":type,"project_id":project_id,"task_details":tasks] as  [String: Any]
            let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
            print("reqBody", reqBody)
            print("req", request)
            
            Alamofire.request(request).responseJSON { (response) in
                completionHandler(response.result.value, nil)

            }
            
            
        }
    func getOperationSaveAssignWork(type:String,project_id:String,assign_work:[String], completionHandler : @escaping( Any?, Error?) -> Void){
             
                let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationSaveAssignWork
                
                print("requrl", completeURL)
        let assign = assign_work.joined(separator: ",")
        let reqBody = ["type":type,"project_id":project_id,"assign_work":assign] as  [String: Any]
                let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
                print("reqBody", reqBody)
                print("req", request)
                
                Alamofire.request(request).responseJSON { (response) in
                    completionHandler(response.result.value, nil)

                }
                
                
            }
    func getOperationFinalizeWork(type:String,project_id:String,user_id:String,finalized_status:String, completionHandler : @escaping( Any?, Error?) -> Void){
         
            let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationSaveAssignWork
            
            print("requrl", completeURL)
        let reqBody = ["type":type,"project_id":project_id,"finalized_status":finalized_status,"user_id":user_id] as  [String: Any]
            let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
            print("reqBody", reqBody)
            print("req", request)
            
            Alamofire.request(request).responseJSON { (response) in
                completionHandler(response.result.value, nil)

            }
            
            
        }
    func addOperationProject(type:String,project_title:String,po_number:String,opu:String,status:String,type_of_work:String,type_of_task:String,project_file:String,progress:String, completionHandler : @escaping( Any?, Error?) -> Void){
        let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationAddProject
        
        print("requrl", completeURL)
        let reqBody = ["type":type,"user_id":UseridStr,"project_title":project_title,"po_number":po_number,"opu":opu,"status":status,"type_of_work":type_of_work,"type_of_task":type_of_task,"project_file":project_file,"progress":progress ] as  [String: Any]
        let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
        print("reqBody", reqBody)
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            print(response)
            completionHandler(response.result.value, nil)
        }
        
    }
    
    func getOperationDropdown( completionHandler : @escaping( Any?, Error?) -> Void){
         
         let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationProjectDropdown
         
         print("requrl", completeURL)
        let reqBody = ["":""] as  [String: Any]
         let request = self.getNewRequestWithUrl(urlString: completeURL, postData:reqBody)
         print("reqBody", reqBody)
         print("req", request)
         
         Alamofire.request(request).responseJSON { (response) in
             completionHandler(response.result.value, nil)

         }
         
         
     }
    func addSatisfacgtoryForm(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
             
             let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationSatisfactoryFrom
             
             print("requrl", completeURL)
            //let reqBody = ["":""] as  [String: Any]
             let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
             print("reqBody", parameter)
             print("req", request)
             
             Alamofire.request(request).responseJSON { (response) in
                 completionHandler(response.result.value, nil)

             }
             
             
         }
    func readSatisfacgtoryForm(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
                
                let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationReadSatisfactoryFrom
                
                print("requrl", completeURL)
               //let reqBody = ["":""] as  [String: Any]
                let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
                print("reqBody", parameter)
                print("req", request)
                
                Alamofire.request(request).responseJSON { (response) in
                    completionHandler(response.result.value, nil)

                }
                
                
            }
    func getProjectClosureList(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
                
                let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationProjectClosure
                
                print("requrl", completeURL)
               //let reqBody = ["":""] as  [String: Any]
                let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
                print("reqBody", parameter)
                print("req", request)
                
                Alamofire.request(request).responseJSON { (response) in
                    completionHandler(response.result.value, nil)

                }
                
                
            }
    func getProjectExecutionList(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
                   
                   let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationProjectExecution
                   
                   print("requrl", completeURL)
                  //let reqBody = ["":""] as  [String: Any]
                   let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
                   print("reqBody", parameter)
                   print("req", request)
                   
                   Alamofire.request(request).responseJSON { (response) in
                       completionHandler(response.result.value, nil)

                   }
                   
                   
               }
    func deleteProjectClosure(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
                  
                  let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationDeleteClosure
                  
                  print("requrl", completeURL)
                 //let reqBody = ["":""] as  [String: Any]
                  let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
                  print("reqBody", parameter)
                  print("req", request)
                  
                  Alamofire.request(request).responseJSON { (response) in
                      completionHandler(response.result.value, nil)

                  }
                  
                  
              }
    func deleteProjectExecution(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
                     
                     let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationDeleteExecution
                     
                     print("requrl", completeURL)
                    //let reqBody = ["":""] as  [String: Any]
                     let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
                     print("reqBody", parameter)
                     print("req", request)
                     
                     Alamofire.request(request).responseJSON { (response) in
                         completionHandler(response.result.value, nil)

                     }
                     
                     
                 }
    
    func getFinalizedAssignList(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.OperationFinalizedAssign
        
        print("requrl", completeURL)
       //let reqBody = ["":""] as  [String: Any]
        let request = self.getNewGETRequestWithUrl(urlString: completeURL, getData:parameter)
        print("reqBody", parameter)
        print("req", request)
        
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)

        }
        
        
    }
    //Audit Module api call
    func getAuditList(parameter:String, completionHandler : @escaping( Any?, Error?) -> Void){
           
           let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.GetAuditList
           
           print("requrl", completeURL)
          //let reqBody = ["":""] as  [String: Any]
           //let request = self.getNewRequestWithUrl(urlString: completeURL, postData:parameter)
           print("reqBody", parameter)
           print("req", request)
           
        //http://13.250.181.252/api/Internal_audit/get_audit_plan?get_type=get_audit_plan&user_id=1&pagination=0
        let urlstr = completeURL + parameter
        let request = self.getRequestWithUrl(urlString: urlstr)
           Alamofire.request(request).responseJSON { (response) in
               completionHandler(response.result.value, nil)
            print("response", response.result.value)
           }
           
           
       }
    func getAuditDetail(parameter:String, completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.GetAuditDetail
        
        print("requrl", completeURL)
       //let reqBody = ["":""] as  [String: Any]
        
        let urlstr = completeURL + parameter
        let request = self.getRequestWithUrl(urlString: urlstr)
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)
         print("response", response.result.value)
        }
        
        
    }
    
    func deleteAudit(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
           
           let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.DeleteAudit
           
           print("requrl", completeURL)
          //let reqBody = ["":""] as  [String: Any]
           
           let urlstr = completeURL
          // let request = self.getRequestWithUrl(urlString: urlstr)
        
        let request = self.getNewDELETERequestWithUrl(urlString: urlstr, getData: parameter)
           Alamofire.request(request).responseJSON { (response) in
               completionHandler(response.result.value, nil)
            print("response", response.result.value)
           }
           
           
       }
    func getEmpAudit(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
              
              let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.GetEmpAudit
              
              print("requrl", completeURL)
             //let reqBody = ["":""] as  [String: Any]
              
              let urlstr = completeURL
             // let request = self.getRequestWithUrl(urlString: urlstr)
             let request = self.getNewRequestWithUrl(urlString: urlstr, postData:parameter)
              Alamofire.request(request).responseJSON { (response) in
                  completionHandler(response.result.value, nil)
               print("response", response.result.value)
              }
              
              
          }
    func getAuditReportList(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.GetAuditReport
        
        print("requrl", completeURL)
       //let reqBody = ["":""] as  [String: Any]
        
        let urlstr = completeURL
       // let request = self.getRequestWithUrl(urlString: urlstr)
       let request = self.getNewRequestWithUrl(urlString: urlstr, postData:parameter)
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)
         print("response", response.result.value)
        }
        
        
    }
    func getAuditReportChartList(parameter:[String:Any], completionHandler : @escaping( Any?, Error?) -> Void){
        
        let completeURL =  Constant.ServiceApi.DomainUrl +  Constant.ServiceApi.GetAuditReportChart
        
        print("requrl", completeURL)
       //let reqBody = ["":""] as  [String: Any]
        
        let urlstr = completeURL
       // let request = self.getRequestWithUrl(urlString: urlstr)
       let request = self.getNewRequestWithUrl(urlString: urlstr, postData:parameter)
        Alamofire.request(request).responseJSON { (response) in
            completionHandler(response.result.value, nil)
         print("response", response.result.value)
        }
        
        
    }
}
