//
//  ExpenseList.swift
//  HRSB
//
//  Created by Salman on 24/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct ExpenseList: Codable {
    let total_claims: String?
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [ExpenseData]?
}

struct ExpenseData: Codable {
    let id: String?
    let claim_id: String?
    let expense_type_id: String?
    let purchased_by: String?
    let expense_type: String?
    let company_name: String?
    let amount: String?
    let date: String?
    let status: String?
}
