//
//  CompensationDeatilVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CompensationDeatilVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func buttonActionCreateClaim(_ sender: UIButton) {
        Console.log("buttonActionCreateClaim")
        let vc = EditClaimVC.instantiate(fromAppStoryboard: .main)
        vc.claimId = claimId
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonSubmit(_ sender: Any) {
        let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
        requestServerSubmit(param: ["user_id": userID, "claim_id": claimId])
    }
}
