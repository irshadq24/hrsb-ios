//
//  CompensationDeatilVC+TableView.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit


extension CompensationDeatilVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenseList?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableViewClaimList.dequeueReusableCell(withIdentifier: "ExpenseClaimListCell", for: indexPath)as? ExpenseClaimListCell else {
            return UITableViewCell()
        }
        cell.labelTime.isHidden = true
        let data = expenseList?.data?[indexPath.row]
        cell.labelDate.text = data?.date
        cell.labelFacility.text = data?.amount
        if data?.status == "pending" {
            
        } else {
            
        }
        return cell
    }
}

extension CompensationDeatilVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = expenseList?.data?[indexPath.row]
        let vc = ClaimDetailVC.instantiate(fromAppStoryboard: .main)
        vc.id = data?.id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
