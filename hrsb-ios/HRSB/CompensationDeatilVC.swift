//
//  CompensationDeatilVC.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CompensationDeatilVC: UIViewController {

    @IBOutlet weak var tableViewClaimList: UITableView!
    var claimId = ""
    var date = ""
    var titleName = ""
    var pending = ""
    var expenseList: ExpenseList?
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        registerTableViewCell()
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
        requestServer(param: ["user_id": userID, "claim_id": claimId])
    }
    func setup() {
        if let date = self.date.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            labelDate.text = date.toString(dateFormat: "dd-MM-yyyy")
            labelTime.text = date.toString(dateFormat: "hh:mm a")
        }
        labelTitle.text = titleName
        if pending == "2" || pending == "3" || pending == "7"{
            buttonSubmit.isHidden = true
            buttonEdit.isHidden = true
        } else {
            buttonSubmit.isHidden = false
            buttonEdit.isHidden = false
        }
        buttonSubmit.makeRoundCorner(23)
        
    }

    
    func registerTableViewCell() {
        tableViewClaimList.delegate = self
        tableViewClaimList.dataSource = self
        tableViewClaimList.register(UINib(nibName: "ExpenseClaimListCell", bundle: nil), forCellReuseIdentifier: "ExpenseClaimListCell")
    }
    
}
