//
//  CompensationViewController.swift
//  HRSB
//
//  Created by Air 3 on 24/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents

class CompensationViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate,UIImagePickerControllerDelegate {
    
    
    
    @IBOutlet weak var viewclaimclose: UIButton!
    
    
    @IBOutlet weak var addplus: UIButton!
    
    
    @IBOutlet var view_Corner_Edit: UIView!
    
    @IBOutlet var view_claimdtail: UIView!
    
    @IBOutlet weak var closeaddfile: UIButton!
    
    
    @IBOutlet weak var choosefile: UIButton!
    
    
    @IBOutlet weak var takepoto: UIButton!
    
    
    
    @IBOutlet var view_addfile: UIView!
    
    
    @IBOutlet weak var scrollAddClaim: UIScrollView!
    
    
    @IBOutlet weak var SUBMIT: UIButton!
    
    
    
    @IBOutlet var view_corner: UIView!
    
    
    @IBOutlet weak var plus: UIButton!
    
    
    @IBOutlet weak var close_addclaimbrtn: UIButton!
    
    @IBOutlet var view_addclaim: UIView!
    
    let reuseIdentifier = "CompensationCell"
    
    var items = ["Claim Title", "Claim Title"]
    
    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet weak var tble: UITableView!
    
    
    @IBOutlet weak var back: UIButton!
    
    
    var appBarViewController = MDCAppBarViewController()
    var expenseDetail: ExpenseClaimList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // self.view.addSubview(view_corner)
        
        // view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
        
        
        
        
        //        self.addChild(self.appBarViewController)
        //        self.view.addSubview(self.appBarViewController.view)
        //        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        //  self.appBarViewController.headerView.trackingScrollView = nil
        
        
        // scrollAddClaim.contentSize = CGSize(width: view.frame.width, height: 700)
        
        
        
        // self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        //view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        //        let menuItemImage = UIImage(named: "menu.png")
        //        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        //
        //
        //        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        //
        //        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
        //                                       style: .plain,
        //                                       target: self,
        //                                       action: nil)
        //        self.navigationItem.leftBarButtonItem = menuItem
        
        //        let searchItemImage = UIImage(named: "bell.png")
        //        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        //        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
        //                                         style: .plain,
        //                                         target: self,
        //                                         action: #selector(self.NotifyAction))
        //
        //
        //        let tuneItemImage = UIImage(named: "Bell1")
        //        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        //        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
        //                                       style: .plain,
        //                                       target: self,
        //                                       action: nil)
        //        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        //
        //        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        //        imageView.contentMode = .scaleAspectFit
        //
        //        let image = UIImage(named: "toplogo.png")
        //        imageView.image = image
        //
        //        self.appBarViewController.headerStackView.addSubview(imageView)
        //
        //
        //
        //
        //        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
        
        
        
        tble.dataSource = self
        tble.delegate = self
        
        //   SUBMIT.layer.cornerRadius = 25
        
        //        addSlideMenuButton()
        //        addSlideMenuButton1()
        
        
        //    self.setheight()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID])
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func back_Action(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
        //        let transition = CATransition()
        //        transition.duration = 0.2
        //        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //        transition.type = CATransitionType.push
        //        transition.subtype = CATransitionSubtype.fromLeft
        //        self.view.window!.layer.add(transition, forKey: nil)
        //
        //        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    @objc func NotifyAction(){
        
        
        
        //        let transition = CATransition()
        //        transition.duration = 0.3
        //        transition.type = CATransitionType.push
        //        transition.subtype = CATransitionSubtype.fromRight
        //        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        
        navigationController?.pushViewController(presentedVC, animated: true)
        
        //   present(presentedVC, animated: false, completion: nil)
        
        
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.expenseDetail?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! CompensationCell
        let data = expenseDetail?.data?[indexPath.row]
        cell.l1.text = data?.claim_title
        if let date = data?.claim_time?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            cell.labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            cell.l2.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        if data?.status == "1" {
            cell.l3.text = "Status: Draft"
        } else if data?.status == "2" {
            cell.l3.text = "Status: Approved"
        } else if data?.status == "3" {
            cell.l3.text = "Status: Rejected"
        } else if data?.status == "4" {
            cell.l3.text = "Status: First level approval"
        } else if data?.status == "5" {
            cell.l3.text = "Status: Canceled"
        } else if data?.status == "6" {
            cell.l3.text = "Status: Second level approval"
        } else if data?.status == "7" {
            cell.l3.text = "Status: Submitted"
        }
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.view.addSubview(view_claimdtail)
//        view_claimdtail.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
//
//        self.view.addSubview(view_Corner_Edit)
//
//
//        view_Corner_Edit.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
        let data = expenseDetail?.data?[indexPath.row]
        let vc = CompensationDeatilVC.instantiate(fromAppStoryboard: .main)
        vc.claimId = data?.claim_id ?? ""
        vc.date = data?.claim_time ?? ""
        vc.titleName = data?.claim_title ?? ""
        vc.pending = data?.status ?? ""
        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    
    
    
    @IBAction func plus_btn(_ sender: Any) {
        
        
        self.view.addSubview(view_addclaim)
        
        view_addclaim.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
        
        
    }
    
    
    
    
    
    @IBAction func closeAddClaim_Acton(_ sender: Any) {
        
        
        view_addclaim.removeFromSuperview()
        
        
        
    }
    
    
    @IBAction func submit_Actiom(_ sender: Any) {
        
        
        
        
    }
    
    
    
    @IBAction func closeaddfile_Acton(_ sender: Any) {
        
        view_addfile.removeFromSuperview()
        
        
    }
    
    
    
    
    @IBAction func choosefile_Acton(_ sender: Any) {
        
        
        
        
        
    }
    
    
    
    
    
    
    @IBAction func TakePoto_Acton(_ sender: Any) {
    }
    
    
    
    
    @IBAction func addplus_Acton(_ sender: Any) {
        
        view_addfile.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        
        view.addSubview(view_addfile)
        
        view_addfile.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        
        
    }
    
    
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (70 * items.count))
        
        
        
        
        
    }
    
    
    
    
    
    @IBAction func Action_CloseViewDat(_ sender: Any) {
        
        
        
        view_claimdtail.removeFromSuperview()
        
        view_Corner_Edit.removeFromSuperview()
    }
    
    @IBAction func buttonActionCreateClaim(_ sender: UIButton) {
        Console.log("buttonActionCreateClaim")
        let vc = CreateClaimVC.instantiate(fromAppStoryboard: .main)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension Date {
    func toString(dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func monthString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: self).capitalized
    }
    
    func month()-> Int? {
        return Calendar.current.ordinality(of: .month, in: .year, for: self)
    }
    
    func day()-> Int? {
        return Calendar.current.ordinality(of: .day, in: .month, for: self)
    }
    
    func year()-> Int? {
        return Calendar.current.component(.year, from: self)
    }
    
}
