//
//  Constants.swift
//  Fabco
//
//  Created by Salman on 24/07/19.
//  Copyright © 2019 Pankaj. All rights reserved.
//


import Foundation
import UIKit

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate

struct Constants {
    static let sharedApplication = UIApplication.shared
    static let sharedAppDelegate = sharedApplication.delegate as! AppDelegate
    static let sharedWindow = sharedApplication.keyWindow!
    static let userDefaults  = UserDefaults.standard
    static let screen = UIScreen.main
    static let fileManager = FileManager.default
    static let userListPlaceholder = UIImage(named: "userListPlaceholder")
    
    
    struct  FontName {
        static let harrisonSerifProDEMOFont = "HarrisonSerifProDEMO-Bold"
        static let helveticaLTStdRoman = "HelveticaLTStd-Roman"
        static let helveticaLTStdBOLD = "HELVETICALTSTD-BOLD"
        static let helveticaLTStdObl = "HelveticaLTStd-Obl"
        static let merriweatherRegular = "Merriweather-Regular"
    }
    struct Color {
        static let appDefaultViolet = UIColor(hexString: "#683894")
        static let appDefaultBlack1 = UIColor(hexString: "#000000")
        static let appDefaultLightBlack = UIColor(hexString: "#757575")
        
        static let appDefaultYellow = UIColor(hexString: "#FDC300")
        static let appDefaultGray = UIColor(hexString: "#D0D0D0")
        static let appDefaultGreen = UIColor(hexString: "#6897B1")
        static let appDefaultGreen1 = UIColor(hexString: "#396E8B")
        static let appDefaultWhite = UIColor(hexString: "#F0F0F7")
        static let appDefaultDarkGray = UIColor(hexString: "#2D2D2D")
        static let appDefaultLightGray = UIColor(hexString: "#CDCED2")
        static let appDefaultRed = UIColor(hexString: "#FD0000")
        static let appDefaultFaddedGray = UIColor(hexString: "#707070")
        static let appDefaultBlue = UIColor(hexString: "#143258")
        static let appDeselectTabBar = UIColor(hexString: "#F09295")
        static let appNavigationBar = UIColor(hexString: "#ebebeb")
        
        
    }
    
    struct ErrorMessages {
        static let noNetwork = "Check your internet connection."
    }
}

struct DebugMessages {
    static let wrongScreen = "Error in screen transition"
}

struct API {
//    static let baseUrl =    "http://hrsbhr.bestweb.my/api/"
        static let baseUrl = "http://13.250.181.252/api/"
//        static let baseUrl = "http://13.58.75.119/api/"
    static let assetsUrl = ""
    
}

struct CellIdentifier {
    static let tableCellRecruitementDetail = "RecruitementDetailCell"
    
}

struct MethodName {
    static let recruitementDetail = "Job/read/"
    static let complaintList = "complaints/complaint_list"
    static let trainingList = "training/training_list"
    static let trainingJoin = "training/join"
    static let documentDetail = "employees/documents"
    static let expenseList = "expense/expense_list"
    static let expenseClaimDetailList = "expense/claim_detail_list"
    static let expenseListClaim = "expense/list_claim"
    static let expenseDetail = "expense/details"
    static let deleteExpense = "expense/delete_expense"
    static let createClaim = "expense/add_claim"
    static let updateClaim = "expense/update_claim"
    static let addExpence = "expense/add_expense"
    static let bookingRoomList = "facilitiese/list_room_booking"
    static let bookingDeatil = "facilitiese/detail_room_booking"
    static let employeeList = "employees/employees_list"
    static let addComplaint = "complaints/add_complaint"
    static let memoRead = "memo/read"
    static let warningRead = "warning/read"
    static let editComplaint = "complaints/update_complaint"
    static let quipmentMaintenance = "maintenance/maintenance_list"
    static let getAttandence = "timesheet/attendance_list"
    static let leaveList = "timesheet/leave_list"
    static let vehicleList = "vehicle/vehicle_list"
    static let myBooking = "vehicle/my_booking"
    static let leaveApprovalList = "timesheet/leaves_applied_list"
    static let leaveDetail = "timesheet/leave_detail"
    static let leaveApprovalReject = "timesheet/leave_approval"
    static let updateLeave = "timesheet/edit_leave"
    static let addLeave = "timesheet/add_leave"
    static let addMeetingBooking = "facilitiese/add_facility_booking"
    static let expenseCategory = "expense/expense_category"
    static let vehicleApprovalList = "vehicle/approval_listing"
    static let leaveCategory = "timesheet/leave_category"
    static let vehicleBookingDetails = "vehicle/booking_details"
    static let vehicleDetailView = "vehicle/vehicle_details"
    static let bookVehicle = "vehicle/book_vehicle"
    static let maintenanceDetails = "maintenance/maintenance_details"
    static let updateMaintenance = "maintenance/update_maintenance"
    static let dropdown = "facilitiese/all_dropdowns"
    static let addMaintenance = "maintenance/add_maintenance"
    static let submitClaim = "expense/submit_claim"
    static let updateBooking = "vehicle/update_booking"
    static let updateMeetingBooking = "facilitiese/update_facility_booking"
    static let hrsqUploadDocument = "hseq/add_quality_document"
    static let calendarData = "calendar/calendar"
    static let fetchTicket = "tickets/ticket_list"
    static let fetchFiles = "tickets/attachment_list"
    static let readTicket = "tickets/read_ticket"
    static let getCategory = "tickets/ticket_category_dropdown"
    static let getAssetCategory = "Assets/asset_category_dropdown"
    static let addAsset = "Assets/add_asset"
    static let updateAsset = "Assets/update_asset"
    static let deleteAsset = "Assets/delete_asset"
    static let assetDropdown = "Assets/asset_license_dropdown"
    static let getAssetDetail = "Assets/asset_details"
    static let addAssetUpdate = "Assets/add_asset_update"
    static let getAssetUpdateList = "Assets/asset_update_list"
    static let getSubCategory = "tickets/ticket_sub_category_dropdown"
    static let addTicket = "tickets/add_ticket"
    static let updateTicket = "tickets/update_ticket"
    static let addAttachment = "tickets/add_attachment"
    static let getAllComment = "tickets/comments_list"
    static let addComment = "tickets/set_comment"
    static let deleteComment = "tickets/comment_delete"
    static let fetchAsset = "Assets/asset_list"
    static let knowledgeBase = "knowledge_base/knowledge_base_list"
    static let getEmployee = "tickets/ticket_it_employees_dropdown"
    static let updateTicketStatus = "tickets/update_status"
    static let upateNote = "tickets/update_ticket_note"
    static let getAssetMenufacturer = "Assets/asset_manufacturer_dropdown"
    static let getTenderList = "procurement/tender_list"
    static let getProposalTrackingList = "procurement/proposal_tracking_list"
    static let getTenderDetail = "procurement/read_tender"
    static let getProposalDetail = "procurement/read_proposal"
    static let getNormalEmployee = "employees/employees_list"
    static let filterAsset = "Assets/asset_filter"
    static let updateProposal = "procurement/update_proposal"
    static let getProcurementDropdown = "procurement/all_drop_downs"
    static let getAuditFindingList = "audit_finding/audit_finding_list"
    static let getAuditDetails = "audit_finding/get_audit_finding_details/"
    static let getAttachmentDetails = "audit_finding/audit_attachment_list/"
    static let addAuditAttachment = "audit_finding/add_audit_attachment/"
    static let getAuditTitle = "audit_finding/audit_title_dropdown/"
    static let getAuditDropdown = "audit_finding/audit_finding_dropdown_list/"
    static let getAuditeeList = "audit_finding/auditee_dropdown_list/"
    
}

struct WebViewUrl {
    
}

struct StatusCode{
    static let success = 200
    static let pageNotFound = 404
    static let dataError = 401
    static let noDataFound = 400
}

struct Validation{
    static let errorFirstNameEmpty = "Please enter first name."
    static let errorLastNameEmpty = "Please enter last name."
    static let errorEmailEmpty = "Please enter email address."
    static let errorCreateUsername = "Please create username."
    static let errorConfirmUsername = "Please confirm username."
    static let errorUsernameNotMetched = "Username mismatched."
    static let errorEmailInvalid = "Please enter valid email address."
    static let errorSelectIndustry = "Please select Industry."
    static let errorSelectLocation = "Please select location."
    static let errorUsername = "Please enter username."
    
    static let errorEnterOldPassword = "Please enter old password."
    static let errorEnterNewPassword = "Please enter new password."
    static let errorEnterConfirmPassword = "Please enter confirm password."
    static let errorEmptyCountry = "Please select country."
    static let errorEmptyPhoneNumber = "Please enter phone number."
    static let errorEmptyCountryField = "Please select Country."
    static let errorNotNumeric = "Please enter numbers."
    static let errorPhoneLength = "Phone Number should be between 8 to 15 digits."
    
    static let errorNameInvalid = "Please enter valid name"
    static let errorNameLength = "Name should be between 3 to 10 characters. "
    static let errorPasswordEmpty = "Please enter password."
    static let errorPasswordInvalid = "Password must contain characters between 8 to 15 and it must be alphanumeric."
    static let errorPasswordLength = "Password should be less than 15 characters."
    static let errorPasswordLengthInvalid = "Password must contain characters between 6 to 10."
    static let errorConfirmPasswordLengthInvalid = "Password must contain characters between 6 to 10."
    static let errorPasswordMismatch = "Password and confirm password should be same."
    static let errorInvalidCountry = "Please select country."
}

struct ErrorMessages{
    static let errorToHandleInSuccess = "Something is wrong while getting success"
    static let errorToHandleInFailure = "Something is wrong while getting failure"
    static let errorNothingToLog = "There is nothing to log on console"
    static let somethingWentWrong = "Something went wrong"
    static let unableToParseError = "Unable to parse error response"
    static let invalidUserId = "Unable to find userId"
    static let networkError = "Network not available"
    
}

struct Console{
    static func log(_ message: Any?){
       print(message ?? ErrorMessages.errorNothingToLog)
    }
}

struct ServerKeys {
    static let firstName = "first_name"
    static let image = "image"
    static let data = "data"
    static let message = "message"
    
}

struct UserDefaultKeys{
    static let token = "token"
    static let userId = "userId"
    static let fontSize = "fontSize"
    
}

enum LoginType: String {
    case normal
    case facebook
    case google
}

enum CurrentScreen: String {
    case home
    case option
    case dashboard
}
