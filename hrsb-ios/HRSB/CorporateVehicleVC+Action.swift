//
//  CorporateVehicleVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 07/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CorporateVehicleVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionUpdate(_ sender: UIButton) {
        Console.log("buttonActionUpdate")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            requestServerBookingUpdate(param: ["user_id": userID, "booking_id": bookingId, "add_type": buttonSelectStatus.titleLabel?.text! ?? "", "remarks": ""])
        }
        
    }
    
    func isValidate() -> Bool {
        if buttonSelectStatus.titleLabel?.text == "Select status" {
            DisplayBanner.show(message: "Please select status.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionSelectStatus(_ sender: UIButton) {
        Console.log("buttonActionSelectStatus")
        chooseStatus.show()
    }
}
