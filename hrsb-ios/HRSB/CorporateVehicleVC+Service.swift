//
//  CorporateVehicleVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 08/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CorporateVehicleVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.vehicleBookingDetails
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    viewMainCard.isHidden = false
                    viewcard.isHidden = true
                    self.vehicleBooking = try decoder.decode(VehicleBooking.self, from: data)
                    Console.log("VehicleBooking:- \(String(describing: self.vehicleBooking))")
                    setupView(vehicleBooking: self.vehicleBooking)
               } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func setupView(vehicleBooking: VehicleBooking?) {
        let data = vehicleBooking?.data
        labelBooingTitle.text = data?.title
        labelDateFrom.text = data?.date_from
        labelDateTo.text = data?.date_to
        labelEmployeeName.text = data?.booked_by
        labelTransport.text = data?.transport_type
        labelEmployeeId.text = data?.booking_id
        labelType.text = data?.type
        labelPlatNumber.text = data?.plat_no
        viewcard.isHidden = false
        stackHeight.constant = 180
        if status != "1" {
            viewcard.isHidden = true
            stackHeight.constant = 65
        }
    }
    
    
    func requestServerBookingUpdate(param: [String: Any]?) {
        let endPoint = MethodName.updateBooking
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessBookingUpdate(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessBookingUpdate(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    navigationController?.popViewController(animated: true)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}
