//
//  CorporateVehicleVC.swift
//  HRSB
//
//  Created by Air 3 on 07/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class CorporateVehicleVC: UIViewController {

    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonUpdate: UIButton!
    @IBOutlet weak var labelTimeTitle: UILabel!
    @IBOutlet weak var buttonSelectStatus: UIButton!
    @IBOutlet weak var labelEmployeeId: UILabel!
    @IBOutlet weak var labelEmployeeName: UILabel!
    @IBOutlet weak var labelDateFrom: UILabel!
    @IBOutlet weak var labelDateTo: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelPlatNumber: UILabel!
    @IBOutlet weak var labelBookingPurpose: UILabel!
    @IBOutlet weak var labelProject: UILabel!
    @IBOutlet weak var labelTransport: UILabel!
    @IBOutlet weak var labelBooingTitle: UILabel!
    @IBOutlet weak var viewcard: UIView!
    @IBOutlet weak var viewMainCard: UIView!
    var leaveId = ""
    var vehicleBooking: VehicleBooking?
    let chooseStatus = DropDown()
    var statusValue = ""
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseStatus,
        ]
    }()
    
    var bookingId = ""
    var purpose = ""
    var project = ""
    var date_from = ""
    var status = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
        viewMainCard.isHidden = true
        viewcard.isHidden = true
        requestServer(param: ["booking_id": bookingId])
    }
    
    func setup() {
        buttonUpdate.makeRoundCorner(25)
        labelBookingPurpose.text = purpose
        labelProject.text = project
        labelTimeTitle.text = date_from
        
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
        
    
    
    func setupDropDowns() {
        setupChooseStatusDropDown()
    }
    
    func setupChooseStatusDropDown() {
        chooseStatus.anchorView = buttonSelectStatus
        chooseStatus.bottomOffset = CGPoint(x: 0, y: buttonSelectStatus.bounds.height)
        chooseStatus.dataSource = [
            "approve",
            "reject"
        ]
        // Action triggered on selection
        chooseStatus.selectionAction = { [weak self] (index, item) in
            self?.buttonSelectStatus.setTitle(item, for: .normal)
            self?.statusValue = item
        }
    }
}

