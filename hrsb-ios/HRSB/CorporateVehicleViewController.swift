//
//  CorporateVehicleViewController.swift
//  HRSB
//
//  Created by BestWeb on 27/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
import DropDown

class CorporateVehicleViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var scroll_vehiclebookingform: UIScrollView!
    
    @IBOutlet weak var SubmitVehiclebtn: UIButton!
    @IBOutlet weak var buttonVihicleList: UIButton!
    @IBOutlet weak var buttonMyBooking: UIButton!

    @IBOutlet weak var view1: MDCCard!
    
    @IBOutlet weak var lbl_roadtax1: UILabel!
    
    @IBOutlet weak var lbl_roadtax: UILabel!
    
    @IBOutlet weak var lbl_insurancedate: UILabel!
    
    
    @IBOutlet weak var lbl_pupakom: UILabel!
    
    @IBOutlet weak var lbl_insurancedate1: UILabel!
    
    @IBOutlet weak var lbl_vehicletype1: UILabel!
    
    @IBOutlet weak var lbl_vehicletype: UILabel!
    @IBOutlet weak var lbl_bank: UILabel!
    
    
    @IBOutlet weak var lbl_bank1: UILabel!
    @IBOutlet weak var lbl_owner: UILabel!
    
    @IBOutlet weak var remarks1: UILabel!
    
    @IBOutlet weak var lbl_remarks: UILabel!
    
    @IBOutlet weak var lbl_puspakom1: UILabel!
    
    
    @IBOutlet weak var lbl_owner1: UILabel!
    
    @IBOutlet weak var close_ViewCorporateVehi: UIButton!
    
    var myInt: Int?

    @IBOutlet weak var Scroll_CoreporateVehi: UIScrollView!
    
    @IBOutlet var view_CorporateVehi: UIView!
    
    @IBOutlet weak var day2btn: UIButton!
    
    
    @IBOutlet weak var day1btn: UIButton!
    
    
    @IBOutlet weak var month1btn: UIButton!
    @IBOutlet weak var month2btn: UIButton!
    
    
    @IBOutlet weak var year1btn: UIButton!
    
    @IBOutlet weak var year2btn: UIButton!
    
    
    
    @IBOutlet weak var txt_year1: UITextField!
    
    @IBOutlet weak var txt_year2: UITextField!
    
    @IBOutlet weak var txt_month1: UITextField!
    
    @IBOutlet weak var lbl_dayfrom: UILabel!
    
    @IBOutlet weak var txt_project: UITextField!
    
    @IBOutlet weak var txt_bookingpurpose: UITextField!
    @IBOutlet weak var txt_month2: UITextField!
    
    @IBOutlet weak var lbl_dayuntil: UILabel!
    
    @IBOutlet weak var lbl_bookingpurpose: UILabel!
    
    @IBOutlet weak var lbl_project: UILabel!
   
    
    @IBOutlet weak var txt_day_until: UITextField!
    
    @IBOutlet weak var txt_day_from: UITextField!

    
    
    
    @IBOutlet weak var closevehiclebookingform: UIButton!
    @IBOutlet var view_black1: UIView!
    
    @IBOutlet weak var closeblackviewbtn: UIButton!
    
    @IBOutlet weak var tble_transporttype: UITableView!
    
    @IBOutlet weak var tble_transportlocatn: UITableView!
    
    @IBOutlet weak var addbtn: UIButton!
    
    @IBOutlet var view_black: UIView!
    
    @IBOutlet var view_corner: UIView!
    
    @IBOutlet var view_vehiclebookingform: UIView!
    
    @IBOutlet weak var Transpotypbtn: UIButton!
    
    @IBOutlet weak var TransportLocationbtn: UIButton!
    
    @IBOutlet var view_one: UIView!
    
    var appBarViewController = MDCAppBarViewController()
    @IBOutlet weak var view_white: UIView!
    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtTransportType: UITextField!
    @IBOutlet weak var btnCloseTransportText: UIButton!
    @IBOutlet weak var btnCloseLocationText: UIButton!

    var statusValue = ""
    
   
    
    @IBOutlet weak var closetransportloc: UIButton!
    var vehicleListModel: VehicleListModel?
    var myBookingModel: MyBookingModel?
    
    let reuseIdentifier = "CorporateVehicleCell"
    let reuseIdentifier1 = "TransporttypeCell"
    let reuseIdentifier2 = "TransportLocationCell"

    
    
    @IBOutlet weak var tble: UITableView!
    
    
    override func viewDidLoad() {
         super.viewDidLoad()
         buttonVihicleList.isSelected = true
         buttonVihicleList.tintColor = .clear
         buttonMyBooking.tintColor = .clear
         buttonVihicleList.setTitleColor(.white, for: .selected)
         buttonVihicleList.setTitleColor(.getRGBColor(107, g: 58, b: 143), for: .normal)
         buttonMyBooking.setTitleColor(.white, for: .selected)
         buttonMyBooking.setTitleColor(.getRGBColor(107, g: 58, b: 143), for: .normal)
         buttonActionVihicleList(buttonVihicleList)
        
       
        
//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
//
//
//
//
//        let searchItemImage = UIImage(named: "bell.png")
//        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
//        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
//                                         style: .plain,
//                                         target: self,
//                                         action: #selector(self.NotifyAction))
//
//
//        let tuneItemImage = UIImage(named: "Bell1")
//        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
//        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
//
//        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
//        imageView.contentMode = .scaleAspectFit
//
//        let image = UIImage(named: "toplogo.png")
//        imageView.image = image
//
//        self.appBarViewController.headerStackView.addSubview(imageView)
//
//
//
//
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
//
//
//        tble.dataSource = self
//        tble.delegate = self
//
//
//        tble_transportlocatn.dataSource = self
//        tble_transportlocatn.delegate = self
//
//
//        tble_transporttype.dataSource = self
//        tble_transporttype.delegate = self
//
//
//
//
//        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
//
//        self.view.addSubview(view_one)
//
//
//        view_one.frame = CGRect(x: 0, y: view_white.frame.origin.y + view_white.frame.height + 1, width: self.view.frame.width, height: 60)
//
//
//
//
//
////        self.view.addSubview(view_corner)
////
////        view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
//
//
//        self.setheight()
//
//
//        addSlideMenuButton()
//
//        addSlideMenuButton1()
//
//
//
        tble.dataSource = self
        tble.delegate = self
        
        //
        
        registerTableViewCell()
        // Do any additional setup after loading the view.
    }

    func registerTableViewCell() {
        tble.register(UINib(nibName: "MyBookingCell", bundle: nil), forCellReuseIdentifier: "MyBookingCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID])
        requestServerMyBooking(param: ["employee_id": userID])
        btnCloseLocationText.isHidden = true
        btnCloseTransportText.isHidden = true
    }
    
    @objc func NotifyAction(){
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        navigationController?.pushViewController(presentedVC, animated: true)
    }
    
    
    @IBAction func textDidChangeLocation(_ sender: UITextField) {
        if sender.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            btnCloseLocationText.isHidden = true
        }else {
            btnCloseLocationText.isHidden = false
        }
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID, "transport_type":txtTransportType.text!, "location": sender.text!])
    }
    
    @IBAction func textDidChangeTransportType(_ sender: UITextField) {
        if sender.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            btnCloseTransportText.isHidden = true
        }else {
            btnCloseTransportText.isHidden = false
        }
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID, "transport_type":sender.text!, "location": txtLocation.text!])
    }
    
    
    @IBAction func tapCloseText(_ sender: UIButton) {
        sender.isHidden = true
        if sender == btnCloseTransportText {
            txtTransportType.text = ""
            let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
            requestServer(param: ["user_id": userID, "transport_type":txtTransportType.text!, "location": txtLocation.text!])
        } else {
            txtLocation.text = ""
            let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
            requestServer(param: ["user_id": userID, "transport_type":txtTransportType.text!, "location": txtLocation.text!])
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if buttonVihicleList.isSelected {
            return vehicleListModel?.data?.count ?? 0
        } else if buttonMyBooking.isSelected {
            return myBookingModel?.data?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if buttonVihicleList.isSelected {
            let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! CorporateVehicleCell
            let data = vehicleListModel?.data?[indexPath.row]
            cell.labelItemName.text = data?.transport_type
            cell.labelLocation.text = data?.location
            cell.labelItemNumber.text = data?.plat_no
            cell.labelAvailable.text = "Available"
            cell.viebtn.tag = indexPath.row
            cell.bookbtn.tag = indexPath.row
            cell.viebtn.addTarget(self, action: #selector(buttonActionViewDetail(_:)), for: .touchUpInside)
            cell.bookbtn.addTarget(self, action: #selector(buttonActionBooking(_:)), for: .touchUpInside)
            viewHeight.constant = 60
            return cell
        } else if buttonMyBooking.isSelected {
            let cell = self.tble.dequeueReusableCell(withIdentifier: "MyBookingCell", for: indexPath)as! MyBookingCell
            let data = myBookingModel?.data?[indexPath.row]
            cell.labelDate.text = "Date: \(data?.date_from ?? "")"
            cell.labelTitle.text = data?.title
            viewHeight.constant = 0
            
            if data?.status == "1" {
                cell.labelStatus.text = "Status: Pending"
            } else if data?.status == "2" {
                cell.labelStatus.text = "Status: Approved"
            } else if data?.status == "3" {
                cell.labelStatus.text = "Status: Rejected"
            } else if data?.status == "4" {
                cell.labelStatus.text = "Status: First level approval"
            } else if data?.status == "5" {
                cell.labelStatus.text = "Status: Canceled"
            } else if data?.status == "6" {
                cell.labelStatus.text = "Status: Second level approval"
            } else if data?.status == "7" {
                cell.labelStatus.text = "Status: Submitted"
            }
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    

    @objc func buttonSelected(sender: UIButton){
        print(sender.tag)
        
        
        view.addSubview(view_vehiclebookingform)
        
        view_vehiclebookingform.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
        
        
        
        
        
        
        lbl_dayfrom.frame = CGRect(x: 15, y: 30, width: view.frame.width - 30, height: 16)
        
        
        txt_day_from.frame = CGRect(x: 15, y: lbl_dayfrom.frame.origin.y + lbl_dayfrom.frame.height + 10, width: 100, height: 30)
        
        txt_month1.frame = CGRect(x: view.frame.width/2 - 50, y: lbl_dayfrom.frame.origin.y + lbl_dayfrom.frame.height + 10, width: 100, height: 30)
        
        txt_year1.frame = CGRect(x: 300, y: lbl_dayfrom.frame.origin.y + lbl_dayfrom.frame.height + 10, width: 100, height: 30)
        
        
        
        lbl_dayuntil.frame = CGRect(x: 15, y: txt_day_from.frame.origin.y + txt_day_from.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
        
        
        
        txt_day_until.frame = CGRect(x: 15, y: lbl_dayuntil.frame.origin.y + lbl_dayuntil.frame.height + 10, width: 100, height: 30)
        
        txt_month2.frame = CGRect(x: view.frame.width/2 - 50, y: lbl_dayuntil.frame.origin.y + lbl_dayuntil.frame.height + 10, width: 100, height: 30)
        
        txt_year2.frame = CGRect(x: 300, y: lbl_dayuntil.frame.origin.y + lbl_dayuntil.frame.height + 10, width: 100, height: 30)
        
        
        
        lbl_bookingpurpose.frame = CGRect(x: 15, y: txt_day_until.frame.origin.y + txt_day_until.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
        txt_bookingpurpose.frame = CGRect(x: 15, y: lbl_bookingpurpose.frame.origin.y + lbl_bookingpurpose.frame.height + 10, width: view.frame.width - 30, height: 30)
        
        
        
        lbl_project.frame = CGRect(x: 15, y: txt_bookingpurpose.frame.origin.y + txt_bookingpurpose.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        
        
        txt_project.frame = CGRect(x: 15, y: lbl_project.frame.origin.y + lbl_project.frame.height + 10, width: view.frame.width - 30, height: 30)
        
        
        
        
        
        
        day1btn.frame = CGRect(x: txt_day_from.frame.width , y: txt_day_from.frame.origin.y + 7, width: 16, height: 16)
        
        day2btn.frame = CGRect(x: txt_day_until.frame.width , y: txt_day_until.frame.origin.y + 7, width: 16, height: 16)
        
        month1btn.frame = CGRect(x: txt_month1.frame.origin.x + txt_month1.frame.width - 16 , y: txt_month1.frame.origin.y + 7, width: 16, height: 16)
        
        month2btn.frame = CGRect(x: txt_month2.frame.origin.x + txt_month2.frame.width - 16 , y: txt_month2.frame.origin.y + 7, width: 16, height: 16)
        
        year1btn.frame = CGRect(x: txt_year1.frame.origin.x + txt_year1.frame.width - 16, y: txt_year1.frame.origin.y + 7, width: 16, height: 16)
        
        year2btn.frame = CGRect(x: txt_year2.frame.origin.x + txt_year2.frame.width - 16 , y: txt_year2.frame.origin.y + 7, width: 16, height: 16)
        

        
        
        SubmitVehiclebtn.frame = CGRect(x: view.frame.width/2 - 75, y: txt_project.frame.origin.y + 70, width: 150, height: 40)
        
        SubmitVehiclebtn.layer.cornerRadius = 20

    }

    
    
    @objc func ViewbuttonSelected(sender: UIButton){
        print(sender.tag)
        
        
        view.addSubview(view_CorporateVehi)
        
        view_CorporateVehi.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
        
        
        
        
        view1.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 120)
        
        
        lbl_roadtax.frame = CGRect(x: 15, y: view1.frame.origin.y + view1.frame.height + 20, width: view.frame.width - 30, height: 16)
        
        lbl_roadtax1.frame = CGRect(x: 15, y: lbl_roadtax.frame.origin.y + lbl_roadtax.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        lbl_insurancedate.frame = CGRect(x: 15, y: lbl_roadtax1.frame.origin.y + lbl_roadtax1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        lbl_insurancedate1.frame = CGRect(x: 15, y: lbl_insurancedate.frame.origin.y + lbl_insurancedate.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        lbl_pupakom.frame = CGRect(x: 15, y: lbl_insurancedate1.frame.origin.y + lbl_insurancedate1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        lbl_puspakom1.frame = CGRect(x: 15, y: lbl_pupakom.frame.origin.y + lbl_pupakom.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        
        lbl_vehicletype.frame = CGRect(x: 15, y: lbl_puspakom1.frame.origin.y + lbl_puspakom1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        lbl_vehicletype1.frame = CGRect(x: 15, y: lbl_vehicletype.frame.origin.y + lbl_vehicletype.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        lbl_bank.frame = CGRect(x: 15, y: lbl_vehicletype1.frame.origin.y + lbl_vehicletype1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        lbl_bank1.frame = CGRect(x: 15, y: lbl_bank.frame.origin.y + lbl_bank.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        lbl_remarks.frame = CGRect(x: 15, y: lbl_bank1.frame.origin.y + lbl_bank1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        remarks1.frame = CGRect(x: 15, y: lbl_remarks.frame.origin.y + lbl_remarks.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        
        lbl_owner.frame = CGRect(x: 15, y: remarks1.frame.origin.y + remarks1.frame.height + 15, width: view.frame.width - 30, height: 16)
        
        lbl_owner1.frame = CGRect(x: 15, y: lbl_owner.frame.origin.y + lbl_owner.frame.height + 10, width: view.frame.width - 30, height: 16)
        
        
        
        
        let pos = lbl_owner1.frame.origin.y
        
        let height = lbl_owner1.frame.size.height
        let sizeOfContent = height + pos + 50
        
        
        print("dsfdf",sizeOfContent)
        
        Scroll_CoreporateVehi.contentSize.height = sizeOfContent
        
        
        Scroll_CoreporateVehi.contentSize.width = view.frame.width


        
        
    }
    

    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if buttonVihicleList.isSelected {
            return 150
        } else if buttonMyBooking.isSelected {
            return 70
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if buttonMyBooking.isSelected {
            let data = myBookingModel?.data?[indexPath.row]
            let vc = MyBookingDetailVC.instantiate(fromAppStoryboard: .main)
            vc.bookingId = data?.booking_id ?? ""
            vc.status = data?.status ?? ""
            navigationController?.pushViewController(vc, animated: true)
        }

    
    
    }
    
    
    @IBAction func back_Acton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)

        
//
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)
        
        
        
    }
    
    
    
    
    
    
    

    
    @IBAction func TransportLoc_Action(_ sender: Any) {
        
        
        
        view.addSubview(view_black1)
        
        view_black1.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        
        view_black1.backgroundColor = UIColor.clear
        
        tble_transportlocatn.frame = CGRect(x: TransportLocationbtn.frame.origin.x, y: view_one.frame.origin.y + view_one.frame.height, width: TransportLocationbtn.frame.width, height: 100)

    }
    
    
    @IBAction func TransportType_ACtion(_ sender: Any) {
        
        
        
        view.addSubview(view_black)
        
        view_black.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        
        view_black.backgroundColor = UIColor.clear
        
        tble_transporttype.frame = CGRect(x: Transpotypbtn.frame.origin.x, y: view_one.frame.origin.y + view_one.frame.height, width: Transpotypbtn.frame.width, height: 100)
        
        
    }
    
    
    @IBAction func closebalkview_Action(_ sender: Any) {
        
        
        view_black.removeFromSuperview()
        
        
    }
    
    
    @IBAction func Addbtn_Action(_ sender: Any) {
        
        
        
        
    }
    
    
    
    @IBAction func closetransportLoc_Action(_ sender: Any) {
        
        
        view_black1.removeFromSuperview()

    }
    
    
    
    
    
    @IBAction func CloseVehicleBookingForm_Action(_ sender: Any) {
        
        
        view_vehiclebookingform.removeFromSuperview()
        
        
    }
    
    
    
    
    @IBAction func day1_Action(_ sender: Any) {
        
        myInt = 0
        
        
        print(myInt as Any)
        
        
        self.table_day()
        
    }
    
    
    
    
    @IBAction func day2_Action(_ sender: Any) {
        
        
        myInt = 1
        
        
        print(myInt as Any)

        self.table_day()

    }
    
    
    @IBAction func Month1_Action(_ sender: Any) {
    }
    
    @IBAction func Month2_Action(_ sender: Any) {
    }
    
    @IBAction func Year1_Action(_ sender: Any) {
    }
    
    @IBAction func Year2_Action(_ sender: Any) {
    }
    
    
    
    
    
    func table_day(){
        
        
        if myInt == 0 {
            
            print("U SELECTED DAY 1")
            
        }
        else if myInt == 1 {
            
            
            print("U SELECTED DAY 2")

        }
    }
    
    
    
    
    @IBAction func Close_CorporateVehi_Action(_ sender: Any) {
        
        
        
        view_CorporateVehi.removeFromSuperview()
        
        
    }
    
    @IBAction func SubmitVehicleForm_Action(_ sender: Any) {
    }
    
    
    
    @objc func buttonActionViewDetail(_ sender: UIButton) {
        Console.log("buttonActionViewDetail")
        let data = vehicleListModel?.data?[sender.tag]
        let vc = CorporateVehicleViewVC.instantiate(fromAppStoryboard: .main)
        vc.vehicleId = data?.vehicle_id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func buttonActionBooking(_ sender: UIButton) {
        Console.log("buttonActionBooking")
        let data = vehicleListModel?.data?[sender.tag]
        let vc = AddBookingVC.instantiate(fromAppStoryboard: .main)
        vc.vehicle_id = data?.vehicle_id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
}
