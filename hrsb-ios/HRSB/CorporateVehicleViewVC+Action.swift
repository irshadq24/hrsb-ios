//
//  CorporateVehicleViewVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CorporateVehicleViewVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionBook(_ sender: UIButton) {
        Console.log("buttonActionBook")
        let data = vehicleDetailView?.data
        let vc = AddBookingVC.instantiate(fromAppStoryboard: .main)
        vc.vehicle_id = data?.vehicle_id ?? ""
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

