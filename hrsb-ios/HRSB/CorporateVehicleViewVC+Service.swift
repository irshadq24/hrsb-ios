//
//  CorporateVehicleViewVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension CorporateVehicleViewVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.vehicleDetailView
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                print(result)
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        
        if let data = data as? [String : Any] {
            print(data)
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.vehicleDetailView = try decoder.decode(VehicleDetailView.self, from: data)
                    Console.log("VehicleDetailView:- \(self.vehicleDetailView)")
                    setupViewData(detail: self.vehicleDetailView)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
   
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}

