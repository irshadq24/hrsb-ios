//
//  CorporateVehicleViewVC.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CorporateVehicleViewVC: UIViewController {

    @IBOutlet weak var labelVehicleType: UILabel!
    @IBOutlet weak var labelPlatNumber: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var buttonBook: UIButton!
    @IBOutlet weak var labelRoadtax: UILabel!
    @IBOutlet weak var labelInsuranceDate: UILabel!
    @IBOutlet weak var labelPuspakomDate: UILabel!
    @IBOutlet weak var labelVehicleType1: UILabel!
    @IBOutlet weak var labelBank: UILabel!
    @IBOutlet weak var labelOwner: UILabel!
    
    @IBOutlet weak var labelRemark: UILabel!
    var vehicleDetailView: VehicleDetailView?
    var vehicleId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        requestServer(param: ["vehicle_id": vehicleId])
        // Do any additional setup after loading the view.
    }
    func setup() {
        buttonBook.makeRoundCorner(15)
        buttonBook.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
    }
    


    func setupViewData(detail: VehicleDetailView?) {
        let data = detail?.data
        labelBank.text = data?.bank ?? ""
        labelOwner.text = "No name"
        labelRemark.text = data?.remarks ?? ""
        labelRoadtax.text = data?.roadtax_date ?? ""
        labelPlatNumber.text = data?.plat_no ?? ""
        labelPuspakomDate.text = data?.puspakom_date ?? ""
        labelInsuranceDate.text = data?.insurans_date ?? ""
        labelStatus.text = data?.status ?? ""
        labelVehicleType.text = data?.transport_type ?? ""
        labelVehicleType1.text = data?.transport_type ?? ""
        labelLocation.text = data?.location ?? ""
    }
    
}
