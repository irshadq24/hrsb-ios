//
//  CreateClaim.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct CreateClaim: Codable {
    let status: String?
    let message: String?
    let data: Claim?
}

struct Claim: Codable {
    let employee_id: String?
    let claim_title: String?
    let claim_month: String?
    let claim_year: String?
    
}

