//
//  CreateClaimVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension CreateClaimVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        if isValidate() {
            let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
            requestServer(param: ["title": textFieldClaimTitle.text!, "user_id": userID,"month": "\(selectedDate.toString(dateFormat: "MM"))", "year": "\(selectedDate.toString(dateFormat: "yyyy"))"])
        }
        
    }
    
    func isValidate() -> Bool{
        if textFieldClaimTitle.text == "" {
            DisplayBanner.show(message: "Enter claim title.")
            return false
        }else if buttonMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select Month.")
            return false
        }else if buttonYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select Year.")
            return false
        }
        return true
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton) {
        Console.log("buttonActionMonth")
        chooseMonth.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton) {
        Console.log("buttonActionYear")
        chooseYear.show()
    }
    
}
