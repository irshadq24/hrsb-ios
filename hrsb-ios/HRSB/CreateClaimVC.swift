//
//  CreateClaimVC.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class CreateClaimVC: UIViewController {

    @IBOutlet weak var textFieldClaimTitle: UITextField!
    @IBOutlet weak var textFieldSelectDate: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var buttonMonth: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    var selectedDate = Date()
    var createClaim: CreateClaim?
    
    
    let chooseMonth = DropDown()
    let chooseYear = DropDown()
    
    var monthValue = ""
    var yearValue = ""
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseMonth,
            self.chooseYear
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupDropDowns()
    }
    
    func setup() {
        buttonSubmit.makeRoundCorner(25)
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(didChange(_:)), for: .valueChanged)
        //textFieldSelectDate.inputView = picker
    }
    
    @objc func didChange(_ sender: UIDatePicker) {
        selectedDate = sender.date
        //textFieldSelectDate.text = sender.date.toString(dateFormat: "yyyy MM")
    }
    
    func setupDropDowns() {
        setupChooseMonthDropDown()
        setupChooseYearDropDown()
    }

    func setupChooseMonthDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonth.anchorView = buttonMonth
        chooseMonth.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonth.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonth.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.monthValue = item
        }
    }
    func setupChooseYearDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYear.anchorView = buttonYear
        chooseYear.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYear.dataSource = arrayYear
        // Action triggered on selection
        chooseYear.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.yearValue = item
        }
    }
    
    
}
