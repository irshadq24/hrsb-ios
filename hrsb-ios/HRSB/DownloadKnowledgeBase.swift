//
//  DownloadKnowledgeBase.swift
//  HRSB
//
//  Created by Salman on 06/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class DownloadKnowledgeBase: UIViewController {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var model: ReferenceModel?
    
    @IBOutlet weak var lblFileName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let arrDate = model?.date.components(separatedBy: " "), arrDate.count > 1 {
            lblDate.text = arrDate[0]
            lblTime.text = arrDate[1]
        }
        lblFileName.text = model?.referenceTitle
    }
    
    @IBAction func tapDownload(_ sender: Any) {
        let strURL = (model?.attachmentPath ?? "")+(model?.attachmentName ?? "")
        if let url = URL(string: strURL)  {
            self.downloadImage(from: url)
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
