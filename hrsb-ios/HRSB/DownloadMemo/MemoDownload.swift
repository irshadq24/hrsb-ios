//
//  MemoDownload.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct MemoDownload: Codable {
    let announcement_id: String?
    let download_memo: String?
    let view_memo: String?
    
}

