//
//  AllDropdown.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct AllDropdown: Codable {
    
//    let all_branches: [AllBranch]?
    let all_facilities: [AllFacilities]?
    let all_timeslots: [AllTimeslots]?
    let get_all_companies: [GetCompanies]?
}

//struct AllBranch: Codable {
//
//}
//
struct AllFacilities: Codable {
    let subsidiary: String?
    let facility_id: String?
    let branch: String?
    let facility_name: String?
}

struct AllTimeslots: Codable {
    let timeslot_id: String?
    let time_to: String?
    let time_from: String?
}


struct GetCompanies: Codable {
    let name: String?
    let city: String?
}

