//
//  EditClaimVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension EditClaimVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isValidate() {
            var category = ""
            var type = ""
            if (self.expenseCategory?.result?.count) ?? 0 > self.buttonCategory.tag {
                category = self.expenseCategory?.result?[self.buttonCategory.tag].expense_type_id ?? ""
            }
            if (self.expenseType?.result?.count) ?? 0 > self.buttonCategory.tag {
                type = self.expenseType?.result?[self.buttonCategory.tag].expense_detail_id ?? ""
            }
            if claimId != "" { //ADD
                let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
                let compId = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
                requestServerToAddExpence(param: ["project_code": textFieldProjectCode.text!, "amount" : textFieldClaimAmount.text!, "remarks": textViewRemark.text!, "employee_id": userID, "expense_type": category ,"expense_sub_type": type, "claim_id": claimId, "image": chooseImage,"add_type": "expense","claim_date": "\(yearValue)-\(monthValue)-\(dayValue)",
                     "company_id":compId])

            }else {
                requestServer(param: ["project_code": textFieldProjectCode.text!, "amount" : textFieldClaimAmount.text!, "remarks": textViewRemark.text!, "employee_id": userID, "expense_type": category ,"expense_sub_type": type, "id": id, "image": chooseImage,"edit_type": "expense","claim_date": "\(yearValue)-\(monthValue)-\(dayValue)"])
            }
            //expense_sub_type_id
            
        }
    }
    
    
    func isValidate() -> Bool {
        if buttonDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please select Day.")
            return false
        }else if buttonMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select Month.")
            return false
        }else if buttonYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select Year.")
            return false
        }  else if textFieldProjectCode.text == "" {
            DisplayBanner.show(message: "Please select project code.")
            return false
        } else if buttonCategory.titleLabel?.text == "Select category" {
            DisplayBanner.show(message: "Please select category.")
            return false
        } else if buttonSelectType.titleLabel?.text == "Select type" {
            DisplayBanner.show(message: "Please select type.")
            return false
        } else if textViewRemark.text == "" {
            DisplayBanner.show(message: "Please write some remark.")
            return false
        } else if textFieldClaimAmount.text == "" {
            DisplayBanner.show(message: "Please enter claim amount.")
            return false
        }
//        else if self.chooseImage.jpegData(compressionQuality: 0.5)?.count == 0 || self.chooseImage.jpegData(compressionQuality: 0.5) == nil {
//            DisplayBanner.show(message: "Please select image.")
//            return false
//        }
        return true
    }
    
    @IBAction func buttonActionDay(_ sender: UIButton) {
        Console.log("buttonActionDay")
        chooseDay.show()
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton) {
        Console.log("buttonActionMonth")
        chooseMonth.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton) {
        Console.log("buttonActionYear")
        chooseYear.show()
    }
    
    @IBAction func buttonActionSelectCategory(_ sender: UIButton) {
        Console.log("buttonActionSelectCategory")
        chooseCategory.show()
    }
    
    @IBAction func buttonActionSelectType(_ sender: UIButton) {
        Console.log("buttonActionSelectType")
        chooseType.show()
    }
    
    @IBAction func tapUpload(_ sender: Any) {
        self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
            if selected == 1 {
                self.openCamera()
            } else if selected == 2 {
                self.openGallery()
            }
        }
    }
}

