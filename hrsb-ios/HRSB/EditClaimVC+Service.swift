//
//  EditClaimVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit
extension EditClaimVC {
    func requestServer(param: [String: Any]) {
        
        let endPoint = MethodName.updateClaim
        var params = param
        let imgInfo = ["bill_copy": params["image"] as! UIImage]
        params.removeValue(forKey: "image")
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params as! Dictionary<String, String>, imageInfo: imgInfo, unReachable: {
            Loader.hide()
            print("unreachable")
        }) { (response, code) in
            Loader.hide()
            if code == 200 {
                self.navigationController?.popViewController(animated: true)
            }
        }
//        ApiManager.requestMultipartApiServer(path: endPoint, parameters: param, methodType: .post, result: { [weak self](result) in
//            switch result {
//            case .success(let data):
//                self?.handleSuccess(data: data)
//            case .failure(let error):
//                self?.handleFailure(error: error)
//            case .noDataFound(_):
//                break
//            }
//        }) { (progress) in
//            Console.log("progress \(progress)")
//        }
        
    }
    func requestServerToAddExpence(param: [String: Any]) {
        let endPoint = MethodName.addExpence
        var params = param
        let imgInfo = ["bill_copy": params["image"] as! UIImage]
        params.removeValue(forKey: "image")
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params as! Dictionary<String, String>, imageInfo: imgInfo, unReachable: {
            Loader.hide()
            print("unreachable")
        }) { (response, code) in
            Loader.hide()
            if code == 200 {
                self.navigationController?.popViewController(animated: true)
            }
        }
//        ApiManager.requestMultipartApiServer(path: endPoint, parameters: param, methodType: .post, result: { [weak self](result) in
//            switch result {
//            case .success(let data):
//                self?.handleSuccess(data: data)
//            case .failure(let error):
//                self?.handleFailure(error: error)
//            case .noDataFound(_):
//                break
//            }
//        }) { (progress) in
//            Console.log("progress \(progress)")
//        }
        
    }
    func handleSuccess(data: Any) {
        if let data = data as? [String: Any] {
            do {
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.updateClaimModel = try decoder.decode(UpdateClaimModel.self, from: data)
                    Console.log("UpdateClaimModel:- \(self.updateClaimModel)")
                    DisplayBanner.show(message: self.updateClaimModel?.message)
                    self.navigationController?.popViewController(animated: true)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func requestServerCategory(param: [String: Any], isForType: Bool = false) {
        let endPoint = MethodName.expenseCategory
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccessCategory(data: data, isForType: isForType)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccessCategory(data: Any, isForType: Bool = false) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    if isForType {
                        self.expenseType = try decoder.decode(ExpenseCategory.self, from: data)
                        setupChooseTypeDropDown(data: self.expenseType)
                    }
                    else {
                        self.expenseCategory = try decoder.decode(ExpenseCategory.self, from: data)
                        setupChooseCategoryDropDown()
                    }
                    Console.log("ExpenseCategory:- \(self.expenseCategory)")
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
