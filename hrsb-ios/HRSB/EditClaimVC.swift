//
//  EditClaimVC.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class EditClaimVC: UIViewController {

    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var textFieldProjectCode: UITextField!
    @IBOutlet weak var buttonCategory: UIButton!
    @IBOutlet weak var buttonSelectType: UIButton!
    
    @IBOutlet weak var textFieldClaimAmount: UITextField!
    @IBOutlet weak var buttonAttachment: UIButton!
    @IBOutlet weak var textViewRemark: GrowingTextView!
    @IBOutlet weak var  buttonDay: UIButton!
    @IBOutlet weak var  buttonMonth: UIButton!
    @IBOutlet weak var  buttonYear: UIButton!
    var chooseImage = UIImage()
    var imagePicker = UIImagePickerController()
    var updateClaimModel: UpdateClaimModel?
    var expenseCategory: ExpenseCategory?
    var expenseType: ExpenseCategory?
    var id = ""
    var claimId = ""
    var selectedDate = Date()
    var expenseDetail: Expense?
    
    var dayValue = ""
    var monthValue = ""
    var yearValue = ""
    
    let chooseDay = DropDown()
    let chooseMonth = DropDown()
    let chooseYear = DropDown()
    let chooseCategory = DropDown()
    let chooseType = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDay,
            self.chooseMonth,
            self.chooseYear,
            self.chooseCategory,
            self.chooseType
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        imagePicker.delegate = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestServerCategory(param: ["expense_type_id": ""])
        setupDropDowns()
    }
    
    func setup() {
        buttonSubmit.makeRoundCorner(25)
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(didChange(_:)), for: .valueChanged)
        if claimId == "" {
            setupEditViewData(expenseDetail: expenseDetail)
        }
    }
    
    func setupEditViewData(expenseDetail: Expense?) {
        if let date = expenseDetail?.data?.claim_date?.getDateInstance(validFormat: "yyyy-MM-dd") {
            buttonDay.setTitle(date.toString(dateFormat: "dd"), for: .normal)
            buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
            buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
        }
        let data = expenseDetail?.data
        textViewRemark.text = data?.remarks
        textFieldClaimAmount.text = (data?.amount ?? "")+" MYR"
        textFieldProjectCode.text = data?.project_code
       
    }
    
    @objc func didChange(_ sender: UIDatePicker) {
        selectedDate = sender.date
      //  textFieldDate.text = sender.date.toString(dateFormat: "dd-MM-yyyy")
    }
    
    func setupDropDowns() {
        setupChooseDayDropDown()
        setupChooseMonthDropDown()
        setupChooseYearDropDown()
        
    }
    
    func setupChooseDayDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDay.anchorView = buttonDay
        chooseDay.bottomOffset = CGPoint(x: 0, y: buttonDay.bounds.height)
        chooseDay.dataSource = arrayDay
        // Action triggered on selection
        chooseDay.selectionAction = { [weak self] (index, item) in
            self?.buttonDay.setTitle(item, for: .normal)
            self?.dayValue = item
        }
    }
    func setupChooseMonthDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonth.anchorView = buttonMonth
        chooseMonth.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonth.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonth.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.monthValue = item
        }
    }
    func setupChooseYearDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYear.anchorView = buttonYear
        chooseYear.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYear.dataSource = arrayYear
        // Action triggered on selection
        chooseYear.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.yearValue = item
        }
    }
    
    func setupChooseCategoryDropDown() {
        var category = [String]()
        for obj in expenseCategory?.result ?? [] {
            category.append(obj.name ?? "")
        }
        chooseCategory.anchorView = buttonCategory
        chooseCategory.bottomOffset = CGPoint(x: 0, y: buttonCategory.bounds.height)
        chooseCategory.dataSource = category
        // Action triggered on selection
        chooseCategory.selectionAction = { [weak self] (index, item) in
            self?.buttonCategory.setTitle(item, for: .normal)
            self?.buttonCategory.tag = index
            if let id = self?.expenseCategory?.result?[index].expense_type_id {
                self?.requestServerCategory(param: ["expense_type_id": id], isForType: true)
            }
        }
    }
    
    func setupChooseTypeDropDown(data: ExpenseCategory?) {
        var category = [String]()
        for obj in data?.result ?? [] {
            category.append(obj.name ?? "")
        }
        chooseType.anchorView = buttonSelectType
        chooseType.bottomOffset = CGPoint(x: 0, y: buttonSelectType.bounds.height)
        chooseType.dataSource = category
        // Action triggered on selection
        chooseType.selectionAction = { [weak self] (index, item) in
            self?.buttonSelectType.tag = index
            self?.buttonSelectType.setTitle(item, for: .normal)
        }
    }
    

    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }
    
    
}
