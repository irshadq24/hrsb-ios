//
//  EditPurposalVC.swift
//  HRSB
//
//  Created by Salman on 07/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit


class CommonCellDataSource {
    var apiKey = ""
    var apiValue = ""
    var title = ""
    var displayText = ""
    var isForDropDown = false
    var dropDownDataSource = [String]()
    var index = 0
    var dropdownSelectedIndex: Int?
    var instance: AddTicketVC!
    init(title: String, displayText: String, apiValue: String = "", isForDropDown: Bool = false, dropDownDataSource:[String] = [String](), index:Int = 0, apiKey: String) {
        self.title = title
        self.displayText = displayText
        self.isForDropDown = isForDropDown
        self.dropDownDataSource = dropDownDataSource
        self.apiKey = apiKey
        self.index = index
        self.apiValue = apiValue
        
    }
}

class EditPurposalVC: UIViewController {
    
    var arrPurposalDatasource = [CommonCellDataSource]()
    var dictProposalDetail: [String: Any]!
    var proposalId = ""
    var selectedImage: UIImage?
    let picker = SGImagePicker(enableEditing: false)
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    var arrTenderType = [[String: Any]]()
    var arrSendingMethod = [[String: Any]]()
    var arrStatus = [[String: Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureData()
        let strUrl = (dictProposalDetail["url_path"] as? String ?? "")
        btnAttachment.imageView?.kf.setImage(with: URL(string: strUrl), completionHandler: { image, _, _, _ in
            self.selectedImage = image
            self.btnAttachment.setImage(image, for: .normal)
            self.btnAttachment.imageView?.makeRoundCorner(5)
            self.btnAttachment.contentMode = .scaleAspectFill
        })
        btnSubmit.makeRoundCorner(25)
        getDropDown()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDropDown() {
        let endPoint = MethodName.getProcurementDropdown
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["tender_type"] as? [[String: Any]] {
                    var arrTender = [String]()
                    for sub in subCate {
                        let name = sub["tender_type_name"] as? String ?? ""
                        arrTender.append(name)
                    }
                    self?.arrPurposalDatasource[3].dropDownDataSource = arrTender
                    self?.arrTenderType = subCate
                }
                if let dict = data as? [String: Any], let subCate = dict["meathod_of_sending"] as? [[String: Any]] {
                    var arrTender = [String]()
                    for sub in subCate {
                        let name = sub["tender_meathod_of_sending_name"] as? String ?? ""
                        arrTender.append(name)
                    }
                    self?.arrPurposalDatasource[8].dropDownDataSource = arrTender
                    self?.arrSendingMethod = subCate
                }
                if let dict = data as? [String: Any], let subCate = dict["proposal_status"] as? [[String: Any]] {
                    var arrTender = [String]()
                    for sub in subCate {
                        let name = sub["status"] as? String ?? ""
                        arrTender.append(name)
                    }
                    self?.arrPurposalDatasource[10].dropDownDataSource = arrTender
                    self?.arrStatus = subCate
                }
                self?.tblView.reloadData()
            case .failure(_):
                break
                
            case .noDataFound(_):
                break
            }
        }
    }
    
    
    func configureData() {
        arrPurposalDatasource.append(CommonCellDataSource(title: "Company", displayText: dictProposalDetail["company"] as? String ?? "",  apiValue: dictProposalDetail["company"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "company"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Client", displayText: dictProposalDetail["client"] as? String ?? "",  apiValue: dictProposalDetail["client"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "client"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Tender Date Received", displayText: dictProposalDetail["tender_date_receive"] as? String ?? "",  apiValue: dictProposalDetail["tender_date_receive"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "tender_date_receive"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Type", displayText: dictProposalDetail["tender_type"] as? String ?? "",  apiValue: dictProposalDetail["tender_type"] as? String ?? "", isForDropDown: true, index: arrPurposalDatasource.count, apiKey: "tender_type"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Description",displayText: dictProposalDetail["description"] as? String ?? "",  apiValue: dictProposalDetail["description"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "description"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Closing Date", displayText: dictProposalDetail["closing_submitted_date"] as? String ?? "",  apiValue: dictProposalDetail["closing_submitted_date"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "closing_submitted_date"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Time", displayText: dictProposalDetail["time"] as? String ?? "",  apiValue: dictProposalDetail["time"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "time"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Tender / RFQ / ITQ REF NO.", displayText: dictProposalDetail["ref_no"] as? String ?? "",  apiValue: dictProposalDetail["reference_no"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "reference_no"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Method of Sending.", displayText: dictProposalDetail["meathod_of_sending"] as? String ?? "",  apiValue: dictProposalDetail["meathod_of_sending"] as? String ?? "", isForDropDown: true, index: arrPurposalDatasource.count, apiKey: "meathod_of_sending"))
         arrPurposalDatasource.append(CommonCellDataSource(title: "Site visit / Tender Briefing", displayText: dictProposalDetail["site_visit"] as? String ?? "",  apiValue: dictProposalDetail["site_visit"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "site_visit"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Status", displayText: dictProposalDetail["status"] as? String ?? "",  apiValue: dictProposalDetail["status"] as? String ?? "", isForDropDown: true, index: arrPurposalDatasource.count, apiKey: "status"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Value (RM)", displayText: "RM \(dictProposalDetail["value"] as? String ?? "")",  apiValue: dictProposalDetail["value"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "value"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Remark", displayText: dictProposalDetail["remark"] as? String ?? "",  apiValue: dictProposalDetail["remark"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "remark"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "1st extension", displayText: dictProposalDetail["proposal_submission_date_one"] as? String ?? "",  apiValue: dictProposalDetail["proposal_submission_date_one"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "extension_date_one"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "2nd extension", displayText: dictProposalDetail["extension_date_two"] as? String ?? "",  apiValue: dictProposalDetail["extension_date_two"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "extension_date_two"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "3rd extension", displayText: dictProposalDetail["extension_date_three"] as? String ?? "",  apiValue: dictProposalDetail["extension_date_three"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "extension_date_three"))
        arrPurposalDatasource.append(CommonCellDataSource(title: "Reason xfor extension", displayText: dictProposalDetail["reason"] as? String ?? "",  apiValue: dictProposalDetail["reason"] as? String ?? "", index: arrPurposalDatasource.count, apiKey: "reason"))
    }
    
    @IBAction func tapAddImage(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
        }
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        let endPoint = MethodName.updateProposal
        var params = [String: String]()
        for data in arrPurposalDatasource {
            if data.apiValue == "" {
                self.showAlert(title: "", message: "Please enter all values.")
                return
            }
        }
        
        if selectedImage == nil {
            self.showAlert(title: "", message: "Please add attachment.")
            return
        }
        
        arrPurposalDatasource[3].apiValue = arrTenderType[arrPurposalDatasource[3].dropdownSelectedIndex ?? 0]["tender_type_id"] as? String ?? ""
        arrPurposalDatasource[8].apiValue = arrSendingMethod[arrPurposalDatasource[8].dropdownSelectedIndex ?? 0]["tender_meathod_of_sending_id"] as? String ?? ""
        arrPurposalDatasource[10].apiValue = arrStatus[arrPurposalDatasource[10].dropdownSelectedIndex ?? 0]["status_id"] as? String ?? ""
        
        for data in arrPurposalDatasource {
             params[data.apiKey] = data.apiValue
        }
        
        params["type"] = "update_proposal_general"
        params["proposal_id"] = proposalId
        print(params)
        
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["tender_file": self.selectedImage!], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
    }
    
}



extension EditPurposalVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellEditPurposal", for: indexPath) as! CellEditPurposal
        cell.info = arrPurposalDatasource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPurposalDatasource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
