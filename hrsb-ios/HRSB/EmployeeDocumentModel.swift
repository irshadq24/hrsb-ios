//
//  EmployeeDocumentModel.swift
//  HRSB
//
//  Created by Salman on 23/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct EmployeeDocumentModel: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [DocumentList]?
}

struct DocumentList: Codable {
    let document_id: String?
    let document_type: String?
    let document_title: String?
    let notification_email: String?
    let expiry_date: String?
    let download_document: String?
    let view_document: String?
}
