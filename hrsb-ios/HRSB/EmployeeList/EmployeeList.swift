//
//  EmployeeList.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct EmployeeList: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [EmployeeListArray]?
}

struct EmployeeListArray: Codable {
    let user_id: String?
    let employee_id: String?
    let department_designation: String?
    let employee_name: String?
    let comp_name: String?
    let basic_salary: String?
    let email: String?
    let role: String?
    let status: String?
}
