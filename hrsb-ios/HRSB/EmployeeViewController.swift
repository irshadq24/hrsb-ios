//
//  EmployeeViewController.swift
//  HRSB
//
//  Created by Air 3 on 24/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class EmployeeViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {
    
    
    @IBOutlet weak var back: UIButton!
    
    var appBarViewController = MDCAppBarViewController()
    
    @IBOutlet var view_corner: UIView!
    
    @IBOutlet var view_coc: UIView!
    
    
    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet weak var backcoc: UIButton!
    
    @IBOutlet weak var backpayslips: UIButton!
    
    @IBOutlet var view_loa: UIView!
    
    @IBOutlet weak var download: UIButton!
    
    
    
    
    
    @IBOutlet var view_payslips: UIView!
    
    
    @IBOutlet weak var backLOA: UIButton!
    
    let reuseIdentifier = "EmployeCell"
    //============== OLD CODE ====================
    var items = ["Letter of Appointment (LOA)", "Payslips","Code of Conduct (COC)","Memo","Grievance","Domestic Inquiries"]

    var images = ["loa.png", "payslip.png","coc.png","memo.png","grievance.png","domesticinq.png"]

    //============== OLD CODE ====================
    
//    var items = ["Letter of Appointment (LOA)","Code of Conduct (COC)","Memo","Grievance","Domestic Inquiries"]
//
//      var images = ["loa.png","coc.png","memo.png","grievance.png","domesticinq.png"]
//
//
    @IBOutlet weak var tble: UITableView!
    var employeeDocumentModel: EmployeeDocumentModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": "5"])
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        //        let menuItemImage = UIImage(named: "menu.png")
        //        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        //
        //
        //        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        //
        //        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
        //                                       style: .plain,
        //                                       target: self,
        //                                       action: nil)
        //        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
        
        
        
        tble.dataSource = self
        tble.delegate = self
        
        
        addSlideMenuButton()
        addSlideMenuButton1()
        
        self.setheight()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    @IBAction func back_Acton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        //        let transition = CATransition()
        //        transition.duration = 0.2
        //        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //        transition.type = CATransitionType.push
        //        transition.subtype = CATransitionSubtype.fromLeft
        //        self.view.window!.layer.add(transition, forKey: nil)
        //
        //        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    
    
    @IBAction func backLOA_Acton(_ sender: Any) {
        
        
        
        view_loa.removeFromSuperview()
        view_corner.removeFromSuperview()
        
        
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! EmployeCell
        
        
        
        
        
        cell.l2.text = items[indexPath.row]
        
        cell.img.image = UIImage(named: images[indexPath.row])
        
        
        return cell
        
    }
    
    
    
    @objc func NotifyAction(){
        
        
        
        //        let transition = CATransition()
        //        transition.duration = 0.3
        //        transition.type = CATransitionType.push
        //        transition.subtype = CATransitionSubtype.fromRight
        //        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //        view.window!.layer.add(transition, forKey: kCATransition)
        //
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        
        navigationController?.pushViewController(presentedVC, animated: true)
        
        
        // present(presentedVC, animated: false, completion: nil)
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 60
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //=============OLD CODE =========
        
        if indexPath.row == 0{
            print(0)
            let titleValue = "Letter of appointment"
            let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
            vc.titleValue = titleValue
            vc.document = employeeDocumentModel?.data?[0]
            navigationController?.pushViewController(vc, animated: true)
            
            //
            //
            //            self.view.addSubview(view_loa)
            //
            //
            //            view_loa.frame =  CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            //
            //
            //
            //            self.view.addSubview(view_corner)
            //
            //            view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
            
            
            
        }
        else if indexPath.row == 1{
            
            
            print(0)
            //let titleValue = "Payslips"
            let vc = PayslipDateVC.instantiate(fromAppStoryboard: .main)
          //  vc.titleValue = titleValue
            navigationController?.pushViewController(vc, animated: true)
            
            
            //            self.view.addSubview(view_payslips)
            //
            //
            //            view_payslips.frame =  CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            //
            //
            //            self.view.addSubview(view_corner)
            //
            //            view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
            
            
            
        }
            
        else if indexPath.row == 2{
            print(0)
            let titleValue = "Code of Conduct (COC)"
            let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
            vc.titleValue = titleValue
            vc.document = employeeDocumentModel?.data?[1]
            navigationController?.pushViewController(vc, animated: true)
            
            //            self.view.addSubview(view_coc)
            //
            //
            //            view_coc.frame =  CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            //
            //
            //            self.view.addSubview(view_corner)
            //
            //            view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
            
            
            
        }
        else if indexPath.row == 3{
            
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.MemoVC)
            // self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
        }else if indexPath.row == 4{
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.GrievanceVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
            
        }
        else if indexPath.row == 5{
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.DomesticInqVC)
            //  self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
            
        }
        
          //=============OLD CODE =========
        
        
        
        /*
            if indexPath.row == 0{
                print(0)
                let titleValue = "Letter of appointment"
                let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
                vc.titleValue = titleValue
                vc.document = employeeDocumentModel?.data?[0]
                navigationController?.pushViewController(vc, animated: true)
                
                //
                //
                //            self.view.addSubview(view_loa)
                //
                //
                //            view_loa.frame =  CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
                //
                //
                //
                //            self.view.addSubview(view_corner)
                //
                //            view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
                
                
                
            }
            else if indexPath.row == 1{
                
               print(0)
                              let titleValue = "Code of Conduct (COC)"
                              let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
                              vc.titleValue = titleValue
                              vc.document = employeeDocumentModel?.data?[1]
                              navigationController?.pushViewController(vc, animated: true)
                              
                              //            self.view.addSubview(view_coc)
                              //
                              //
                              //            view_coc.frame =  CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
                              //
                              //
                              //            self.view.addSubview(view_corner)
                              //
                              //            view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
                              
                              
                
            }
                
            else if indexPath.row == 2{

               
               
               let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.MemoVC)
               // self.present(HrVc, animated: true, completion: nil)
               navigationController?.pushViewController(HrVc, animated: true)
               
               
                
            }
            else if indexPath.row == 3{
                
                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.GrievanceVC)
                //self.present(HrVc, animated: true, completion: nil)
                navigationController?.pushViewController(HrVc, animated: true)
                
                
            }else if indexPath.row == 4{
                
                
                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.DomesticInqVC)
                //  self.present(HrVc, animated: true, completion: nil)
                navigationController?.pushViewController(HrVc, animated: true)
                
                
                
                
            }
            else if indexPath.row == 5{
                
                
                
                
            }*/
    }
    
    
    
    
    @IBAction func backpayslips_Action(_ sender: Any) {
        
        
        
        view_payslips.removeFromSuperview()
        view_corner.removeFromSuperview()
        
        
        
    }
    
    
    
    
    @IBAction func backcoc_Action(_ sender: Any) {
        
        
        view_coc.removeFromSuperview()
        view_corner.removeFromSuperview()
        
        
    }
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (70 * items.count))
        
        
        
        
        
    }
    
    
    
    
    
    
    
    @IBAction func download_Acton(_ sender: Any) {
        
        
        
        
        
    }
    
    
    
}
