//
//  EquipmentDetailModel.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct EquipmentDetail: Codable {
    let data: EquipmentDetailModel?
    let status: String?
    
}

struct EquipmentDetailModel: Codable {
    let phone: String?
    let subsidiary: String?
    let employee_name: String?
    let date_of_complaint: String?
    let maintenance_title: String?
    let maintenance_id: String?
    let comment: String?
    let other_defect: String?
    let branch: String?
    let location: String?
    let employee_id: String?
    let date_admin_check: String?
    let comment_by_admin: String?
    let type_of_defect: String?
    let status: String?
    let get_all_companies: [GetAllCompanies]?
}


struct GetAllCompanies: Codable {
    let name: String?
    let city: String?
}

