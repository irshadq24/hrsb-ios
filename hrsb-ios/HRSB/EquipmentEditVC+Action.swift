//
//  EquipmentEditVC+Action.swift
//  HRSB
//
//  Created by asif hussain on 8/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension EquipmentEditVC {
    
    @IBAction func buttonActionBack(_ sender: UIButton){
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionSubsidiaries(_ sender: UIButton){
        Console.log("buttonActionSubsidiaries")
        chooseSubdidiaries.show()
    }
    @IBAction func buttonActionBranch(_ sender: UIButton) {
        Console.log("buttonActionBranch")
        chooseBranch.show()
    }
    
    @IBAction func buttonTypeofDefect(_ sender: UIButton) {
        Console.log("buttonTypeofDefect")
        chooseDefect.show()
    }
    @IBAction func buttonStatus(_ sender: UIButton) {
        Console.log("buttonStatus")
        chooseStatus.show()
    }
    
    @IBAction func buttonActionSubmit(_ sender: UIButton) {
        Console.log("buttonActionSubmit")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        let data = equipmentDetail?.data
        if isValidate() {
            Console.log("Success")
            if isUpdate == "Update" {
                requestServerAddMaintainance(param: ["add_type": "maintentance" , "maintenance_id": data?.maintenance_id ?? "", "user_id": userID, "maintenance_title": TextViewTitle.text!, "subsidiary": ButtonSubsidiaries.titleLabel?.text! ?? "", "branch": ButtonBrand.titleLabel?.text! ?? "", "location": TextViewLocation.text!, "phone": TextViewPhoneNumber.text!, "comment": TextViewComments.text!])
            } else {
                requestServer(param: ["edit_type": "maintentance" , "maintenance_id": data?.maintenance_id ?? "", "user_id": userID, "maintenance_title": TextViewTitle.text!, "subsidiary": ButtonSubsidiaries.titleLabel?.text! ?? "", "branch": ButtonBrand.titleLabel?.text! ?? "", "location": TextViewLocation.text!, "phone": TextViewPhoneNumber.text!, "comment": TextViewComments.text!])
            }
        }
    }
    
    func isValidate() -> Bool {
        if TextViewTitle.text == "" {
            DisplayBanner.show(message: "Please enter title.")
            return false
        } else if ButtonSubsidiaries.titleLabel?.text == "" {
            DisplayBanner.show(message: "Please select subsidiaries.")
            return false
        } else if ButtonBrand.titleLabel?.text == "" {
            DisplayBanner.show(message: "Please select branch.")
            return false
        } else if TextViewLocation.text == "" {
            DisplayBanner.show(message: "Please wirte location.")
            return false
        } else if TextViewPhoneNumber.text == "" {
            DisplayBanner.show(message: "Please enter phone.")
            return false
        } else if TextViewDetailsComplaint.text == "" {
            DisplayBanner.show(message: "Please enter description.")
            return false
        } else if ButtonTypeOfDefect.titleLabel?.text == "" {
            DisplayBanner.show(message: "Please select defect.")
            return false
        } else if ButtonStatus.titleLabel?.text == "" {
            DisplayBanner.show(message: "Please select status.")
            return false
        } else if TextViewComments.text! == "" {
            DisplayBanner.show(message: "Please write comments.")
            return false
        }
        
        return true
    }
    
    
}

