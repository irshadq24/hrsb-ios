//
//  EquipmentEditVC.swift
//  HRSB
//
//  Created by asif hussain on 8/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//
import UIKit
import DropDown

class EquipmentEditVC: UIViewController {
    
    @IBOutlet weak var viewConstraintsHeight: NSLayoutConstraint!
    @IBOutlet weak var TextViewComments: GrowingTextView!
    @IBOutlet weak var ButtonStatus: UIButton!
    @IBOutlet weak var TextViewDate: GrowingTextView!
    @IBOutlet weak var ButtonTypeOfDefect: UIButton!
    @IBOutlet weak var TextViewDetailsComplaint: GrowingTextView!
    @IBOutlet weak var TextViewPhoneNumber: GrowingTextView!
    @IBOutlet weak var TextViewLocation: GrowingTextView!
    @IBOutlet weak var ButtonBrand: UIButton!
    @IBOutlet weak var ButtonSubsidiaries: UIButton!
    @IBOutlet weak var TextViewTitle: GrowingTextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var viewCardAdmin: UIView!
    var equipmentDetail: EquipmentDetail?
    var isUpdate = ""
    let chooseSubdidiaries = DropDown()
    let chooseBranch = DropDown()
    let chooseDefect = DropDown()
    let chooseStatus = DropDown()
    var allDropdown: AllDropdown?
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseSubdidiaries,
            self.chooseBranch,
            self.chooseDefect,
            self.chooseStatus
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateEquipmentDetail()
        setup()
        requestServerDropdown()
    }
    
    func setup() {
        buttonSubmit.layer.cornerRadius = 30
        buttonSubmit.clipsToBounds = true
        
        let userRole = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.userRoleId)as? String ?? ""
        if userRole != "1" {
            viewConstraintsHeight.constant = 0
            viewCardAdmin.isHidden = true
        } else {
            viewCardAdmin.isHidden = false
        }
    }
    
    func updateEquipmentDetail(){
        let data = equipmentDetail?.data
        TextViewLocation.text = data?.location
        TextViewPhoneNumber.text = data?.phone
        TextViewTitle.text = data?.maintenance_title
        TextViewDetailsComplaint.text = data?.comment
        
    }
    
    func setupDropDowns() {
        setupChooseSubdidiariesDropDown()
        setupChooseBranchDropDown()
        setupChooseDefectDropDown()
        setupChooseStatusDropDown()
    }
    
    func setupChooseSubdidiariesDropDown() {
        var list = [String]()
        for subsi in (allDropdown?.get_all_companies)! {
            list.append(subsi.name ?? "")
        }
        
        chooseSubdidiaries.anchorView = ButtonSubsidiaries
        chooseSubdidiaries.bottomOffset = CGPoint(x: 0, y: ButtonSubsidiaries.bounds.height)
        chooseSubdidiaries.dataSource = list
        // Action triggered on selection
        chooseSubdidiaries.selectionAction = { [weak self] (index, item) in
            self?.ButtonSubsidiaries.setTitle(item, for: .normal)
            
        }
    }
    
    func setupChooseBranchDropDown() {
        var branch = [String]()
        for brn in (allDropdown?.get_all_companies)! {
            branch.append(brn.city ?? "")
        }
        chooseBranch.anchorView = ButtonBrand
        chooseBranch.bottomOffset = CGPoint(x: 0, y: ButtonBrand.bounds.height)
        chooseBranch.dataSource = branch
        // Action triggered on selection
        chooseBranch.selectionAction = { [weak self] (index, item) in
            self?.ButtonBrand.setTitle(item, for: .normal)
            
        }
    }
    func setupChooseDefectDropDown() {
        chooseDefect.anchorView = ButtonTypeOfDefect
        chooseDefect.bottomOffset = CGPoint(x: 0, y: ButtonTypeOfDefect.bounds.height)
        chooseDefect.dataSource = [
            "Lamps",
            "Door",
            "Air Conditioner",
            "Buildings",
            "Other(state in the text box)"
        ]
        // Action triggered on selection
        chooseDefect.selectionAction = { [weak self] (index, item) in
            self?.ButtonTypeOfDefect.setTitle(item, for: .normal)
            
        }
    }
   
    func setupChooseStatusDropDown() {
        chooseStatus.anchorView = ButtonStatus
        chooseStatus.bottomOffset = CGPoint(x: 0, y: ButtonStatus.bounds.height)
        chooseStatus.dataSource = [
            "Complete",
            "Incomplete"
        ]
        // Action triggered on selection
        chooseStatus.selectionAction = { [weak self] (index, item) in
            self?.ButtonStatus.setTitle(item, for: .normal)
            
        }
    }
    
}
