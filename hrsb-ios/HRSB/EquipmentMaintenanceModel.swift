//
//  EquipmentMaintenanceModel.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct EquipmentMaintenanceModel: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [EquipmentList]?
}

struct EquipmentList: Codable {
    let maintenance_id: String?
    let title: String?
    let date_of_complaint: String?
    let employee_name: String?
    let branch: String?
    let status: String?
}

