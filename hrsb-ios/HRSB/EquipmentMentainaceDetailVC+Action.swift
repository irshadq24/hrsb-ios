//
//  EquipmentMentainaceDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension EquipmentMentainaceDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionEdit(_ sender: UIButton) {
        Console.log("buttonActionEdit")
        let vc = EquipmentEditVC.instantiate(fromAppStoryboard: .main)
        vc.equipmentDetail = self.equipmentDetailModel
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

