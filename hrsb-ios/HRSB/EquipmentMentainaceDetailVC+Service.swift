//
//  EquipmentMentainaceDetailVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension EquipmentMentainaceDetailVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.maintenanceDetails
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.equipmentDetailModel = try decoder.decode(EquipmentDetail.self, from: data)
                    Console.log("equipmentDetailModel:- \(self.equipmentDetailModel)")
                    setupEquipmentDetail(detail: self.equipmentDetailModel)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func setupEquipmentDetail(detail: EquipmentDetail?) {
        let data = detail?.data
        labelTitle.text = data?.maintenance_title ?? ""
        labelDate.text = data?.date_of_complaint ?? ""
        labelBranch.text = data?.branch ?? "--"
        labelRemark.text = data?.comment ?? "--"
        labelLocation.text = data?.location ?? "--"
        labelMobileNumber.text = data?.phone ?? ""
        labelSubsidiaries.text = data?.subsidiary
        
        
    }
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}
