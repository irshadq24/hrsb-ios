//
//  EquipmentMentainaceDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EquipmentMentainaceDetailVC: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSubsidiaries: UILabel!
    @IBOutlet weak var labelBranch: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelMobileNumber: UILabel!
    @IBOutlet weak var labelRemark: UILabel!
    
    
    var maintainanceId = ""
    var equipmentDetailModel: EquipmentDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestServer(param: ["maintenance_id": maintainanceId])
    }
    


}
