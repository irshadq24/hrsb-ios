//
//  EquipmentMentainaceVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension EquipmentMentainaceVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionAddMaintainace(_ sender: UIButton) {
        Console.log("buttonActionAddMaintainace")
        let vc = EquipmentEditVC.instantiate(fromAppStoryboard: .main)
        vc.isUpdate = "Update"
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

