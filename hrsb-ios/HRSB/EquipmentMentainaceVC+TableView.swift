//
//  EquipmentMentainaceVC+TableView.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension EquipmentMentainaceVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return equipmentMaintenanceModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableViewEquipmentMentenance.dequeueReusableCell(withIdentifier: "ExpenseClaimListCell", for: indexPath)as? ExpenseClaimListCell else {
            return UITableViewCell()
        }
        let data = equipmentMaintenanceModel?.data?[indexPath.row]
        cell.labelFacility.text = data?.title
        if let date = data?.date_of_complaint?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            cell.labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            cell.labelDate.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        return cell
    }
}

extension EquipmentMentainaceVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data  = equipmentMaintenanceModel?.data?[indexPath.row]
        let vc = EquipmentMentainaceDetailVC.instantiate(fromAppStoryboard: .main)
        vc.maintainanceId = data?.maintenance_id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
