//
//  EquipmentMentainaceVC.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EquipmentMentainaceVC: UIViewController {
    @IBOutlet weak var tableViewEquipmentMentenance: UITableView!
    
    var equipmentMaintenanceModel: EquipmentMaintenanceModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        registerTableViewCell()
    }
    
    func registerTableViewCell() {
        tableViewEquipmentMentenance.delegate = self
        tableViewEquipmentMentenance.dataSource = self
        tableViewEquipmentMentenance.register(UINib(nibName: "ExpenseClaimListCell", bundle: nil), forCellReuseIdentifier: "ExpenseClaimListCell")
    }
    
    func setup() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID])
    }
    

}


