//
//  ExpenseCategory.swift
//  HRSB
//
//  Created by Air 3 on 05/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct ExpenseCategory: Codable {
    let result: [ExpenseCategoryList]?
}

struct ExpenseCategoryList: Codable {
    let expense_type_id: String?
    let company_id: String?
    let name: String?
    let status: String?
    let created_at: String?
    let expense_detail_id: String?
}


