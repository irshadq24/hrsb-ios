//
//  ExpenseClaimList.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct ExpenseClaimList: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [ClaimList]?
}

struct ClaimList: Codable {
    let claim_time: String?
    let claim_title: String?
    let employee_id: String?
    let status: String?
    let claim_month: String?
    let claim_id: String?
    let employee_name: String?
    let claim_year: String?
    
}

