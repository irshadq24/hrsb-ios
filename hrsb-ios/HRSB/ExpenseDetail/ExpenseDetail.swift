//
//  ExpenseDetail.swift
//  HRSB
//
//  Created by Air 3 on 25/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct Expense: Codable {
    let status: String?
    let data: ExpenseDetail?
}

struct ExpenseDetail: Codable {
    let id: String?
    let claim_id: String?
    let employee_id: String?
    let company_id: String?
    let employee_name: String?
    let expense_type_id: String?
    let billcopy_file: String?
    let billcopy_file_view: String?
    let amount: String?
    let project_code: String?
    let remarks: String?
    let status: String?
    let created_at: String?
    let claim_date: String?
    let billcopy_download: String?
    let all_expense_types: [ExpenseType]?
}

struct ExpenseType: Codable {
    let expense_type_id: String?
    let company_id: String?
    let status: String?
    let created_at: String?
    let name: String?
}
