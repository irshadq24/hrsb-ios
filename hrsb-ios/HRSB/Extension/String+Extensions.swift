//
//  String+Extensions.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 13/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit
extension String
{
    
    func getDateInstance(validFormat: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = validFormat
        return dateFormatter.date(from: self)
    }
    
    func isPasswordValid() -> Bool{
        let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    func isValidEmail() -> Bool { //  "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isValidName() -> Bool {
        let nameRegEx = "[A-Za-z]+(\\s[A-Za-z]+)?"
        //"[a-zA-Z'-. ]+"
        
        //"^[a-zA-Z\\s]*$]"
        
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: self)
    }
//    func isContactNumberValid() -> Bool {
//        if(self.characters.count >= 8 && self.characters.count <= 16){
//            return true
//        }
//        else{
//            return false
//        }
//    }
    
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    func trailingTrim(_ characterSet : CharacterSet) -> String {
        if let range = rangeOfCharacter(from: characterSet, options: [.anchored, .backwards]) {
            return self.substring(to: range.lowerBound).trailingTrim(characterSet)
        }
        return self
    }
    func trimFromLeading() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    func isPasswordSame(_ confirmPassword : String) -> Bool {
        if confirmPassword == self{
            return true
        }else{
            return false
        }
    }
    
    func isLengthValid(_ length : Int) -> Bool {
        if self.count == length{
            return true
        }else{
            return false
        }
    }
    
    func isLengthValid(minLength: Int , maxLength: Int) -> Bool {
        return self.count >= minLength && self.count <= maxLength
    }
    func isNumeric() -> Bool{
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }
    func isAllDigitsZero() -> Bool{
        if let validValue = Int(self){
            if validValue == 0{
                return true
            }
        }
        return false
    }
    func isMinimumLengthValid(minLength : Int) -> Bool {
        return self.count >= minLength
    }
    func isMaximumLengthValid(maxLength : Int) -> Bool  {
        return self.count <= maxLength
    }
    func isValidPassword() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: self)
    }
    func isAlphabet() -> Bool {
        return CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: self))
    }
    static func basicAuth(userName : String , password : String) -> String{
        let authString = String(format: "%@:%@", userName, password)
        let authStringData = authString.data(using: String.Encoding.utf8)!
        let base64EncodedCredential = authStringData.base64EncodedString()
        let basicAuthString = "Basic \(base64EncodedCredential)"
        return basicAuthString
    }
    
    func isNameValid() -> String? {
        if self == "" {
            
            return Validation.errorFirstNameEmpty
        }else if !self.isValidName() {
            return Validation.errorNameInvalid
        }else if !self.isLengthValid(minLength: 3, maxLength: 10) {
            return Validation.errorNameLength
        }
        return nil
    }
    
    func isPhoneNumberValid() -> String?{
        let value = self
        if value.isEmpty {
            return Validation.errorEmptyPhoneNumber
        }
        else if !value.isNumeric() {
            return Validation.errorNotNumeric
            
        }else if !value.isLengthValid(minLength: 8, maxLength: 15){
            return Validation.errorPhoneLength
        }
        return nil
        
    }
    
    func isEmailValid() -> String?{
        let value = self
        if value == "" {
            return Validation.errorEmailEmpty
        }
        else if !value.isValidEmail() {
            return  Validation.errorEmailInvalid
            
        }
        return nil
        
    }
    
    func isAlphanumeric() -> Bool {
        return self.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil && self != ""
    }
    func validateEmail(){
        
    }
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "color: #\(color.hexString) !important;" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    
}

