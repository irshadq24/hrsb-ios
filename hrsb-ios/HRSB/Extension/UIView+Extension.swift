//
//  UIView+Extension.swift
//  WeekInChina
//
//  Created by Vikas Sachan on 05/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit
extension UIView{
    func makeRoundCorner(_ radius:CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    func makeRounded(){
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    func close() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
        }, completion: { (compelete) in
            if compelete {
                self.removeFromSuperview()
            }
        })
    }
    func makeBorder(_ width:CGFloat,color:UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }
    func addShadowWithRadius(radius: CGFloat ,cornerRadius: CGFloat ){
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = radius
        layer.cornerRadius = cornerRadius
    }
    func setShadowWithColor() {
        layer.cornerRadius = 10
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 0 ,height: 3)
        self.layer.shadowRadius = 5
        
    }
    
    class func fromNib<T : UIView>() -> T {
        let viewDescription =  T.debugDescription()
        let className = viewDescription.components(separatedBy: ".")[1]
        return Bundle.main.loadNibNamed(className, owner: nil, options: nil )![0] as! T
    }
    class func fromNib<T : UIView>(xibName: String) -> T {
        return Bundle.main.loadNibNamed(xibName, owner: nil, options: nil )![0] as! T
    }
    func setRadius(_ corner : CGFloat){
        layer.cornerRadius = corner
        clipsToBounds = true
    }
    var parentViewController: UIViewController? {
        for responder in sequence(first: self, next: { $0.next }) {
            if let viewController = responder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
