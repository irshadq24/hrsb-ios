//
//  UIViewController+Extension.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 08/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//


import Foundation
import UIKit

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    func showAlertWithActions(msg: String, titles: [String], handler:@escaping (_ clickedIndex: Int) -> Void) {
        if titles.last?.lowercased() == "cancel" {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
            for title in titles {
                if title.lowercased() == "cancel" {
                    let action  = UIAlertAction(title: title, style: .cancel, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                } else {
                    let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                }
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
            
            for title in titles {
                
                let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                    //Call back fall when user clicked
                    let index = titles.index(of: alertAction.title!)
                    if index != nil {
                        handler(index!+1)
                    }
                    else {
                        handler(0)
                    }
                    
                })
                alert.addAction(action)
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
    }
    
    func showOkCancelAlertWithAction(_ msg: String, handler:@escaping (_ isOkAction: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction =  UIAlertAction(title: "Ok", style: .default) { (action) -> Void in
            return handler(true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            return handler(false)
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    func showAlert(title: String, message: String)  {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "ok", style: .default) { (_) in
        }
        alertController.addAction(confirmAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showOkAlertWithHandler(_ msg: String,handler: @escaping ()->Void){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (type) -> Void in
            handler()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func delay(time:TimeInterval,completionHandler: @escaping ()->()) {
        let when = DispatchTime.now() + time
        DispatchQueue.main.asyncAfter(deadline: when) {
            completionHandler()
        }
    }
    func setLeftBarButtonItem(imageName: String){
        let image =  UIImage.init(named: imageName)?.withRenderingMode(.alwaysOriginal)
        let barButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: nil)
        navigationItem.leftBarButtonItem = barButtonItem
    }
    func setRightBarButtonItem(imageName: String){
        let image =  UIImage.init(named: imageName)?.withRenderingMode(.alwaysOriginal)
        let barButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = barButtonItem
        //navigationItem.setRightBarButton(barButtonItem, animated: true)
    }
    
    private func saveImageToGallry(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.updateStatus(image:error:context:)), nil)
    }
    
    @objc func updateStatus(image: UIImage, error: Error?, context: UnsafeRawPointer) {
        DisplayBanner.show(message: "Downloaded successfully.")
    }
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print("heelo",response?.suggestedFilename ?? url.lastPathComponent)
            guard let image = UIImage(data: data) else { return }
            self.saveImageToGallry(image: image)
            print("Saved")
        }
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func savePdf(urlString:String, fileName:String) {
        if urlString.contains(".jpg") || urlString.contains(".png") {
            downloadImage(from: URL(string: urlString)!)
        }
        else if urlString.contains(".pdf") {
            DispatchQueue.main.async {
                let url = URL(string: urlString)
                let pdfData = try? Data.init(contentsOf: url!)
                let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
                let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                do {
                    try pdfData?.write(to: actualPath, options: .atomic)
                    let activityController = UIActivityViewController(activityItems: [pdfData!], applicationActivities: nil)
                    self.present(activityController, animated: true, completion: nil)
                } catch {
                    print("Pdf could not be saved")
                }
            }
        }
        else if urlString.contains(".txt") {
            DispatchQueue.main.async {
                let url = URL(string: urlString)
                let pdfData = try? Data.init(contentsOf: url!)
                let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                let pdfNameFromUrl = "YourAppName-\(fileName).txt"
                let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                do {
                    try pdfData?.write(to: actualPath, options: .atomic)
                    let activityController = UIActivityViewController(activityItems: [pdfData!], applicationActivities: nil)
                    self.present(activityController, animated: true, completion: nil)
                } catch {
                    print("txt could not be saved")
                }
            }
        }
        else {
            DisplayBanner.show(message: "Invalid link.")
        }
    }
    func centerTextMessage(text: String, show: Bool) {
        if !show {
            if let lblFound = self.view.viewWithTag(1312) as? UILabel{
                lblFound.removeFromSuperview()
            }
            return
        }
        if let lblFound = self.view.viewWithTag(1312) as? UILabel{
            lblFound.text = text
            return
        }
        
        var frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        frame.origin = CGPoint(x: self.view.center.x-100, y: self.view.center.y-200)
        
        let label = UILabel(frame: frame)
        label.text = text
        label.tag = 1314
        label.textAlignment = .center
        label.numberOfLines = 0
        self.view.addSubview(label)
    }
}
