//
//  FacilityBooingRoom.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct FacilityBooingRoom: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [FacilityBooingRoomList]?
}

struct FacilityBooingRoomList: Codable {
    let facilities_room_id: String?
    let subsidiary: String?
    let facility: String?
    let branch: String?
    let date_from: String?
    let purpose: String?
}

