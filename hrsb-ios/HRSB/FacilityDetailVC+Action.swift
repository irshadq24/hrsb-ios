//
//  FacilityDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 27/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension FacilityDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionEdit(_ sender: UIButton) {
        Console.log("buttonActionEdit")
        let data = bookingDetail?.data?[sender.tag]
        let vc = AddMeetingRoomBookingVC.instantiate(fromAppStoryboard: .main)
        vc.isEdit = "isEdit"
        vc.bookingDetailData = data
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
