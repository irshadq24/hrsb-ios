//
//  FacilityDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 27/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class FacilityDetailVC: UIViewController {
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSlotTime: UILabel!
    @IBOutlet weak var labelSubsidiaries: UILabel!
    @IBOutlet weak var labelBranch: UILabel!
    @IBOutlet weak var labelBookingPurpose: UILabel!
    @IBOutlet weak var labelRemarks: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var labelMeetingRoomName: UILabel!
    
    var bookingDetail: BookingDetail?
    var facilityId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["employee_id": userID, "facilities_room_id" : facilityId])
    }
}
