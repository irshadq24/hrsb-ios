//
//  FilterAssetVC.swift
//  HRSB
//
//  Created by Salman on 04/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown


class FilterAssetVC: UIViewController {
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnEmployee: UIButton!
    @IBOutlet weak var btnWorking: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    var arrCategory = [[String: Any]]()
    var arrEmployee = [[String: Any]]()
    var arrComapny = [[String: Any]]()
    
    var selectedCompany = [String: Any]()
    var selectedEmployee = [String: Any]()
    var selectedCategory = [String: Any]()
    
    let dropDown = DropDown()
    var assetVC: AssetVC?
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllCategory()
        getAllEmployee()
        getAllCompany()
        btnSubmit.makeRoundCorner(25)
        // Do any additional setup after loading the view.
    }
    
    func getAllCategory() {
        let endPoint = MethodName.getAssetCategory
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrCategory = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    func getAllEmployee() {
        //let endPoint = MethodName.employeeList
        let endPoint = MethodName.getNormalEmployee
        ApiManager.request(path: endPoint, parameters: ["user_id":getUserDefault(Constant.UserDefaultsKey.UserId)], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrEmployee = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    
    func getAllCompany() {
        let endPoint = MethodName.dropdown
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["get_all_companies"] as? [[String: Any]] {
                    self?.arrComapny = subCate
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }

    @IBAction func tapSelectCategory(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrCategory {
            let name = sub["category_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            sender.setTitle(item, for: .normal)
            self?.selectedCategory = self?.arrCategory[index] ?? [:]
        }
        dropDown.show()
    }
    @IBAction func tapCompany(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrComapny {
            let name = sub["name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedCompany = self?.arrComapny[index] ?? [:]
            sender.setTitle(item, for: .normal)
        }
        dropDown.show()
    }
    
    @IBAction func tapEmployee(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrEmployee {
            let name = sub["employee_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedEmployee = self?.arrEmployee[index] ?? [:]
            sender.setTitle(item, for: .normal)
        }
        dropDown.show()
    }
    

    
    @IBAction func tapIsWorking(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.dataSource = ["YES", "NO"]
        dropDown.selectionAction = { [weak self] (index, item) in
            sender.setTitle(item, for: .normal)
            sender.tag = index+1
        }
        dropDown.show()
    }
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func tapSubmit(_ sender: Any) {
        let endPoint = MethodName.filterAsset
        let params = ["type":"filter_asset", "assets_category_id":self.selectedCategory["assets_category_id"] as? String ?? "", "is_working": "\(btnWorking.tag)", "company_id": self.selectedCompany["company_id"] as? String ?? "", "employee_id":self.selectedEmployee["user_id"] as? String ?? "", "user_id":Constant.UserDefaultsKey.UserId]
        
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                 print(data)
                if let dict = data as? [String: Any], let assets = dict["data"] as? [[String: Any]] {
                    self?.assetVC?.arrAsset.removeAll()
                    for asset in assets {
                        self?.assetVC?.arrAsset.append(AssetModel(info: asset))
                    }
                    self?.assetVC?.isFilterEnable = true
                    self?.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
//        ApiManager.request(path: endPoint, parameters: params, methodType: .post) { [weak self](result) in
//            switch result {
//            case .success(let data):
//                print(data)
//                if let dict = data as? [String: Any], let employee = dict["data"] as? [[String: String]] {
//                    print(data)
//                }
//            case .failure(let error):
//                self?.showAlert(title: "", message: error.debugDescription)
//            case .noDataFound(_):
//                break
//            }
//        }
    }
}
