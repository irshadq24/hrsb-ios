//
//  FilterAttendenceVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension FilterAttendenceVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func buttonActionFilterAttendence(_ sender: UIButton) {
        Console.log("buttonActionFilterAttendence")
        if isValidate() {
            DisplayBanner.show(message: "Under working now...")
        }
    }
    
    
    @IBAction func buttonActionDay(_ sender: UIButton) {
        Console.log("buttonActionDay")
        chooseDay.show()
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton) {
        Console.log("buttonActionMonth")
        chooseMonth.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton) {
        Console.log("buttonActionYear")
        chooseYear.show()
    }
    
    func isValidate() -> Bool {
        if buttonDay.titleLabel?.text == "Day" {
            DisplayBanner.show(message: "Please select Day.")
            return false
        }else if buttonMonth.titleLabel?.text == "Month" {
            DisplayBanner.show(message: "Please select Month.")
            return false
        }else if buttonYear.titleLabel?.text == "Year" {
            DisplayBanner.show(message: "Please select Year.")
            return false
        }
        return true
    }
}
