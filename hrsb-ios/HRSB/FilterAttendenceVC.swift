//
//  FilterAttendenceVC.swift
//  HRSB
//
//  Created by Air 3 on 04/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class FilterAttendenceVC: UIViewController {

    @IBOutlet weak var buttonFilterAttendence: UIButton!
    @IBOutlet weak var buttonDay: UIButton!
    @IBOutlet weak var buttonMonth: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    
    var dayValue = ""
    var monthValue = ""
    var yearValue = ""
    
    let chooseDay = DropDown()
    let chooseMonth = DropDown()
    let chooseYear = DropDown()
   
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDay,
            self.chooseMonth,
            self.chooseYear
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDowns()
        // Do any additional setup after loading the view.
    }
    
    func setupDropDowns() {
        setupChooseDayDropDown()
        setupChooseMonthDropDown()
        setupChooseYearDropDown()
    }
    
    func setupChooseDayDropDown() {
        var arrayDay = [String]()
        for day in 1...30 {
            arrayDay.append("\(day)")
        }
        chooseDay.anchorView = buttonDay
        chooseDay.bottomOffset = CGPoint(x: 0, y: buttonDay.bounds.height)
        chooseDay.dataSource = arrayDay
        // Action triggered on selection
        chooseDay.selectionAction = { [weak self] (index, item) in
            self?.buttonDay.setTitle(item, for: .normal)
            self?.dayValue = item
        }
    }
    func setupChooseMonthDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonth.anchorView = buttonMonth
        chooseMonth.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonth.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonth.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.monthValue = item
        }
    }
    func setupChooseYearDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYear.anchorView = buttonYear
        chooseYear.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYear.dataSource = arrayYear
        // Action triggered on selection
        chooseYear.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.yearValue = item
        }
    }


}
