//
//  ForgotPasswordViewController.swift
//  HRSB
//
//  Created by BestWeb on 03/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    
    
    
    @IBOutlet var txt: UITextField!
    
    @IBOutlet var back: UIButton!
    
    
    
    @IBOutlet var send: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func Send_action(_ sender: Any) {
        let mail = CommonFunction.shared.trimString(text: self.txt.text)
        if mail == "" {
            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Please enter your email id.")
        }else {
            self.Forgot(Email: mail)
        }

        
        
    }
    
    
   private func Forgot(Email: String){
    Loader.show()
    ServiceHelper.sharedInstance.ForgotPassword(email: Email){(result, error)in
        Loader.hide()
            if let resultValue = result as? [String : Any]{
                if let Msg = resultValue["message"] as?  [String : Any] {
                    print("dat", Msg)
                    if let token = Msg["result"] as? String, token != ""{
                        let respo = Msg["csrf_hash"] as? String
                        if let error = Msg["result"] as? String {
                            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: error)
                        }
                        print("respo", token)
                    }else {
                        if let error = Msg["error"] as? String {
                            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: error)
                        }
                    }
                }
            }
        }
    }
        
        
    
    @IBAction func back_Action(_ sender: Any) {
        
        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)
        navigationController?.popViewController(animated: true)

    }
    
    
    
    
    
    
}
