//
//  GrievanceDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 28/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension GrievanceDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func buttonActionEdit(_ sender: UIButton) {
        Console.log("buttonActionEdit")
        
        let vc = AddGrievanceVC.instantiate(fromAppStoryboard: .main)
        vc.editDetail = data
        vc.isEdit = "1"
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
