//
//  GrievanceDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 28/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit


class GrievanceDetailVC: UIViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelComplaintDate: UILabel!
    @IBOutlet weak var labelComplaintAgainst: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelComplaintTitle: UILabel!
    
    var data: GrievanceArrayList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Console.log("DTAA:- \(data)")
        labelDescription.text = data?.description
        labelComplaintAgainst.text = data?.complaint_against
        labelComplaintDate.text = data?.complaint_date
        labelComplaintTitle.text = data?.title
        if let date = data?.created_date?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            labelDate.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        
    }
    
    
}
