//
//  GrievanceList.swift
//  HRSB
//
//  Created by Air 3 on 28/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct GrievanceListData: Codable {
    let recordsFiltered: Int?
    let recordsTotal: Int?
    let data: [GrievanceArrayList]?
}

struct GrievanceArrayList: Codable {
    let complaint_id: String?
    let complaint_from: String?
    let complaint_against: String?
    let company_name: String?
    let title: String?
    let description: String?
    let status: String?
    let complaint_date: String?
    let created_date: String?
}
