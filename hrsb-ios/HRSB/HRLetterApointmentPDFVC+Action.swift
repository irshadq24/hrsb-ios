//
//  HRLetterApointmentPDFVC+Action.swift
//  HRSB
//
//  Created by Salman on 19/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension HRLetterApointmentPDFVC {
    
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func buttonActionDownloadPdfFile(_ sender: UIButton) {
        Console.log("buttonActionDownloadPdfFile")
        if titleValue == "Code of Conduct (COC)" {
            let downloadPDF = document?.download_document ?? ""
            savePdf(urlString: downloadPDF, fileName: "HRBS")
        } else if titleValue == "Letter of appointment" {
            let downloadPDF = document?.download_document ?? ""
            savePdf(urlString: downloadPDF, fileName: "HRBS")
        } else if titleValue == "Payslips" {
            let downloadPDF = document?.download_document ?? ""
            savePdf(urlString: downloadPDF, fileName: "HRBS")
        }else if titleValue == "Memo" {
            let downloadPDF = memoDownload?.download_memo ?? ""
            savePdf(urlString: downloadPDF, fileName: "HRBS")
        } else if titleValue == "Warning Letter" {
            let download = warningRead?.warning_file_download ?? ""
            Console.log("URL:- \(download)")
            savePdf(urlString: download, fileName: "HRBS")
            //self.downloadImage(from: URL(string: download)!)
        }
    }
    
    
    
    
    
}
