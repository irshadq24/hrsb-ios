//
//  HRLetterApointmentPDFVC.swift
//  HRSB
//
//  Created by Salman on 19/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class HRLetterApointmentPDFVC: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewPlaceholder: UIImageView!
    
    var announcementId = ""
    var isReadMemo = ""
    var isWarning = ""
    var warning_id = ""
    var document: DocumentList?
    var warningRead: WarningReadList?
    var imageArray = ["letter-of-employment","","code-of-conduct","memo","","domestic-inquiries"]
    var titleValue = ""
    var memoDownload: MemoDownload?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Console.log("document:- \(String(describing: document))")
        setup()
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        if isReadMemo == "1" {
            requestServer(param: ["announcement_id": announcementId])
        }
        if isWarning == "2" {
            requestServerWarning(param: ["warning_id": warning_id,"user_id": userID])
        }
    }
    
    
    
    func setup() {
        labelTitle.text = titleValue
        if titleValue == "Letter of appointment" {
            imageViewPlaceholder.image = UIImage(named: "letter-of-employment")
        } else if titleValue == "Code of Conduct (COC)"{
            imageViewPlaceholder.image = UIImage(named: "code-of-conduct")
        } else if titleValue == "Memo" {
            imageViewPlaceholder.image = UIImage(named: "memo-1")
        } else if titleValue == "Warning Letter" {
            imageViewPlaceholder.image = UIImage(named: "domestic-inquiries")
        }
    }
    
    


}
