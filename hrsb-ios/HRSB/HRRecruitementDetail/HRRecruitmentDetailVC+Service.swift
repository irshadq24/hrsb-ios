//
//  HRRecruitmentDetailVC+Service.swift
//  HRSB
//
//  Created by Salman on 18/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension HRRecruitmentDetailVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.recruitementDetail
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    let data = try decoder.decode(RecruitementDetail.self, from: data)
                    self.sectionList[0]["points"] = [data.data?.responsibility ?? ""]
                    self.sectionList[1]["points"] = [data.data?.qualification ?? ""]
                    labelPrice.text = data.data?.basic_salary
                    labelLocation.text = data.data?.location
                    labelExeprience.text = data.data?.minimum_experience
                    labelCompanyName.text = data.data?.job_title
                    labelManagerName.text = data.data?.long_description
                    tableViewRecruitementDetail.reloadData()
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
