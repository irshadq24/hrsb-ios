//
//  HRRecruitmentDetailVC+TableView.swift
//  HRSB
//
//  Created by Salman on 18/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension HRRecruitmentDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let title = sectionList[section]["title"] as? String {
            return title
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let points = sectionList[section]["points"] as? [String] {
            return points.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.tableCellRecruitementDetail, for: indexPath) as! RecruitementDetailCell
        if let points = sectionList[indexPath.section]["points"] as? [String] {
             cell.lblDescription.text = points[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
//    if indexPath.section == 0 {
//    guard let cell = tableViewRecruitementDetail.dequeueReusableCell(withIdentifier: CellIdentifier.tableCellRecruitementDetail, for: indexPath)as? RecruitementDetailCell  else {
//    return UITableViewCell()
//    }
//    return cell
//    } else if indexPath.section == 1 {
//    guard let cell = tableViewRecruitementDetail.dequeueReusableCell(withIdentifier: CellIdentifier.tableCellRecruitementDetail, for: indexPath)as? RecruitementDetailCell  else {
//    return UITableViewCell()
//    }
//
//    return cell
//    }
//    return UITableViewCell()
    
  
   
    
}
