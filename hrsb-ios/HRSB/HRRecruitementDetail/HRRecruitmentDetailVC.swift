//
//  HRRecruitmentDetailVC.swift
//  HRSB
//
//  Created by Salman on 18/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class HRRecruitmentDetailVC: UIViewController {

    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var tableViewRecruitementDetail: UITableView!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelExeprience: UILabel!
    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var labelManagerName: UILabel!
    
    var recruitementDetail: RecruitementDetail?
    var sectionList = [["title":"Responsibility","points":["abc"]], ["title":"Qualification","points":["abc"]]]
    var id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        registerTableViewCell()
        requestServer(param: ["job_id": id])
    }

    func setup() {
        tableViewRecruitementDetail.rowHeight = UITableView.automaticDimension
        tableViewRecruitementDetail.estimatedRowHeight = 50
        //lblCompany.text = recruitementDetail?.data?.company
    }
    
    
    func registerTableViewCell() {
        tableViewRecruitementDetail.delegate = self
        tableViewRecruitementDetail.dataSource = self
    }
    
    @IBAction func buttonActionBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

}
