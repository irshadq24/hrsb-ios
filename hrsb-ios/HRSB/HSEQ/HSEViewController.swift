//
//  HSEViewController.swift
//  HRSB
//
//  Created by BestWeb on 26/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class HSEViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    
    let reuseIdentifier = "HSECell"
    
    var appBarViewController = MDCAppBarViewController()
    
    
    var items = ["Incident Listing Form", "Man Hours Listing Form"]
    
    
    var Images = ["incidentlistingform.png", "manhourslistingform.png"]

    
    
    @IBOutlet weak var view_white: UIView!
    
    
    @IBOutlet weak var tble: UITableView!
    
    
    @IBOutlet weak var back: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        
        
        
        
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: nil)
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)

        
        tble.dataSource = self
        tble.delegate = self
        
        
        
        self.setheight()

        addSlideMenuButton()
        addSlideMenuButton1()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func back_Action(_ sender: Any) {
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)
        navigationController?.popViewController(animated: true)

    }
    
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! HSECell
        
        
        
        
        
        cell.l1.text = items[indexPath.row]
        
        cell.img.image = UIImage(named: Images[indexPath.row])
        
        
        
        
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = IncidentListingFormVC.instantiate(fromAppStoryboard: .main)
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = IncidentListingGeneralVC.instantiate(fromAppStoryboard: .main)
            vc.manListing = "Man Hours Listing Summary"
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            
            
            print(1)
//            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.EmployeVC)
//            self.present(HrVc, animated: true, completion: nil)
            
            
            
        }
        
    }

    
    

}
