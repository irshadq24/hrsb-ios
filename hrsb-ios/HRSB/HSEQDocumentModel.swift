//
//  HSEQDocumentModel.swift
//  HRSB
//
//  Created by Zakir Khan on 17/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct HSEQDocument: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [DocumentListData]
}

struct DocumentListData: Codable {
    let document_title: String?
    let document_id: String?
    let document_type_id: String?
    let date_create: String?
}
