//
//  HomeMemoCell.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class HomeMemoCell: UICollectionViewCell {
    @IBOutlet weak var l1: UILabel!
    @IBOutlet weak var l2: UILabel!
    @IBOutlet weak var l3: UILabel!
    
    var info: Memo? {
        didSet {
            l1.text = info?.title
            l2.text = info?.startdate
            l3.text = info?.summary
        }
    }
}
