//
//  HomeTrainingCell.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class HomeTrainingCell: UICollectionViewCell {
    @IBOutlet weak var l1: UILabel!
    @IBOutlet weak var l2: UILabel!
    @IBOutlet weak var l3: UILabel!
    
    var info: TraningDevelopment? {
        didSet{
            l1.text = info?.trainer_name
            l2.text = info?.training_date
            l3.text = info?.description
        }
    }
}
