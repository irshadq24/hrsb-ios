//
//  HomeViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class HomeViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var memobtn: UIButton!
    @IBOutlet weak var trainingbtn: UIButton!
    @IBOutlet weak var calendarbtn: UIButton!
    @IBOutlet weak var view_white: UIView!
    @IBOutlet weak var lbl_welcomuser: UILabel!
    @IBOutlet var vie_memo: UIView!
    @IBOutlet weak var lbl_highlits: UILabel!
    @IBOutlet var vie_calender: UIView!
    @IBOutlet var vie_traing: UIView!
    @IBOutlet var lbl_memo: UILabel!
    @IBOutlet var lbl_calender: UILabel!
    @IBOutlet var lbl_training: UILabel!
    var items = ["Human Resources", "Operation", "Corporate Health"]
    var items1 = ["Information Technology", "Finance", "Procurement"]
    var itemsimg = ["HR.png", "operation.png", "corporate healthhseq.png"]
    var itemsimg1 = ["information technology.png", "procurementhome.png", "calendarhome.png"]
    var memotitle = ["memotitle", "memotitle", "memotitle"]
    var color = [UIColor.blue, UIColor.gray, UIColor.green]
    var color1 = [UIColor.red, UIColor.yellow, UIColor.orange]
    let reuseIdentifier = "HomeOneCell"
    let reuseIdentifier1 = "HomeTwoCell"
    let reuseIdentifiermemo = "HomeMemoCell"
    let reuseIdentifiercalender = "HomeCalenderCell"
    let reuseIdentifiertraining = "HomeTrainingCell"
    var appBarViewController = MDCAppBarViewController()
    @IBOutlet weak var clctn: UICollectionView!
    @IBOutlet weak var clctn2: UICollectionView!
    @IBOutlet weak var clctn_home: UICollectionView!
    @IBOutlet var clctn_calender: UICollectionView!
    @IBOutlet var clctn_training: UICollectionView!
    var memoList = [Memo]()
    var trainingList: TraningAndDevelopment?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        print("TESTMAIL", Constant.UserDefaultsKey.init())
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        addSlideMenuButton()
        fetchMemoData()
        fetchTrainingData()
        // Set the tracking scroll view.
        //self.appBarViewController.headerView.trackingScrollView = self.scrol
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
       // scrol.addSubview(view_white)
        
//        view_white.frame = CGRect(x: 0, y: appBarViewController.headerStackView.frame.origin.y + appBarViewController.headerStackView.frame.height, width: self.view.frame.width, height: 60)
        
        
        //view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)

        

      //  scrol.frame = CGRect(x: 0, y: self.view_white.frame.height, width: view.frame.width, height: view.frame.height)
        
        
        
        
        
    //    scrol.isHidden = true
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//        
//        
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//        
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: #selector(self.MenuAction))
//        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))


        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.ProfileAction))
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
     //   let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        
        
        let imageView = UIImageView()

        
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
        
        self.appBarViewController.headerStackView.addSubview(imageView)
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
      //  navigationItem.titleView = imageView
        
     //   scrol.contentSize = CGSize(width: view.frame.width , height: 1100)
      //  scrol.delegate = self
        clctn.dataSource = self
        clctn.delegate = self
        clctn2.dataSource = self
        clctn2.delegate = self
        clctn_home.dataSource = self
        clctn_home.delegate = self
        clctn_training.dataSource = self
        clctn_training.delegate = self
       // self.scrol.addSubview(clctn_training)
        
        
        
//        self.scrol.addSubview(vie_memo)
//        self.scrol.addSubview(vie_calender)
//        self.scrol.addSubview(vie_traing)

        
        

//        clctn.frame = CGRect(x: 10, y: 10, width: self.view.frame.width - 20, height: 107)
//
//        clctn2.frame = CGRect(x: 10, y: clctn.frame.origin.y + clctn.frame.height + 10, width: self.view.frame.width - 20, height: 107)
//
//
//
//        lbl_welcomuser.frame = CGRect(x: 0, y: clctn2.frame.origin.y + clctn2.frame.height + 10, width: self.view.frame.width, height: 40)
        
        lbl_welcomuser.text = "       Welcome user"
        
        
//        lbl_highlits.frame = CGRect(x: 0, y: lbl_welcomuser.frame.origin.y + lbl_welcomuser.frame.height + 10, width: self.view.frame.width, height: 40)
//
//        lbl_highlits.text = "       Highlights"
//
//
//        vie_memo.frame = CGRect(x: 0, y: lbl_highlits.frame.origin.y + lbl_highlits.frame.height + 10, width: self.view.frame.width, height: 40)
//
//
//
//        clctn_home.frame = CGRect(x: 10, y: vie_memo.frame.origin.y + vie_memo.frame.height + 10, width: self.view.frame.width - 20, height: 107)
//
//
//
//        vie_traing.frame = CGRect(x: 0, y: clctn_home.frame.origin.y + clctn_home.frame.height + 10, width: self.view.frame.width, height: 40)
//
//
//        clctn_training.frame = CGRect(x: 10, y: vie_traing.frame.origin.y + vie_traing.frame.height + 10, width: self.view.frame.width - 20, height: 107)
//
//
//        clctn_calender.isHidden = true
//
//
//
//        vie_calender.frame = CGRect(x: 0, y: clctn_training.frame.origin.y + clctn_training.frame.height + 10, width: self.view.frame.width, height: 40)

        addSlideMenuButton1()
       // scrol.contentSize.width = view.frame.width

        
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   
    func fetchTrainingData() {
        let endPoint = MethodName.trainingList
        let param = ["user_id": getUserDefault(Constant.UserDefaultsKey.UserId)]
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.trainingList = try decoder.decode(TraningAndDevelopment.self, from: data)
                    self.clctn_training.reloadData()
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func fetchMemoData() {
        let userid = getUserDefault(Constant.UserDefaultsKey.UserId)
        ServiceHelper.sharedInstance.MemoList(userid: userid){ (result, error) in
            if let resultValue = result as? [String : Any]{
                let data = resultValue["data"] as! [[String: Any]]
                for dataItem in data{
                    print("summary", dataItem["summary"] as! String)
                    self.memoList.append(Memo(summary: dataItem["summary"] as! String, title: dataItem["title"] as! String, startdate: dataItem["start_date"] as! String, enddate: dataItem["end_date"] as! String, publishedFor: dataItem["published_for"] as! String, deptId: dataItem["department_id"] as! String, companyname: dataItem["company_name"] as! String, companyId: dataItem["company_id"] as! String, AnnouncementId: dataItem["announcement_id"] as! String))
                    self.clctn_home.reloadData()
                }
            }
        }
    }
    
    @objc func MenuAction(){

    }
    @objc func NotifyAction(){
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        presentedVC.view.frame = CGRect(x: 40, y: 40, width: 300, height: 500)
        
        navigationController?.pushViewController(presentedVC, animated: true)
      //  present(presentedVC, animated: false, completion: nil)
    }
    
    @objc func ProfileAction(){}

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.clctn{
            return self.items.count
        } else if collectionView == self.clctn_home{
            return self.memoList.count
        }
        else if collectionView == self.clctn_training{
            return self.trainingList?.data?.count ?? 0
        } else {
            return self.items1.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.clctn {
            let cell = clctn.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as! HomeOneCell
            cell.img.image = UIImage.init(named: itemsimg[indexPath.row])
//            if indexPath.item == 0 || indexPath.item == 2{
//                cell.blurView.isHidden = true
//            }
            cell.blurView.isHidden = true
            cell.img.layer.cornerRadius = 5
            return cell
        }
        else if collectionView == self.clctn_home {
            let cell = clctn_home.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifiermemo, for: indexPath) as! HomeMemoCell
            cell.info = memoList[indexPath.row]
            cell.layer.cornerRadius = 5
            return cell
        }
        else if collectionView == self.clctn_calender{
            let cell = clctn_calender.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifiercalender, for: indexPath) as! HomeCalenderCell
            cell.l1.text = "15"
            cell.l2.text = "August"
            cell.l3.text = "2019"
            //   cell.backgroundColor = UIColor.blue
            cell.layer.cornerRadius = 5
            return cell
        }
        else if collectionView == self.clctn_training{
            guard let cell = clctn_training.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifiertraining, for: indexPath)as? HomeTrainingCell else {
                return UICollectionViewCell()
            }
            cell.info = self.trainingList?.data?[indexPath.row]
            cell.layer.cornerRadius = 5
            return cell
        }
        else{
            let cell = clctn2.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier1, for: indexPath) as! HomeTwoCell
            cell.img.image = UIImage.init(named: itemsimg1[indexPath.row])
            cell.img.layer.cornerRadius = 5
          
            return cell
        }
    }
    

    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == clctn {
            if indexPath.row == 0{
                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.HrVc)
                navigationController?.pushViewController(HrVc, animated: true)
            }
            else  if indexPath.row == 1{
                
                
                
                
                //=======OLD CODE ===========//
                
                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.OperationVC)
//                let transition = CATransition()
//                transition.duration = 0.25
//                transition.type = CATransitionType.push
//                transition.subtype = CATransitionSubtype.fromRight
//                self.view.window!.layer.add(transition, forKey: kCATransition)

          //      present(HrVc, animated: false)

                navigationController?.pushViewController(HrVc, animated: true)

                //=======OLD CODE ===========//
            }
            else  if indexPath.row == 2{
                
                
                
                
                
                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.HSEQ)
//                let transition = CATransition()
//                transition.duration = 0.25
//                transition.type = CATransitionType.push
//                transition.subtype = CATransitionSubtype.fromRight
//                self.view.window!.layer.add(transition, forKey: kCATransition)
//
//                present(HrVc, animated: false)
                navigationController?.pushViewController(HrVc, animated: true)

                
                
            }
            
            
        }
        else if collectionView == clctn2 {
            
            //=======OLD CODE ===========//
            if indexPath.row == 0{
                let HrVc = self.storyboard?.instantiateViewController(withIdentifier: "ITMenuVC")
//                let transition = CATransition()
//                transition.duration = 0.25
//                transition.type = CATransitionType.push
//                transition.subtype = CATransitionSubtype.fromRight
//                self.view.window!.layer.add(transition, forKey: kCATransition)

            //    present(HrVc, animated: false)
                navigationController?.pushViewController(HrVc!, animated: true)



            }
            else  if indexPath.row == 1{





                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ProcureVC)
//                let transition = CATransition()
//                transition.duration = 0.25
//                transition.type = CATransitionType.push
//                transition.subtype = CATransitionSubtype.fromRight
//                self.view.window!.layer.add(transition, forKey: kCATransition)
//
//                present(HrVc, animated: false)
                navigationController?.pushViewController(HrVc, animated: true)



            }
            else  if indexPath.row == 2{

                let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CalendarVC)
//                let transition = CATransition()
//                transition.duration = 0.25
//                transition.type = CATransitionType.push
//                transition.subtype = CATransitionSubtype.fromRight
//                self.view.window!.layer.add(transition, forKey: kCATransition)
//
//                present(HrVc, animated: false)
                navigationController?.pushViewController(HrVc, animated: true)



            }
            
            
            
        }
            
            
            
        
        
        
        
    }
    

    
    
    
    @IBAction func memo_Actionn(_ sender: Any) {
        
        let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.MemoVC)
        navigationController?.pushViewController(HrVc, animated: true)

        
    }
    
    
    
    @IBAction func Calendar_Action(_ sender: Any) {
        Console.log("Calendar_Action")
        //============== OLD CODE ===========
//        let vc = CalendarViewController.instantiate(fromAppStoryboard: .main)
//        navigationController?.pushViewController(vc, animated: true)
         //============== OLD CODE ===========
    }
    
    
    
    
    @IBAction func Training_Action(_ sender: Any) {
        
        
        let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.TrainingDeveVC)

        navigationController?.pushViewController(HrVc, animated: true)

        
        
        
        
    }
    

    
  
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
       
    }


}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == clctn {
            return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        } else if collectionView == clctn2 {
            return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }
        return CGSize(width: collectionView.frame.size.width / 1.6, height: 120)
    }
}


