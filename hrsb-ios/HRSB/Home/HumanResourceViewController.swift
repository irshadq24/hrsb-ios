//
//  HumanResourceViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class HumanResourceViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {
    

    var appBarViewController = MDCAppBarViewController()

    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet weak var back: UIButton!
    
    
    
    let reuseIdentifier = "HumanCell"

    
    @IBOutlet weak var tble: UITableView!
    //================OLD CODE=============================
    var items = ["Recruitment", "Employee", "Staff Relation","Training and Development","Claim","Admin","Calendar"]


    var Images = ["Recruitment.png", "Employee.png", "Staffrelation.png","TraningDevelopment.png","Compensationbenifits.png","Admin.png","Calendar.png"]
 //================OLD CODE=============================
//    var items = ["Recruitment", "Employee","Training and Development","Admin","Calendar"]
//
//
//       var Images = ["Recruitment.png", "Employee.png","TraningDevelopment.png","Admin.png","Calendar.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        
        
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//        
//        
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//        
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)

        
        tble.dataSource = self
        tble.delegate = self

        
        
        self.setheight()
        
        
        addSlideMenuButton()
        
        addSlideMenuButton1()



        
      //  tble.frame =  CGRect(x: 0, y: view_white.frame.origin.y + view_white.frame.width, width: view.frame.width, height: (60 * items.count))
        

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @objc func NotifyAction(){
        
        
//
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
        
        //present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! HumanCell
        
        

        
        
        cell.lbl.text = items[indexPath.row]
        
        cell.img.image = UIImage(named: Images[indexPath.row])
        
        
        
        
        
        return cell

    }

    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

     
        
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //========================== OLD CODE====================
        
        if indexPath.row == 0{


            print(0)

            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.RecruitVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
            navigationController?.pushViewController(HrVc, animated: true)


        }else if indexPath.row == 1{


            print(1)
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.EmployeVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
            navigationController?.pushViewController(HrVc, animated: true)



        }else if indexPath.row == 2{


            print(2)

            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.StaffRelationVc)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
             navigationController?.pushViewController(HrVc, animated: true)


        }else if indexPath.row == 3{


            print(3)

            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.TrainingDeveVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
             navigationController?.pushViewController(HrVc, animated: true)


        }else if indexPath.row == 4{



           // let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CompenstaionVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
//
//            print(4)
            let vc = CompensationViewController.instantiate(fromAppStoryboard: .main)
            navigationController?.pushViewController(vc, animated: true)


        }else if indexPath.row == 5{


            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.AdminVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
             navigationController?.pushViewController(HrVc, animated: true)


            print(5)


        }
        else if indexPath.row == 6{


            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CalendarVC)
//            let transition = CATransition()
//            transition.duration = 0.25
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            self.view.window!.layer.add(transition, forKey: kCATransition)
//
//            present(HrVc, animated: false)
             navigationController?.pushViewController(HrVc, animated: true)


            print(5)


        }
        
            //========================== OLD CODE====================
//            if indexPath.row == 0{
//
//
//                    print(0)
//
//                    let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.RecruitVC)
//        //            let transition = CATransition()
//        //            transition.duration = 0.25
//        //            transition.type = CATransitionType.push
//        //            transition.subtype = CATransitionSubtype.fromRight
//        //            self.view.window!.layer.add(transition, forKey: kCATransition)
//        //
//        //            present(HrVc, animated: false)
//                    navigationController?.pushViewController(HrVc, animated: true)
//
//
//                }else if indexPath.row == 1{
//
//
//                    print(1)
//                    let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.EmployeVC)
//        //            let transition = CATransition()
//        //            transition.duration = 0.25
//        //            transition.type = CATransitionType.push
//        //            transition.subtype = CATransitionSubtype.fromRight
//        //            self.view.window!.layer.add(transition, forKey: kCATransition)
//        //
//        //            present(HrVc, animated: false)
//                    navigationController?.pushViewController(HrVc, animated: true)
//
//
//
//                }else if indexPath.row == 2{
//
//
//                    print(2)
//
//                   let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.TrainingDeveVC)
//                         //            let transition = CATransition()
//                         //            transition.duration = 0.25
//                         //            transition.type = CATransitionType.push
//                         //            transition.subtype = CATransitionSubtype.fromRight
//                         //            self.view.window!.layer.add(transition, forKey: kCATransition)
//                         //
//                         //            present(HrVc, animated: false)
//                                      navigationController?.pushViewController(HrVc, animated: true)
//
//
//                }else if indexPath.row == 3{
//
//
//                    print(3)
//                    let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.AdminVC)
//                    //            let transition = CATransition()
//                    //            transition.duration = 0.25
//                    //            transition.type = CATransitionType.push
//                    //            transition.subtype = CATransitionSubtype.fromRight
//                    //            self.view.window!.layer.add(transition, forKey: kCATransition)
//                    //
//                    //            present(HrVc, animated: false)
//                                 navigationController?.pushViewController(HrVc, animated: true)
//
//
//
//                }else if indexPath.row == 4{
//
//
//            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CalendarVC)
//                    //            let transition = CATransition()
//                    //            transition.duration = 0.25
//                    //            transition.type = CATransitionType.push
//                    //            transition.subtype = CATransitionSubtype.fromRight
//                    //            self.view.window!.layer.add(transition, forKey: kCATransition)
//                    //
//                    //            present(HrVc, animated: false)
//                                 navigationController?.pushViewController(HrVc, animated: true)
//
//
//
//                print(5)
//
//
//                }else if indexPath.row == 5{
//
//
//
//
//                     print(5)
//
//
//                }
//                else if indexPath.row == 6{
//
//
//
//                }
        
    }
    
    
    
    @IBAction func back_Acton(_ sender: Any) {
   
    //    self.dismiss(animated: false, completion: nil)
//
//
//
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//        self.dismiss(animated: false, completion: nil)
        navigationController?.popViewController(animated: true)
    
    
    }
    
    
    
    
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width

        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }
    
    

}
