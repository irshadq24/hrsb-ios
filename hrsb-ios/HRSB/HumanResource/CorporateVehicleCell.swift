//
//  CorporateVehicleCell.swift
//  HRSB
//
//  Created by BestWeb on 27/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CorporateVehicleCell: UITableViewCell {

    
    
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var labelItemNumber: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var viebtn: UIButton!
    @IBOutlet weak var labelAvailable: UILabel!
    @IBOutlet weak var bookbtn: UIButton!
    @IBOutlet weak var imageItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        viebtn.makeRoundCorner(15)
        viebtn.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
        bookbtn.makeRoundCorner(15)
        bookbtn.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
