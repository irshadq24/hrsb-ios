//
//  DomesticInqViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class DomesticInqViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    @IBOutlet var lbl_desc: UILabel!
    
    
    
    
    @IBOutlet var lbl_subjcet: UILabel!
    
    @IBOutlet var lbl_warningby: UILabel!
    
    @IBOutlet var lbl_status: UILabel!
    
    
    @IBOutlet var lbl_warningdate: UILabel!
    @IBOutlet var lbl_company: UILabel!
    
    
    @IBOutlet var lbl_warningdate_tit: UILabel!
    
    @IBOutlet var lbl_subject_tit: UILabel!
    
    @IBOutlet var lbl_warningto: UILabel!
    
    @IBOutlet var lbl_desc_tit: UILabel!
    
    @IBOutlet var scrol_domes: UIScrollView!
    
    @IBOutlet var lbl_warningtype: UILabel!
    
    @IBOutlet var lbl_warningby_tit: UILabel!
    
    @IBOutlet var lbl_status_tit: UILabel!
    
    
    @IBOutlet var lbl_company_tit: UILabel!
    
    
    @IBOutlet var lbl_warningto_tit: UILabel!
    
    @IBOutlet var lbl_warningtyp_tit: UILabel!
    
    
    
    @IBOutlet var view_warniong: UIView!
    
    
    struct Warning {
        
        let Warningid: String
        let Warningto: String
        let Warningfrom: String
        let Companyname: String
        let Warningdate: String
        let WarningType: String
        let Subject: String
        let Status: String
        
    }
    
    var WarningList = [Warning]()

    
    @IBOutlet weak var warningbackk: UIButton!
    
    
    
    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var tble: UITableView!
    
    
    
    let reuseIdentifier = "DomesticInqCell"
    
    var items = ["first", "second","third"]

    var appBarViewController = MDCAppBarViewController()

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tble.dataSource = self
        tble.delegate = self

        
//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
//
//
//        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//
//
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
        
//        let searchItemImage = UIImage(named: "bell.png")
//        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
//        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
//                                         style: .plain,
//                                         target: self,
//                                         action: #selector(self.NotifyAction))
//
//
//        let tuneItemImage = UIImage(named: "Bell1")
//        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
//        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
//
//        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
//        imageView.contentMode = .scaleAspectFit
//
//        let image = UIImage(named: "toplogo.png")
//        imageView.image = image
//
//        self.appBarViewController.headerStackView.addSubview(imageView)
//
//
//
//
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
//
//
//        addSlideMenuButton()
//        addSlideMenuButton1()
        
        
        let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)


        
        self.Warninglist(userid: UseridStr)
        
        
        scrol_domes.contentSize = CGSize(width: view.frame.width, height: 1000)

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
        
       // present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    @IBAction func back_btn(_ sender: Any) {
 
        navigationController?.popViewController(animated: true)

        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)

    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.WarningList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! DomesticInqCell
        
        
        
        
        
        let detail = self.WarningList[indexPath.row]
        cell.l1.text = detail.WarningType
        
        
        var string = "From : "
        var resultStr = string + detail.Warningfrom
        
        
        print("JOINED STR:", resultStr)
        var Date = "Date : "

        
        cell.l2.text =  string + detail.Warningfrom
        cell.l3.text = Date + detail.Warningdate

        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 70
    }
    
    

    
    
    
    @IBAction func warningAction(_ sender: Any) {
        
        
        view_warniong.removeFromSuperview()
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titleTitle = "Warning Letter"
        let detail = self.WarningList[indexPath.row]
        let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
        vc.titleValue = titleTitle
        vc.isWarning = "2"
        vc.warning_id = detail.Warningid
        navigationController?.pushViewController(vc, animated: true)
        
//        self.view.addSubview(view_warniong)
//        view_warniong.frame = CGRect(x: 0, y: appBarViewController.headerStackView.frame.origin.y + appBarViewController.headerStackView.frame.height, width: view.frame.width, height: view.frame.height)
//        let detail = self.WarningList[indexPath.row]
//
//
//        print("did", detail.Companyname)
//
//
//        print("did", detail.Warningid)
//
//
//        lbl_company.text = detail.Companyname
//
//
//        let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
//
//
//
//        self.WarningRead(userid: UseridStr, warningid: detail.Warningid)
        
    }
    
    
    
    
    private func Warninglist(userid : String){
        
        
        //        let url = URL(string: "http://hrsbhr.bestweb.my/api/memo/announcement_list")
        //        Alamofire.request(url!, method: .post, parameters: nil).responseJSON { response in
        //            print(response)
        //            print("res", response)
        
        
        ServiceHelper.sharedInstance.WarningList(userid: userid){ (result, error) in
            
            
            
            if let resultValue = result as? [String : Any]{
                
                print("resultValue", resultValue)
                
                
                let data = resultValue["data"] as! [[String: Any]]
                print("da", data)
                
                
                for dataItem in data{
                    self.WarningList.append(Warning(Warningid: dataItem["warning_id"] as! String, Warningto: dataItem["warning_to"] as! String, Warningfrom: dataItem["warning_from"] as! String, Companyname: dataItem["company_name"] as! String, Warningdate: dataItem["warning_date"] as! String, WarningType: dataItem["warning_type"] as! String, Subject: dataItem["subject"] as! String, Status: dataItem["status"] as! String))
                    self.tble.reloadData()
                    
                }
                self.centerTextMessage(text: "Domestic enquiries data not found", show: self.WarningList.count == 0)
            }
            
        }
        
        
        
    }
    


    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (70 * WarningList.count))
        
        
        
        
        
    }
    
    
    
    private func WarningRead(userid : String,warningid : String){
        
        
        ServiceHelper.sharedInstance.WarningRead(userid: userid, warningid: warningid){ (result, error) in
            
            
            
            if let resultValue = result as? [String : Any]{
                
                print("resultValue", resultValue)
                
                
                
                if let WarningBy = resultValue["warning_by"] as? String{
                    
                    
                    let WarningDate = resultValue["warning_date"] as? String
                    let WarningTo = resultValue["warning_to"] as? String

                    let status = resultValue["status"] as? String
                    let subject = resultValue["subject"] as? String
                    let warningtypeid = resultValue["warning_type_id"] as? String

                    
                   self.lbl_warningby.text = WarningBy
                    self.lbl_warningdate.text = WarningDate
                    self.lbl_warningto.text = WarningTo
                    
                    
                    self.lbl_status.text = status
                    self.lbl_subjcet.text = subject
                    self.lbl_warningtype.text = warningtypeid


                    
                
                }
                
                
            }
        }
        
        
    }
    

    
    
    

}
