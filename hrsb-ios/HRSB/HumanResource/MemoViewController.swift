//
//  MemoViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire

struct Memo {
    let summary: String
    let title: String
    let startdate: String
    let enddate: String
    let publishedFor: String
    let deptId: String
    let companyname: String
    let companyId: String
    let AnnouncementId: String
}

class MemoViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {
    var MemoList = [Memo]()
    @IBOutlet weak var download: UIButton!
    @IBOutlet weak var view_memoclose: UIButton!
    @IBOutlet var view_corner: UIView!
    @IBOutlet var view_memo: UIView!
    @IBOutlet weak var view_white: UIView!
    @IBOutlet weak var tble: UITableView!
    let reuseIdentifier = "MemoCell"
    var items = ["Letter of Appointment (LOA)", "Payslips","Code of Conduct (COC)","Memo","Grievance","Domestic inquiry"]

    var appBarViewController = MDCAppBarViewController()

    
    @IBOutlet weak var back: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tble.dataSource = self
        tble.delegate = self

//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
//
//
//        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//
//
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
        
//        let searchItemImage = UIImage(named: "bell.png")
//        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
//        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
//                                         style: .plain,
//                                         target: self,
//                                         action: #selector(self.NotifyAction))
//
//
//        let tuneItemImage = UIImage(named: "Bell1")
//        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
//        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
//
//        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
//        imageView.contentMode = .scaleAspectFit
//
//        let image = UIImage(named: "toplogo.png")
//        imageView.image = image
//
//        self.appBarViewController.headerStackView.addSubview(imageView)
//
//
//
//
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
//
//        addSlideMenuButton()
//        addSlideMenuButton1()
//
//
//
//
        
        
        let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
        self.memolist(userid : userID)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func back_Acton(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)

        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)

        
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.MemoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! MemoCell
        
        
        let detail = self.MemoList[indexPath.row]
        cell.l1.text = detail.title
        var string = "From : "
        var resultStr = string + detail.publishedFor
        cell.l2.text = string + detail.publishedFor
        cell.l3.text = "Date: \(detail.startdate)"

        
        
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titleTitle = "Memo"
        let detail = self.MemoList[indexPath.row]
        let vc = HRLetterApointmentPDFVC.instantiate(fromAppStoryboard: .main)
        vc.titleValue = titleTitle
        vc.announcementId = detail.AnnouncementId
        vc.isReadMemo = "1"
        navigationController?.pushViewController(vc, animated: true)

//        self.view.addSubview(view_memo)
//
//
//        view_memo.frame = CGRect(x: 0, y: appBarViewController.headerStackView.frame.origin.y + appBarViewController.headerStackView.frame.height, width: view.frame.width, height: view.frame.height)
//
//        self.view.addSubview(view_corner)
//
//        view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)

    }
    
    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
        //present(presentedVC, animated: false, completion: nil)
        
        
    }
    
  
    
    
    @IBAction func view_memocloseAction(_ sender: Any) {
        view_memo.removeFromSuperview()
    }
    
    @IBAction func download_Action(_ sender: Any) {
    }
    
    
    
    
    
    
    
    private func memolist(userid : String){
        ServiceHelper.sharedInstance.MemoList(userid: userid){ (result, error) in
            if let resultValue = result as? [String : Any]{
                let data = resultValue["data"] as! [[String: Any]]
                for dataItem in data{
                    print("summary", dataItem["summary"] as! String)
                    self.MemoList.append(Memo(summary: dataItem["summary"] as! String, title: dataItem["title"] as! String, startdate: dataItem["start_date"] as! String, enddate: dataItem["end_date"] as! String, publishedFor: dataItem["published_for"] as! String, deptId: dataItem["department_id"] as! String, companyname: dataItem["company_name"] as! String, companyId: dataItem["company_id"] as! String, AnnouncementId: dataItem["announcement_id"] as! String))
                     print("MemoList", self.MemoList)
                    self.tble.reloadData()
                }
            }
        }
    }
    
    
}
