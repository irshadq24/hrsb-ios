//
//  RecruitmentViewController.swift
//  HRSB
//
//  Created by BestWeb on 19/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
class RecruitmentViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    struct Job {
        
        let company: String
        let create: String
        let close: String
        let designation: String
        let id: String
        let vacancy: String
        let status: String
        let title: String
        let type: String
        let location: String
        let basic_salary: String?
        
    }


    let DesigArr = NSMutableArray()
    
    @IBOutlet weak var lbl_txtqualify: UILabel!
    
    @IBOutlet weak var lbl_qualifi: UILabel!
    @IBOutlet weak var lbl_txtrespon: UILabel!
    
    @IBOutlet weak var lbl_responsbties: UILabel!
    
    @IBOutlet weak var lbl_jobdesc: UILabel!
    @IBOutlet weak var lbl_designation: UILabel!
    
    @IBOutlet weak var views_one: UIView!
    
    @IBOutlet weak var lbl_compnyname: UILabel!
    
    
    @IBOutlet var black_view: UIView!
    
    @IBOutlet weak var lbl_address: UILabel!
    
    
    @IBOutlet weak var lbl_money: UILabel!
    
    @IBOutlet weak var lbl_exp: UILabel!
    
    
    
    @IBOutlet var tble_position: UITableView!
    
    
    @IBOutlet weak var scroll_jobApp: UIScrollView!
    
    @IBOutlet weak var closeJobApp: UIButton!
    
    
    
    @IBOutlet weak var year: UIButton!
    
    @IBOutlet weak var submit: UIButton!
    
    
    @IBOutlet weak var addpic: UIButton!
    
    @IBOutlet weak var day: UIButton!
    
    
    @IBOutlet weak var month: UIButton!
    
    
    
    @IBOutlet weak var closejobpost: UIButton!
    
    
    
    @IBOutlet var view_jobpostdtal: UIView!
    
    
    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet var view_jobapplicatn: UIView!
    
    @IBOutlet weak var position: UIButton!
    
    
    let reuseIdentifier = "RecruitmentCell"
    
    let reuseIdentifier1 = "PositionCell"

    
    @IBOutlet weak var scroll_jobpostdtail: UIScrollView!
    
    
    var items = ["Recruitment", "Recruitment", " Recruitment"]

    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var vie_corner: UIView!
    
    @IBOutlet weak var tble: UITableView!
    
    var appBarViewController = MDCAppBarViewController()

    
    @IBOutlet weak var plus: UIButton!
    
    
    var jobList = [Job]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        submit.layer.cornerRadius = 25
        
        
//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
//
//
//        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
//
//
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//
//
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
//
//        let searchItemImage = UIImage(named: "bell.png")
//        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
//        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
//                                         style: .plain,
//                                         target: self,
//                                         action: #selector(self.NotifyAction))
//
//
//        let tuneItemImage = UIImage(named: "Bell1")
//        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
//        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
//
//        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
//        imageView.contentMode = .scaleAspectFit
//
//        let image = UIImage(named: "toplogo.png")
//        imageView.image = image
//
//        self.appBarViewController.headerStackView.addSubview(imageView)
//
//
//
//
//        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
//
//

//
//
//        scroll_jobApp.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
//
//
//
//        addSlideMenuButton()
//        addSlideMenuButton1()

        tble.dataSource = self
        tble.delegate = self

        tble_position.dataSource = self
        tble_position.delegate = self

        self.Joblist()
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
//
//    func setheight(){
//
//
//        var heig = Float()
//
//        heig = Float(100 * items.count)
//
//        print(heig)
//
//
//
//        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
//
//
//        let Widht = self.view.frame.width
//
//
//
//        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width:Int(Widht) , height: (100 * jobList.count))
//
//
//
//
//
//    }

    
    
    
    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        
        
        navigationController?.pushViewController(presentedVC, animated: true)

      //  present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    @IBAction func Back_Action(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)

//
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        if tableView == tble{
        
        return self.jobList.count
    }
        else{
            
            
            return self.items.count
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tble{

        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! RecruitmentCell
        
        
        
        
            let detail = self.jobList[indexPath.row]
            cell.l1.text = detail.title
            cell.l2.text = detail.company
            cell.labelLocation.text = detail.location
            cell.labelSalary.text = detail.basic_salary
        
        return cell
        
    }
        else{

            let cell = self.tble_position.dequeueReusableCell(withIdentifier: self.reuseIdentifier1, for: indexPath) as! PositionCell





            cell.l1.text = items[indexPath.row]




            return cell


        }
        
        }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tble{
            return 120
        }
        else{
            return 60
        }
    }
    
    
    

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tble {
            let detail = self.jobList[indexPath.row]
            let vc = HRRecruitmentDetailVC.instantiate(fromAppStoryboard: .main)
            vc.id = detail.id
            navigationController?.pushViewController(vc, animated: true)
            
//            let detail = self.jobList[indexPath.row]
//            print("did", detail.id)
//            lbl_designation.text = detail.designation
//            lbl_compnyname.text = detail.company
//            self.JobRead(jobid: detail.id)
//            self.view.addSubview(view_jobpostdtal)
//            view_jobpostdtal.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)
//       }
//       else{
            
        }
    }
    
    
    @IBAction func plus_btn(_ sender: Any) {
        
        
        self.view.addSubview(view_jobapplicatn)
        
        
        view_jobapplicatn.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: self.view.frame.height)

        
        
    }
    
    
    
    
    
    

    
    
    @IBAction func closejobpost_Action(_ sender: Any) {
        
        
        view_jobpostdtal.removeFromSuperview()
        
    }
    
    
    
    
    
    
    @IBAction func ClosejobApp_btn(_ sender: Any) {
        
        
        view_jobapplicatn.removeFromSuperview()

        
    }
    
    
    
    
    
    
    
    @IBAction func position_action(_ sender: Any) {
        
        black_view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        
        view.addSubview(black_view)
        
        black_view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)

        
        
        view.addSubview(tble_position)
        
        tble_position.frame = CGRect (x: 20, y: 100, width: view.frame.width - 40, height: 250)
        
        
    }
    
    
    
    
    @IBAction func year_Action(_ sender: Any) {
    }
    
    
    
    @IBAction func month_Action(_ sender: Any) {
    }
    
    @IBAction func Day_Action(_ sender: Any) {
    }
    

    
    func Joblist(){
    
    
        
                
        let url = URL(string: "http://18.139.70.23/api/job/job_list")
        Alamofire.request(url!, method: .post, parameters: nil).responseJSON { response in
            print(response)
            print("res", response)
            if let locationJSON = response.result.value as? [String: Any] {
                let data = locationJSON["data"] as! [[String: Any]]
                print("da", data)
                
                for dataItem in data {
                    print("company", dataItem["company"] as! String)
                    print("status", dataItem["status"] as! String)
                    print("designation", dataItem["designation"] as! String)
                    print("job_vacancy", dataItem["job_vacancy"] as! String)
                    
                    self.jobList.append(Job(company: dataItem["company"] as! String, create: dataItem["created_at"] as! String, close: dataItem["date_of_closing"] as! String, designation: dataItem["designation"] as! String, id: dataItem["id"] as! String, vacancy: dataItem["job_vacancy"] as! String, status: dataItem["status"] as! String, title: dataItem["title"] as! String, type: dataItem["type"] as! String, location: dataItem["location"] as! String, basic_salary: dataItem["basic_salary"] as? String))
                    
                    print("joblist", self.jobList)
                    self.tble.reloadData()


            
                }
            }
        }
            
            
    

            
    }
    
    private func JobRead(jobid : String){
        
        
        
        ServiceHelper.sharedInstance.HumanJobRead(jobid: jobid){ (result, error) in
            
//            if let resultValue = result as? [String : Any]{
//
//
//                //    print("failed",resultValue)
//
//                let status = resultValue["status"] as! String
//
//
//                if let resultdata = resultValue["data"]  as? [[String : Any]]{
//
//                    print("datt", resultdata)

        
        if let resultValue = result as? [String : Any]{
            
            print("resultValue", resultValue)
            
            if let resultValue = result as? [String : Any]{
                
                if let dat = resultValue["data"] as?  [String : Any] {
                    print("dat", dat)

                    if let token = dat["job_title"] as? String{
                        
                        
                        let respo = dat["short_description"] as? String
                        
                        print("respo", respo)
                        
                        let quali = dat["long_description"] as? String

                        
                        
                        self.lbl_txtqualify.text = quali

                        
                        self.lbl_txtrespon.text = respo

                        self.lbl_exp.text = token
                        
                        self.views_one.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150)
                        self.lbl_jobdesc.frame = CGRect(x: 20, y: self.views_one.frame.origin.y + self.views_one.frame.height + 20, width: self.view.frame.width - 40, height: 16)
                        
                        
                        
                        
                        self.lbl_responsbties.frame = CGRect(x: 20, y: self.lbl_jobdesc.frame.origin.y + self.lbl_jobdesc.frame.height + 10, width: self.view.frame.width - 40, height: 16)
                        
                        
                        self.lbl_txtrespon.frame = CGRect(x: 20, y: self.lbl_responsbties.frame.origin.y + self.lbl_responsbties.frame.height + 10, width: self.view.frame.width - 40, height: 50)
                        
                        
                        self.lbl_qualifi.frame = CGRect(x: 20, y: self.lbl_txtrespon.frame.origin.y + self.lbl_txtrespon.frame.height + 20, width: self.view.frame.width - 40, height: 16)
                        
                        
                        //
                        //            lbl_txtqualify.numberOfLines = 0
                        //            lbl_txtqualify.lineBreakMode = NSLineBreakMode.byWordWrapping
                        //
                        //
                        //            lbl_txtqualify.sizeToFit()
                        
                        
                        self.lbl_txtqualify.frame = CGRect(x: 20, y: self.lbl_qualifi.frame.origin.y + self.lbl_qualifi.frame.height + 10, width: self.view.frame.width - 40, height: 250)
                        
                        
                        
                        
                        
                        
                        
                        
                        let pos = self.lbl_txtqualify.frame.origin.y

                        let height = self.lbl_txtqualify.frame.size.height
                        let sizeOfContent = height + pos + 200


                        print("dsfdf",sizeOfContent)

                        self.scroll_jobpostdtail.contentSize.height = sizeOfContent


                        self.scroll_jobpostdtail.contentSize.width = self.view.frame.width
                        

                       // self.scroll_jobpostdtail.contentSize = CGSize(width: self.view.frame.width, height: 1100)
                        

                        
                        
                    }

                }
            }
            
            }
        }
            
        
    }

    
    
    
}






