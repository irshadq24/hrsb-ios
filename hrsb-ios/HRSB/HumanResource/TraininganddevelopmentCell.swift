//
//  TraininganddevelopmentCell.swift
//  HRSB
//
//  Created by BestWeb on 19/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TraininganddevelopmentCell: UITableViewCell {

    
    
    
    @IBOutlet weak var join: UIButton!
    @IBOutlet weak var l1: UILabel!
    @IBOutlet weak var l_date: UILabel!
    @IBOutlet weak var l_dtal: UILabel!
    @IBOutlet weak var buttonView: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
       setup()
    }

    func setup() {
        join.makeRoundCorner(20)
        join.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
        buttonView.makeRoundCorner(20)
        buttonView.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
