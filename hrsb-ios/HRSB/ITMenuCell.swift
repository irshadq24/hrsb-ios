//
//  ITMenuCell.swift
//  HRSB
//
//  Created by Salman on 01/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ITMenuCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var img: UIImageView!
}
