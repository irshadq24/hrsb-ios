//
//  ITMenuVC.swift
//  
//
//  Created by Salman on 01/09/19.
//

import UIKit

class ITMenuVC: UIViewController {
    
    @IBOutlet weak var tbleview: UITableView!
    var arrImg = ["ticket_icon","asset_icon","knowledge_base_icon"]
    var arrLbl = ["Ticket","Asset","Knowledge Base"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if getUserDefault(Constant.UserDefaultsKey.depId) != "5" {
            arrImg.remove(at: 1)
            arrLbl.remove(at: 1)
        }
        tbleview.reloadData()
    }
   
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

//UITableViewDelegate

extension ITMenuVC: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLbl.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ITMenuCell", for: indexPath) as! ITMenuCell
        cell.lblName.text = arrLbl[indexPath.row]
        cell.img.image = UIImage(named: arrImg[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ListTicketViewController")as! ListTicketViewController
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 1 {
            let asset = storyboard?.instantiateViewController(withIdentifier: "AssetVC") as! AssetVC
            navigationController?.pushViewController(asset, animated: true)
        }
        if indexPath.row == 2 {
            let knoledge = storyboard?.instantiateViewController(withIdentifier: "KnowledgeBaseVC") as! KnowledgeBaseVC
            navigationController?.pushViewController(knoledge, animated: true)
        }
        
    }
}
