//
//  IncidentListingFormVC.swift
//  HRSB
//
//  Created by Zakir Khan on 18/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class IncidentListingFormVC: UIViewController {

    @IBOutlet weak var tableViewListingForm: UITableView!
    var arrIncidentListingForm = ["Incident Listing Summary - General", "Incident Listing Summary - Subsidiaries"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setup()
    }
    
    func setup() {
        tableViewListingForm.delegate = self
        tableViewListingForm.dataSource = self
        tableViewListingForm.reloadData()
    }
    
}

extension IncidentListingFormVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrIncidentListingForm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "QualityDocCell", for: indexPath)as? QualityDocCell else {
            return UITableViewCell()
        }
        cell.l1.text = arrIncidentListingForm[indexPath.row]
        return cell
    }
}
extension IncidentListingFormVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = IncidentListingGeneralVC.instantiate(fromAppStoryboard: .main)
            vc.isGeneral = "Incident Listing Summary - General"
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = IncidentListingGeneralVC.instantiate(fromAppStoryboard: .main)
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension IncidentListingFormVC {
    @IBAction func buttonActionBack(_ sender: UIButton){
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
}
