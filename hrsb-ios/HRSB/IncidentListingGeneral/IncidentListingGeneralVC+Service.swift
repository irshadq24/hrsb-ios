//
//  IncidentListingGeneralVC+Service.swift
//  HRSB
//
//  Created by Zakir Khan on 18/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension IncidentListingGeneralVC {
    func requestServer(param: [String: Any]?) {
        var endPoint = ""
        if isGeneral == "Incident Listing Summary - General" {
            endPoint = "hseq/incident_general"
        } else if manListing == "Man Hours Listing Summary" {
            endPoint = "hseq/manhour_listing"
        } else {
            endPoint = "hseq/incident_type_sub"
        }
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    if isGeneral == "Incident Listing Summary - General" {
                        self.incidentSummary = try decoder.decode(IncidentSummary.self, from: data)
                        if let data =  incidentSummary?.getRowColumn() {
                            var rowCol = data
                            self.header = rowCol[0]
                            rowCol.remove(at: 0)
                            self.data = rowCol
                            spreadsheetView.reloadData()
                            Console.log("IncidentSummary:- \(self.incidentSummary)")
                        }
                    } else if manListing == "Man Hours Listing Summary" {
                        self.manHoursListing = try decoder.decode(ManHoursListing.self, from: data)
                        if let data =  manHoursListing?.getRowColumn() {
                            var rowCol = data
                            self.header = rowCol[0]
                            rowCol.remove(at: 0)
                            self.data = rowCol
                            spreadsheetView.reloadData()
                            Console.log("ManHoursListing:- \(self.manHoursListing)")
                        }
                    } else {
                        self.subsidiariesListing = try decoder.decode(SubsidiariesListing.self, from: data)
                        if let data =  subsidiariesListing?.getRowColumn() {
                            var rowCol = data
                            self.header = rowCol[0]
                            rowCol.remove(at: 0)
                            self.data = rowCol
                            spreadsheetView.reloadData()
                            Console.log("SubsidiariesListing:- \(self.subsidiariesListing)")
                        }
                    }
                    
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }

    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
