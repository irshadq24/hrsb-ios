//
//  IncidentListingGeneralVC+TableView.swift
//  HRSB
//
//  Created by Zakir Khan on 19/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import SpreadsheetView

extension IncidentListingGeneralVC:  SpreadsheetViewDataSource , SpreadsheetViewDelegate {
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return header.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1 + data.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        return 140
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 60
        } else {
            return 44
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCell.self), for: indexPath) as! HeaderCell
            cell.label.text = header[indexPath.column]
            cell.label.numberOfLines = 0
            cell.label.textAlignment = .center
//            if case indexPath.column = sortedColumn.column {
//                cell.sortArrow.text = sortedColumn.sorting.symbol
//            } else {
//                cell.sortArrow.text = ""
//            }
            cell.setNeedsLayout()
            
            return cell
        } else {
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
            cell.label.text = data[indexPath.row - 1][indexPath.column]
            cell.label.numberOfLines = 0
            cell.label.textAlignment = .center
            return cell
        }
    }
    
    /// Delegate
    
//    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
//        if case 0 = indexPath.row {
//            if sortedColumn.column == indexPath.column {
//                sortedColumn.sorting = sortedColumn.sorting == .ascending ? .descending : .ascending
//            } else {
//                sortedColumn = (indexPath.column, .ascending)
//            }
//            data.sort {
//                let ascending = $0[sortedColumn.column] < $1[sortedColumn.column]
//                return sortedColumn.sorting == .ascending ? ascending : !ascending
//            }
//            spreadsheetView.reloadData()
//        }
//    }
}
