//
//  IncidentListingGeneralVC.swift
//  HRSB
//
//  Created by Zakir Khan on 18/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown
import SpreadsheetView

class IncidentListingGeneralVC: UIViewController {

    @IBOutlet weak var spreadsheetView: SpreadsheetView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonMonth: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    var isGeneral = ""
    var manListing = ""
    //var isSubsidiaries = ""
    var manHoursListing: ManHoursListing?
    var subsidiariesListing: SubsidiariesListing?
    var incidentSummary: IncidentSummary?
    let chooseMonthFrom = DropDown()
    let chooseYearFrom = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseMonthFrom,
            self.chooseYearFrom
        ]
    }()
    
    
    
    var header = [String]()
    var data = [[String]]()
    
    enum Sorting {
        case ascending
        case descending
        
        var symbol: String {
            switch self {
            case .ascending:
                return "\u{25B2}"
            case .descending:
                return "\u{25BC}"
            }
        }
    }
    var sortedColumn = (column: 0, sorting: Sorting.ascending)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self
        spreadsheetView.register(HeaderCell.self, forCellWithReuseIdentifier: String(describing: HeaderCell.self))
        spreadsheetView.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
//        let data = try! String(contentsOf: Bundle.main.url(forResource: "data", withExtension: "tsv")!, encoding: .utf8)
//            .components(separatedBy: "\r\n")
//            .map { $0.components(separatedBy: "\t") }
//        Console.log("Data:- \(data)")
        
        spreadsheetView.allowsSelection = false
        setup()
        setupDropDowns()
    }
    
    func setup() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-yyyy"
        let result = formatter.string(from: date)
        if isGeneral == "Incident Listing Summary - General" {
            labelTitle.text = isGeneral
            if let date = result.getDateInstance(validFormat: "MM-yyyy") {
                buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
                buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
            }
             requestServer(param: ["date": "\(buttonYear.titleLabel?.text! ?? "")-\(buttonMonth.titleLabel?.text! ?? "")"])
        } else if manListing == "Man Hours Listing Summary"{
            labelTitle.text = manListing
            if let date = result.getDateInstance(validFormat: "MM-yyyy") {
                buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
                buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
            }
            requestServer(param: ["date": "\(buttonYear.titleLabel?.text! ?? "")-\(buttonMonth.titleLabel?.text! ?? "")"])
        } else {
            labelTitle.text = "Incident Listing Summary - Subsidiaries"
            if let date = result.getDateInstance(validFormat: "MM-yyyy") {
                buttonMonth.setTitle(date.toString(dateFormat: "MM"), for: .normal)
                buttonYear.setTitle(date.toString(dateFormat: "yyyy"), for: .normal)
            }
            requestServer(param: ["date": "\(buttonYear.titleLabel?.text! ?? "")-\(buttonMonth.titleLabel?.text! ?? "")"])
        }
    }
    
    func reloadDataBackend(){
        var date = "\(buttonMonth.titleLabel?.text! ?? "")"
        if date.count == 1 {
            date = "0\(date)"
        }
        if isGeneral == "Incident Listing Summary - General" {
            requestServer(param: ["date": "\(buttonYear.titleLabel?.text! ?? "")-\(date)"])
        } else if manListing == "Man Hours Listing Summary"{
            requestServer(param: ["date":  "\(buttonYear.titleLabel?.text! ?? "")-\(date)"])
        } else {
            requestServer(param: ["date":  "\(buttonYear.titleLabel?.text! ?? "")-\(date)"])
        }
    }
    
    
    func setupDropDowns() {
        setupChooseMonthFromDropDown()
        setupChooseYearFromDropDown()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        spreadsheetView.flashScrollIndicators()
    }
    
    func setupChooseMonthFromDropDown() {
        var arrayMonth = [String]()
        for month in 1...12{
            arrayMonth.append("\(month)")
        }
        chooseMonthFrom.anchorView = buttonMonth
        chooseMonthFrom.bottomOffset = CGPoint(x: 0, y: buttonMonth.bounds.height)
        chooseMonthFrom.dataSource = arrayMonth
        // Action triggered on selection
        chooseMonthFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonMonth.setTitle(item, for: .normal)
            self?.reloadDataBackend()
        }
    }
    func setupChooseYearFromDropDown() {
        var arrayYear = [String]()
        for year in 2019...2025 {
            arrayYear.append("\(year)")
        }
        chooseYearFrom.anchorView = buttonYear
        chooseYearFrom.bottomOffset = CGPoint(x: 0, y: buttonYear.bounds.height)
        chooseYearFrom.dataSource = arrayYear
        // Action triggered on selection
        chooseYearFrom.selectionAction = { [weak self] (index, item) in
            self?.buttonYear.setTitle(item, for: .normal)
            self?.reloadDataBackend()
        }
    }
    
}

extension IncidentListingGeneralVC {
    @IBAction func buttonActionBack(_ sender: UIButton){
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionMonth(_ sender: UIButton){
        Console.log("buttonActionMonth")
        chooseMonthFrom.show()
    }
    
    @IBAction func buttonActionYear(_ sender: UIButton){
        Console.log("buttonActionYear")
        chooseYearFrom.show()
    }
    
    
    
}



