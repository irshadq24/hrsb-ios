//
//  AddAuditScreenVC.swift
//  HRSB
//
//  Created by Salman on 23/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddAuditInput {
    var cellType = ""
    var apiKey = ""
    var apiValue = ""
    var pickerDatasource = [String]()
    var placeholder = ""
    init(type: String, pickerDatasource: [String] = [], apiKey: String, apiValue: String = "", placeholder: String = "") {
        self.cellType = type
        self.apiKey = apiKey
        self.apiValue = apiValue
        self.pickerDatasource = pickerDatasource
        self.placeholder = placeholder
    }
}


class AddAuditScreenVC: UIViewController {

    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnAddAttchment: UIButton!
    var arrModel = [AddAuditInput]()
    var auditTitle = [[String: Any]]()
    var auditStandard = [[String: Any]]()
    var auditeeName = [[String: Any]]()
    let picker = SGImagePicker(enableEditing: false)
    var selectedImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSubmit.makeRoundCorner(25)
        fetchAuditTitle()
        fetchAuditDropdown()
        buildData()
    }
    
    
    @IBAction func tapAddAttachment(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
        }
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        var params = [String: String]()
        
        print(title)
        for data in arrModel {
            if data.apiValue == "" {
                print(data.apiKey+"--"+data.apiValue)
                self.showAlert(title: "", message: "Please enter all fields")
                return
            }
            params[data.apiKey] = data.apiValue
           
        }
        let title = self.auditTitle.filter { (data) -> Bool in
            return (data["audit_title"] as? String ?? "") == params["audit_title"]
        }
        
//        let findings = params["findings"]
//        let remarks = params["remarks"]
//        let arr = findings?.split(separator: "_")
//        if arr?.count > 0 {
//             params["findings"] = String(arr![0])
//        }
//        if arr!.count > 1 {
//
//        }
        
        
        
        
    }
    
    func buildData() {
        arrModel.append(AddAuditInput(type: "input", apiKey: "audit_id", placeholder: "Audit Title"))
        arrModel.append(AddAuditInput(type: "detail", apiKey: "ref_no", apiValue: "4", placeholder: "Reference Number"))
        arrModel.append(AddAuditInput(type: "detail", apiKey: "date_issue", apiValue: Date().toString(dateFormat: "YYYY-mm-dd"), placeholder: "Date of Issue"))
        arrModel.append(AddAuditInput(type: "input", pickerDatasource: [], apiKey: "standard_id", placeholder: "Standard"))
        arrModel.append(AddAuditInput(type: "input", apiKey: "clause_no", placeholder: "Clause number"))
        arrModel.append(AddAuditInput(type: "input", apiKey: "process", placeholder: "Process"))
        arrModel.append(AddAuditInput(type: "input", apiKey: "requirement", placeholder: "Requirement"))
        arrModel.append(AddAuditInput(type: "input", pickerDatasource: [], apiKey: "auditee", placeholder: "Auditee"))
        arrModel.append(AddAuditInput(type: "input", pickerDatasource: ["Accept","Reject"], apiKey: "status", placeholder: "Select status for this project"))
        arrModel.append(AddAuditInput(type:"radio", apiKey: "type_of_finding"))
        arrModel.append(AddAuditInput(type:"textView", apiKey: "findings"))
        arrModel.append(AddAuditInput(type:"textView", apiKey: "remarks"))
    }
    
    func fetchAuditee(auditId: String) {
        let endPoint = MethodName.getAuditeeList
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["audit_id": auditId, "type": "auditee_drop_down_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let list = dict["data"] as? [[String: Any]] {
                    self?.auditeeName = list
                    var title = [String]()
                    for src in list {
                        title.append(src["employee_name"] as? String ?? "")
                    }
                    self?.arrModel[8].pickerDatasource = title
                    self?.tblView.reloadData()
                    
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func fetchAuditTitle() {
        let endPoint = MethodName.getAuditTitle
        
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["user_id":getUserDefault(Constant.UserDefaultsKey.UserId), "type": "audit_title_dropdown"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let list = dict["data"] as? [[String: Any]] {
                    self?.auditTitle = list
                    var title = [String]()
                    for src in list {
                        title.append(src["audit_title"] as? String ?? "")
                    }
                    self?.arrModel[0].pickerDatasource = title
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func fetchAuditDropdown() {
        let endPoint = MethodName.getAuditDropdown
        
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["user_id":getUserDefault(Constant.UserDefaultsKey.UserId), "type": "audit_finding_dropdown_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let list = dict["standard"] as? [[String: Any]] {
                    self?.auditStandard = list
                    var title = [String]()
                    for src in list {
                        title.append(src["standard_name"] as? String ?? "")
                    }
                    self?.arrModel[3].pickerDatasource = title
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddAuditScreenVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrModel[indexPath.row].cellType == "input" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInput", for: indexPath) as! CellInput
            cell.info = arrModel[indexPath.row]
            return cell
        } else if arrModel[indexPath.row].cellType == "radio" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellRadio", for: indexPath) as! CellRadio
            cell.info = arrModel[indexPath.row]
            return cell
        }
        else if arrModel[indexPath.row].cellType == "detail" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellReportDetail", for: indexPath)
            let lblTitle = cell.viewWithTag(120) as! UILabel
            let lblDescription = cell.viewWithTag(121) as! UILabel
            lblTitle.text = arrModel[indexPath.row].placeholder
            lblDescription.text = arrModel[indexPath.row].apiValue
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTextView", for: indexPath) as! CellTextView
            cell.info = arrModel[indexPath.row]
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrModel[indexPath.row].cellType == "input" {
            return 50
        } else if arrModel[indexPath.row].cellType == "detail" {
            return 70
        } else if arrModel[indexPath.row].cellType == "radio" {
            return 250
        } else {
            return 250
        }
        
    }
}
