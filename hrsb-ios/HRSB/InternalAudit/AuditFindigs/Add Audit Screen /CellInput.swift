//
//  CellInput.swift
//  HRSB
//
//  Created by Salman on 23/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class CellInput: UITableViewCell {
    
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var viewPicker: UIView!
    
    let dropDown = DropDown()
    
    override func awakeFromNib() {
        txtInput.addTarget(self, action: #selector(self.editingDidChange(_:)), for: .editingChanged)
    }
    
    var info: AddAuditInput? {
        didSet {
            txtInput.text = info?.apiValue
            txtInput.placeholder = info?.placeholder
            
            if info!.pickerDatasource.count > 0 {
                txtInput.isHidden = true
                viewPicker.isHidden = false
                dropDown.dataSource = info?.pickerDatasource ?? []
                dropDown.anchorView = btnSelect
                dropDown.bottomOffset = CGPoint(x: 0, y: btnSelect.bounds.height)
                dropDown.selectionAction = { [weak self] (index, item) in
                    self?.info?.apiValue = item
                    self?.btnSelect.setTitle(item, for: .normal)
                    self?.btnSelect.setTitleColor(UIColor.black, for: .normal)
                }
            }else {
                viewPicker.isHidden = true
                txtInput.isHidden = false
            }
        }
    }
    
    @objc func editingDidChange(_ sender: UITextField) {
        self.info?.placeholder = sender.text ?? ""
        self.info?.apiValue = sender.text ?? ""
    }
    @IBAction func tapSelect(_ sender: Any) {
        dropDown.show()
    }
}
