//
//  CellRadio.swift
//  HRSB
//
//  Created by Salman on 23/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellRadio: UITableViewCell {

    @IBOutlet var imgViewSelet: [UIImageView]!
    @IBOutlet var btnRadio: [UIButton]!
    @IBOutlet var lblItems: [UILabel]!
    
    
    
    var info: AddAuditInput? {
        didSet {
            
        }
    }
    
    @IBAction func tapRadio(_ sender: UIButton) {
        for btn in btnRadio {
            btn.isSelected = false
        }
        for img in imgViewSelet {
            img.image = UIImage(named: "unselect")
        }
        sender.isSelected = true
        imgViewSelet[sender.tag].image = UIImage(named: "select")
        let txt = lblItems[sender.tag].text ?? ""
        let string = txt.replacingOccurrences(of: " ", with: "_")
        let smallStr = string.lowercased()
        print(smallStr)
    }
    
    
}
