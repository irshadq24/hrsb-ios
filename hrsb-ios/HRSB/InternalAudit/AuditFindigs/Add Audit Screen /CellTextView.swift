//
//  CellTextView.swift
//  HRSB
//
//  Created by Salman on 23/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellTextView: UITableViewCell {
    @IBOutlet weak var txtViewFindings: UITextView!
    @IBOutlet weak var txtViewCause: UITextView!
    
    var info: AddAuditInput? {
        didSet {
            txtViewCause.makeRoundCorner(10)
            txtViewFindings.makeRoundCorner(10)
            txtViewCause.makeBorder(1, color: .black)
            txtViewFindings.makeBorder(1, color: .black)
            txtViewCause.delegate = self
            txtViewFindings.delegate = self
            let txt = info?.apiValue ?? ""
            let arr = txt.split(separator: "_")
            if arr.count > 0 {
                txtViewFindings.text = String(arr[0])
            }
            if arr.count > 1 {
                txtViewCause.text = String(arr[1])
            }
        }
    }
    
}


extension CellTextView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        info?.apiValue = txtViewFindings.text!+" _ "+txtViewCause.text!
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtViewCause {
            if textView.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                txtViewCause.text = "Root Cause (s)"
            }
        }
        if textView == txtViewFindings {
            if textView.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                txtViewFindings.text = "Findings (s)"
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtViewCause {
            if textView.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "Root Cause (s)" {
                txtViewCause.text = ""
            }
        }
        if textView == txtViewFindings {
            if textView.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "Findings (s)" {
                txtViewFindings.text = ""
            }
        }
    }
    
}
