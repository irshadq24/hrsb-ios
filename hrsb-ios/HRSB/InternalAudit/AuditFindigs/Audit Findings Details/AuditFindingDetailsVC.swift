//
//  AuditFindingDetailsVC.swift
//  HRSB
//
//  Created by Salman on 22/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AuditFindingDetailsVC: UIViewController {

    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var viewUploadAttachment: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    let picker = SGImagePicker(enableEditing: false)
    var audit: AuditFindingModel?
    var arrModel = [ReportModel]()
    var isForDetail = true
    var details = [String: Any]()
    var arrAttachment = [[String: Any]]()
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        fetchAuditDetail()
        getAuditAttachment()
    }
    
    func getAuditAttachment() {
        let endPoint = MethodName.getAttachmentDetails
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["audit_finding_id": audit?.auditFindingId ?? "", "type":"audit_attachment_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                print(data)
                if let detail = data as? [String: Any], let info = detail["data"] as? [[String: Any]] {
                    self?.arrAttachment = info
                }
                self?.tblView.reloadData()
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func fetchAuditDetail() {
        lblTitle.text = audit?.auditTitle ?? ""
        lblDate.text = audit?.dateIssue ?? ""
        
        let endPoint = MethodName.getAuditDetails
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["audit_finding_id": audit?.auditFindingId ?? "", "type":"get_audit_finding_details"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
               print(data)
               if let detail = data as? [String: Any], let info = detail["data"] as? [String: Any] {
                    self?.setupModel(info: info, forDetail: true)
               }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func setupModel(info: [String: Any], forDetail: Bool = false) {
        isForDetail = forDetail
        details = info
        viewUploadAttachment.isHidden = true
        if forDetail {
            arrModel.append(ReportModel(info: ["title": "Reference Number", "description": info["ref_no"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Standard", "description": info["standard_name"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Clause Number", "description": info["edit_process"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Process", "description": info["edit_process"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Requirement", "description": info["requirement"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Type of Findings", "description": info["type_of_finding"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Auditee", "description": info["auditee_name"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Findings(s)", "description": info["findings"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Root Cause(s)", "description": info["root_cause"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Corrections", "description": info["correction"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Corrective Actions", "description": info["correction_action"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Remark (Optional)", "description": info["remarks"] as? String ?? ""]))
           
            arrModel.append(ReportModel(info: ["title": "Attachment", "description": info["attachment_file"] as? String ?? ""]))
        } else {
            viewUploadAttachment.isHidden = false
        }
        tblView.reloadData()
    }
    

    @IBAction func tapAddAttachment(_ sender: UIButton) {
        self.showAlertWithActions(msg: "", titles: ["Camera", "Gallery", "Cancel"]) { (index) in
            if index == 1 {
                self.picker.getImage(from: .camera, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
            if index == 2 {
                self.picker.getImage(from: .library, completion: { (image) in
                    sender.imageView?.makeRoundCorner(5)
                    sender.setImage(image, for: .normal)
                    self.selectedImage = image!
                    sender.imageView?.contentMode = .scaleAspectFill
                })
            }
        }
    }
    
    @IBAction func tapSaveAttachment(_ sender: Any) {
        if selectedImage == nil {
            self.showAlert(title: "", message: "Please select image.")
            return
        }
        if txtTitle.text == "" || txtDescription.text == "" {
            self.showAlert(title: "", message: "Please enter details.")
            return
        }
        Loader.show()
        let params = ["type":"add_audit_attachment", "title":txtTitle.text!, "description":txtDescription.text!, "audit_finding_id":audit?.auditFindingId ?? "", "user_id":getUserDefault("UserId")]
        
        let endPoint = MethodName.addAuditAttachment
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["attachment_file": self.selectedImage!], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.txtTitle.text = ""
                    self.txtDescription.text = ""
                    self.selectedImage = nil
                    
                    self.getAuditAttachment()
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDetailAttachment(_ sender: UIButton) {
        btnDetails.backgroundColor = UIColor.white
        btnAttachment.backgroundColor = UIColor.white
        btnAttachment.setTitleColor(UIColor.getRGBColor(104, g: 56, b: 148), for: .normal)
        btnDetails.setTitleColor(UIColor.getRGBColor(104, g: 56, b: 148), for: .normal)
        sender.backgroundColor = UIColor.getRGBColor(104, g: 56, b: 148)
        sender.setTitleColor(.white, for: .normal)
        isForDetail = !isForDetail
        if isForDetail {
            setupModel(info: ["":""], forDetail: true)
        }else {
            setupModel(info: ["":""], forDetail: false)
        }
    }
    
    
    
    
}

extension AuditFindingDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isForDetail {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellReportDetail", for: indexPath)
            let lblTitle = cell.viewWithTag(120) as! UILabel
            let lblDescription = cell.viewWithTag(121) as! UILabel
            lblTitle.text = arrModel[indexPath.row].title
            lblDescription.text = arrModel[indexPath.row].description
            if arrModel.count-1 == indexPath.row {
                lblDescription.textColor = .blue
            }else {
                lblDescription.textColor = .black
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellAttachment", for: indexPath) as! CellAttachment
            cell.info = self.arrAttachment[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForDetail {
            return arrModel.count
        }
        return self.arrAttachment.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrModel.count-1 == indexPath.row && isForDetail {
            if let url = URL(string: details["url_path"] as? String ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}
