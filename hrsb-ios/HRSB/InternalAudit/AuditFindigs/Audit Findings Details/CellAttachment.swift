//
//  CellAttachment.swift
//  HRSB
//
//  Created by Salman on 23/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class CellAttachment: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    var info: [String: Any]? {
        didSet {
            lblTitle.text = info?["title"] as? String ?? ""
            lblDate.text = info?["description"] as? String ?? ""
        }
    }

}
