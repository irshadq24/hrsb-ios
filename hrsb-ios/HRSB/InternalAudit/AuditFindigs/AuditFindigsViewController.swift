//
//  AuditFindigsViewController.swift
//  HRSB
//
//  Created by Javed Multani on 15/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit


class AuditFindingModel {
    var auditId = ""
    var auditTitle = ""
    var auditFindingId = ""
    var dateIssue =  ""
    var empName = ""
    var status = ""
    var employeeRole = ""
    var employeeId = ""
    var refNo = ""
    
    init(info: [String: Any]) {
        auditId = info["audit_id"] as? String ?? ""
        auditFindingId = info["audit_finding_id"] as? String ?? ""
        auditTitle = info["audit_title"] as? String ?? ""
        dateIssue = info["date_issue"] as? String ?? ""
        empName = info["employee_name"] as? String ?? ""
        employeeRole = info["employee_role"] as? String ?? ""
        employeeId = info["employee_id"] as? String ?? ""
        status = info["status"] as? String ?? ""
        refNo = info["ref_no"] as? String ?? ""
    }
}


class AuditFindigsViewController: UIViewController {
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var arrAudit = [AuditFindingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAudit()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapAdd(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddAuditScreenVC") as! AddAuditScreenVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchAudit() {
        let endPoint = MethodName.getAuditFindingList
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["user_id": getUserDefault("UserId"), "type":"audit_finding_list","pagination":"0"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let assets = dict["data"] as? [[String: Any]] {
                    self?.arrAudit.removeAll()
                    for asset in assets {
                        self?.arrAudit.append(AuditFindingModel(info: asset))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
}


extension AuditFindigsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAuditFinding", for: indexPath) as! CellAuditFinding
        cell.info = arrAudit[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAudit.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AuditFindingDetailsVC") as! AuditFindingDetailsVC
        vc.audit = arrAudit[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
