//
//  CellAuditFinding.swift
//  HRSB
//
//  Created by Salman on 22/11/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit



class CellAuditFinding: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuditee: UILabel!
    @IBOutlet weak var lblDateIssue: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRefNo: UILabel!
    
    var info: AuditFindingModel? {
        didSet {
            lblAuditee.text = "Auditee: \(info?.empName ?? "")"
            lblTitle.text = info?.auditTitle ?? ""
            lblStatus.text = "Status: \(info?.status ?? "")"
            lblRefNo.text = "Ref. Number: \(info?.refNo ?? "")"
            lblDateIssue.text = "Date Issue: \(info?.dateIssue ?? "")"
        }
    }
    
    
}
