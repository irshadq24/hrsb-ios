//
//  AuditFindigsReportViewController.swift
//  HRSB
//
//  Created by Javed Multani on 15/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON
import PieCharts
import DropDown

class AuditFindigsReportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PieChartDelegate {
    func onSelected(slice: PieSlice, selected: Bool) {
        
    }
    var arrayChart1:[ObjAuditReportChart1] = [ObjAuditReportChart1]()
    var arrayChart2:[ObjAuditReportChart2] = [ObjAuditReportChart2]()
    
    var sortArray:[ObjAuditSort] = [ObjAuditSort]()
    var arrayReport:[ObjAuditReport] = [ObjAuditReport]()
    public var arrayColor:[UIColor] = [UIColor]()
    @IBOutlet weak var auditTableView: UITableView!
    
    let chooseSort =  DropDown()
    
    var order_by_field = ""
    var order_by_value = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayColor.append(UIColor(red:0.45, green:0.61, blue:0.80, alpha:1.0))
        arrayColor.append(UIColor(red:0.83, green:0.53, blue:0.32, alpha:1.0))
        arrayColor.append(UIColor(red:0.53, green:0.53, blue:0.53, alpha:1.0))
        arrayColor.append(UIColor(red:0.94, green:0.76, blue:0.36, alpha:1.0))
        
        
        auditTableView.delegate = self
        auditTableView.dataSource = self
        
        auditTableView.register(UINib(nibName: "ReportTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportTitleTableViewCell")
        auditTableView.register(UINib(nibName: "ReportHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportHeaderTableViewCell")
        auditTableView.register(UINib(nibName: "ReportDataTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportDataTableViewCell")
        auditTableView.register(UINib(nibName: "ReportAuditChartTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportAuditChartTableViewCell")
        
        self.loadSort()
        self.getList()
        self.getChartList()
        // Do any additional setup after loading the view.
    }
    
    func loadSort(){
        add(ui: "Date-Latest", name: "date", value: "desc")
         add(ui: "Date-Oldest", name: "date", value: "asc")
         add(ui: "Status", name: "status", value: "open,replied,closed")
         add(ui: "Type Of Finding", name: "type_of_finding", value: "major_nc,minor_nc,ofi,positive_finding,others")
         add(ui: "Standard", name: "standard", value: "standard_id")
    }
    func add(ui:String,name:String,value:String){
        let dic = ["figma_ui_sort_by":ui,
                   "order_by_field":name,
        "order_by_value":value] as [String:Any]
        let json = JSON(dic)
        let obj = ObjAuditSort.init(json: json)
        self.sortArray.append(obj)
        
        
    }
    //MARK: - button action
    @objc func buttonClicked(sender:UIButton) {
        self.chooseSort.show()
        
    }
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - table delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayReport.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportTitleTableViewCell", for: indexPath) as! ReportTitleTableViewCell
            
            var arrayvalue = [String]()
            for obj in sortArray{
                arrayvalue.append("\(obj.figmaUiSortBy!)")
            }
            chooseSort.anchorView = cell
            chooseSort.bottomOffset = CGPoint(x: 0, y: 0)
            chooseSort.dataSource = arrayvalue
            // Action triggered on selection
            chooseSort.selectionAction = { [weak self] (index, item) in
                // self?.buttonMonth.setTitle(item, for: .normal)
                ///self?.monthValue = item
                self!.order_by_field = (self?.sortArray[index].orderByField)!
                self!.order_by_value = (self?.sortArray[index].orderByValue)!
                self!.chooseSort.hide()
                self!.getList()
                self!.auditTableView.reloadData()
            }
            cell.sortButton.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportHeaderTableViewCell", for: indexPath) as! ReportHeaderTableViewCell
            
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.row == self.arrayReport.count + 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportAuditChartTableViewCell", for: indexPath) as! ReportAuditChartTableViewCell
            
            cell.chart1.isUserInteractionEnabled = false
            cell.chart2.isUserInteractionEnabled = false
            
            cell.chart1.delegate = self
            cell.chart1.models = createChart1Models()
            cell.chart1.layers = []
            
            cell.chart2.layers = []
            cell.chart2.delegate = self
            cell.chart2.models = createChart2Models()
            
            let num = self.arrayReport.count + 2
            
            cell.t1View.backgroundColor = arrayColor[0]
            cell.t2View.backgroundColor = arrayColor[1]
            cell.t3View.backgroundColor = arrayColor[2]
            cell.t4View.backgroundColor = arrayColor[3]
            
            if self.arrayChart1.count > 2{
            let obj1 = self.arrayChart1[0]
            let obj2 = self.arrayChart1[1]
            let obj3 = self.arrayChart1[2]
            let obj4 = self.arrayChart1[3]
            
            cell.t1Label.text = "\(obj1.typeOfFinding!) - \(obj1.value!) %"
            cell.t2Label.text = "\(obj2.typeOfFinding!) - \(obj2.value!) %"
            cell.t3Label.text = "\(obj3.typeOfFinding!) - \(obj3.value!) %"
            cell.t4Label.text = "\(obj4.typeOfFinding!) - \(obj4.value!) %"
            
            }
            cell.s1View.backgroundColor = arrayColor[0]
            cell.s2View.backgroundColor = arrayColor[1]
            cell.s3View.backgroundColor = arrayColor[2]
            
            
            if self.arrayChart2.count > 1{
            let objs1 = self.arrayChart2[0]
            let objs2 = self.arrayChart2[1]
            let objs3 = self.arrayChart2[2]
            
            cell.s1Label.text = "\(objs1.status!) - \(objs1.value!) %"
            cell.s2Label.text = "\(objs2.status!) - \(objs2.value!) %"
            cell.s3Label.text = "\(objs3.status!) - \(objs3.value!) %"
            }
            
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDataTableViewCell", for: indexPath) as! ReportDataTableViewCell
            
            if self.arrayReport.count>0{
                let obj = self.arrayReport[indexPath.row-2]
                cell.numLabel.text = obj.refNo
                cell.processLabel.text = obj.process
                cell.auditeeLabel.text = obj.audtieeName
                cell.dateLabel.text = obj.dateIssue
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 60
        }else if indexPath.row == 1{
            return 60
        }else if indexPath.row == self.arrayReport.count + 2{
            return 420
        }else{
            return 60
        }
    }
    //MARK: - API call
    func getList(){
        Loader.show()
        let dic = ["type":"audit_finding_report_list",
                   "pagination":"0",
                   "order_by_field":order_by_field,
                   "order_by_value":order_by_value] as [String:Any]
        ServiceHelper.sharedInstance.getAuditReportList(parameter: dic) { (result, error) in
            Loader.hide()
            
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.arrayReport.removeAll()
            let arrayMain = responseDic["data"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjAuditReport = ObjAuditReport.init(json: json)
                self.arrayReport.append(obj)
            }
            self.auditTableView.reloadData()
            
        }
        
    }
 
       func getChartList(){
           Loader.show()
           let dic = ["type":"chart"] as [String:Any]
           ServiceHelper.sharedInstance.getAuditReportChartList(parameter: dic) { (result, error) in
               Loader.hide()
               
               guard let data =  result as? [String : Any] else {
                   CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                   return
               }
            guard let responseDic =  data["data"] as? [String : Any] else {
                              CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                              return
                          }
                       
            
               self.arrayChart1.removeAll()
               let arrayMain = responseDic["type_of_finding_chart"] as! [NSArray]
               let swiftArray = arrayMain as AnyObject as! [Any]
               for i in 0..<swiftArray.count {
                   let json = JSON(swiftArray[i])
                   let obj: ObjAuditReportChart1 = ObjAuditReportChart1.init(json: json)
                   self.arrayChart1.append(obj)
               }
            
            self.arrayChart2.removeAll()
            let arrayMain1 = responseDic["status_chart"] as! [NSArray]
            let swiftArray1 = arrayMain1 as AnyObject as! [Any]
            for i in 0..<swiftArray1.count {
                let json = JSON(swiftArray1[i])
                let obj: ObjAuditReportChart2 = ObjAuditReportChart2.init(json: json)
                self.arrayChart2.append(obj)
            }
            
            
            
               self.auditTableView.reloadData()
               
           }
           
       }
    
    // MARK: - Chart Models
       
       fileprivate func createChart1Models() -> [PieSliceModel] {

        var arrayModel:[PieSliceModel] = [PieSliceModel]()
        
        for i in 0..<self.arrayChart1.count{
            let pie = PieSliceModel(value: Double(self.arrayChart1[i].value!), color: arrayColor[i])
            arrayModel.append(pie)
            
        }
//           let models = [
//               PieSliceModel(value: 2, color: arrayColor[0]),
//               PieSliceModel(value: 2, color: arrayColor[1]),
//               PieSliceModel(value: 2, color: arrayColor[2])
//           ]
           
           return arrayModel
       }
    fileprivate func createChart2Models() -> [PieSliceModel] {

//             let models = [
//                 PieSliceModel(value: 2, color: arrayColor[0]),
//                 PieSliceModel(value: 2, color: arrayColor[1]),
//                 PieSliceModel(value: 2, color: arrayColor[2])
//             ]
//
//             return models
        
        
        var arrayModel:[PieSliceModel] = [PieSliceModel]()
        
        for i in 0..<self.arrayChart2.count{
            let pie = PieSliceModel(value: Double(self.arrayChart1[i].value!), color: arrayColor[i])
            arrayModel.append(pie)
            
        }
        return arrayModel
         }
       
}
