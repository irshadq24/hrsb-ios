//
//  ReportAuditChartTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 24/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import PieCharts

class ReportAuditChartTableViewCell: UITableViewCell {
    @IBOutlet weak var chart1: PieChart!
    
    @IBOutlet weak var chart2: PieChart!
    @IBOutlet weak var t3Label: UILabel!
    @IBOutlet weak var t4Label: UILabel!
    
    @IBOutlet weak var t4View: UIView!
    @IBOutlet weak var t3View: UIView!
    @IBOutlet weak var t1View: UIView!
    @IBOutlet weak var t1Label: UILabel!
    @IBOutlet weak var t2Label: UILabel!
    
    @IBOutlet weak var t2View: UIView!
    
    //second chart
    
    @IBOutlet weak var s3Label: UILabel!
    @IBOutlet weak var s3View: UIView!
    @IBOutlet weak var s2Label: UILabel!
    @IBOutlet weak var s2View: UIView!
    @IBOutlet weak var s1View: UIView!
    @IBOutlet weak var s1Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
