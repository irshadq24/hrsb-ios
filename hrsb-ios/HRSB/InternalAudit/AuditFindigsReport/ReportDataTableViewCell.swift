//
//  ReportDataTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 24/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ReportDataTableViewCell: UITableViewCell {
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var auditeeLabel: UILabel!
    @IBOutlet weak var processLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
