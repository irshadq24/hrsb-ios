//
//  AddAuditDataTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 23/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddAuditDataTableViewCell: UITableViewCell {
    @IBOutlet weak var dayButton: UIButton!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearButton: UIButton!
    @IBOutlet weak var monthButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
