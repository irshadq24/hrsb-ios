//
//  AddAuditViewController.swift
//  HRSB
//
//  Created by Javed Multani on 23/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class AddAuditViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var isEdit = false
    var empArray:[ObjAuditEmp] = [ObjAuditEmp]()
    var roleArray:[ObjAuditRole] = [ObjAuditRole]()
    var auditeeArray:[ObjAuditee] = [ObjAuditee]()
    
    //var objAuditee:ObjAuditee? = nil
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var auditTableView: UITableView!
    
    var imagePicker = UIImagePickerController()
    var chooseImage = UIImage(named:"addImage")
    
    var day = "Day"
    var month = "Month"
    var year = "Year"
    
    var dayArray = [String]()
    var monthArray = [String]()
    var yearArray = [String]()
    
    //dropdown
    let chooseRole = DropDown()
    let chooseEmp =  DropDown()
    
    let chooseDay =  DropDown()
    let chooseMonth =  DropDown()
    let chooseYear =  DropDown()
    //
    var auditTitle = ""
    var role = ""
    var roleID = ""
    
    var emp = ""
    var empID = ""
    var empUserID = ""
    
    //Edit details
    var urlPath = ""
    var date = ""
    var auditID = ""
    
    //MARK: - Boiler plate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadRole()
        self.getEmp()
      
        
        auditTableView.delegate = self
        auditTableView.dataSource = self
        
        auditTableView.register(UINib(nibName: "AddProjectDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectDetailTableViewCell")
        auditTableView.register(UINib(nibName: "AddProjectSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectSubmitTableViewCell")
        
        
        auditTableView.register(UINib(nibName: "EditProjectAddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProjectAddMemberTableViewCell")
        
      auditTableView.register(UINib(nibName: "AddAuditDataTableViewCell", bundle: nil), forCellReuseIdentifier: "AddAuditDataTableViewCell")
          auditTableView.register(UINib(nibName: "AddProjectUploadTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectUploadTableViewCell")
        
        
        auditTableView.register(UINib(nibName: "AddGridAuditHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "AddGridAuditHeaderTableViewCell")
        auditTableView.register(UINib(nibName: "AddAuditDataGridTableViewCell", bundle: nil), forCellReuseIdentifier: "AddAuditDataGridTableViewCell")
        
        // add date
        for i in 1..<13{
            self.monthArray.append("\(i)")
        }
        for i in 1..<31{
            self.dayArray.append("\(i)")
        }
        for i in 2015..<2021{
            self.yearArray.append("\(i)")
        }
        
        if self.isEdit{
              self.titleLabel.text = "Edit Audit Plan"
            let a = UIImageView()
                                 a.kf.setImage(with: URL(string: self.urlPath ?? ""), completionHandler: { image, _, _, _ in
                                    if image != nil
                                    {
                                        self.chooseImage = image
                                       
                                        
                                    }else{
                                        self.chooseImage =  UIImage(named:"addImage")
                                        
                                    }
                                  
                                               
                                           })
            
            let dateArr = self.date.components(separatedBy: "-")
            self.year = dateArr[0]
            self.month = dateArr[1]
            self.day = dateArr[2]
            
            self.auditTableView.reloadData()
            
            
        }else{
             self.titleLabel.text = "Create Audit Plan"
         
            
        }
        // Do any additional setup after loading the view.
    }
    //MARK: - table delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.auditeeArray.count>0{
            return self.auditeeArray.count + 8
        }else{
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.auditeeArray.count > 0{

            switch indexPath.row {
            case 0,2,3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectDetailTableViewCell", for: indexPath) as! AddProjectDetailTableViewCell
                cell.dropdownButton.tag = indexPath.row
                
                cell.dropdownButton.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
                cell.dropdownButton.isHidden = true
                cell.imageDropdown.isHidden = true
                cell.textFieldValue.tag = indexPath.row
                if indexPath.row == 0{
                    cell.textFieldValue.placeholder = "Audit title"
                    cell.textFieldValue.text = auditTitle
                }else if indexPath.row == 1{
                    //......
                }else if indexPath.row == 2{
                    cell.textFieldValue.placeholder = "Role"
                    cell.textFieldValue.text = role
                    cell.dropdownButton.isHidden = false
                    cell.imageDropdown.isHidden = false
                    
                    var arrayvalue = [String]()
                    for obj in roleArray{
                        arrayvalue.append("\(obj.roleName!)")
                    }
                    chooseRole.anchorView = cell.textFieldValue
                    chooseRole.bottomOffset = CGPoint(x: 0, y: 0)
                    chooseRole.dataSource = arrayvalue
                    // Action triggered on selection
                    chooseRole.selectionAction = { [weak self] (index, item) in
                        // self?.buttonMonth.setTitle(item, for: .normal)
                        ///self?.monthValue = item
                        self!.roleID = (self?.roleArray[index].roleID)!
                        self!.role = (self?.roleArray[index].roleName)!
                        self!.chooseRole.hide()
                        self!.auditTableView.reloadData()
                    }
                    
                }else if indexPath.row == 3{
                    cell.textFieldValue.placeholder = "Employee"
                    cell.textFieldValue.text = emp
                    cell.dropdownButton.isHidden = false
                    cell.imageDropdown.isHidden = false
                    
                    var arrayvalue = [String]()
                    for obj in empArray{
                        arrayvalue.append("\(obj.employeeName!)")
                    }
                    chooseEmp.anchorView = cell.textFieldValue
                    chooseEmp.bottomOffset = CGPoint(x: 0, y: 0)
                    chooseEmp.dataSource = arrayvalue
                    // Action triggered on selection
                    chooseEmp.selectionAction = { [weak self] (index, item) in
                        // self?.buttonMonth.setTitle(item, for: .normal)
                        ///self?.monthValue = item
                        self!.empID = (self?.empArray[index].employeeId)!
                        self!.emp = (self?.empArray[index].employeeName)!
                        self!.empUserID = (self?.empArray[index].userId)!
                        self!.chooseEmp.hide()
                        self!.auditTableView.reloadData()
                    }
                }
                cell.textFieldValue.delegate = self
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddAuditDataTableViewCell", for: indexPath) as! AddAuditDataTableViewCell
                cell.dayLabel.text = day
                cell.monthLabel.text = month
                cell.yearLabel.text = year
                
                cell.dayButton.addTarget(self,action:#selector(buttonDayClicked), for: .touchUpInside)
                cell.monthButton.addTarget(self,action:#selector(buttonMonthClicked), for: .touchUpInside)
                cell.yearButton.addTarget(self,action:#selector(buttonYearClicked), for: .touchUpInside)
                
                
                chooseDay.anchorView = cell.dayButton
                chooseDay.bottomOffset = CGPoint(x: 0, y: 0)
                chooseDay.dataSource = dayArray
                // Action triggered on selection
                chooseDay.selectionAction = { [weak self] (index, item) in
                    self!.day = self!.dayArray[index]
                    self!.chooseRole.hide()
                    self!.auditTableView.reloadData()
                }
                
                chooseMonth.anchorView = cell.monthButton
                chooseMonth.bottomOffset = CGPoint(x: 0, y: 0)
                chooseMonth.dataSource = monthArray
                // Action triggered on selection
                chooseMonth.selectionAction = { [weak self] (index, item) in
                    self!.month = self!.monthArray[index]
                    self!.chooseMonth.hide()
                    self!.auditTableView.reloadData()
                }
                
                chooseYear.anchorView = cell.yearButton
                chooseYear.bottomOffset = CGPoint(x: 0, y: 0)
                chooseYear.dataSource = yearArray
                // Action triggered on selection
                chooseYear.selectionAction = { [weak self] (index, item) in
                    self!.year = self!.yearArray[index]
                    self!.chooseYear.hide()
                    self!.auditTableView.reloadData()
                }
                
                
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                cell.addButton.addTarget(self,action:#selector(buttonAddMemberClicked), for: .touchUpInside)
                
                //            if self.objProject?.finalizedStatus! == "assign_work"{
                //                cell.addButton.isUserInteractionEnabled = false
                //            }else{
                //                cell.addButton.isUserInteractionEnabled = true
                //            }
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
                 case 5://section
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddGridAuditHeaderTableViewCell", for: indexPath) as! AddGridAuditHeaderTableViewCell
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
                
                case self.auditeeArray.count + 7:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectSubmitTableViewCell", for: indexPath) as! AddProjectSubmitTableViewCell
                               cell.submitButton.addTarget(self,action:#selector(buttonSubmitClicked), for: .touchUpInside)
                               cell.contentView.backgroundColor = UIColor .clear
                               cell.backgroundColor = UIColor .clear
                               cell.selectionStyle = .none
                               
                               return cell
                
                case self.auditeeArray.count + 6:
             
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectUploadTableViewCell", for: indexPath) as! AddProjectUploadTableViewCell
                cell.buttonAdd.addTarget(self,action:#selector(buttonAddImageClicked(sender:)), for: .touchUpInside)
                if self.isEdit{
                    cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
                }else{
                    cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
                }
                
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            
            default:
                 let cell = tableView.dequeueReusableCell(withIdentifier: "AddAuditDataGridTableViewCell", for: indexPath) as! AddAuditDataGridTableViewCell
                 
                 let obj = self.auditeeArray[indexPath.row - 6]
                 cell.nameLabel.text = obj.firstName!
                 cell.ideLabel.text = obj.employeeId!
                 cell.roleLabel.text = obj.role!
                 cell.numLabel.text = "\(indexPath.row - 5)"
                           cell.deleteButton.tag = indexPath.row - 6
                           cell.deleteButton.addTarget(self,action:#selector(buttonDeleteMemeberClicked), for: .touchUpInside)
                                      cell.contentView.backgroundColor = UIColor .clear
                                      cell.backgroundColor = UIColor .clear
                                      cell.selectionStyle = .none
                                      
                                      return cell
            }
        }else{

            switch indexPath.row {
            case 0,2,3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectDetailTableViewCell", for: indexPath) as! AddProjectDetailTableViewCell
                cell.dropdownButton.tag = indexPath.row
                
                cell.dropdownButton.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
                cell.dropdownButton.isHidden = true
                cell.imageDropdown.isHidden = true
                cell.textFieldValue.tag = indexPath.row
                if indexPath.row == 0{
                    cell.textFieldValue.placeholder = "Audit title"
                    cell.textFieldValue.text = auditTitle
                }else if indexPath.row == 1{
                    //......
                }else if indexPath.row == 2{
                    cell.textFieldValue.placeholder = "Role"
                    cell.textFieldValue.text = role
                    cell.dropdownButton.isHidden = false
                    cell.imageDropdown.isHidden = false
                    
                    var arrayvalue = [String]()
                    for obj in roleArray{
                        arrayvalue.append("\(obj.roleName!)")
                    }
                    chooseRole.anchorView = cell.textFieldValue
                    chooseRole.bottomOffset = CGPoint(x: 0, y: 0)
                    chooseRole.dataSource = arrayvalue
                    // Action triggered on selection
                    chooseRole.selectionAction = { [weak self] (index, item) in
                        // self?.buttonMonth.setTitle(item, for: .normal)
                        ///self?.monthValue = item
                        self!.roleID = (self?.roleArray[index].roleID)!
                        self!.role = (self?.roleArray[index].roleName)!
                        self!.chooseRole.hide()
                        self!.auditTableView.reloadData()
                    }
                    
                }else if indexPath.row == 3{
                    cell.textFieldValue.placeholder = "Employee"
                    cell.textFieldValue.text = emp
                    cell.dropdownButton.isHidden = false
                    cell.imageDropdown.isHidden = false
                    
                    var arrayvalue = [String]()
                    for obj in empArray{
                        arrayvalue.append("\(obj.employeeName!)")
                    }
                    chooseEmp.anchorView = cell.textFieldValue
                    chooseEmp.bottomOffset = CGPoint(x: 0, y: 0)
                    chooseEmp.dataSource = arrayvalue
                    // Action triggered on selection
                    chooseEmp.selectionAction = { [weak self] (index, item) in
                        // self?.buttonMonth.setTitle(item, for: .normal)
                        ///self?.monthValue = item
                        self!.empID = (self?.empArray[index].employeeId)!
                        self!.emp = (self?.empArray[index].employeeName)!
                        self!.empUserID = (self?.empArray[index].userId)!
                        self!.chooseEmp.hide()
                        self!.auditTableView.reloadData()
                    }
                }
                cell.textFieldValue.delegate = self
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddAuditDataTableViewCell", for: indexPath) as! AddAuditDataTableViewCell
                cell.dayLabel.text = day
                cell.monthLabel.text = month
                cell.yearLabel.text = year
                
                cell.dayButton.addTarget(self,action:#selector(buttonDayClicked), for: .touchUpInside)
                cell.monthButton.addTarget(self,action:#selector(buttonMonthClicked), for: .touchUpInside)
                cell.yearButton.addTarget(self,action:#selector(buttonYearClicked), for: .touchUpInside)
                
                
                chooseDay.anchorView = cell.dayButton
                chooseDay.bottomOffset = CGPoint(x: 0, y: 0)
                chooseDay.dataSource = dayArray
                // Action triggered on selection
                chooseDay.selectionAction = { [weak self] (index, item) in
                    self!.day = self!.dayArray[index]
                    self!.chooseRole.hide()
                    self!.auditTableView.reloadData()
                }
                
                chooseMonth.anchorView = cell.monthButton
                chooseMonth.bottomOffset = CGPoint(x: 0, y: 0)
                chooseMonth.dataSource = monthArray
                // Action triggered on selection
                chooseMonth.selectionAction = { [weak self] (index, item) in
                    self!.month = self!.monthArray[index]
                    self!.chooseMonth.hide()
                    self!.auditTableView.reloadData()
                }
                
                chooseYear.anchorView = cell.yearButton
                chooseYear.bottomOffset = CGPoint(x: 0, y: 0)
                chooseYear.dataSource = yearArray
                // Action triggered on selection
                chooseYear.selectionAction = { [weak self] (index, item) in
                    self!.year = self!.yearArray[index]
                    self!.chooseYear.hide()
                    self!.auditTableView.reloadData()
                }
                
                
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                cell.addButton.addTarget(self,action:#selector(buttonAddMemberClicked), for: .touchUpInside)
                
                //            if self.objProject?.finalizedStatus! == "assign_work"{
                //                cell.addButton.isUserInteractionEnabled = false
                //            }else{
                //                cell.addButton.isUserInteractionEnabled = true
                //            }
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            case  5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectUploadTableViewCell", for: indexPath) as! AddProjectUploadTableViewCell
                cell.buttonAdd.addTarget(self,action:#selector(buttonAddImageClicked(sender:)), for: .touchUpInside)
                if self.isEdit{
                    cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
                }else{
                    cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
                }
                
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            case 6:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectSubmitTableViewCell", for: indexPath) as! AddProjectSubmitTableViewCell
                cell.submitButton.addTarget(self,action:#selector(buttonSubmitClicked), for: .touchUpInside)
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
          
            default:
                 let cell = tableView.dequeueReusableCell(withIdentifier: "AddAuditDataGridTableViewCell", for: indexPath) as! AddAuditDataGridTableViewCell
                           cell.deleteButton.tag = indexPath.row
                           cell.deleteButton.addTarget(self,action:#selector(buttonDeleteMemeberClicked), for: .touchUpInside)
                                      cell.contentView.backgroundColor = UIColor .clear
                                      cell.backgroundColor = UIColor .clear
                                      cell.selectionStyle = .none
                                      
                                      return cell
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.auditeeArray.count > 0{

            switch indexPath.row {
            case 0,2,3:
                return 65.0
            case 1:
                return 75.0
            case 4:
                return 65.0
            case 5://section
                return 65.0
            case self.auditeeArray.count + 6://image pick
                return 110
            case self.auditeeArray.count + 7://submit button
                 return 65
//            case 55 :
//                return 110
//            case 6:
//                return 65.0
//            case 7:
//                return 65.0
//            case 8:
//                return 44.0
            default://array values
                return 44.0
            }
        }else{

            switch indexPath.row {
            case 0,2,3:
                return 65.0
            case 1:
                return 75.0
            case 4:
                return 65.0
            case 5 :
                return 110
            case 6:
                return 65.0
            default:
                return 44.0
            }
        }
        
    }
    //MARK: - textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = (textField.text ?? "") + string
        switch textField.tag
        {
        case 0:
            auditTitle = kActualText;
        case 1:
            // projectCode = kActualText;
            print("ok")
        case 2:
            role = kActualText;
        case 3:
            emp = kActualText;
        case 4:
            // headWork = kActualText;
            print("ok")
        default:
            print("It is nothing");
        }
        return true;
    }
    
    //MARK: - button sction
    @objc func buttonAddMemberClicked(sender:UIButton) {
        if empID == ""{
            return
        }
        let dic = ["role":roleID,
                   "user_id":empUserID,
                   "first_name":emp,
                   "last_name":"",
                   "employee_id":empID] as [String:Any]
        let json = JSON(dic)
        let obj = ObjAuditee.init(json: json)
        self.auditeeArray.append(obj)
        self.auditTableView.reloadData()
        
    }
    @objc func buttonDeleteMemeberClicked(sender:UIButton) {
        self.auditeeArray.remove(at: sender.tag)
        self.auditTableView.reloadData()
    }
    
    @objc func buttonSubmitClicked(sender:UIButton) {
        if self.chooseImage == nil {
            DisplayBanner.show(message: "please select the file")
            return
        }
        if self.isEdit{
           self.editAudit(image: self.chooseImage!)
        }else{
           self.addAudit(image: self.chooseImage!)
        }
       
        
    }
    @objc func buttonDayClicked(sender:UIButton) {
        chooseDay.show()
    }
    @objc func buttonMonthClicked(sender:UIButton) {
        chooseMonth.show()
    }
    @objc func buttonYearClicked(sender:UIButton) {
        chooseYear.show()
    }
    @objc func buttonAddImageClicked(sender:UIButton) {
        self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
            if selected == 1 {
                self.openCamera()
            } else if selected == 2 {
                self.openGallery()
            }
        }
    }
    @objc func buttonClicked(sender:UIButton) {
        
        let buttonRow = sender.tag
        switch buttonRow
        {
            
        case 2:
            chooseRole.show()
            
        case 3:
            chooseEmp.show()
            
        default:
            print("do nothing")
        }
        
    }
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //Add Role
    func loadRole(){
        addRole(roleID: "auditor", roleName: "Auditor")
        addRole(roleID: "auditee", roleName: "Auditee")
//        addRole(roleID: "1", roleName: "Super Admin")
//        addRole(roleID: "2", roleName: "HSEQ Employee")
//        addRole(roleID: "3", roleName: "Head Of Department")
//        addRole(roleID: "4", roleName: "HR Admin Employee")
//        addRole(roleID: "5", roleName: "Normal Employee")
//        addRole(roleID: "6", roleName: "President")
    }
    func addRole(roleID:String,roleName:String){
        let dic = ["RoleID":roleID,
                   "RoleName":roleName] as [String:Any]
        let json = JSON(dic)
        let objRole = ObjAuditRole.init(json: json)
        self.roleArray.append(objRole)
        
    }
    //MARK: - UIImagePicker delegate
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        if let image = info[.editedImage] as? UIImage{
            self.chooseImage = image
            self.auditTableView.reloadData()
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - API call
       func addAudit(image:UIImage){
           Loader.show()
           let endPoint = Constant.ServiceApi.AddAudit
           let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
           
        let auditeeList = self.auditeeArray.filter{$0.role == "auditee"}
        let auditorList = self.auditeeArray.filter{$0.role == "auditor"}
        var auditeeListSelect = [String]()
           for type in auditeeList{
               auditeeListSelect.append(type.userId!)
           }
          let auditee = auditeeListSelect.joined(separator: ",")
        
        var auditorListSelect = [String]()
         for type in auditorList{
             auditorListSelect.append(type.userId!)
         }
        let auditor = auditorListSelect.joined(separator: ",")
           
           
           var params = [String: String]()
           params["post_type"] = "add_audit_plan"
           params["title"] = self.auditTitle
           params["date"] = "\(year)-\(month)-\(day)"
           params["user_id"] = userID
           params["auditor"] = auditor
           params["auditee"] = auditee
               
           ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["audit_file": image], unReachable: {
               print("no internet")
               Loader.hide()
           }) { (result, progress) in
               Loader.hide()
               guard let dict = result else {
                   return
               }
               if let status = dict["status"] as? String, status == "1" {
                   self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                       self.navigationController?.popViewController(animated: true)
                   })
               } else {
                   self.showAlert(title: "", message: dict["message"] as? String ?? "")
               }
               print(result)
           }
       }
    
    
       func editAudit(image:UIImage){
           Loader.show()
           let endPoint = Constant.ServiceApi.EditAudit
           let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
           
        let auditeeList = self.auditeeArray.filter{$0.role == "auditee"}
        let auditorList = self.auditeeArray.filter{$0.role == "auditor"}
        var auditeeListSelect = [String]()
           for type in auditeeList{
               auditeeListSelect.append(type.userId!)
           }
          let auditee = auditeeListSelect.joined(separator: ",")
        
        var auditorListSelect = [String]()
         for type in auditorList{
             auditorListSelect.append(type.userId!)
         }
        let auditor = auditorListSelect.joined(separator: ",")
           
           
           var params = [String: String]()
           params["post_type"] = "edit_audit_plan"
           params["title"] = self.auditTitle
           params["date"] = "\(year)-\(month)-\(day)"
           params["audit_id"] = auditID
           params["user_id"] = userID
           params["auditor"] = auditor
           params["auditee"] = auditee
               
           ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["audit_file": image], unReachable: {
               print("no internet")
               Loader.hide()
           }) { (result, progress) in
               Loader.hide()
               guard let dict = result else {
                   return
               }
               if let status = dict["status"] as? String, status == "1" {
                   self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                       self.navigationController?.popViewController(animated: true)
                   })
               } else {
                   self.showAlert(title: "", message: dict["message"] as? String ?? "")
               }
               print(result)
           }
       }
    func getEmp(){
        let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
        Loader.show()
        let dic = ["user_id":UseridStr] as [String:Any]
        ServiceHelper.sharedInstance.getEmpAudit(parameter: dic) { (result, error) in
            Loader.hide()
            
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.empArray.removeAll()
            let arrayMain = responseDic["data"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjAuditEmp = ObjAuditEmp.init(json: json)
                self.empArray.append(obj)
            }
            self.auditTableView.reloadData()
        }
        
    }
    
}
