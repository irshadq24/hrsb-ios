//
//  GirdDataTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 01/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddGirdAuditDataTableViewCell: UITableViewCell {
  
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var roleLabel: UILabel!
  
    @IBOutlet weak var identiLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
