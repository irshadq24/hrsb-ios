//
//  AuditDetailViewController.swift
//  HRSB
//
//  Created by Javed Multani on 20/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuditDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {
   @IBOutlet weak var listTableView: UITableView!
    var auditArray:[ObjAuditee] = [ObjAuditee]()
    var auditID = ""
    var count = 0
    
    var aud_title = ""
    var date = ""
    var ur_path = ""
    var attachment = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         listTableView.delegate = self
         listTableView.dataSource = self
        
            listTableView.register(UINib(nibName: "GridAuditHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "GridAuditHeaderTableViewCell")
         listTableView.register(UINib(nibName: "GirdAuditDataTableViewCell", bundle: nil), forCellReuseIdentifier: "GirdAuditDataTableViewCell")
        listTableView.register(UINib(nibName: "DateTableViewCell", bundle: nil), forCellReuseIdentifier: "DateTableViewCell")
        listTableView.register(UINib(nibName: "DeleteAuditButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "DeleteAuditButtonTableViewCell")
        listTableView.register(UINib(nibName: "AuditlDetailLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "AuditlDetailLabelTableViewCell")
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getDetail()
        
    }
    //MARK: - button action
       @IBAction func buttonHandlerBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func buttonhandlerAdd(_ sender: Any) {
        
               let vc = storyboard?.instantiateViewController(withIdentifier: "AddAuditViewController")as! AddAuditViewController
        vc.auditeeArray = self.auditArray
        vc.auditID = self.auditID
        vc.isEdit = true
        vc.urlPath = self.ur_path
        vc.date = self.date
        vc.auditTitle = self.aud_title
               navigationController?.pushViewController(vc, animated: true)
        
        
        
       }
       @objc func buttonDeleteClicked(sender:UIButton) {
        self.deleteAudit()
        
    }

       //MARK: - table delegate
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if count > 0 {
                return count + 4
            }else{
                return 0
            }
            
           }
           
           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DateTableViewCell", for: indexPath) as! DateTableViewCell
                cell.titleLabel.text = self.aud_title
                cell.dateLabel.text = self.date
                           cell.contentView.backgroundColor = UIColor .clear
                           cell.backgroundColor = UIColor .clear
                           cell.selectionStyle = .none
                           
                           return cell
            }else if indexPath.row == count + 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteAuditButtonTableViewCell", for: indexPath) as! DeleteAuditButtonTableViewCell
                cell.deleteButton.layer.cornerRadius = 12.0
                cell.deleteButton.clipsToBounds = true
                        cell.deleteButton.addTarget(self,action:#selector(buttonDeleteClicked), for: .touchUpInside)
                                       cell.contentView.backgroundColor = UIColor .clear
                                       cell.backgroundColor = UIColor .clear
                                       cell.selectionStyle = .none
                                       
                                       return cell
            }else if indexPath.row == count + 3{
                            let cell = tableView.dequeueReusableCell(withIdentifier: "AuditlDetailLabelTableViewCell", for: indexPath) as! AuditlDetailLabelTableViewCell
                cell.titleLabel.text = "Attachment"
                cell.valueLabel.text = self.attachment ?? ""
                                                   cell.contentView.backgroundColor = UIColor .clear
                                                   cell.backgroundColor = UIColor .clear
                                                   cell.selectionStyle = .none
                                                   
                                                   return cell
            }else if indexPath.row == 1{
             let cell = tableView.dequeueReusableCell(withIdentifier: "GridAuditHeaderTableViewCell", for: indexPath) as! GridAuditHeaderTableViewCell
             
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "GirdAuditDataTableViewCell", for: indexPath) as! GirdAuditDataTableViewCell
                
                let obj = auditArray[indexPath.row - 2]
                cell.numLabrl.text = "\(indexPath.row - 1)"
                cell.idLabel.text = obj.userId
                cell.roleLabel.text = obj.role
                cell.nameLabel.text = obj.firstName
                            
                                       cell.contentView.backgroundColor = UIColor .clear
                                       cell.backgroundColor = UIColor .clear
                                       cell.selectionStyle = .none
                                       
                                       return cell
            }
            
           }
           func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.row == 0{
                 return 137
            }else if indexPath.row == count + 2{
                 return 135
            }else if indexPath.row == count + 3{
                 return 62
            }else{
                 return 65
            }
              
           }
           func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             
           }
    //MARK: - api call
      func getDetail(){
          Loader.show()
       
        let str = "?get_type=get_audit_plan_details&audit_id=\(auditID)"
          ServiceHelper.sharedInstance.getAuditDetail(parameter: str) { (result, error) in
              Loader.hide()
              guard let responseDic =  result as? [String : Any] else {
                  CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                  return
              }
            guard let title =  responseDic["title"] as? String else {
                             CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                             return
                         }
            self.aud_title = title
            
            guard let adate =  responseDic["date"] as? String else {
                             CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                             return
                         }
            self.date = adate
            
            guard let aur_path =  responseDic["ur_path"] as? String else {
                                        CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                                        return
                                    }
                       self.ur_path = aur_path
            guard let aattachment =  responseDic["attachment"] as? String else {
                                        CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                                        return
                                    }
                       self.attachment = aattachment
            
            
              self.auditArray.removeAll()
            guard let arrayMain =  responseDic["Auditor_Auditee_details"] as? NSArray else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            
             // let arrayMain = responseDic["Auditor_Auditee_details"] as! [NSArray]
              let swiftArray = arrayMain as AnyObject as! [Any]
              for i in 0..<swiftArray.count {
                  let json = JSON(swiftArray[i])
                  let obj: ObjAuditee = ObjAuditee.init(json: json)
                  self.auditArray.append(obj)
              }
            self.count = self.auditArray.count
              self.listTableView.reloadData()
              
          }
          
      }
    func deleteAudit(){
        Loader.show()
        let dic = ["delete_type":"delete_audit_plan",
                   "audit_id":self.auditID] as [String:Any]
        ServiceHelper.sharedInstance.deleteAudit(parameter: dic) { (result, error) in
            Loader.hide()
                        guard let responseDic =  result as? [String : Any] else {
                            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                            return
                        }
            guard let msg =  responseDic["message"] as? String  else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
             CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message:msg)
                print(msg)
            
        }
    }
      

}
