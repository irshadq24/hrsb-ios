//
//  AuditlDetailLabelTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 20/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AuditlDetailLabelTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
