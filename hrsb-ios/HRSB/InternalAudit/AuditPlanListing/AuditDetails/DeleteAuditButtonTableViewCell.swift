//
//  DeleteAuditButtonTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 20/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class DeleteAuditButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
