//
//  GirdDataTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 01/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class GirdAuditDataTableViewCell: UITableViewCell {
    @IBOutlet weak var numLabrl: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
