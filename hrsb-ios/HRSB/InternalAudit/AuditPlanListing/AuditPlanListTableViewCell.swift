//
//  AuditPlanListTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 15/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AuditPlanListTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
