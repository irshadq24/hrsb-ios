//
//  AuditPlanListingViewController.swift
//  HRSB
//
//  Created by Javed Multani on 15/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuditPlanListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var auditArray:[ObjAuditList] = [ObjAuditList]()
    @IBOutlet weak var listTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        listTableView.delegate = self
        listTableView.dataSource = self
        
        listTableView.register(UINib(nibName: "AuditPlanListTableViewCell", bundle: nil), forCellReuseIdentifier: "AuditPlanListTableViewCell")
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getList()
    }
    //MARK: - button action
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonhandlerAdd(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddAuditViewController")as! AddAuditViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - table delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.auditArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuditPlanListTableViewCell", for: indexPath) as! AuditPlanListTableViewCell
        cell.titleLabel.text = self.auditArray[indexPath.row].title!
        cell.dateLabel.text = self.auditArray[indexPath.row].date!
        cell.contentView.backgroundColor = UIColor .clear
        cell.backgroundColor = UIColor .clear
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AuditDetailViewController")as! AuditDetailViewController
        vc.auditID = self.auditArray[indexPath.row].auditId!
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - api call
    func getList(){
        Loader.show()
       let UseridStr = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
       let str = "?get_type=get_audit_plan&user_id=\(UseridStr)&pagination=0"
        ServiceHelper.sharedInstance.getAuditList(parameter: str) { (result, error) in
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.auditArray.removeAll()
            let arrayMain = responseDic["audit_plan_list"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjAuditList = ObjAuditList.init(json: json)
                self.auditArray.append(obj)
            }
            self.listTableView.reloadData()
            
        }
        
    }
    
}
