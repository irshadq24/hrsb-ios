//
//  InternalAuditViewController.swift
//  HRSB
//
//  Created by Javed Multani on 15/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class InternalAuditViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var titleArray = ["Audit Plan Listing","Audit Findings","Audit Findings Report"]
    var imageArray = ["attendance","attendance","loa"]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "DetailProjectSectionTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectSectionTableViewCell")
        // Do any additional setup after loading the view.
    }
    //MARK -  button action
    @IBAction func buttonBackHandler(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //NARK: - table delegate
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return titleArray.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectSectionTableViewCell", for: indexPath) as! DetailProjectSectionTableViewCell
                    cell.titleLabel.text = titleArray[indexPath.row]
                            cell.iconImage.image = UIImage(named: imageArray[indexPath.row])
                   cell.contentView.backgroundColor = UIColor .clear
                   cell.backgroundColor = UIColor .clear
                   cell.selectionStyle = .none
                   
                   return cell
      }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 55
      }
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if indexPath.row == 0 {
                 let vc = storyboard?.instantiateViewController(withIdentifier: "AuditPlanListingViewController")as! AuditPlanListingViewController
                            navigationController?.pushViewController(vc, animated: true)
          }else if indexPath.row == 1 {
                 let vc = storyboard?.instantiateViewController(withIdentifier: "AuditFindigsViewController")as! AuditFindigsViewController
                            navigationController?.pushViewController(vc, animated: true)
          }else if indexPath.row == 2 {
                   let vc = storyboard?.instantiateViewController(withIdentifier: "AuditFindigsReportViewController")as! AuditFindigsReportViewController
                            navigationController?.pushViewController(vc, animated: true)
          }else{
             //:(
          }
              
      }

}
