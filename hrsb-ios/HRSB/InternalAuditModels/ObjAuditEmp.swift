//
//  ObjAuditEmp.swift
//
//  Created by Javed Multani on 23/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditEmp: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditEmpEmployeeNameKey: String = "employee_name"
  private let kObjAuditEmpStatusKey: String = "status"
  private let kObjAuditEmpBasicSalaryKey: String = "basic_salary"
  private let kObjAuditEmpEmailKey: String = "email"
  private let kObjAuditEmpUserIdKey: String = "user_id"
  private let kObjAuditEmpCompNameKey: String = "comp_name"
  private let kObjAuditEmpDepartmentDesignationKey: String = "department_designation"
  private let kObjAuditEmpEmployeeIdKey: String = "employee_id"
  private let kObjAuditEmpRoleKey: String = "role"

  // MARK: Properties
  public var employeeName: String?
  public var status: String?
  public var basicSalary: String?
  public var email: String?
  public var userId: String?
  public var compName: String?
  public var departmentDesignation: String?
  public var employeeId: String?
  public var role: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    employeeName = json[kObjAuditEmpEmployeeNameKey].string
    status = json[kObjAuditEmpStatusKey].string
    basicSalary = json[kObjAuditEmpBasicSalaryKey].string
    email = json[kObjAuditEmpEmailKey].string
    userId = json[kObjAuditEmpUserIdKey].string
    compName = json[kObjAuditEmpCompNameKey].string
    departmentDesignation = json[kObjAuditEmpDepartmentDesignationKey].string
    employeeId = json[kObjAuditEmpEmployeeIdKey].string
    role = json[kObjAuditEmpRoleKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = employeeName { dictionary[kObjAuditEmpEmployeeNameKey] = value }
    if let value = status { dictionary[kObjAuditEmpStatusKey] = value }
    if let value = basicSalary { dictionary[kObjAuditEmpBasicSalaryKey] = value }
    if let value = email { dictionary[kObjAuditEmpEmailKey] = value }
    if let value = userId { dictionary[kObjAuditEmpUserIdKey] = value }
    if let value = compName { dictionary[kObjAuditEmpCompNameKey] = value }
    if let value = departmentDesignation { dictionary[kObjAuditEmpDepartmentDesignationKey] = value }
    if let value = employeeId { dictionary[kObjAuditEmpEmployeeIdKey] = value }
    if let value = role { dictionary[kObjAuditEmpRoleKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.employeeName = aDecoder.decodeObject(forKey: kObjAuditEmpEmployeeNameKey) as? String
    self.status = aDecoder.decodeObject(forKey: kObjAuditEmpStatusKey) as? String
    self.basicSalary = aDecoder.decodeObject(forKey: kObjAuditEmpBasicSalaryKey) as? String
    self.email = aDecoder.decodeObject(forKey: kObjAuditEmpEmailKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kObjAuditEmpUserIdKey) as? String
    self.compName = aDecoder.decodeObject(forKey: kObjAuditEmpCompNameKey) as? String
    self.departmentDesignation = aDecoder.decodeObject(forKey: kObjAuditEmpDepartmentDesignationKey) as? String
    self.employeeId = aDecoder.decodeObject(forKey: kObjAuditEmpEmployeeIdKey) as? String
    self.role = aDecoder.decodeObject(forKey: kObjAuditEmpRoleKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(employeeName, forKey: kObjAuditEmpEmployeeNameKey)
    aCoder.encode(status, forKey: kObjAuditEmpStatusKey)
    aCoder.encode(basicSalary, forKey: kObjAuditEmpBasicSalaryKey)
    aCoder.encode(email, forKey: kObjAuditEmpEmailKey)
    aCoder.encode(userId, forKey: kObjAuditEmpUserIdKey)
    aCoder.encode(compName, forKey: kObjAuditEmpCompNameKey)
    aCoder.encode(departmentDesignation, forKey: kObjAuditEmpDepartmentDesignationKey)
    aCoder.encode(employeeId, forKey: kObjAuditEmpEmployeeIdKey)
    aCoder.encode(role, forKey: kObjAuditEmpRoleKey)
  }

}
