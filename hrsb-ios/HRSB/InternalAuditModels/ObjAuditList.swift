//
//  ObjAuditList.swift
//
//  Created by Javed Multani on 20/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditListDateKey: String = "date"
  private let kObjAuditListAuditIdKey: String = "audit_id"
  private let kObjAuditListTitleKey: String = "title"
  private let kObjAuditListIsOwnerKey: String = "is_owner"

  // MARK: Properties
  public var date: String?
  public var auditId: String?
  public var title: String?
  public var isOwner: Bool = false

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    date = json[kObjAuditListDateKey].string
    auditId = json[kObjAuditListAuditIdKey].string
    title = json[kObjAuditListTitleKey].string
    isOwner = json[kObjAuditListIsOwnerKey].boolValue
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = date { dictionary[kObjAuditListDateKey] = value }
    if let value = auditId { dictionary[kObjAuditListAuditIdKey] = value }
    if let value = title { dictionary[kObjAuditListTitleKey] = value }
    dictionary[kObjAuditListIsOwnerKey] = isOwner
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.date = aDecoder.decodeObject(forKey: kObjAuditListDateKey) as? String
    self.auditId = aDecoder.decodeObject(forKey: kObjAuditListAuditIdKey) as? String
    self.title = aDecoder.decodeObject(forKey: kObjAuditListTitleKey) as? String
    self.isOwner = aDecoder.decodeBool(forKey: kObjAuditListIsOwnerKey)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(date, forKey: kObjAuditListDateKey)
    aCoder.encode(auditId, forKey: kObjAuditListAuditIdKey)
    aCoder.encode(title, forKey: kObjAuditListTitleKey)
    aCoder.encode(isOwner, forKey: kObjAuditListIsOwnerKey)
  }

}
