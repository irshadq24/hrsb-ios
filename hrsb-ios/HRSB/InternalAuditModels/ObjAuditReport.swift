//
//  ObjAuditReport.swift
//
//  Created by Javed Multani on 24/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditReport: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditReportFindingsKey: String = "findings"
  private let kObjAuditReportDateIssueKey: String = "date_issue"
  private let kObjAuditReportAuditeeKey: String = "auditee"
  private let kObjAuditReportRefNoKey: String = "ref_no"
  private let kObjAuditReportTypeOfFindingKey: String = "type_of_finding"
  private let kObjAuditReportProcessKey: String = "process"
  private let kObjAuditReportRequirementKey: String = "requirement"
  private let kObjAuditReportCorrectionActionKey: String = "correction_action"
  private let kObjAuditReportAudtieeNameKey: String = "audtiee_name"
  private let kObjAuditReportCorrectionKey: String = "correction"
  private let kObjAuditReportRemarksKey: String = "remarks"
  private let kObjAuditReportStatusKey: String = "status"
  private let kObjAuditReportOthersKey: String = "others"
  private let kObjAuditReportRootCauseKey: String = "root_cause"
  private let kObjAuditReportCreatedByKey: String = "created_by"
  private let kObjAuditReportAttachmentKey: String = "attachment"
  private let kObjAuditReportCreatedOnKey: String = "created_on"
  private let kObjAuditReportStandardNameKey: String = "standard_name"
  private let kObjAuditReportAuditIdKey: String = "audit_id"
  private let kObjAuditReportStandardIdKey: String = "standard_id"
  private let kObjAuditReportClauseNoKey: String = "clause_no"
  private let kObjAuditReportAuditFindingIdKey: String = "audit_finding_id"

  // MARK: Properties
  public var findings: String?
  public var dateIssue: String?
  public var auditee: String?
  public var refNo: String?
  public var typeOfFinding: String?
  public var process: String?
  public var requirement: String?
  public var correctionAction: String?
  public var audtieeName: String?
  public var correction: String?
  public var remarks: String?
  public var status: String?
  public var others: String?
  public var rootCause: String?
  public var createdBy: String?
  public var attachment: String?
  public var createdOn: String?
  public var standardName: String?
  public var auditId: String?
  public var standardId: String?
  public var clauseNo: String?
  public var auditFindingId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    findings = json[kObjAuditReportFindingsKey].string
    dateIssue = json[kObjAuditReportDateIssueKey].string
    auditee = json[kObjAuditReportAuditeeKey].string
    refNo = json[kObjAuditReportRefNoKey].string
    typeOfFinding = json[kObjAuditReportTypeOfFindingKey].string
    process = json[kObjAuditReportProcessKey].string
    requirement = json[kObjAuditReportRequirementKey].string
    correctionAction = json[kObjAuditReportCorrectionActionKey].string
    audtieeName = json[kObjAuditReportAudtieeNameKey].string
    correction = json[kObjAuditReportCorrectionKey].string
    remarks = json[kObjAuditReportRemarksKey].string
    status = json[kObjAuditReportStatusKey].string
    others = json[kObjAuditReportOthersKey].string
    rootCause = json[kObjAuditReportRootCauseKey].string
    createdBy = json[kObjAuditReportCreatedByKey].string
    attachment = json[kObjAuditReportAttachmentKey].string
    createdOn = json[kObjAuditReportCreatedOnKey].string
    standardName = json[kObjAuditReportStandardNameKey].string
    auditId = json[kObjAuditReportAuditIdKey].string
    standardId = json[kObjAuditReportStandardIdKey].string
    clauseNo = json[kObjAuditReportClauseNoKey].string
    auditFindingId = json[kObjAuditReportAuditFindingIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = findings { dictionary[kObjAuditReportFindingsKey] = value }
    if let value = dateIssue { dictionary[kObjAuditReportDateIssueKey] = value }
    if let value = auditee { dictionary[kObjAuditReportAuditeeKey] = value }
    if let value = refNo { dictionary[kObjAuditReportRefNoKey] = value }
    if let value = typeOfFinding { dictionary[kObjAuditReportTypeOfFindingKey] = value }
    if let value = process { dictionary[kObjAuditReportProcessKey] = value }
    if let value = requirement { dictionary[kObjAuditReportRequirementKey] = value }
    if let value = correctionAction { dictionary[kObjAuditReportCorrectionActionKey] = value }
    if let value = audtieeName { dictionary[kObjAuditReportAudtieeNameKey] = value }
    if let value = correction { dictionary[kObjAuditReportCorrectionKey] = value }
    if let value = remarks { dictionary[kObjAuditReportRemarksKey] = value }
    if let value = status { dictionary[kObjAuditReportStatusKey] = value }
    if let value = others { dictionary[kObjAuditReportOthersKey] = value }
    if let value = rootCause { dictionary[kObjAuditReportRootCauseKey] = value }
    if let value = createdBy { dictionary[kObjAuditReportCreatedByKey] = value }
    if let value = attachment { dictionary[kObjAuditReportAttachmentKey] = value }
    if let value = createdOn { dictionary[kObjAuditReportCreatedOnKey] = value }
    if let value = standardName { dictionary[kObjAuditReportStandardNameKey] = value }
    if let value = auditId { dictionary[kObjAuditReportAuditIdKey] = value }
    if let value = standardId { dictionary[kObjAuditReportStandardIdKey] = value }
    if let value = clauseNo { dictionary[kObjAuditReportClauseNoKey] = value }
    if let value = auditFindingId { dictionary[kObjAuditReportAuditFindingIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.findings = aDecoder.decodeObject(forKey: kObjAuditReportFindingsKey) as? String
    self.dateIssue = aDecoder.decodeObject(forKey: kObjAuditReportDateIssueKey) as? String
    self.auditee = aDecoder.decodeObject(forKey: kObjAuditReportAuditeeKey) as? String
    self.refNo = aDecoder.decodeObject(forKey: kObjAuditReportRefNoKey) as? String
    self.typeOfFinding = aDecoder.decodeObject(forKey: kObjAuditReportTypeOfFindingKey) as? String
    self.process = aDecoder.decodeObject(forKey: kObjAuditReportProcessKey) as? String
    self.requirement = aDecoder.decodeObject(forKey: kObjAuditReportRequirementKey) as? String
    self.correctionAction = aDecoder.decodeObject(forKey: kObjAuditReportCorrectionActionKey) as? String
    self.audtieeName = aDecoder.decodeObject(forKey: kObjAuditReportAudtieeNameKey) as? String
    self.correction = aDecoder.decodeObject(forKey: kObjAuditReportCorrectionKey) as? String
    self.remarks = aDecoder.decodeObject(forKey: kObjAuditReportRemarksKey) as? String
    self.status = aDecoder.decodeObject(forKey: kObjAuditReportStatusKey) as? String
    self.others = aDecoder.decodeObject(forKey: kObjAuditReportOthersKey) as? String
    self.rootCause = aDecoder.decodeObject(forKey: kObjAuditReportRootCauseKey) as? String
    self.createdBy = aDecoder.decodeObject(forKey: kObjAuditReportCreatedByKey) as? String
    self.attachment = aDecoder.decodeObject(forKey: kObjAuditReportAttachmentKey) as? String
    self.createdOn = aDecoder.decodeObject(forKey: kObjAuditReportCreatedOnKey) as? String
    self.standardName = aDecoder.decodeObject(forKey: kObjAuditReportStandardNameKey) as? String
    self.auditId = aDecoder.decodeObject(forKey: kObjAuditReportAuditIdKey) as? String
    self.standardId = aDecoder.decodeObject(forKey: kObjAuditReportStandardIdKey) as? String
    self.clauseNo = aDecoder.decodeObject(forKey: kObjAuditReportClauseNoKey) as? String
    self.auditFindingId = aDecoder.decodeObject(forKey: kObjAuditReportAuditFindingIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(findings, forKey: kObjAuditReportFindingsKey)
    aCoder.encode(dateIssue, forKey: kObjAuditReportDateIssueKey)
    aCoder.encode(auditee, forKey: kObjAuditReportAuditeeKey)
    aCoder.encode(refNo, forKey: kObjAuditReportRefNoKey)
    aCoder.encode(typeOfFinding, forKey: kObjAuditReportTypeOfFindingKey)
    aCoder.encode(process, forKey: kObjAuditReportProcessKey)
    aCoder.encode(requirement, forKey: kObjAuditReportRequirementKey)
    aCoder.encode(correctionAction, forKey: kObjAuditReportCorrectionActionKey)
    aCoder.encode(audtieeName, forKey: kObjAuditReportAudtieeNameKey)
    aCoder.encode(correction, forKey: kObjAuditReportCorrectionKey)
    aCoder.encode(remarks, forKey: kObjAuditReportRemarksKey)
    aCoder.encode(status, forKey: kObjAuditReportStatusKey)
    aCoder.encode(others, forKey: kObjAuditReportOthersKey)
    aCoder.encode(rootCause, forKey: kObjAuditReportRootCauseKey)
    aCoder.encode(createdBy, forKey: kObjAuditReportCreatedByKey)
    aCoder.encode(attachment, forKey: kObjAuditReportAttachmentKey)
    aCoder.encode(createdOn, forKey: kObjAuditReportCreatedOnKey)
    aCoder.encode(standardName, forKey: kObjAuditReportStandardNameKey)
    aCoder.encode(auditId, forKey: kObjAuditReportAuditIdKey)
    aCoder.encode(standardId, forKey: kObjAuditReportStandardIdKey)
    aCoder.encode(clauseNo, forKey: kObjAuditReportClauseNoKey)
    aCoder.encode(auditFindingId, forKey: kObjAuditReportAuditFindingIdKey)
  }

}
