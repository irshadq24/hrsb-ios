//
//  ObjAuditReportChart1.swift
//
//  Created by Javed Multani on 24/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditReportChart1: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditReportChart1ValueKey: String = "value"
  private let kObjAuditReportChart1TypeOfFindingKey: String = "type_of_finding"

  // MARK: Properties
  public var value: Int?
  public var typeOfFinding: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    value = json[kObjAuditReportChart1ValueKey].int
    typeOfFinding = json[kObjAuditReportChart1TypeOfFindingKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = value { dictionary[kObjAuditReportChart1ValueKey] = value }
    if let value = typeOfFinding { dictionary[kObjAuditReportChart1TypeOfFindingKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.value = aDecoder.decodeObject(forKey: kObjAuditReportChart1ValueKey) as? Int
    self.typeOfFinding = aDecoder.decodeObject(forKey: kObjAuditReportChart1TypeOfFindingKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(value, forKey: kObjAuditReportChart1ValueKey)
    aCoder.encode(typeOfFinding, forKey: kObjAuditReportChart1TypeOfFindingKey)
  }

}
