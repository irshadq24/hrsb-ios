//
//  ObjAuditReportChart2.swift
//
//  Created by Javed Multani on 24/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditReportChart2: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditReportChart2StatusKey: String = "status"
  private let kObjAuditReportChart2ValueKey: String = "value"

  // MARK: Properties
  public var status: String?
  public var value: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    status = json[kObjAuditReportChart2StatusKey].string
    value = json[kObjAuditReportChart2ValueKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[kObjAuditReportChart2StatusKey] = value }
    if let value = value { dictionary[kObjAuditReportChart2ValueKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: kObjAuditReportChart2StatusKey) as? String
    self.value = aDecoder.decodeObject(forKey: kObjAuditReportChart2ValueKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: kObjAuditReportChart2StatusKey)
    aCoder.encode(value, forKey: kObjAuditReportChart2ValueKey)
  }

}
