//
//  ObjAuditRole.swift
//
//  Created by Javed Multani on 23/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditRole: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditRoleRoleIDKey: String = "RoleID"
  private let kObjAuditRoleRoleNameKey: String = "RoleName"

  // MARK: Properties
  public var roleID: String?
  public var roleName: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    roleID = json[kObjAuditRoleRoleIDKey].string
    roleName = json[kObjAuditRoleRoleNameKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = roleID { dictionary[kObjAuditRoleRoleIDKey] = value }
    if let value = roleName { dictionary[kObjAuditRoleRoleNameKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.roleID = aDecoder.decodeObject(forKey: kObjAuditRoleRoleIDKey) as? String
    self.roleName = aDecoder.decodeObject(forKey: kObjAuditRoleRoleNameKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(roleID, forKey: kObjAuditRoleRoleIDKey)
    aCoder.encode(roleName, forKey: kObjAuditRoleRoleNameKey)
  }

}
