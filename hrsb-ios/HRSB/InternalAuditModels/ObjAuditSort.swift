//
//  ObjAuditSort.swift
//
//  Created by Javed Multani on 24/11/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditSort: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditSortFigmaUiSortByKey: String = "figma_ui_sort_by"
  private let kObjAuditSortOrderByFieldKey: String = "order_by_field"
  private let kObjAuditSortOrderByValueKey: String = "order_by_value"

  // MARK: Properties
  public var figmaUiSortBy: String?
  public var orderByField: String?
  public var orderByValue: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    figmaUiSortBy = json[kObjAuditSortFigmaUiSortByKey].string
    orderByField = json[kObjAuditSortOrderByFieldKey].string
    orderByValue = json[kObjAuditSortOrderByValueKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = figmaUiSortBy { dictionary[kObjAuditSortFigmaUiSortByKey] = value }
    if let value = orderByField { dictionary[kObjAuditSortOrderByFieldKey] = value }
    if let value = orderByValue { dictionary[kObjAuditSortOrderByValueKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.figmaUiSortBy = aDecoder.decodeObject(forKey: kObjAuditSortFigmaUiSortByKey) as? String
    self.orderByField = aDecoder.decodeObject(forKey: kObjAuditSortOrderByFieldKey) as? String
    self.orderByValue = aDecoder.decodeObject(forKey: kObjAuditSortOrderByValueKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(figmaUiSortBy, forKey: kObjAuditSortFigmaUiSortByKey)
    aCoder.encode(orderByField, forKey: kObjAuditSortOrderByFieldKey)
    aCoder.encode(orderByValue, forKey: kObjAuditSortOrderByValueKey)
  }

}
