//
//  ObjAuditee.swift
//
//  Created by Javed Multani on 20/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjAuditee: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjAuditeeLastNameKey: String = "last_name"
  private let kObjAuditeeUserIdKey: String = "user_id"
  private let kObjAuditeeEmployeeIdKey: String = "employee_id"
  private let kObjAuditeeRoleKey: String = "role"
  private let kObjAuditeeFirstNameKey: String = "first_name"

  // MARK: Properties
  public var lastName: String?
  public var userId: String?
  public var employeeId: String?
  public var role: String?
  public var firstName: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    lastName = json[kObjAuditeeLastNameKey].string
    userId = json[kObjAuditeeUserIdKey].string
    employeeId = json[kObjAuditeeEmployeeIdKey].string
    role = json[kObjAuditeeRoleKey].string
    firstName = json[kObjAuditeeFirstNameKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[kObjAuditeeLastNameKey] = value }
    if let value = userId { dictionary[kObjAuditeeUserIdKey] = value }
    if let value = employeeId { dictionary[kObjAuditeeEmployeeIdKey] = value }
    if let value = role { dictionary[kObjAuditeeRoleKey] = value }
    if let value = firstName { dictionary[kObjAuditeeFirstNameKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastName = aDecoder.decodeObject(forKey: kObjAuditeeLastNameKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kObjAuditeeUserIdKey) as? String
    self.employeeId = aDecoder.decodeObject(forKey: kObjAuditeeEmployeeIdKey) as? String
    self.role = aDecoder.decodeObject(forKey: kObjAuditeeRoleKey) as? String
    self.firstName = aDecoder.decodeObject(forKey: kObjAuditeeFirstNameKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastName, forKey: kObjAuditeeLastNameKey)
    aCoder.encode(userId, forKey: kObjAuditeeUserIdKey)
    aCoder.encode(employeeId, forKey: kObjAuditeeEmployeeIdKey)
    aCoder.encode(role, forKey: kObjAuditeeRoleKey)
    aCoder.encode(firstName, forKey: kObjAuditeeFirstNameKey)
  }

}
