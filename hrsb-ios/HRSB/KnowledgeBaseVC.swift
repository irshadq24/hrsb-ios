//
//  KnowledgeBaseVC.swift
//  HRSB
//
//  Created by Salman on 06/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ReferenceModel {
    var referenceId = ""
    var referenceTitle = ""
    var date =  ""
    var attachmentName = ""
    var attachmentPath = ""
    
    init(info: [String: Any]) {
        referenceId = info["knowledge_base_id"] as? String ?? ""
        referenceTitle = info["document_title"] as? String ?? ""
        date = info["created_at"] as? String ?? ""
        attachmentName = info["attachment_name"] as? String ?? ""
        attachmentPath = info["attachment_path"] as? String ?? ""
    }
}

class KnowledgeBaseVC: UIViewController {

    var arrReference = [ReferenceModel]()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchKnowledgeBase()
    }
    
    func fetchKnowledgeBase() {
        let endPoint = MethodName.knowledgeBase
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "knowledge_base_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let assets = dict["data"] as? [[String: Any]] {
                    for asset in assets {
                        self?.arrReference.append(ReferenceModel(info: asset))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension KnowledgeBaseVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellReference", for: indexPath) as! CellReference
        cell.info = arrReference[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReference.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DownloadKnowledgeBase") as! DownloadKnowledgeBase
        vc.model = arrReference[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
