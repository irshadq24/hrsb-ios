//
//  LeaveApprovalCell.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class LeaveApprovalCell: UITableViewCell {

    
    @IBOutlet weak var labelLeaveType: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelApplicant: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
