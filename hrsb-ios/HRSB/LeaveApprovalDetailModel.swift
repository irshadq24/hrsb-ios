//
//  LeaveApprovalDetailModel.swift
//  HRSB
//
//  Created by Air 3 on 03/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation


struct LeaveApprovalDetailModel: Codable {
    let to_date: String?
    let from_date: String?
    let status: String?
    let company_id: String?
    let reason: String?
    let avidence_file_view: String?
    let title: String?
    let avidence_file_download: String?
    let breadcrumbs: Bool?
    let remarks: String?
    let role_id: String?
    let duration: String?
    let path_url: String?
    let employee_id: String?
    let applied_on: String?
    let department_name: String?
    let created_at: String?
    let leave_type_id: String?
    let type: String?
    let full_name: String?
    let leave_id: String?
    let all_leave_types: [LeaveListDetail]?
    let leave_type: String?
}

struct LeaveListDetail: Codable {
    let status: String?
    let created_at: String?
    let days_per_year: String?
    let leave_type_id: String?
    let company_id: String?
    let type_name: String?
}

