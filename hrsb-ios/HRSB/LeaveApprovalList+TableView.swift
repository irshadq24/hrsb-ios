//
//  LeaveApprovalList+TableView.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension LeaveApprovalList: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaveListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewApprovalList.dequeueReusableCell(withIdentifier: "LeaveApprovalCell", for: indexPath)as! LeaveApprovalCell
        let data = leaveListModel?.data?[indexPath.row]
        cell.labelStatus.text = "Status: \(data?.status ?? "")"
        cell.labelLeaveType.text = data?.leave_type
        if let date = data?.applied_on?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            cell.labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            cell.labelDate.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        cell.labelApplicant.text =  "Applicant: \(data?.employee_name ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = leaveListModel?.data?[indexPath.row]
        let vc = ApprovalLeaveDetailVC.instantiate(fromAppStoryboard: .main)
        vc.leaveId = data?.leave_id ?? ""
        vc.currentStatus = data?.status ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
