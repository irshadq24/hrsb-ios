//
//  LeaveListModel.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct LeaveListModel: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [LeaveList]?
}


struct LeaveList: Codable {
    let leave_id: String?
    let leave_type: String?
    let employee_name: String?
    let department: String?
    let from_date: String?
    let to_date: String?
    let duration: String?
    let applied_on: String?
    let reasosn: String?
    let status: String?
    let created_at: String?
}

