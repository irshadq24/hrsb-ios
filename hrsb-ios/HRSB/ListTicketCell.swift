//
//  ListTicketCell.swift
//  HRSB
//
//  Created by Salman on 01/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ListTicketCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    
    var info: TicketModel? {
        didSet {
            lblPriority.text = "Priority \(info?.priority ?? "")"
            lblStatus.text = "Status \(info?.status ?? "")"
            lblSubject.text = "\(info?.subject ?? "")"
            if let date = info?.date.toString(dateFormat: "dd-MM-yyyy"), let time = info?.date.toString(dateFormat: "hh-mm a"){
                lblDate.text = "Date \(date)"
                lblTime.text = "Time \(time)"
            }
        }
        
    }
    
}
