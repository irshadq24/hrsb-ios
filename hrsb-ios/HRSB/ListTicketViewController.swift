//
//  ListTicketViewController.swift
//  HRSB
//
//  Created by Salman on 01/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit




class TicketModel {
    var ticketId = ""
    var ticketCode = ""
    var empName = ""
    var company = ""
    var subject = ""
    var priority = ""
    var status = ""
    var companyId = ""
    var date = Date()
    
    var empID = ""
    var ticketPriority = ""
    var description = ""
    var ticketNote = ""
    var category = ""
    var subCategory = ""
    var assignTo = ""
    var attachment = ""
    
    init(info: [String: Any]) {
        self.ticketId = info["ticket_id"] as? String ?? ""
        self.ticketCode = info["ticket_code"] as? String ?? ""
        self.empName = info["employee_name"] as? String ?? ""
        self.company = info["company"] as? String ?? ""
        self.subject = info["subject"] as? String ?? ""
        self.priority = info["priority"] as? String ?? ""
        self.status = info["status"] as? String ?? ""
        print(info)
        if let strDate = info["created_date"] as? String, let date = strDate.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            self.date = date
        }
        
        
    }
}


class ListTicketViewController: UIViewController {
   
    var arrTickets = [TicketModel]()
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        fetchAllTicket()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func fetchAllTicket() {
        let endPoint = MethodName.fetchTicket
        ApiManager.request(path: endPoint, parameters: ["user_id": getUserDefault("UserId")], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let tickets = dict["data"] as? [[String: Any]] {
                    self?.arrTickets.removeAll()
                    for ticket in tickets {
                        self?.arrTickets.append(TicketModel(info: ticket))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapAdd(_ sender: Any) {
        let addTicket = self.storyboard?.instantiateViewController(withIdentifier: "AddTicketVC")
        self.navigationController?.pushViewController(addTicket!, animated: true)
    }
    
}

extension ListTicketViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTickets.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTicketCell", for: indexPath) as! ListTicketCell
        cell.info = arrTickets[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TicketDetailsVC") as! TicketDetailsVC
        vc.ticketModel = arrTickets[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
