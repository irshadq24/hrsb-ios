//
//  LoginViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
import BWWalkthrough

class LoginViewController: UIViewController,UITextFieldDelegate,BWWalkthroughViewControllerDelegate {

    
    
    let usernameStr = String()
    var iconClick = true

    
    
    @IBOutlet var Forgot: UIButton!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
    
    @IBOutlet var hide: UIButton!
    
    
    @IBOutlet weak var txt_username: UITextField!
    
    
    
    @IBOutlet weak var txt_pwd: UITextField!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
          //    CHECKING APP LAUNCHING FIRST TIME....
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            
        }else{
         print("First launch, setting UserDefault.")
                UserDefaults.standard.set(true, forKey: "launchedBefore")
            self.showWalkthrough()
            
        }
      
         
        
        txt_pwd.delegate = self
        txt_username.delegate = self
        txt_username.text = "xcheong"//admin"
        txt_pwd.text = "password"
        
        
        login.layer.cornerRadius = 20


        // Do any additional setup after loading the view.
    }

    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        
        txt_username.resignFirstResponder()
        txt_pwd.resignFirstResponder()
        
        return true
    }

    
    
    
    
    
    @IBAction func login_Acton(_ sender: Any) {
        
        
//
//        let login = "admins"
//        let password = "password"
//        let deviceId = "01"
//
//
//        self.loginRest(login: login, password: password, deviceId: deviceId)
//
//
//
//
        let userName = CommonFunction.shared.trimString(text:  self.txt_username.text)
        let password = CommonFunction.shared.trimString(text: self.txt_pwd.text)

        if userName == "" {
           CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Enter phone number")
        } else if password == "" {
            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Enter password")
        } else {
            self.checkLoginCredential(userName: userName, password: password)
        }
    }
    
    func loginRest(login: String, password: String, deviceId: String){
        let url = Constant.ServiceApi.DomainUrl
        let urlStr = "\(url)admin/login/admin/login"//http://18.139.70.23/api/admin/login"
        
//        let urlStr = "http://13.250.181.252/api/admin/login/admin/login"//http://18.139.70.23/api/admin/login"
        let params = ["username":login, "password":password, "usei_id":deviceId]
        _ = try! JSONSerialization.data(withJSONObject: params)
        
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                print("SUKCES with \(response)")
            case .failure(let error):
                print("ERROR with '\(error)")
                
                
            }
        }
        
    }
    
    
    
    
//    let url = URL(string: "http://hrsbhr.bestweb.my/api/job/job_list")
//    Alamofire.request(url!, method: .post, parameters: nil).responseJSON { response in
//    print(response)
//    print("res", response)
//    if let locationJSON = response.result.value as? [String: Any] {
//    let data = locationJSON["data"] as! [[String: Any]]
//    print("da", data)
//
//    for dataItem in data {
//    print("company", dataItem["company"] as! String)
//    print("status", dataItem["status"] as! String)
//    print("designation", dataItem["designation"] as! String)
//    print("job_vacancy", dataItem["job_vacancy"] as! String)
//
//    self.jobList.append(Job(company: dataItem["company"] as! String, create: dataItem["created_at"] as! String, close: dataItem["date_of_closing"] as! String, designation: dataItem["designation"] as! String, id: dataItem["id"] as! String, vacancy: dataItem["job_vacancy"] as! String, status: dataItem["status"] as! String, title: dataItem["title"] as! String, type: dataItem["type"] as! String))
//
//    print("joblist", self.jobList)
//    self.tble.reloadData()

    
    
    private func checkLoginCredential(userName : String, password : String){
        Loader.show()
        ServiceHelper.sharedInstance.checkLogins(userName: userName, password: password){ (result, error) in
            print(result)
            Loader.hide()
             if let resultValue = result as? [String : Any], let status = resultValue["status"] as? String {
                if let resultdata = resultValue["data"]  as? [String : Any], status != "0" {
                    print("datt", resultdata)
                            let email = resultdata["email"] as? String ?? ""
                            let mobile = resultdata["contact_no"] as? String ?? ""
                            let name = resultdata["username"] as? String ?? ""
                            let userid = resultdata["user_id"] as? String ?? ""
                            let companyId = resultdata["company_id"] as? String ?? ""
                            let designation = resultdata["designation"] as? String ?? ""
                            let empId = resultdata["employee_id"] as? String ?? ""
                            let userRoleId = resultdata["user_role_id"] as? String ?? ""
                            let profilePic = resultdata["profile_picture_view"] as? String ?? ""
                            let departmentId = resultdata["department_id"] as? String ?? ""
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.depId : departmentId])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.companyName : resultdata["company_name"] as? String ?? ""])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.empCode : resultdata["username"] as? String ?? ""])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.EmpId : empId])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.Designation : designation])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.Email : email])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.MobileNumber : mobile])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.Name : name])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.ULogin : "1"])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.UserId : userid])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.CompanyId : companyId])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.userRoleId : userRoleId])
                            CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.profilePic : profilePic])

                            self.MoveToHome()
                }
                else {
                    let message = resultValue["message"] as! String
                    CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: message)
                    
                }
        
    
            }
        }
    
    }
    
    
    
    
    func MoveToHome(){
        kAppDelegate.login()
    }
    
    
    
    
    

    /*
    
    private func checkLoginCredential(userName : String, password : String){
        
   //     CustomActivityIndicator.shared.showProgressView()
        
        ServiceHelper.sharedInstance.checkLogin(userName: userName, password: password) { ( result, error) in
            
     //       CustomActivityIndicator.shared.hideProgressView()
            
            if let resultValue = result as? [String : Any]{
                
                
                if let _ = resultValue["userId"] as? [String : Any]{
                    
                    
                    print(resultValue)
                    
                    
                    
                    CommonFunction.shared.saveDetailsToUserDefault(detailDict: [Constant.UserDefaultsKey.MobileNumber : userName])
                    
                    self.navigateToInitializationView()
                    
                } else {
                    
              //      CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Unable to connect with server.")
                    
                }
            } else {
                let errorCode = (error! as NSError).code
                
                
                print("ERROR CODE",errorCode)
                
                
                
                if errorCode == 422 {
                    
                    self.showActivateUserAlert(userName: userName)
                    
                } else {
                    
                //    CustomAlertController.alert(title: Constants.ErrorMessage.Alert, message: error!.localizedDescription)
                    
                }
            }
        }
    }

    */
    
    
    private func navigateToInitializationView(){

    
        let HomeVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.Home)
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        self.view.window!.layer.add(transition, forKey: kCATransition)
        
       // present(HomeVc, animated: false)
        navigationController?.pushViewController(HomeVc, animated: true)

    
    
    }
    
    
    
    private func showActivateUserAlert(userName: String){
        _ = CustomAlertController.alert(title: "Account Inactive", message: "This account is currently inactive, click Yes to activate" , buttons: ["No","Yes"], tapBlock: { (alert, index) in
            if index == 1{
                self.generateOTP(mobileNumber: userName)
            }
        })
    }
    
    
    
    
    
    
    
    
    
    private func generateOTP(mobileNumber: String ){
     //   CustomActivityIndicator.shared.showProgressView()
//        ServiceHelper.sharedInstance.generateOTP(mobileNumber: mobileNumber) { (result, error) in
//            CustomActivityIndicator.shared.hideProgressView()
//            if let _ = result as? Bool {
//                self.navigateToValidateOTPView(mobileNumber : mobileNumber)
//            } else {
//                CustomAlertController.alert(title: Constants.ErrorMessage.Alert, message: error!.localizedDescription)
//            }
//        }
    }

    
    
    
    
    @IBAction func back_action(_ sender: Any) {
        
        
       // dismiss(animated: false, completion: nil)
        navigationController?.popViewController(animated: true)

    }
    
    
    
    
    @IBAction func forgot_Action(_ sender: Any) {
        let HomeVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ForgotPwd)
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        self.view.window!.layer.add(transition, forKey: kCATransition)
        //present(HomeVc, animated: false)
        navigationController?.pushViewController(HomeVc, animated: true)

        
    }
    
    
    
    @IBAction func hide_Action(_ sender: Any) {
        
        if(iconClick == true) {
            txt_pwd.isSecureTextEntry = false
            
            let image = UIImage(named: "invision.png") as UIImage?
            
            hide.setImage(image, for: UIControl.State.normal)
            
            

        } else {
            txt_pwd.isSecureTextEntry = true
            
            let image = UIImage(named: "eye.png") as UIImage?
            
            hide.setImage(image, for: UIControl.State.normal)

            
        }
        
        iconClick = !iconClick

        
    }
    
    
    
    
    
    @IBAction func showWalkthrough(){
        
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Main", bundle: nil)
        
        
        let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        let page_zero = stb.instantiateViewController(withIdentifier: "walk0")
        let page_one = stb.instantiateViewController(withIdentifier: "walk1")
        let page_two = stb.instantiateViewController(withIdentifier: "walk2")
        let page_three = stb.instantiateViewController(withIdentifier: "walk3")
        let page_four = stb.instantiateViewController(withIdentifier: "walk4")

        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_four)

        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        walkthrough.add(viewController:page_three)
        walkthrough.add(viewController:page_zero)

       // self.present(walkthrough, animated: true, completion: nil)
        self.navigationController?.pushViewController(walkthrough, animated: true)
    }
    
    // MARK: - Walkthrough delegate -
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }
    
    func walkthroughCloseButtonPressed() {
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    

    

}
