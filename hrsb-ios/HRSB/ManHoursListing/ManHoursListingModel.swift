//
//  ManHoursListingModel.swift
//  HRSB
//
//  Created by Zakir Khan on 19/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct ManHoursListing: Codable {
    let cols: ManHoursListingCols?
    let rows: [ManHoursListingRows]?
    
    func getRowColumn()->[[String]] {
        var arrRowCol = [[String]]()
        for row in rows! {
            arrRowCol.append(row.getDataInArray())
        }
        arrRowCol.insert(["COMPANY", "OFFICE/WAREHOUSE", "PROJECT/TEAM CONTACT", "TA", "TOTAL", "TOTAL CALCULATIVE"], at: 0)
        return arrRowCol
    }
}

struct ManHoursListingCols: Codable {
    let office_total: String?
    let ta_total: String?
    let overall_total: Int?
    let project_total: String?
}

struct ManHoursListingRows: Codable {
    let subsidiary: String?
    let ta: String?
    //let office/warehouse: String?
    //let project/term: String?
    let total: String?
    let created_at: String?
    func getDataInArray()-> [String] {
        return [subsidiary ?? "", "wearhouse", "project team", ta ?? "", total ?? "", "-"]
    }
}
