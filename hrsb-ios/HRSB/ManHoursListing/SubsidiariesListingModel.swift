//
//  SubsidiariesListingModel.swift
//  HRSB
//
//  Created by Zakir Khan on 20/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct SubsidiariesListing: Codable {
    let subsidiaries: [SubsidiariesData]?
    let incident_type: [IncidentType]?
    
    func getRowColumn()->[[String]] {
        var arrRowCol = [[String]]()
        for row in subsidiaries! {
            arrRowCol.append(row.getDataInArray())
        }
        arrRowCol.insert(["TYPE OF INCIDENT", "HOLDINGS", "P.SERVICES", "E&C", "HYBRID POWER", "POWER CONTROL","TRANING","TOTAL 2019"], at: 0)
        return arrRowCol
    }
    
}

struct SubsidiariesData: Codable {
    let registration_no: String?
    let address_2: String?
    let zipcode: String?
    let country: String?
    let website_url: String?
    let added_by: String?
    let type_id: String?
    let contact_number: String?
    let government_tax: String?
    let city: String?
    let name: String?
    let state: String?
    let address_1: String?
    let company_id: String?
    let is_active: String?
    let email: String?
    let logo: String?
    let password: String?
    let created_at: String?
    let trading_name: String?
    let username: String?
    
    func getDataInArray()-> [String] {
        return [name ?? "name", address_2 ?? "address_2", zipcode ?? "zipcode", country ?? "country", added_by ?? "added_by", trading_name ?? "trading_name", email ?? "", trading_name ?? "trading_name"]
    }
}


struct IncidentType: Codable {
    let incident_type: String?
    let incident_name: String?
    let incident_id: String?
    
}

struct IncidentSummary: Codable {
    let data: [IncidentData]?
    
    
    func getRowColumn()->[[String]] {
        var arrRowCol = [[String]]()
        for row in data! {
            arrRowCol.append(row.getDataInArray())
        }
        arrRowCol.insert(["TYPE OF INCIDENT", "AUGUST","Total 2019"], at: 0)
        return arrRowCol
    }
    
}

struct IncidentData: Codable {
    let incident_type: String?
    let total: String?
    //let 2019-08: String?
    

    func getDataInArray()-> [String] {
        return [incident_type ?? "incident_type", total ?? "total" , "1"]
    }
}

