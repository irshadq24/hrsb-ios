//
//  ManHoursListingFormVC.swift
//  HRSB
//
//  Created by Zakir Khan on 18/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ManHoursListingFormVC: UIViewController {

    
    var arrIncidentListingForm = ["Incident Listing Summary - General", "Incident Listing Summary - Subsidiaries"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}

//extension ManHoursListingFormVC: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrIncidentListingForm.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        <#code#>
//    }
//
//
//}

extension ManHoursListingFormVC {
    @IBAction func buttonActionBack(_ sender: UIButton){
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
}
