//
//  ApiManager.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 13/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import Foundation
import Alamofire
class ApiManager{
    //    static let shared = ApiManager()
    //    private init(){
    //
    //    }
    enum HTTPMethodType {
        case post
        case get
        case put
        case delete
    }
    enum Result<T> {
        case success(T)
        case failure(Error?)
        //case noNetwork(Error)
        //case progress(Float)
        case noDataFound(String)
        //case pageNotFound
    }
    class func request(path:String,parameters: [String:Any]?, methodType: HTTPMethodType, showLoader: Bool = true, result: @escaping (Result<Any>) -> ()) {
        let serviceUrlPathString = API.baseUrl + path
        Console.log("requested with url \(serviceUrlPathString) and parameter as \(parameters)")
        var method: HTTPMethod = .post
        switch methodType {
        case .get:
            method = .get
        case .post:
            method = .post
        case .put:
            method = .put
        case .delete:
            method = .delete
        }
        if showLoader {
           Loader.show()
        }
        var headers = [String: String]()
        if let token = Constants.userDefaults.value(forKey: UserDefaultKeys.token) as? String {
            headers = [
                "token": token
            ]
        }
        Alamofire.request(serviceUrlPathString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString(completionHandler: { (response) in
            print("String response",response)
        })
            
            .responseJSON { (response) in
            if showLoader {
              Loader.hide()
            }
            logRequest(response: response, parms: parameters)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value, let code = response.response?.statusCode {
                    prettyPrint(json: data, statusCode: code)
                    ApiManager.handleSuccess(data: data, statusCode: code, result: { (res) in
                        switch(res) {
                        case .success(let data):
                            result(Result.success(data))
                        case .failure(let error):
                            result(Result.failure(error))
                        case .noDataFound(let message):
                            result(Result.noDataFound(message))
                        }
                    })
                }else{
                    Console.log(ErrorMessages.errorToHandleInSuccess)
                }
            case .failure(_):
                if let error = response.result.error  {
                    result(Result.failure(error))
                    Console.log("failure: \(error)")
                }else{
                    result(Result.failure(nil))

                    Console.log(ErrorMessages.errorToHandleInFailure)
                }
            }
        }
    }
    private class func handleSuccess(data: Any, statusCode: Int, result:(Result<Any>) -> ()){
        if statusCode == StatusCode.success {
            result(Result.success(data))
        }else {
            if let data = data as? [String : Any] {
                let errorModel = ResponseModel(response: data)
                if let message = errorModel.message {
                    DisplayBanner.show(message: message)
                    if statusCode == StatusCode.noDataFound {
                    result(Result.noDataFound(message))
                    }
                }else{
                    result(Result.failure(nil))
                }
            }else{
                result(Result.failure(nil))
            }
        }
    }

    class func prettyPrint(json: Any, statusCode: Int){
        Console.log("Status code is == \(statusCode)")
        if let json = json as? [String: Any]{
            Console.log(prettyPrintDict(with: json))
        }else if let json = json as? [Any]{
            Console.log(prettyPrintArray(with: json))
        }
    }
    private class func prettyPrintArray(with json: [Any]) -> String{
        do{
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let string = String(data: data, encoding: String.Encoding.utf8)
            if let string  = string{

                return string
            }
        }catch {
            Console.log(error.localizedDescription)
        }
        //print("something went wrong")
        return ""
    }
    private class func prettyPrintDict(with json: [String : Any]) -> String{
        do{
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let string = String(data: data, encoding: String.Encoding.utf8)
            if let string  = string{

                return string
            }
        }catch{
            Console.log(error.localizedDescription)

        }
        //print("something went wrong")
        return ""
    }
    private class func logRequest(response: DataResponse<Any>, parms: [String:Any]?){
        if let url = response.request?.url{
            Console.log("URL: \(url)")
        }
        if let headerFields = response.request?.allHTTPHeaderFields {
            Console.log("HeaderFields:  \(headerFields)")
        }
        if let requestType = response.request?.httpMethod {
            Console.log("requestType:  \(requestType)")
        }
        if let parms = parms {
            Console.log("parms:  \(parms)")
        }
    }
    
    class func getImage(_ url:String,handler: @escaping (UIImage?)->Void) {
        Alamofire.request(url, method: .get).responseData { response in
            if let data = response.result.value {
                handler(UIImage(data: data))
            } else {
                handler(nil)
            }
        }
    }
    
    class func hitMultipartForImage(path:String,_ params:Dictionary<String, Any>, imageInfo:Dictionary<String,UIImage>,unReachable:(() -> Void),handler:@escaping ((Dictionary<String,Any>?,Int) -> Void)) {
        let url = API.baseUrl + path
        print(url)
        print(params)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if let tags = value as? [String] {
                        for tag in tags{
                            multipartFormData.append(((tag as Any) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "\(key)[]")
                        }
                    }else {
                        multipartFormData.append(((value as Any) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
                for (_,info) in imageInfo.enumerated() {
                    let imgData = info.value.jpegData(compressionQuality: 0.2)
                    if imgData != nil {
                        multipartFormData.append(imgData!, withName: info.key, fileName: "file.png", mimeType: "image/png")
                    }
                }
        },
            to: url,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload,_,_):
                        upload
                        .responseString(completionHandler: { (response) in
                            print(response)
                        })
                        .responseJSON { response in
                        print(response)
                        switch response.result {
                        case .success:
                            if let jsonDict = response.result.value as? Dictionary<String,Any> {
                                //      print_debug("Json Response: \(jsonDict)") // serialized json response
                                handler(jsonDict,(response.response!.statusCode))
                            }
                            else{
                                handler(nil,(response.response!.statusCode))
                            }
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Server Response: \(utf8Text)") // original server data as UTF8 string
                            }
                            break
                        case .failure(let error):
                            handler(nil,1001)
                            break
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
    class func requestMultipartApiServer(path:String,parameters: [String:Any]?, methodType: HTTPMethodType, showLoader: Bool = true, result: @escaping (Result<Any>) -> () , _ onProgress: @escaping (Double)-> ()){
        let serviceUrlPathString = API.baseUrl + path
        //Console.log("requested with url \(serviceUrlPathString) and parameter as \(parameters)")
        var method: HTTPMethod = .post
        switch methodType {
        case .get:
            method = .get
        case .post:
            method = .post
        case .put:
            method = .put
        case .delete:
            method = .delete
        }
        if showLoader {
           Loader.show()
        }
        var headers = [String: String]()
        if let token = Constants.userDefaults.value(forKey: UserDefaultKeys.token) as? String {
            headers = [
                "token": token,
                "Content-Type":"application/x-www-form-urlencoded"
            ]
        }
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters! {
                print("key \(key) value: \(value)")
                if key == ServerKeys.image{
                    if let imageDataArray = value as? [Data] {
                        for data in imageDataArray {
                            multipartFormData.append(data, withName: "bill_copy", fileName: "sample", mimeType: "image/jpeg")
                        }
                    }
                }
                else{
                    if let tags = value as? [String]{
                        for tag in tags{
                            multipartFormData.append(((tag as Any) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "\(key)[]")
                        }
                    } else if let image = value as? UIImage {
                        let imgData = image.jpegData(compressionQuality: 0.2)
                        Console.log("ImageTesting: \(imgData), and key \(key)")
                        multipartFormData.append(imgData!, withName: key, fileName: "\(Date().timeIntervalSinceNow)", mimeType: "image/jpeg")
                    }
                    else {
                        print("added else \(key) \(value)")
                        multipartFormData.append(((value as Any) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
            }

        }
            , usingThreshold: UInt64.init(), to: serviceUrlPathString, method: method, headers: headers, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in

                        onProgress(Progress.fractionCompleted)
                    })
                    upload
                        .responseString(completionHandler: { (response) in
                            print(response)
                        })
                        .responseJSON {
                        response in
                        logRequest(response: response, parms: parameters)
                        if showLoader {
                         Loader.hide()
                        }
                        switch(response.result) {
                        case .success(_):
                            if let data = response.result.value, let code = response.response?.statusCode {
                                prettyPrint(json: data, statusCode: code)
                                ApiManager.handleSuccess(data: data, statusCode: code, result: { (res) in
                                    switch(res) {
                                    case .success(let data):
                                        result(Result.success(data))
                                    case .failure(let error):
                                        result(Result.failure(error))
                                    case .noDataFound(let message):
                                        result(Result.noDataFound(message))
                                    }
                                })
                            }else{
                                Console.log(ErrorMessages.errorToHandleInSuccess)
                            }
                        case .failure(_):
                            if let error = response.result.error  {
                                result(Result.failure(error))
                                Console.log("failure: \(error)")
                            }else{
                                result(Result.failure(nil))

                                Console.log(ErrorMessages.errorToHandleInFailure)
                            }
                        }

                    }
                case .failure(let encodingError):

                    Console.log(encodingError.localizedDescription)
                 result(Result.failure(encodingError))
                }

        })
    }
//    func postAction() {
//
//        guard let serviceUrl = URL(string: "http://hireswiftdeveloper.com/FindingOona/api/user/register") else { return }
//        let imageStr = #imageLiteral(resourceName: "check_category").toBase64()
//        let params =  ["email": "vikassa@gmail.com", "password": "123456", "location": "Sector 4, Noida, India, 201301 ", "DOB": "2016-11-27", "lastName": "Gupta", "firstName": "Ankur", "gender": "male", "phoneNumber": "7500052810", "requestType": "normal", "profileImage": imageStr]
//        var request = URLRequest(url: serviceUrl)
//        request.httpMethod = "POST"
    
//        // request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
//            return
//        }
//        request.httpBody = httpBody
//
//        let session = URLSession.shared
//        session.dataTask(with: request) { (data, response, error) in
//            if let response = response {
//                print(response)
//            }
//            if let data = data {
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: [])
//                    print(json)
//                }catch {
//                    print(error)
//                }
//            }
//            if let error = error {
//                print("error \(error.localizedDescription)")
//            }
//            }.resume()
//    }
}
