//
//  DisplayBanner.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 13/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import BRYXBanner
class DisplayBanner {
    class  func show(message:String?){
        let banner = Banner(title: message ?? ErrorMessages.somethingWentWrong, subtitle: "", image: nil, backgroundColor: UIColor.init(hexString: "#683894"))
//            let banner = Banner(title: message, subtitle: "", image: nil, backgroundColor: UIColor(red:0.00/255.0, green:124.00/255.0, blue:198.00/255.0, alpha:1.000))
            banner.dismissesOnTap = true
            banner.show(duration: 1.2)

    }

}
