//
//  Loader.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 13/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

class Loader {
    class func show() {
        Console.log("Loader Show")
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.spinnerColor(Constants.Color.appDefaultViolet)
        SKActivityIndicator.show("", userInteractionStatus: false)
    }
    class func hide() {
        Console.log("Loader Hide")
        SKActivityIndicator.dismiss()
    }

}
