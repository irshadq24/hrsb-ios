//
//  ResponseModel.swift
//  WeekInChina
//
//  Created by Kanika Mishra on 13/03/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import Foundation
struct ResponseModel {
    var message: String?
    init(response: [String: Any]) {
        if let data = response[ServerKeys.data] as? [String: Any]{
            message = response[ServerKeys.message] as? String
            var messageArray  = [String]()
       let keys = data.keys
            for key in keys {
                if let errorArray = data[key] as? [String] , let errorMessage = errorArray.first {
                    messageArray.append(errorMessage)

                }
            }
            if messageArray.count > 0 {
                message = messageArray.joined(separator: "\n")

            }

        }else {
            message = response[ServerKeys.message] as? String
        }
    }
}
