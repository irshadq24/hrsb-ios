//
//  MeetingBookingRoomVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension MeetingBookingRoomVC {
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionAddBooking(_ sender: UIButton) {
        Console.log("buttonActionAddBooking")
        let vc = AddMeetingRoomBookingVC.instantiate(fromAppStoryboard: .main)
        navigationController?.pushViewController(vc, animated: true)
    }
  
    
}
  
