//
//  MeetingBookingRoomVC+TableView.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension MeetingBookingRoomVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facilityBooingRoom?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableViewMeetingBooking.dequeueReusableCell(withIdentifier: "ExpenseClaimListCell", for: indexPath)as? ExpenseClaimListCell else {
            return UITableViewCell()
        }
        cell.labelTime.isHidden = true
        let data = facilityBooingRoom?.data?[indexPath.row]
        cell.labelFacility.text = data?.facility
        cell.labelDate.text = "Date: \(data?.date_from ?? "")"
//        if let date = data?.date_from?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
//            cell.labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
//            cell.labelTime.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
//        }
        return cell
    }
}
 
extension MeetingBookingRoomVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = facilityBooingRoom?.data?[indexPath.row]
        let vc = FacilityDetailVC.instantiate(fromAppStoryboard: .main)
        vc.facilityId = data?.facilities_room_id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
