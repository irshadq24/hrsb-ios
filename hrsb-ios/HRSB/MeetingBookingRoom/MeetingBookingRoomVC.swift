//
//  MeetingBookingRoomVC.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class MeetingBookingRoomVC: UIViewController {

    @IBOutlet weak var tableViewMeetingBooking: UITableView!
    @IBOutlet weak var buttonAddBooking: UIButton!
    var facilityBooingRoom: FacilityBooingRoom?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        registerTableViewCell()
    }
 
    func registerTableViewCell() {
        tableViewMeetingBooking.delegate = self
        tableViewMeetingBooking.dataSource = self
        tableViewMeetingBooking.register(UINib(nibName: "ExpenseClaimListCell", bundle: nil), forCellReuseIdentifier: "ExpenseClaimListCell")
    }
    
    func setup() {
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["employee_id": userID])
    }
    
    

}
