//
//  MenuViewController.swift
//  BaseMenu
//
//  Created by BestWeb on 25/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents



protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
         
    
}

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    @IBOutlet weak var vc: UIButton!
    
    
    @IBOutlet weak var home: UIButton!
    
    @IBOutlet weak var sec: UIButton!
    
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    
    
    @IBOutlet weak var vies: UIView!
    let reuseIdentifier = "MenuCell"
    
    var appBarViewController = MDCAppBarViewController()

    //==============================+OLD CODE==================================
    var items = ["Home", "Human Resource", "Operation","HSEQ","Information Technology","Procurement","Calendar"]


    var Images = ["home.png", "HR1.png", "operation1.png","hseqq.png","information technology1.png","procu.png","Calendar.png"]
    //===============================================================================
    
     //==============================+NEW CODE==================================
//    var items = ["Home", "Human Resource","HSEQ"]
//
//
//    var Images = ["home.png", "HR1.png","hseqq.png"]
    
    @IBOutlet weak var tble: UITableView!
    
    
    
    @IBOutlet weak var btnCloseMenuOverlay: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tble.delegate = self
        tble.dataSource = self
        
        
        
        
        
        self.addChild(self.appBarViewController)
        self.vies.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 113/255.0, blue: 103/255.0, alpha: 1.0)

        
        self.appBarViewController.headerView.frame = CGRect(x: 0, y: 0, width: vies.frame.width , height: view.frame.height)
        
        
        self.navigationItem.title = "Menu"
        

        
    
        

        vies.frame = CGRect(x: 0, y: 0, width: view.frame.width - 75, height: view.frame.height)


        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        
        
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(sender.tag)
            if(sender == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })

        
    }
    
    
    
    @IBAction func home_Action(_ sender: Any) {
        
        let mainstorbord:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let DVC = mainstorbord.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        
        self.navigationController?.pushViewController(DVC, animated: true)
        
    }
    
    
    
    
    
    
    @IBAction func Sec_action(_ sender: Any) {
        
        let mainstorbord:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let DVC = mainstorbord.instantiateViewController(withIdentifier: "HumanResourceViewController") as! HumanResourceViewController
        
        
        self.navigationController?.pushViewController(DVC, animated: true)

        
        
        
        
    }
    
    
    
    @IBAction func vc_Action(_ sender: Any) {
        
        
        
        let mainstorbord:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let DVC = mainstorbord.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        
        self.navigationController?.pushViewController(DVC, animated: true)
        

    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! MenuCell
        
        
        
        
        
        cell.lbl.text = items[indexPath.row]
        
        cell.img.image = UIImage(named: Images[indexPath.row])
        
        
        
        
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            //==============================+NEW CODE==================================
        /*if indexPath.row == 0{
                   
                   
                   print(0)
                   
                   let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.Home)
                   //self.present(HrVc, animated: true, completion: nil)
                   navigationController?.pushViewController(HrVc, animated: true)
                   
                   
               }else if indexPath.row == 1{
                   
                   
                   print(1)
                   let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.HrVc)
                   //self.present(HrVc, animated: true, completion: nil)
                   navigationController?.pushViewController(HrVc, animated: true)
                   
                   
        }else{
            
                      let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.InFoVC)
                      //self.present(HrVc, animated: true, completion: nil)
                      navigationController?.pushViewController(HrVc, animated: true)
                      print(4)
        }*/
        //==========================OLD CODE===================================
        if indexPath.row == 0{
            
            
            print(0)
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.Home)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
        }else if indexPath.row == 1{
            
            
            print(1)
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.HrVc)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
        }else if indexPath.row == 2{
            
            
            print(2)
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.OperationVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
        }else if indexPath.row == 3{
            
            
            print(3)
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.HSEQ)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
        }else if indexPath.row == 4{
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.InFoVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            print(4)
            
            
        }else if indexPath.row == 5{
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ProcureVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
            print(5)
            
            
        }
        else if indexPath.row == 6{
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.CalendarVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)
            
            
            print(5)
            
            
        }
       
        //===============================================
        
    }
    

}
