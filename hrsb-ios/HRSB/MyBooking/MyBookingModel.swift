//
//  MyBookingModel.swift
//  HRSB
//
//  Created by Air 3 on 01/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct MyBookingModel: Codable{
    let data: [MyBooking]?
}


struct MyBooking: Codable {
    let booking_id: String?
    let title: String?
    let purpose: String?
    let date_from: String?
    let date_to: String?
    let project: String?
    let status: String?
}
