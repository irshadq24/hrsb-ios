//
//  MyBookingCell.swift
//  HRSB
//
//  Created by Air 3 on 01/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class MyBookingCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
