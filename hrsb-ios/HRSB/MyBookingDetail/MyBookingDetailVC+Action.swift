//
//  MyBookingDetailVC+Action.swift
//  HRSB
//
//  Created by Zakir Khan on 14/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension MyBookingDetailVC {
    @IBAction func buttonActionBack(_ sender: UIButton){
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonActionEdit(_ sender: UIButton){
        Console.log("buttonActionEdit")
        let data = vehicleBooking?.data
        let vc = AddBookingVC.instantiate(fromAppStoryboard: .main)
        vc.vehicleBookingData = data
        vc.isUpdate = "isUpdate"
        navigationController?.pushViewController(vc, animated: true)
    }
    
   
}
