//
//  MyBookingDetailVC+Service.swift
//  HRSB
//
//  Created by Zakir Khan on 14/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension MyBookingDetailVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.vehicleBookingDetails
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.vehicleBooking = try decoder.decode(VehicleBooking.self, from: data)
                    Console.log("VehicleBooking:- \(String(describing: self.vehicleBooking))")
                    setupView(vehicleBooking: self.vehicleBooking)
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func setupView(vehicleBooking: VehicleBooking?) {
        let data = vehicleBooking?.data
        bookingTitle.text = data?.title
        labelDateFrom.text = data?.date_from
        labelDateTo.text = data?.date_to
        labelTransportType.text = data?.transport_type
        labelType.text = data?.type
        labelPlatNumber.text = data?.plat_no
        labelProject.text = data?.project
        labelBookingPurpose.text = data?.purpose
        labelDate.text = data?.date_from
        labelTitle.text = data?.title
    }
    
    
   
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
}
