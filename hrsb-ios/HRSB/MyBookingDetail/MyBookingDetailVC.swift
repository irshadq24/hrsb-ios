//
//  MyBookingDetailVC.swift
//  HRSB
//
//  Created by Zakir Khan on 14/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class MyBookingDetailVC: UIViewController {

    @IBOutlet weak var bookingTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelTransportType: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelPlatNumber: UILabel!
    @IBOutlet weak var labelDateFrom: UILabel!
    @IBOutlet weak var labelDateTo: UILabel!
    @IBOutlet weak var labelBookingPurpose: UILabel!
    @IBOutlet weak var labelProject: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    
    var vehicleBooking: VehicleBooking?
    var bookingId = ""
    var status = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEdit.isHidden = true
        checkStatus()
        requestServer(param: ["booking_id": bookingId])
       
    }
    
    func checkStatus(){
        if status == "1" {
            buttonEdit.isHidden = false
        }
    }

}
