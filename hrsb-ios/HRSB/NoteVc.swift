//
//  NoteVc.swift
//  HRSB
//
//  Created by Salman on 05/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class NoteVc: UIViewController {

    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    var ticketModel: TicketModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.makeRoundCorner(25)
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTap(_ sender: Any) {
        if txtNote.text!.isEmpty  {
            self.showAlert(title: "", message: "Please enter the note.")
            return
        }
        let endPoint = MethodName.upateNote
        let params = ["edit_type":"update_note", "ticket_id":ticketModel?.ticketId ?? "", "ticket_note": txtNote.text!]
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                guard let dict = data as? [String: Any] else {
                    return
                }
                if let status = dict["status"] as? String, status == "1" {
                    self?.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                        self?.navigationController?.popViewController(animated: true)
                    })
                } else {
                    self?.showAlert(title: "", message: dict["message"] as? String ?? "")
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
}
