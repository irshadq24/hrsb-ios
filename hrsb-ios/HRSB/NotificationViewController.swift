//
//  NotificationViewController.swift
//  HRSB
//
//  Created by BestWeb on 21/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class NotificationViewController: UIViewController,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
   var appBarViewController = MDCAppBarViewController()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
      //  self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        self.appBarViewController.headerView.backgroundColor = UIColor.white

        
        
        
        
        
        let menuItemImage = UIImage(named: "back.png")
        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        
        
        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        
        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                       style: .done,
                                       target: self,
                                       action: #selector(self.BackAction))
        self.navigationItem.leftBarButtonItem = menuItem
        
        self.navigationItem.title = "Notification"
        
        
        var navigationBarAppearace = UINavigationBar.appearance()


        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        
        


      //  self.appBarViewController.title = "Notification"
        
        
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    
    
    
    @objc func BackAction(){
        navigationController?.popViewController(animated: true)
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)
//
        
        
    }
    
    
    
    
    
}
