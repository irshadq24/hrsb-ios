//
//  EditHeadOfTaskViewController.swift
//  HRSB
//
//  Created by Javed Multani on 24/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditHeadOfTaskViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var projectArray : [ObjOperationProject] = [ObjOperationProject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "ProjectListTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectListTableViewCell")
        getProjectList()
        // Do any additional setup after loading the view.
    }
   // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.projectArray.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 93.0
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectListTableViewCell", for: indexPath) as! ProjectListTableViewCell
        let obj = self.projectArray[indexPath.row]
        cell.titleLabel.text = obj.projectTitle!
        cell.codeLabel.text = "Project Code: " + obj.code!
        cell.dateLabel.text = "Date of Closure: " + obj.dateOfClosure!
        cell.progressLabel.text = "Progress: \(obj.progress!)%"
        cell.statusLabel.text = "Status: " + obj.finalizedStatus!
        cell.contentView.backgroundColor = UIColor .clear
        cell.backgroundColor = UIColor .clear
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
         let vc = storyboard?.instantiateViewController(withIdentifier: "OperationEditProjectViewController")as! OperationEditProjectViewController
         vc.isFromHeadTask = true
        vc.project_id = self.projectArray[indexPath.row].projectId!
                     navigationController?.pushViewController(vc, animated: true)
     }
    //MARK: - button action
    @IBAction func buttonHandlerback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - API call
    
    func getProjectList(){
        Loader.show()
        
        ServiceHelper.sharedInstance.getOperationProjectList(type: "head_of_task") { (result, error) in
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.projectArray.removeAll()
            let arrayMain = responseDic["data"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationProject = ObjOperationProject.init(json: json)
                self.projectArray.append(obj)
            }
            self.tableView.reloadData()
        }
    }
}
