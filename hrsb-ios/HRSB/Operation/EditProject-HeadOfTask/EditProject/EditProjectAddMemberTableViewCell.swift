//
//  EditProjectAddMemberTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 31/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EditProjectAddMemberTableViewCell: UITableViewCell {
    @IBOutlet weak var addButton: UIButton!
    public var themeRedColor = UIColor(red:0.93, green:0.40, blue:0.36, alpha:1.0)
    override func awakeFromNib() {
        super.awakeFromNib()
        addButton.layer.cornerRadius = addButton.frame.height / 2.0
        addButton.clipsToBounds = true
        
        addButton.layer.borderColor = themeRedColor.cgColor
        addButton.layer.borderWidth = 1.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
