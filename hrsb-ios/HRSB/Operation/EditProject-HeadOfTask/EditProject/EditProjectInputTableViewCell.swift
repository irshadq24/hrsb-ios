//
//  EditProjectInputTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 31/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EditProjectInputTableViewCell: UITableViewCell {

    @IBOutlet weak var textFieldInput: UITextField!
    @IBOutlet weak var imageDropDown: UIImageView!
    @IBOutlet weak var dropdownButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
