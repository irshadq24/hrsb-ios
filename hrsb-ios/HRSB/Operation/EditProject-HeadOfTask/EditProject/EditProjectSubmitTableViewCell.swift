//
//  EditProjectSubmitTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 31/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EditProjectSubmitTableViewCell: UITableViewCell {
public var themeRedColor = UIColor(red:0.93, green:0.40, blue:0.36, alpha:1.0)
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var firstButon: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        firstButon.layer.cornerRadius = firstButon.frame.height / 2.0
               firstButon.clipsToBounds = true
           
        secondButton.layer.cornerRadius = secondButton.frame.height / 2.0
        secondButton.clipsToBounds = true
        
        secondButton.layer.borderColor = themeRedColor.cgColor
               secondButton.layer.borderWidth = 1.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
