//
//  EditProjectTypeTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 06/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class EditProjectTypeTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var tagCollectionView: UICollectionView!
    
    @IBOutlet weak var dropdownButton: UIButton!
    
    @IBOutlet weak var textFieldInput: UITextField!
    
    var selectedTypeTaskArray:[ObjOperationTypeTask] = [ObjOperationTypeTask]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tagCollectionView.register(UINib(nibName: "TypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TypeCollectionViewCell")
        
        tagCollectionView.delegate = self
        tagCollectionView.dataSource = self
        
        self.textFieldInput.layer.borderColor = UIColor.gray.cgColor
        self.textFieldInput.layer.borderWidth = 1.5
        
        self.textFieldInput.layer.cornerRadius = 8.0
        self.textFieldInput.clipsToBounds = true
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @objc func buttonTypeClicked(sender:UIButton) {
        NotificationCenter.default.post(name: .removeTag, object: "Javed is Best", userInfo: ["index": sender.tag])
    }
    func reloadData(selectedTypeTaskArray:[ObjOperationTypeTask]){
        self.selectedTypeTaskArray = selectedTypeTaskArray
        self.tagCollectionView.reloadData()
    }
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedTypeTaskArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeCollectionViewCell", for: indexPath as IndexPath) as! TypeCollectionViewCell
        cell.titleLabel.text = selectedTypeTaskArray[indexPath.item].typeName!
        cell.titleLabel.textColor = UIColor.black
        
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self,action:#selector(buttonTypeClicked(sender:)), for: .touchUpInside)
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = selectedTypeTaskArray[indexPath.item].typeName!
        let w = str.width(withConstrainedHeight: 30, font: UIFont.systemFont(ofSize: 13))
        return CGSize(width: w + 80, height: 40)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
