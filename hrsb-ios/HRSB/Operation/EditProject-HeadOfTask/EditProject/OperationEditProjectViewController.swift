//
//  OperationEditProjectViewController.swift
//  HRSB
//
//  Created by Javed Multani on 31/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class OperationEditProjectViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    var objTask:ObjOperationProjectTask? = nil
    var objProject:ObjOperationProjectDetails? = nil
    var project_id = ""
    
    
    var finalizedAssignArray:[ObjFinalizeAssign] = [ObjFinalizeAssign]()
    var empArray : [ObjOperationEmp] = [ObjOperationEmp]()
    var assignEmpArray : [ObjOperationAssignEmp] = [ObjOperationAssignEmp]()
    var typeTaskEmpArray :[ObjOperationTypeTaskEmp] = [ObjOperationTypeTaskEmp]()
    var typeTaskArray:[ObjOperationTypeTask] = [ObjOperationTypeTask]()
    
    
    var selectedEmpArray : [ObjOperationEmp] = [ObjOperationEmp]()
    var selectedTypeTaskArray:[ObjOperationTypeTaskEmp] = [ObjOperationTypeTaskEmp]()
    
    
    @IBOutlet weak var detailButton: UIButton!
    public var themeColor = UIColor(red:0.38, green:0.24, blue:0.56, alpha:1.0)
    @IBOutlet weak var headButton: UIButton!
    @IBOutlet weak var ediitTableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var isFromHeadWork = false
    var isFromHeadTask = false
    var isFromFinalize = false
    
    var isDetail = true
    var user_id = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.UserId)
    //dropdown
    
    let chooseEmp = DropDown()
    let chooseTypeTask = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseEmp,
            self.chooseTypeTask,
        ]
    }()
    
    var row = 0
    var selectedEmp = 0
    var selectedTaskType = 0
    
    var empName = ""
    var empID = ""
    
    var typeTaskName = ""
    var typeTaskID = ""
    
    var textTypeOfWork = ""
    var textHeadTask = ""
    
    var initialHeadOfTaskID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ediitTableView.delegate = self
        ediitTableView.dataSource = self
        
        // EditProjectSubmitTableViewCell-58
       
        ediitTableView.register(UINib(nibName: "GridHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "GridHeaderTableViewCell")
        ediitTableView.register(UINib(nibName: "GirdDataTableViewCell", bundle: nil), forCellReuseIdentifier: "GirdDataTableViewCell")
        ediitTableView.register(UINib(nibName: "EditProjectAddMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProjectAddMemberTableViewCell")
        ediitTableView.register(UINib(nibName: "EditProjectInputTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProjectInputTableViewCell")
        ediitTableView.register(UINib(nibName: "EditProjectSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProjectSubmitTableViewCell")
        ediitTableView.register(UINib(nibName: "DetailProjectTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectTitleTableViewCell")//137
        ediitTableView.register(UINib(nibName: "DetailProjectTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectTableViewCell")//70
        
        self.updateUI()
        self.getAllEmp()
        self.getProjectDetail(project_id: project_id)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
        if self.isFromHeadWork{
            self.getAssignEmp(type: "assigned_team_list", project_id: self.project_id)
            self.headButton.setTitle("Head of Work", for: .normal)
            self.titleLabel.text = "Edit Project"
            self.row = 4
            self.ediitTableView.reloadData()
            
            
        }else if self.isFromHeadTask{
            self.headButton.setTitle("Head of Task", for: .normal)
            self.titleLabel.text = "Edit Project"
            self.getTypeTaskEmp(type: "type_of_task", project_id: self.project_id, user_id:self.user_id, response_list: "head_of_task")
            self.row = 5
        }else{
            self.getAssignedForFinalized()
            self.headButton.setTitle("Finalize Task", for: .normal)
            self.titleLabel.text = "Project Details"
            self.getTypeTaskEmp(type: "type_of_task", project_id: self.project_id, user_id:self.user_id, response_list: "head_of_task")
            self.row = 5
            self.ediitTableView.reloadData()
        }
        }
        
        
        //=======HeadWork=======
        //
        // self.getAssignEmp(type: "assigned_team_list", project_id: project_id)//assigned_team_list /
        //self.getAllEmp()
        //self.getProjectDetail(project_id: project_id)
        //=============================
        
        
        //==============HeadTask=======
        //self.getTypeTaskEmp(type: "type_of_task", project_id: "37", user_id: "1", response_list: "head_of_task")
        //self.getAllEmp()
        //self.updateAllTaskDetails(type: "update_all_task_details", project_id: "37", task_details: ["23-16-21","23-14-21"])
        //=====>getAssignEmpTask()
        //http://13.250.181.252/api/operation/list_of_assigned_team_task
        //http://13.250.181.252/api/operation/save_assigned_work_team
        //http://13.250.181.252/api/operation/list_of_assigned_team_task
        //==================
        
        
        
        //==============Finalize======
        //self.getTypeTaskEmp(type: "type_of_task", project_id: "37", user_id: "1", response_list: "finalized_task")
        //self.getAllEmp()
        //self.updateAllTaskDetails(type: "update_all_task_details", project_id: "37", task_details: ["23-16-21","23-14-21"])
        
        //==================
    }
    //MARK: - DropDown
    func setupDropDowns(){
        self.setupChooseEmpDropDown()
        self.setupChooseTaskTypeDropDown()
        
    }
    func setupChooseEmpDropDown() {
        var arrayEmp = [String]()
        for obj in empArray{
            arrayEmp.append("\(obj.employeeName!)")
        }
        chooseEmp.anchorView = self.ediitTableView
        chooseEmp.bottomOffset = CGPoint(x: 0, y: 0)
        chooseEmp.dataSource = arrayEmp
        // Action triggered on selection
        chooseEmp.selectionAction = { [weak self] (index, item) in
            // self?.buttonMonth.setTitle(item, for: .normal)
            ///self?.monthValue = item
            self?.selectedEmp = index
            self!.empName = item
            self!.empID = (self?.empArray[index].employeeId)!
            self!.chooseEmp.hide()
            self!.ediitTableView.reloadData()
        }
        
    }
    func setupChooseTaskTypeDropDown() {
        var arrayEmp = [String]()
        for obj in typeTaskEmpArray{
            arrayEmp.append("\(obj.typeName!)")
        }
        chooseTypeTask.anchorView = self.ediitTableView
        chooseTypeTask.bottomOffset = CGPoint(x: 0, y: 0)
        chooseTypeTask.dataSource = arrayEmp
        // Action triggered on selection
        chooseTypeTask.selectionAction = { [weak self] (index, item) in
            // self?.buttonMonth.setTitle(item, for: .normal)
            ///self?.monthValue = item
            self!.selectedTaskType = index
            self!.typeTaskName = item
            self!.typeTaskID = (self?.typeTaskEmpArray[index].typeId)!
            self!.textHeadTask = (self?.typeTaskEmpArray[index].employeeName)!
            self!.chooseTypeTask.hide()
            self!.ediitTableView.reloadData()
        }
        
    }
    func updateUI(){
        if self.isDetail{
            self.detailButton.setTitleColor(UIColor.white, for: .normal)
            self.detailButton.backgroundColor = themeColor
            
            self.headButton.setTitleColor(themeColor, for: .normal)
            self.headButton.backgroundColor = UIColor.white
        }else{
            self.detailButton.setTitleColor(themeColor, for: .normal)
            self.detailButton.backgroundColor = UIColor.white
            
            self.headButton.setTitleColor(UIColor.white, for: .normal)
            self.headButton.backgroundColor = themeColor
        }
        self.ediitTableView.reloadData()
    }
    //MARK: - button action
    @objc func buttonSaveClicked(sender:UIButton) {
        if self.isFromHeadWork{
            var assign_work = [String]()
            for emp in selectedEmpArray{
                assign_work.append(emp.userId!)
            }
            
            self.saveAssignWorkTeam(type: "save_assigned_team", project_id: project_id, assign_work: assign_work)//["16"]
        }else if self.isFromHeadTask{
            var task_details = [String]()
    
            for i in 0..<selectedEmpArray.count{
                let task_detail = "\(selectedTypeTaskArray[i].typeId!)-\(selectedEmpArray[i].userId!)-\(selectedEmpArray[i].employeeName!)"
                task_details.append(task_detail)// ["23-16-21","23-14-21"]
            }
            
            self.updateAllTaskDetails(type: "update_all_task_details", project_id: project_id, task_details: task_details)// ["23-16-21","23-14-21"]
        }else{
            var task_details = [String]()
            
                    for i in 0..<selectedEmpArray.count{
                        let task_detail = "\(selectedTypeTaskArray[i].typeId!)-\(selectedEmpArray[i].userId!)"
                        task_details.append(task_detail)// ["23-16-21","23-14-21"]
                    }
                    
                    self.updateAllTaskDetails(type: "update_all_task_details", project_id: project_id, task_details: task_details)// ["23-16-21","23-14-21"]
        }
        
    }
    @objc func buttonFinalizeClicked(sender:UIButton) {
        if self.isFromHeadWork{
            self.finalizeWork(type: "finalized_status_update", project_id: project_id, user_id: user_id, finalized_status: "assign_work")
        }else if self.isFromHeadTask{
            self.finalizeWork(type: "finalized_status_update", project_id: project_id, user_id: user_id, finalized_status: "assign_task")
        }else{
             self.finalizeWork(type: "finalized_status_update", project_id: project_id, user_id: user_id, finalized_status: "finalized_task")
        }
        
    }
    @objc func buttonDeleteMember(sender:UIButton) {
        self.selectedEmpArray.remove(at: sender.tag)
        self.ediitTableView.reloadData()
        
    }
    @objc func buttonAddMember(sender:UIButton) {
        let obj = self.empArray[selectedEmp]
        self.selectedEmpArray.append(obj)
        self.ediitTableView.reloadData()
        if !self.isFromHeadWork{
            let obj = self.typeTaskEmpArray[selectedTaskType]
            self.selectedTypeTaskArray.append(obj)
        }
       
    }
    @objc func buttonDropdownClicked(sender:UIButton) {
        chooseEmp.show()
        
    }
    @objc func buttonTypeTaskDropdownClicked(sender:UIButton) {
        chooseTypeTask.show()
        
    }
    @IBAction func butonHandlerback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonHandlerDetails(_ sender: Any) {
        self.isDetail = true
        self.updateUI()
    }
    
    @IBAction func buttonHandlerHead(_ sender: Any) {
        self.isDetail = false
        self.updateUI()
    }
    //MARK: - textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = (textField.text ?? "") + string
        if self.isFromHeadWork{
            switch textField.tag
            {
            case 0:
                textTypeOfWork = kActualText;
                
            default:
                print("It is nothing");
            }
        }else{
            
            switch textField.tag
            {
            case 1:
                textHeadTask = kActualText;
                
            default:
                print("It is nothing");
            }
        }
        
        return true;
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if self.isDetail{
            return 10
        }else{
            
            var count = 0
            if selectedEmpArray.count > 0 {
                count = selectedEmpArray.count + row + 1//section
            }else{
                count = row
            }
            return count
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isDetail{
            if indexPath.row == 0{
                return 137.0
            }else{
                return 70.0
            }
            
        }else{
            var lastRow = 0
            if selectedEmpArray.count > 0 {
                lastRow = selectedEmpArray.count + row
            }else{
                lastRow = row - 1
            }
            if self.isFromHeadWork{
                
                //if emp selected then
                if selectedEmpArray.count > 0 {
                    if indexPath.row == 0 || indexPath.row == 1{
                        return 50
                    }else if indexPath.row == 2{
                        return 70
                    }else{
                        return 60
                    }
                }else{
                    if indexPath.row == 0 || indexPath.row == 1{
                        return 50
                    }else if indexPath.row == 2{
                        return 70
                    }else if indexPath.row == 3{
                        return 70
                    }else if indexPath.row == lastRow{
                        return 60
                    }else{
                        return 44
                    }
                }
                
                
            }else if self.isFromHeadTask{
                //if emp selected then
                if selectedEmpArray.count > 0 {
                    if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                        return 50
                    }else if indexPath.row == 3{
                        return 70
                    }else if indexPath.row == 4{
                        return 70
                    }else if indexPath.row == lastRow{
                        return 60
                    }else{
                        return 44
                    }
                }else{
                    if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                        return 50
                    }else if indexPath.row == 3{
                        return 70
                    }else{
                        return 60
                    }
                }
                
            }else{
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                    return 50
                }else if indexPath.row == 3{
                    return 70
                }else{
                    return 60
                }
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isDetail{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectTitleTableViewCell", for: indexPath) as! DetailProjectTitleTableViewCell
                
                cell.titleLabel.text = objProject?.projectTitle ?? ""
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrintDate = DateFormatter()
                dateFormatterPrintDate.dateFormat = "dd-MM-yyyy"
                
                let dateFormatterPrintTime = DateFormatter()
                dateFormatterPrintTime.dateFormat = "HH:mm a"
                
                if let date = dateFormatterGet.date(from: objProject?.dateTime ?? "") {
                    print(dateFormatterPrintDate.string(from: date))
                    cell.dateLabel.text = dateFormatterPrintDate.string(from: date)
                } else {
                    print("There was an error decoding the string")
                }
                
                if let date2 = dateFormatterGet.date(from: objProject?.dateTime ?? "") {
                    print(dateFormatterPrintTime.string(from: date2))
                    cell.timeLabel.text = dateFormatterPrintTime.string(from: date2)
                } else {
                    print("There was an error decoding the string")
                }
                
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectTableViewCell", for: indexPath) as! DetailProjectTableViewCell
                switch indexPath.row {
                case 1:
                    cell.titleLabel.text = "Project Code"
                    cell.valueLabel.text = objProject?.poNumber ?? ""
                case 2:
                    cell.titleLabel.text = "Operation Planner Update (OPU)"
                    cell.valueLabel.text = objProject?.opuName ?? ""
                case 3:
                    cell.titleLabel.text = "Type of Work"
                    cell.valueLabel.text = objProject?.typeOfWorkName ?? ""
                case 4:
                    cell.titleLabel.text = "Head of Work"
                    cell.valueLabel.text = objProject?.headOfWorkName ?? ""
                case 5:
                    cell.titleLabel.text = "Type of task"
                    cell.valueLabel.text = objProject?.typeOfTaskName ?? ""
                case 6:
                    cell.titleLabel.text = "Head of task"
                    cell.valueLabel.text = objProject?.headOfTaskName ?? ""
                case 7:
                    cell.titleLabel.text = "Status"
                    cell.valueLabel.text = objProject?.statusName ?? ""
                case 8:
                    cell.titleLabel.text = "Progress"
                    cell.valueLabel.text = objProject?.progress ?? ""
                case 9:
                    cell.titleLabel.text = "Attachment"
                    cell.valueLabel.text = objProject?.attachment ?? ""
                default:
                    print("do nothing")
                }
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
                
                return cell
            }
            
        }else{
            var lastRow = 0
            if selectedEmpArray.count > 0 {
                lastRow = selectedEmpArray.count + row
            }else{
                lastRow = row - 1
            }
            if self.isFromHeadWork{
//                var lastRow = 0
//                if selectedEmpArray.count > 0 {
//                    lastRow = selectedEmpArray.count + row
//                }else{
//                    lastRow = row - 1
//                }
                if selectedEmpArray.count > 0{
                    if indexPath.row == 0 || indexPath.row == 1{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectInputTableViewCell", for: indexPath) as! EditProjectInputTableViewCell
                        cell.textFieldInput.tag = indexPath.row
                        if indexPath.row == 0{
                            cell.dropdownButton.isHidden = true
                            cell.imageDropDown.isHidden = true
                            cell.textFieldInput.placeholder = "Type of Work"
                            cell.textFieldInput.text = objProject?.typeOfWorkName ?? ""
                            cell.textFieldInput.tag = indexPath.row
                            cell.textFieldInput.delegate = self
                        }else if indexPath.row == 1{
                            cell.dropdownButton.isHidden = false
                            cell.imageDropDown.isHidden = false
                            cell.textFieldInput.placeholder = "Select Project Team"
                            cell.dropdownButton.tag = indexPath.row
                            cell.textFieldInput.text = empName
                            cell.dropdownButton.addTarget(self,action:#selector(buttonDropdownClicked), for: .touchUpInside)
                            
                        }
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }else if indexPath.row == 2{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                        cell.addButton.addTarget(self,action:#selector(buttonAddMember), for: .touchUpInside)
                        
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.addButton.isUserInteractionEnabled = false
                        }else{
                            cell.addButton.isUserInteractionEnabled = true
                        }
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }else if indexPath.row == 3{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "GridHeaderTableViewCell", for: indexPath) as! GridHeaderTableViewCell
                        
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }else if indexPath.row == lastRow{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectSubmitTableViewCell", for: indexPath) as! EditProjectSubmitTableViewCell
                        cell.firstButon.tag = indexPath.row
                        cell.secondButton.tag = indexPath.row
                        cell.secondButton.addTarget(self,action:#selector(buttonSaveClicked), for: .touchUpInside)
                        cell.firstButon.addTarget(self,action:#selector(buttonFinalizeClicked), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.firstButon.isUserInteractionEnabled = false
                        }else{
                             cell.secondButton.isUserInteractionEnabled = true
                        }
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        
                        return cell
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "GirdDataTableViewCell", for: indexPath) as! GirdDataTableViewCell
                        let obj = self.selectedEmpArray[indexPath.row - 4]
                        cell.numLabrl.text = "\(indexPath.row - 3)"
                        cell.nameLabel.text = obj.employeeName
                        cell.idLabel.text = obj.employeeName
                        let tag = indexPath.row - 4
                        cell.deleteButton.tag = tag; cell.deleteButton.addTarget(self,action:#selector(buttonDeleteMember), for: .touchUpInside)
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }
                }else{
                    if indexPath.row == 0 || indexPath.row == 1{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectInputTableViewCell", for: indexPath) as! EditProjectInputTableViewCell
                        cell.textFieldInput.tag = indexPath.row
                        if indexPath.row == 0{
                            cell.dropdownButton.isHidden = true
                            cell.imageDropDown.isHidden = true
                            cell.textFieldInput.placeholder = "Type of Work"
                            cell.textFieldInput.text = objProject?.typeOfWorkName ?? ""
                            cell.textFieldInput.tag = indexPath.row
                            
                            cell.textFieldInput.delegate = self
                        }else if indexPath.row == 1{
                            cell.dropdownButton.isHidden = false
                            cell.imageDropDown.isHidden = false
                            cell.textFieldInput.placeholder = "Select Project Team"
                            cell.dropdownButton.tag = indexPath.row
                            cell.textFieldInput.text = empName
                            cell.dropdownButton.addTarget(self,action:#selector(buttonDropdownClicked), for: .touchUpInside)
                            
                        }
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }else if indexPath.row == 2{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                        cell.addButton.addTarget(self,action:#selector(buttonAddMember), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.addButton.isUserInteractionEnabled = false
                        }else{
                            cell.addButton.isUserInteractionEnabled = true
                        }

                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                        
                    }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectSubmitTableViewCell", for: indexPath) as! EditProjectSubmitTableViewCell
                        cell.firstButon.tag = indexPath.row
                        cell.secondButton.tag = indexPath.row
                        cell.secondButton.addTarget(self,action:#selector(buttonSaveClicked), for: .touchUpInside)
                        cell.firstButon.addTarget(self,action:#selector(buttonFinalizeClicked), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.firstButon.isUserInteractionEnabled = false
                             cell.secondButton.isUserInteractionEnabled = false
                        }else{
                            cell.firstButon.isUserInteractionEnabled = true
                             cell.secondButton.isUserInteractionEnabled = false
                            
                        }
                     
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell
                    }
                }
                
                
            }else{
                
                
                if self.selectedEmpArray.count > 0{
                    if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectInputTableViewCell", for: indexPath) as! EditProjectInputTableViewCell
                                     cell.textFieldInput.tag = indexPath.row
                                     if indexPath.row == 0{
                                         cell.dropdownButton.isHidden = false
                                         cell.imageDropDown.isHidden = false
                                         cell.textFieldInput.placeholder = "Type of Task"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = typeTaskName
                                         cell.dropdownButton.addTarget(self,action:#selector(buttonTypeTaskDropdownClicked), for: .touchUpInside)
                                     }else if indexPath.row == 1{
                                         cell.dropdownButton.isHidden = true
                                         cell.imageDropDown.isHidden = true
                                         cell.textFieldInput.placeholder = "Head of Task"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = textHeadTask
                                         cell.textFieldInput.tag = indexPath.row
                                         cell.textFieldInput.delegate = self
                                     }else if indexPath.row == 2{
                                         cell.dropdownButton.isHidden = false
                                         cell.imageDropDown.isHidden = false
                                         cell.textFieldInput.placeholder = "Select Project Team"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = empName
                                         cell.dropdownButton.addTarget(self,action:#selector(buttonDropdownClicked), for: .touchUpInside)
                                         
                                         
                                     }
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell
                                 }else if indexPath.row == 3{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                                     cell.addButton.addTarget(self,action:#selector(buttonAddMember), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.addButton.isUserInteractionEnabled = false
                        }else{
                            cell.addButton.isUserInteractionEnabled = true
                        }
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell
                    }else if indexPath.row == 4{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "GridHeaderTableViewCell", for: indexPath) as! GridHeaderTableViewCell
                                                             cell.contentView.backgroundColor = UIColor .clear
                                                            cell.backgroundColor = UIColor .clear
                                                            cell.selectionStyle = .none
                          return cell
                    }else if indexPath.row == lastRow{
                      
                        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectSubmitTableViewCell", for: indexPath) as! EditProjectSubmitTableViewCell
                        cell.firstButon.tag = indexPath.row
                        cell.secondButton.tag = indexPath.row
                        cell.secondButton.addTarget(self,action:#selector(buttonSaveClicked), for: .touchUpInside)
                        cell.firstButon.addTarget(self,action:#selector(buttonFinalizeClicked), for: .touchUpInside)
                        
                        if self.objProject?.finalizedStatus! == "assign_work"{
                            cell.firstButon.isUserInteractionEnabled = false
                            cell.secondButton.isUserInteractionEnabled = false
                        }else{
                            cell.firstButon.isUserInteractionEnabled = true
                            cell.secondButton.isUserInteractionEnabled = true
                        }
                        cell.contentView.backgroundColor = UIColor .clear
                        cell.backgroundColor = UIColor .clear
                        cell.selectionStyle = .none
                        
                        return cell

                                 }else{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "GirdDataTableViewCell", for: indexPath) as! GirdDataTableViewCell
                                     let obj = self.selectedEmpArray[indexPath.row - 5]
                                     cell.numLabrl.text = "\(indexPath.row - 4)"
                                     cell.nameLabel.text = obj.employeeName
                                     cell.idLabel.text = obj.employeeName
                                     let tag = indexPath.row - 5
                                     cell.deleteButton.tag = tag; cell.deleteButton.addTarget(self,action:#selector(buttonDeleteMember), for: .touchUpInside)
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell

                    }
                }else{
                    if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectInputTableViewCell", for: indexPath) as! EditProjectInputTableViewCell
                                     cell.textFieldInput.tag = indexPath.row
                                     if indexPath.row == 0{
                                         cell.dropdownButton.isHidden = false
                                         cell.imageDropDown.isHidden = false
                                         cell.textFieldInput.placeholder = "Type of Task"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = typeTaskName
                                         cell.dropdownButton.addTarget(self,action:#selector(buttonTypeTaskDropdownClicked), for: .touchUpInside)
                                     }else if indexPath.row == 1{
                                         cell.dropdownButton.isHidden = true
                                         cell.imageDropDown.isHidden = true
                                         cell.textFieldInput.placeholder = "Head of Task"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = textHeadTask
                                         cell.textFieldInput.tag = indexPath.row
                                         cell.textFieldInput.delegate = self
                                     }else if indexPath.row == 2{
                                         cell.dropdownButton.isHidden = false
                                         cell.imageDropDown.isHidden = false
                                         cell.textFieldInput.placeholder = "Select Project Team"
                                         cell.dropdownButton.tag = indexPath.row
                                         cell.textFieldInput.text = empName
                                         cell.dropdownButton.addTarget(self,action:#selector(buttonDropdownClicked), for: .touchUpInside)
                                         
                                         
                                     }
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell
                                 }else if indexPath.row == 3{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectAddMemberTableViewCell", for: indexPath) as! EditProjectAddMemberTableViewCell
                                     cell.addButton.addTarget(self,action:#selector(buttonAddMember), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                                                 cell.addButton.isUserInteractionEnabled = false
                                                
                                             }else{
                                                 cell.addButton.isUserInteractionEnabled = true
                                                 
                                             }
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell
                                 }else{
                                     let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectSubmitTableViewCell", for: indexPath) as! EditProjectSubmitTableViewCell
                                     cell.firstButon.tag = indexPath.row
                                     cell.secondButton.tag = indexPath.row
                                     cell.secondButton.addTarget(self,action:#selector(buttonSaveClicked), for: .touchUpInside)
                                     cell.firstButon.addTarget(self,action:#selector(buttonFinalizeClicked), for: .touchUpInside)
                        if self.objProject?.finalizedStatus! == "assign_work"{
                                                 cell.firstButon.isUserInteractionEnabled = false
                                                 cell.secondButton.isUserInteractionEnabled = false
                                             }else{
                                                 cell.firstButon.isUserInteractionEnabled = true
                                                 cell.secondButton.isUserInteractionEnabled = true
                                             }
                                     cell.contentView.backgroundColor = UIColor .clear
                                     cell.backgroundColor = UIColor .clear
                                     cell.selectionStyle = .none
                                     
                                     return cell
                                 }
                }
                
                
             
            }
        }
    }
    
    //MARK: - API call
    
    //    func getAssignEmpTask(type:String,project_id:String){
    //          Loader.show()
    //          ServiceHelper.sharedInstance.getOperationAssignEmployee(type: type, project_id: project_id) { (result, error) in
    //              Loader.hide()
    //              guard let responseArr =  result as? NSArray else {
    //                  CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
    //                  return
    //              }
    //              print(responseArr)
    //              //Emp
    //              self.assignEmpArray.removeAll()
    //              let swiftArray = responseArr as AnyObject as! [Any]
    //              for i in 0..<swiftArray.count {
    //                  let json = JSON(swiftArray[i])
    //                  let obj: ObjOperationAssignEmp = ObjOperationAssignEmp.init(json: json)
    //                  self.assignEmpArray.append(obj)
    //              }
    //          }
    //      }
    func updateAllTaskDetails(type:String,project_id:String,task_details:[String]){
        Loader.show()
        ServiceHelper.sharedInstance.getOperationUpdateAllTaskDetails(type: type, project_id: project_id, task_details: task_details) { (result, error) in
            Loader.hide()
            print(result as Any)
            guard let responseDic =  result as? [String:Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            if let msg:String = responseDic["message"] as? String{
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: msg)
            }
        }
    }
    
    func saveAssignWorkTeam(type:String,project_id:String,assign_work:[String]){
        Loader.show()
        print(assign_work)
        
        ServiceHelper.sharedInstance.getOperationSaveAssignWork(type: type, project_id: project_id, assign_work: assign_work) { (result, error) in
            Loader.hide()
            print(result!)
            guard let responseDic =  result as? [String:Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            if let msg:String = responseDic["message"] as? String{
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: msg)
            }
            
        }
    }
    func finalizeWork(type:String,project_id:String,user_id:String,finalized_status:String){
        Loader.show()
        
        ServiceHelper.sharedInstance.getOperationFinalizeWork(type: type, project_id: project_id, user_id:user_id,finalized_status: finalized_status) { (result, error) in
            Loader.hide()
            print(result!)
            guard let responseDic =  result as? [String:Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            if let msg:String = responseDic["message"] as? String{
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: msg)
            }
            
        }
    }
    func getTypeTaskEmp(type:String,project_id:String,user_id:String,response_list:String){
        Loader.show()
        
        ServiceHelper.sharedInstance.getOperationTaskTypeEmp(type: type, project_id: project_id, user_id: user_id, response_list: response_list) { (result, error) in
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            print(responseDic)
            self.typeTaskEmpArray.removeAll()
            let arrayMain = responseDic["data"] as! NSArray
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationTypeTaskEmp = ObjOperationTypeTaskEmp.init(json: json)
                if i == 0{
                    self.initialHeadOfTaskID = obj.employeeName!
                    self.getAssignForTask()
                }
                self.typeTaskEmpArray.append(obj)
            }
            self.setupDropDowns()
        }
        
        
    }
    func getAssignEmp(type:String,project_id:String){
        
        let dic = ["type":type,
                   "project_id":project_id] as [String:Any]
        
//        if self.isFromHeadWork{
//            dic = ["type":"assigned_team_list",
//            "project_id":project_id] as [String:Any]
//        }else if self.isFromHeadTask{
//            dic = ["type":"type",
//            "project_id":project_id,
//            "head_of_task_id": objProject?.headOfWorkId!] as [String:Any]
//        }else if self.isFromFinalize{
//            dic = ["type":"type",
//            "project_id":project_id] as [String:Any]
//        }
        
        Loader.show()
        ServiceHelper.sharedInstance.getOperationAssignEmployee(reqBody:dic) { (result, error) in
            Loader.hide()
            guard let responseArr =  result as? NSArray else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            print(responseArr)
            //Emp
            self.assignEmpArray.removeAll()
            let swiftArray = responseArr as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationAssignEmp = ObjOperationAssignEmp.init(json: json)
                self.assignEmpArray.append(obj)
            }
            
            for emp in self.assignEmpArray{
                let dic = ["employee_name":emp.employeeName,
                           "employee_id":emp.employeeId,
                           "user_id":self.user_id]
                
                let json = JSON(dic)
                let obj: ObjOperationEmp = ObjOperationEmp.init(json: json)
                self.selectedEmpArray.append(obj)
               // self.ediitTableView.reloadData()
                
            }
            self.ediitTableView.reloadData()
        }
    }
    func getAllEmp(){
        Loader.show()
        ServiceHelper.sharedInstance.getOperationAllEmployee { (result, error) in
            Loader.hide()
            
            guard let responseArr =  result as? NSArray else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            print(responseArr)
            //Emp
            self.empArray.removeAll()
            let swiftArray = responseArr as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationEmp = ObjOperationEmp.init(json: json)
                self.empArray.append(obj)
            }
            self.setupDropDowns()
        }
    }
    func getProjectDetail(project_id:String){
        Loader.show()
        ServiceHelper.sharedInstance.getOperationProjectDetail(project_id: project_id) { (result, error) in
            Loader.hide()
            
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            let data = responseDic["data"] as! [String : Any]
            let json = JSON(data)
            let obj: ObjOperationProjectDetails = ObjOperationProjectDetails.init(json: json)
            self.objProject = obj
            
            //task
            let arrayMain = data["task"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationProjectTask = ObjOperationProjectTask.init(json: json)
                self.objTask = obj
                
                let tdic = ["employee_name":obj.employeeName!,"type_name":obj.typeName!,"type_employee":obj.typeEmployee!,"type_id":obj.typeId!] as[String:Any]
                let json2 = JSON(tdic)
                let obj2: ObjOperationTypeTaskEmp = ObjOperationTypeTaskEmp.init(json: json2)
                self.selectedTypeTaskArray.append(obj2)
            }
            self.ediitTableView.reloadData()
        }
        
    }
    
    func getAssignForTask(){
        Loader.show()
        let dic = ["type":"list_of_assigned_team","project_id":self.project_id,"head_of_task_id":self.initialHeadOfTaskID] as [String:Any]
        
        ServiceHelper.sharedInstance.getFinalizedAssignList(parameter: dic) { (result, error) in
                  Loader.hide()
                  guard let responseDic =  result as? [String : Any] else {
                                 CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                                 return
                             }
                self.finalizedAssignArray.removeAll()
                  let arrayMain = responseDic["data"] as! NSArray
                  let swiftArray = arrayMain as AnyObject as! [Any]
                  for i in 0..<swiftArray.count {
                      let json = JSON(swiftArray[i])
                      let obj: ObjFinalizeAssign = ObjFinalizeAssign.init(json: json)
                      self.finalizedAssignArray.append(obj)
                  }
                  
                  //add in selected list
                  for emp in self.finalizedAssignArray{
                                let dic = ["employee_name":emp.assignedEmployeeName,
                                           "employee_id":emp.assignedEmployeeId,
                                           "user_id":self.user_id]
                                
                                let json = JSON(dic)
                                let obj: ObjOperationEmp = ObjOperationEmp.init(json: json)
                                self.selectedEmpArray.append(obj)
                               // self.ediitTableView.reloadData()
                                
                            }
                            self.ediitTableView.reloadData()
                  
              }
        
    }
    func getAssignedForFinalized(){
        Loader.show()
        let dic = ["type":"list_of_all_task_details","project_id":self.project_id] as [String:Any]
        
        ServiceHelper.sharedInstance.getFinalizedAssignList(parameter: dic) { (result, error) in
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                           CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                           return
                       }
            print(responseDic)
          self.finalizedAssignArray.removeAll()
            let arrayMain = responseDic["data"] as! NSArray
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjFinalizeAssign = ObjFinalizeAssign.init(json: json)
                self.finalizedAssignArray.append(obj)
            }
            
            //add in selected list
            for emp in self.finalizedAssignArray{
                          let dic = ["employee_name":emp.assignedEmployeeName,
                                     "employee_id":emp.assignedEmployeeId,
                                     "user_id":self.user_id]
                          
                          let json = JSON(dic)
                          let obj: ObjOperationEmp = ObjOperationEmp.init(json: json)
                          self.selectedEmpArray.append(obj)
                         // self.ediitTableView.reloadData()
                          
                      }
            print("=============================\n\n\n======== ",self.selectedEmpArray.count)
                      self.ediitTableView.reloadData()
            
        }
        
    }
}
