//
//  TypeCollectionViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 06/11/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var closeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.layer.borderColor = UIColor.gray.cgColor
        self.contentView.layer.borderWidth = 1.5
        
        self.contentView.layer.cornerRadius = 8.0
        self.contentView.clipsToBounds = true
        
    }

}
