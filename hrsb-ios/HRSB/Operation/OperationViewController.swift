//
//  OperationViewController.swift
//  HRSB
//
//  Created by BestWeb on 21/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class OperationViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate  {

    
    @IBOutlet weak var txt_opu: MDCTextField!
    
    
    @IBOutlet weak var closeVIewOne: UIButton!
    @IBOutlet weak var txt_ponumber: MDCTextField!
    
    @IBOutlet var view_one: UIView!
    
    @IBOutlet weak var Execution: UIButton!
    @IBOutlet weak var scroll_createproj: UIScrollView!
    
    @IBOutlet weak var txt_projname: MDCTextField!
    @IBOutlet weak var txt_task: MDCTextField!
    
    @IBOutlet weak var Claim: UIButton!
    
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var uplaod: UIButton!
    
    @IBOutlet weak var closeviewcreatepjoj: UIButton!
    @IBOutlet var view_corner: UIView!
    
    @IBOutlet weak var Add: UIButton!
    
    @IBOutlet var view_createproj: UIView!
    
    
    @IBOutlet weak var view_white: UIView!
    
    var appBarViewController = MDCAppBarViewController()
    
    
    var items = ["Project Title", "Project Title", "Project Title","Project Title","Project Title","Project Title"]

    
    let reuseIdentifier = "OperationCell"

    
    @IBOutlet weak var back: UIButton!
    
    
    
    @IBOutlet weak var tble: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        submit.layer.cornerRadius = 20


        
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
//        let menuItemImage = UIImage(named: "menu.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//        
//        
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//        
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)

        
        tble.dataSource = self
        tble.delegate = self
        scroll_createproj.delegate = self
        
        
        self.setheight()
        
        
        
        self.view.addSubview(view_corner)
        
        view_corner.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)


        
        addSlideMenuButton()
        addSlideMenuButton1()

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @objc func NotifyAction(){
        
        
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        
        navigationController?.pushViewController(presentedVC, animated: true)

        
       // present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    
    
    
    @IBAction func back_action(_ sender: Any) {
        navigationController?.popViewController(animated: true)

//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)
        
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! OperationCell
        
        
        
        
        
        cell.l1.text = items[indexPath.row]
        
        
        
        cell.l2.text = "dd"
        cell.l3.text = "tttt"
        
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 70
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        view.addSubview(view_one)
        
        
        view_one.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)

    
        
        
        
        
    }
    
    
    
    @IBAction func close_viewcreateproj(_ sender: Any) {
        
        
        view_createproj.removeFromSuperview()
        
        
        
        
    }
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width:Int(Widht) , height: (70 * items.count))
        
        

        
        
    }

    
    
    
    
    
    @IBAction func Add_Action(_ sender: Any) {
        
        view.addSubview(view_createproj)
        
        
        view_createproj.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
        
        
        
        let pos = submit.frame.origin.y
        
        let height = submit.frame.size.height
        let sizeOfContent = height + pos + 20
        
        
        print("dsfdf",sizeOfContent)
        
        scroll_createproj.contentSize.height = 1000
        
        
        scroll_createproj.contentSize.width = view.frame.width

        submit.layer.cornerRadius = 20
        
    }
    
    
   
    
    @IBAction func Upload_Action(_ sender: Any) {
    }
    
    
    
    
    
    @IBAction func submit_Action(_ sender: Any) {
    }
    
    
    
    @IBAction func Execution_Action(_ sender: Any) {
        
        
        let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ExecutionVC)
        //self.present(HrVc, animated: true, completion: nil)
        navigationController?.pushViewController(HrVc, animated: true)

    }
    
    
    
    @IBAction func Claim_Action(_ sender: Any) {
        
        
        let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ClaimVC)
      //  self.present(HrVc, animated: true, completion: nil)
        navigationController?.pushViewController(HrVc, animated: true)

        
    }
    
    
    
    @IBAction func CloseView_Action(_ sender: Any) {
        
        view_one.removeFromSuperview()
        
        
    }
    
    
    
    
    
    
    

}
