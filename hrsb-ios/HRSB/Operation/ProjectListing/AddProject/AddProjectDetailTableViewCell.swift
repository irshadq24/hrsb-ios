//
//  AddProjectDetailTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 26/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddProjectDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var imageDropdown: UIImageView!
    
    @IBOutlet weak var dropdownButton: UIButton!
    @IBOutlet weak var textFieldValue: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
