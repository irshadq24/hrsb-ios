//
//  AddProjectSubmitTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 26/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class AddProjectSubmitTableViewCell: UITableViewCell {
    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.submitButton.layer.cornerRadius = 20.0
        self.submitButton.clipsToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
