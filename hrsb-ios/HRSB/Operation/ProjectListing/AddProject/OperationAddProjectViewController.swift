//
//  OperationAddProjectViewController.swift
//  HRSB
//
//  Created by Javed Multani on 26/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class OperationAddProjectViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var objProject:ObjOperationProjectDetails? = nil
     var objTask:ObjOperationProjectTask? = nil
    
    @IBOutlet weak var titleLabel: UILabel!
    var imagePicker = UIImagePickerController()
    var chooseImage = UIImage(named:"addImage")
    
    var isEdit = false
    var project_id = ""
    
    
    var projectTitle = ""
    var projectCode = ""
    var OPU = ""
    var OPUID = ""
    var typeWork = ""
    var typeTask = ""
    var typeWorkID = ""
    var typeTaskID = ""
    var headWork = ""
    var headTask = ""
    var status = ""
    var statusID = ""
    var progress = ""
    
    var operationPlannerArray:[ObjOperationPlanner] = [ObjOperationPlanner]()
    var typeTaskArray:[ObjOperationTypeTask] = [ObjOperationTypeTask]()
    var statusArray:[ObjOperationStatus] = [ObjOperationStatus]()
    var typeArray:[ObjOperationType] = [ObjOperationType]()
    var typeWorkArray:[ObjOperationTypeWork] = [ObjOperationTypeWork]()
    
       var selectedTypeTaskArray:[ObjOperationTypeTask] = [ObjOperationTypeTask]()
    var selectedTypeHead:[String] = [String]()
    //dropdown
    
    let chooseOPU = DropDown()
    let chooseTypeOfWork = DropDown()
    let chooseTypeOfTask = DropDown()
    let chooseStatus = DropDown()
    
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseOPU,
            self.chooseTypeOfWork,
             self.chooseTypeOfTask,
            self.chooseStatus
        ]
    }()
    
    
    @IBOutlet weak var addTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView.delegate = self
        addTableView.dataSource = self
        
         addTableView.register(UINib(nibName: "EditProjectTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "EditProjectTypeTableViewCell")
        addTableView.register(UINib(nibName: "AddProjectDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectDetailTableViewCell")
        addTableView.register(UINib(nibName: "AddProjectSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectSubmitTableViewCell")
        addTableView.register(UINib(nibName: "AddProjectUploadTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectUploadTableViewCell")
        addTableView.register(UINib(nibName: "AddProjectTypeTaskTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectTypeTaskTableViewCell")
        
        self.getDropDown()
        
        if self.isEdit{
            
            self.titleLabel.text = "Edit"
            projectTitle = (self.objProject?.projectTitle!)!
              projectCode = self.objProject?.poNumber ?? ""
              OPU = self.objProject?.opuName ?? ""
              OPUID = self.objProject?.opuId ?? ""
              typeWork = self.objProject?.typeOfWorkName ?? ""
              typeTask =  self.objTask?.typeName ?? ""
              typeWorkID = self.objProject?.typeOfWorkId ?? ""
              typeTaskID =  self.objTask?.typeId ?? ""
              headWork = self.objProject?.headOfWorkName ?? ""
              headTask = self.objProject?.headOfTaskName ?? ""
              status = self.objProject?.statusName ?? ""
              statusID = self.objProject?.statusId ?? ""
              progress = self.objProject?.progress ?? ""
              project_id = self.objProject?.projectId ?? ""
        
            
            let a = UIImageView()
                        a.kf.setImage(with: URL(string: self.objProject?.urlPath ?? ""), completionHandler: { image, _, _, _ in
                           if image != nil
                           {
                               self.chooseImage = image
                              
                               
                           }else{
                               self.chooseImage =  UIImage(named:"addImage")
                               
                           }
                         
                                      
                                  })
            
            for i in 0..<self.selectedTypeTaskArray.count{
                 self.selectedTypeHead.append((self.selectedTypeTaskArray[i].typeEmployeeName)!)
            }
            self.updateType()
            self.addTableView.reloadData()
        }else{
            self.titleLabel.text = "Create Project"
            //do nothing...
        }
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(myFunction), name: .removeTag, object: nil)

        
    }
    //MARK: - Notification
    @objc func myFunction(notificaiont: Notification) {
        print(notificaiont.object ?? "") //myObject
        print(notificaiont.userInfo ?? "") //[AnyHashable("key"): "Value"]
        let i = notificaiont.userInfo!["index"] as! Int
        self.selectedTypeTaskArray.remove(at: i)
        self.selectedTypeHead.remove(at: i)
        self.updateType()
        self.addTableView.reloadData()
        
    }
    func updateType(){
        let joined = selectedTypeHead.joined(separator: ", ")
        self.headTask = joined
    }
    //MARK: - DropDown
    func setupDropDowns(){
        setupChooseOPUDropDown()
        setupChooseWorkDropDown()
        //setupChooseStatusDropDown()
        setupChooseTaskDropDown()
    }
    func setupChooseOPUDropDown() {
        var arrayMonth = [String]()
        for obj in operationPlannerArray{
            arrayMonth.append("\(obj.opuName!)")
        }
        chooseOPU.anchorView = self.addTableView
        chooseOPU.bottomOffset = CGPoint(x: 0, y: 0)
        chooseOPU.dataSource = arrayMonth
        // Action triggered on selection
        chooseOPU.selectionAction = { [weak self] (index, item) in
            // self?.buttonMonth.setTitle(item, for: .normal)
            ///self?.monthValue = item
            self!.OPU = item
            self!.OPUID = (self?.operationPlannerArray[index].opuId)!
            self!.chooseOPU.hide()
            self!.addTableView.reloadData()
        }
        
    }
    func setupChooseWorkDropDown() {
        var arrayMonth = [String]()
        for obj in typeWorkArray{
            arrayMonth.append("\(obj.typeName!)")
        }
        chooseTypeOfWork.anchorView = self.addTableView
        chooseTypeOfWork.bottomOffset = CGPoint(x: 0, y: 0)
        chooseTypeOfWork.dataSource = arrayMonth
        // Action triggered on selection
        chooseTypeOfWork.selectionAction = { [weak self] (index, item) in
            // self?.buttonMonth.setTitle(item, for: .normal)
            ///self?.monthValue = item
            self!.typeWork = item
            self!.typeWorkID = (self?.typeWorkArray[index].typeId)!
            self!.headWork = (self?.typeWorkArray[index].typeEmployeeName)!
            self!.chooseTypeOfWork.hide()
            self!.addTableView.reloadData()
        }
        
    }
    func setupChooseTaskDropDown() {
          var arrayMonth = [String]()
          for obj in typeTaskArray{
              arrayMonth.append("\(obj.typeName!)")
          }
          chooseTypeOfTask.anchorView = self.addTableView
          chooseTypeOfTask.bottomOffset = CGPoint(x: 0, y: 0)
          chooseTypeOfTask.dataSource = arrayMonth
          // Action triggered on selection
          chooseTypeOfTask.selectionAction = { [weak self] (index, item) in
              // self?.buttonMonth.setTitle(item, for: .normal)
              ///self?.monthValue = item
              self!.typeTask = item
              self!.typeTaskID = (self?.typeTaskArray[index].typeId)!
               //self!.headTask = (self?.typeTaskArray[index].typeEmployeeName)!
            self?.selectedTypeTaskArray.append((self?.typeTaskArray[index])!)
            self?.selectedTypeHead.append((self?.typeTaskArray[index].typeEmployeeName)!)
              self!.chooseTypeOfWork.hide()
            self!.updateType()
              self!.addTableView.reloadData()
          }
          
      }
    func setupChooseStatusDropDown(vw:UIView) {
        var arrayMonth = [String]()
        for obj in statusArray{
            arrayMonth.append("\(obj.status!)")
        }
     
        chooseStatus.anchorView = vw
        chooseStatus.bottomOffset = CGPoint(x: 0, y: 0)
        chooseStatus.dataSource = arrayMonth
        // Action triggered on selection
        chooseStatus.selectionAction = { [weak self] (index, item) in
            // self?.buttonMonth.setTitle(item, for: .normal)
            ///self?.monthValue = item
            self!.status = item
            self!.statusID = (self?.statusArray[index].statusId)!
            self!.chooseStatus.hide()
            self!.addTableView.reloadData()
        }
        
    }
    //MARK: - button action
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func buttonAddImageClicked(sender:UIButton) {
        self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
            if selected == 1 {
                self.openCamera()
            } else if selected == 2 {
                self.openGallery()
            }
        }
    }
      @objc func buttonTypeClicked(sender:UIButton) {
        chooseTypeOfTask.show()
    }
    @objc func buttonClicked(sender:UIButton) {
        
        let buttonRow = sender.tag
        switch buttonRow
        {
    
        case 2:
            chooseOPU.show()
        case 3:
            chooseTypeOfWork.show()
        case 7:
            chooseStatus.show()
        default:
            print("do nothing")
        }
        
    }
    @objc func buttonSubmitClicked(sender:UIButton) {
       if self.chooseImage == nil {
         DisplayBanner.show(message: "please select the file")
            return
        }
        if self.isEdit{
             self.editProject(image: self.chooseImage!)
        }else{
            self.addProject(image: self.chooseImage!)
        }
        
    }
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 11
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 9{
            return 100
        }else if indexPath.row == 5{
            return 120
           // return 85.0
        }else{
            return 65.0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 10{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectSubmitTableViewCell", for: indexPath) as! AddProjectSubmitTableViewCell
            cell.submitButton.addTarget(self,action:#selector(buttonSubmitClicked), for: .touchUpInside)
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
        if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectUploadTableViewCell", for: indexPath) as! AddProjectUploadTableViewCell
         
            cell.buttonAdd.addTarget(self,action:#selector(buttonAddImageClicked(sender:)), for: .touchUpInside)
            if self.isEdit{
                   cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
            }else{
                   cell.buttonAdd.setBackgroundImage(self.chooseImage!, for: .normal)
            }
            
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.row == 5 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectTypeTaskTableViewCell", for: indexPath) as! AddProjectTypeTaskTableViewCell
//              cell.dropdownButton.tag = indexPath.row
//            cell.dropdownButton.addTarget(self,action:#selector(buttonTypeClicked(sender:)), for: .touchUpInside)
//             cell.textFieldType.placeholder = ""
//             cell.textFieldType.text = typeTask
//            cell.contentView.backgroundColor = UIColor .clear
//            cell.backgroundColor = UIColor .clear
//            cell.selectionStyle = .none
//
//            return cell
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProjectTypeTableViewCell", for: indexPath) as! EditProjectTypeTableViewCell
                          cell.dropdownButton.tag = indexPath.row
                        cell.dropdownButton.addTarget(self,action:#selector(buttonTypeClicked(sender:)), for: .touchUpInside)
            cell.reloadData(selectedTypeTaskArray: selectedTypeTaskArray)
                     //cell.textFieldType.placeholder = ""
                     //cell.textFieldInput.text = headTask
            
//              cell.dropdownButton.tag = indexPath.row
//            cell.dropdownButton.addTarget(self,action:#selector(buttonTypeClicked(sender:)), for: .touchUpInside)
//             cell.textFieldType.placeholder = ""
//             cell.textFieldType.text = typeTask
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectDetailTableViewCell", for: indexPath) as! AddProjectDetailTableViewCell
            cell.dropdownButton.tag = indexPath.row
            
            cell.dropdownButton.addTarget(self,action:#selector(buttonClicked(sender:)), for: .touchUpInside)
            cell.dropdownButton.isHidden = true
            cell.imageDropdown.isHidden = true
            cell.textFieldValue.tag = indexPath.row
            if indexPath.row == 0{
                cell.textFieldValue.placeholder = "Project title"
                cell.textFieldValue.text = projectTitle
            }else if indexPath.row == 1{
                cell.textFieldValue.placeholder = "Project Code"
                cell.textFieldValue.text = projectCode
            }else if indexPath.row == 2{
                cell.dropdownButton.isHidden = false
                cell.imageDropdown.isHidden = false
                cell.textFieldValue.placeholder = "Self Operation Planner Update(OPU)"
                cell.textFieldValue.text = OPU
            }else if indexPath.row == 3{
                cell.dropdownButton.isHidden = false
                cell.imageDropdown.isHidden = false
                cell.textFieldValue.placeholder = "Select type of work"
                cell.textFieldValue.text = typeWork
            }else if indexPath.row == 4{
                cell.textFieldValue.placeholder = "Head of work(name)"
                   cell.textFieldValue.text = headWork
            }else if indexPath.row == 5{
                cell.dropdownButton.isHidden = false
                cell.imageDropdown.isHidden = false
                cell.textFieldValue.placeholder = ""
                
                cell.textFieldValue.text = typeTask
            }else if indexPath.row == 6{
                cell.textFieldValue.placeholder = "Head of task(name)"
                cell.textFieldValue.text = headTask
            }else if indexPath.row == 7{
                cell.dropdownButton.isHidden = false
                cell.imageDropdown.isHidden = false
                cell.textFieldValue.placeholder = "Select status for this project"
                cell.textFieldValue.text = status
                
                if self.statusArray.count > 0{
                    self.setupChooseStatusDropDown(vw: cell.contentView)
                }
                
                
            }else if indexPath.row == 8{
                cell.textFieldValue.placeholder = "Progress(%)"
                cell.textFieldValue.text = progress
            }
            cell.textFieldValue.delegate = self
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    //MARK: - textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = (textField.text ?? "") + string
        switch textField.tag
        {
        case 0:
            projectTitle = kActualText;
        case 1:
            projectCode = kActualText;
        case 2:
            OPU = kActualText;
        case 3:
            typeWork = kActualText;
        case 4:
           // headWork = kActualText;
            print("ok")
        case 5:
            print(kActualText)
        case 6:
           // headTask = kActualText;
            print("ok")
        case 7:
            status = kActualText;
        case 8:
            progress = kActualText;
        default:
            print("It is nothing");
        }
        return true;
    }
    
    //MARK: - API call
    func addProject(image:UIImage){
        Loader.show()
        let endPoint = Constant.ServiceApi.OperationAddProject
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        
        var selectedTypeTasksID = [String]()
        
        
        for type in selectedTypeTaskArray{
            selectedTypeTasksID.append(type.typeId!)
        }
        
         let typeTasksID = selectedTypeTasksID.joined(separator: ",")
        
        
        var params = [String: String]()
        params["type"] = "add_operation_project"
        params["project_title"] = projectTitle
        params["po_number"] = projectCode
        params["status"] = statusID
        params["type_of_work"] = typeWorkID
        params["type_of_task"] = typeTasksID//typeTaskID
        params["user_id"] = userID
        params["progress"] = progress
        params["opu"] = OPUID
            
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["project_file": image], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
    }
    func editProject(image:UIImage){
        Loader.show()
        let endPoint = Constant.ServiceApi.OperationEditProject
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        var params = [String: String]()
        params["type"] = "edit_operation_project"
        params["project_title"] = projectTitle
        params["po_number"] = projectCode
        params["status"] = statusID
        params["type_of_work"] = typeWorkID
        params["type_of_task"] = typeTaskID
        params["user_id"] = userID
        params["progress"] = progress
        params["opu"] = OPUID
        params["project_id"] = self.project_id
        
        ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["project_file": image], unReachable: {
            print("no internet")
            Loader.hide()
        }) { (result, progress) in
            Loader.hide()
            guard let dict = result else {
                return
            }
            if let status = dict["status"] as? String, status == "1" {
                self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.showAlert(title: "", message: dict["message"] as? String ?? "")
            }
            print(result)
        }
    }
    func getDropDown(){
        Loader.show()
        ServiceHelper.sharedInstance.getOperationDropdown { (result, error) in
            print(result)
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            
            //ObjOperationPlanner
            self.operationPlannerArray.removeAll()
            let arrayMain = responseDic["operation_planner_update"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationPlanner = ObjOperationPlanner.init(json: json)
                self.operationPlannerArray.append(obj)
            }
            //status
            self.statusArray.removeAll()
            let arrayMain1 = responseDic["status"] as! [NSArray]
            let swiftArray1 = arrayMain1 as AnyObject as! [Any]
            for i in 0..<swiftArray1.count {
                let json = JSON(swiftArray1[i])
                let obj: ObjOperationStatus = ObjOperationStatus.init(json: json)
                self.statusArray.append(obj)
            }
            //type
            self.typeArray.removeAll()
            let arrayMain2 = responseDic["type"] as! [NSArray]
            let swiftArray2 = arrayMain2 as AnyObject as! [Any]
            for i in 0..<swiftArray2.count {
                let json = JSON(swiftArray2[i])
                let obj: ObjOperationType = ObjOperationType.init(json: json)
                self.typeArray.append(obj)
            }
            //type_of_work
            self.typeWorkArray.removeAll()
            let arrayMain3 = responseDic["type_of_work"] as! [NSArray]
            let swiftArray3 = arrayMain3 as AnyObject as! [Any]
            for i in 0..<swiftArray3.count {
                let json = JSON(swiftArray3[i])
                let obj: ObjOperationTypeWork = ObjOperationTypeWork.init(json: json)
                self.typeWorkArray.append(obj)
            }
            
            //type_of_task
            self.typeTaskArray.removeAll()
            let arrayMain4 = responseDic["type_of_task"] as! [NSArray]
            let swiftArray4 = arrayMain4 as AnyObject as! [Any]
            for i in 0..<swiftArray4.count {
                let json = JSON(swiftArray4[i])
                let obj: ObjOperationTypeTask = ObjOperationTypeTask.init(json: json)
                self.typeTaskArray.append(obj)
            }
            self.setupDropDowns()
            self.addTableView.reloadData()
        }
    }
    
    //MARK: - UIImagePicker delegate
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        if let image = info[.editedImage] as? UIImage{
            self.chooseImage = image
            self.addTableView.reloadData()
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}
extension Notification.Name {
    static let removeTag = Notification.Name("removeTag")
}
