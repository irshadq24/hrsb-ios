//
//  ClientSatisfactoryFormViewController.swift
//  HRSB
//
//  Created by Javed Multani on 30/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON


class ClientSatisfactoryFormViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    
    var objTask:ObjOperationProjectTask? = nil
    var objProject:ObjOperationProjectDetails? = nil
    var project_id = ""
    
    
    var companyName = ""
    var companyTitle = ""
    var contractNumber = ""
    var  projectCode = ""
    var  contractTitle = ""
    var personCharge = ""
    var department = ""
    var position = ""
    var progress = ""
    

    let arrRating = NSMutableArray()
    
    var ratingArray:[ObjRating] = [ObjRating]()
    @IBOutlet weak var formTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formTableView.delegate = self
        formTableView.dataSource = self
        formTableView.register(UINib(nibName: "FormHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "FormHeaderTableViewCell")
        formTableView.register(UINib(nibName: "FormInputTableViewCell", bundle: nil), forCellReuseIdentifier: "FormInputTableViewCell")
        formTableView.register(UINib(nibName: "FormStarTableViewCell", bundle: nil), forCellReuseIdentifier: "FormStarTableViewCell")
        formTableView.register(UINib(nibName: "AddProjectSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectSubmitTableViewCell")
        // Do any additional setup after loading the view.
        
        // FormHeaderTableViewCell - 60
        //  FormInputTableViewCell - 50
        //FormStarTableViewCell-100
        self.readForm()
        
        for i in 0..<20{
            let dic = ["rating_id":"\(i+1)","rating_value":"1"]
            arrRating.add(dic)
           
        }
      
    }
    func json(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
    //MARK: - button action
    @objc func buttonSubmitClicked(sender:UIButton) {
        
        self.addForm()
    }
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  35
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        switch indexPath.row {
        case 0:
            return 60.0
        case 1...8:
            return 50.0
        case 9:
            return 60.0
        case 10...13:
            return 100.0
        case 14:
            return 60.0
        case 15...17:
            return 100.0
        case 18:
            return 60.0
        case 19...23:
            return 100.0
        case 24:
            return 60.0
        case 25...28:
            return 100.0
        case 29:
            return 60.0
        case 30...33:
            return 100.0
        case 34:
            return 65.0
        default:
            print("Fallback option")
            return 0.0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Performance Questionnaire"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 1...8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormInputTableViewCell", for: indexPath) as! FormInputTableViewCell
            
            cell.textFieldInput.tag = indexPath.row
            cell.textFieldInput.delegate = self
            switch indexPath.row {
            case 1:
                cell.textFieldInput.placeholder = "Company Name"
                cell.textFieldInput.text = companyName
            case 2:
                cell.textFieldInput.placeholder = "Contract Refrence Number"
                cell.textFieldInput.text = contractNumber
            case 3:
                cell.textFieldInput.placeholder = "Project Code"
                cell.textFieldInput.text = projectCode
            case 4:
                cell.textFieldInput.placeholder = "Contract Title"
                cell.textFieldInput.text = contractTitle
            case 5:
                cell.textFieldInput.placeholder = "Person In Charge(Client)"
                cell.textFieldInput.text = personCharge
            case 6:
                cell.textFieldInput.placeholder = "Department"
                cell.textFieldInput.text = department
            case 7:
                cell.textFieldInput.placeholder = "Position"
                cell.textFieldInput.text = position
            case 8:
                cell.textFieldInput.placeholder = "Progress Completion(%)"
                cell.textFieldInput.text = progress
                
            default:
                print("yes")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Overall"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 10...13:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormStarTableViewCell", for: indexPath) as! FormStarTableViewCell
            cell.starView.didFinishTouchingCosmos = { rating in
                print("rate touched:",indexPath.row-10)
                print("rating:",rating)
                self.tapRating(index: indexPath.row-10, rating: rating)
            }
            let dic = self.arrRating[indexPath.row-10] as! [String:Any]
                       let rate = dic["rating_value"] as! String
                       cell.starView.rating = Double(rate)!
            cell.starView.tag = indexPath.row
            switch indexPath.row {
            case 10:
                cell.titleLabel.text = "Entertainment"
                case 11:
                cell.titleLabel.text = "Traveling"
                case 12:
                cell.titleLabel.text = "Medical"
                case 13:
                cell.titleLabel.text = "Others"
            default:
                print("Code")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 14:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Health, Safety & Environment"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 15...17:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormStarTableViewCell", for: indexPath) as! FormStarTableViewCell
          cell.starView.didFinishTouchingCosmos = { rating in
                        print("rate touched:",indexPath.row-15)
                        print("rating:",rating)
                        self.tapRating(index: indexPath.row-15, rating: rating)
                    }
             let dic = self.arrRating[indexPath.row-15] as! [String:Any]
                       let rate = dic["rating_value"] as! String
                       cell.starView.rating = Double(rate)!
            cell.starView.tag = indexPath.row
            switch indexPath.row {
            case 15:
                cell.titleLabel.text = "Safety & Health Performance"
                case 16:
                cell.titleLabel.text = "Environmental Performance"
                case 17:
                cell.titleLabel.text = "Housekeeping Performance"
               
            default:
                print("Code")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 18:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Quality"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 19...23:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormStarTableViewCell", for: indexPath) as! FormStarTableViewCell
              let dic = self.arrRating[indexPath.row-19] as! [String:Any]
                      let rate = dic["rating_value"] as! String
                      cell.starView.rating = Double(rate)!
            cell.starView.didFinishTouchingCosmos = { rating in
                          print("rate touched:",indexPath.row-19)
                          print("rating:",rating)
                          self.tapRating(index: indexPath.row-19, rating: rating)
                      }
            cell.starView.tag = indexPath.row
            switch indexPath.row {
            case 19:
                cell.titleLabel.text = "Technical Knowledge & Experiences"
                case 20:
                cell.titleLabel.text = "Understand Overall Projects Requirements"
                case 21:
                cell.titleLabel.text = "Quality Performance( Inspections)"
               case 22:
               cell.titleLabel.text = "Reports & Certificates Provided Sufficiently"
                case 23:
                cell.titleLabel.text = "Timely Delivery"
            default:
                print("Code")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 24:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Provision of Resources"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 25...28:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormStarTableViewCell", for: indexPath) as! FormStarTableViewCell
            cell.starView.didFinishTouchingCosmos = { rating in
                          print("rate touched:",indexPath.row-25)
                          print("rating:",rating)
                          self.tapRating(index: indexPath.row-25, rating: rating)
                      }
              let dic = self.arrRating[indexPath.row-25] as! [String:Any]
                      let rate = dic["rating_value"] as! String
                      cell.starView.rating = Double(rate)!
            cell.starView.tag = indexPath.row
            switch indexPath.row {
            case 25:
                cell.titleLabel.text = "Key Personnel Competency/Professionalism"
                case 26:
                cell.titleLabel.text = "Manpower Sufficiency"
                case 27:
                cell.titleLabel.text = "Tool & Equipment Performance"
               case 28:
               cell.titleLabel.text = "Resources Deliverability/Capacity"
                
            default:
                print("Code")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 29:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = "Others"
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 30...33:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormStarTableViewCell", for: indexPath) as! FormStarTableViewCell
            cell.starView.didFinishTouchingCosmos = { rating in
                          print("rate touched:",indexPath.row-30)
                          print("rating:",rating)
                          self.tapRating(index: indexPath.row-30, rating: rating)
                      }
            
            let dic = self.arrRating[indexPath.row-30] as! [String:Any]
            let rate = dic["rating_value"] as! String
            cell.starView.rating = Double(rate)!
            cell.starView.tag = indexPath.row
            switch indexPath.row {
            case 30:
                cell.titleLabel.text = "Mobilization/De-Mobilization Performance"
                case 31:
                cell.titleLabel.text = "Do you satisfy with our services provided?"
                case 32:
                cell.titleLabel.text = "Will you utilize out services again?"
               case 33:
               cell.titleLabel.text = "Would you recommend our service to other company?"
                
            default:
                print("Code")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        case 34:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectSubmitTableViewCell", for: indexPath) as! AddProjectSubmitTableViewCell
            cell.submitButton.addTarget(self,action:#selector(buttonSubmitClicked), for: .touchUpInside)
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormHeaderTableViewCell", for: indexPath) as! FormHeaderTableViewCell
            cell.titleLabel.text = " "
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
        
        
    }
    //MARK: - textfield delegate
     func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
         let kActualText = (textField.text ?? "") + string
         switch textField.tag
         {
         case 1:
             companyName = kActualText;
         case 2:
             contractTitle = kActualText;
         case 3:
            projectCode = kActualText;
             
         case 4:
            contractNumber = kActualText;
             
         case 5:
            personCharge = kActualText;
            
        case 6:
            department = kActualText;
            
             print(kActualText)
         case 7:
            position = kActualText;
          
         case 8:
              progress = kActualText;
         default:
             print("It is nothing");
         }
         return true;
     }
    
    func tapRating(index:Int,rating:Double){
        let dic = ["rating_id":"\(index+1)","rating_value":"\(rating)"]
        self.arrRating.replaceObject(at: index, with: dic)
        
        
    }
    //MARK :  - API Call
    func addForm(){
        Loader.show()
        //print("\(json(from:self.arrRating as Any))")
        let ratingData:String = json(from:self.arrRating as Any)!
        let dic = ["type":"add_satisfactory",
                   "company_name":companyName,
                   "contract_title":contractTitle,
                   "contract_ref_no":contractNumber,
                   "person_incharge":personCharge,
                   "department":department,
                   "position":position,
                   "progress":progress,
                   "all_rating_data":ratingData,
                   "project_id":self.project_id,
                   "comments_for_improvement":" "
            ] as [String : Any]
        print(dic)
        ServiceHelper.sharedInstance.addSatisfacgtoryForm(parameter: dic) { (result, error) in
            Loader.hide()
            print(result)
            guard let dict = result as? [String:Any] else {
                          return
                      }
                      if let status = dict["status"] as? String, status == "1" {
                          self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                              self.navigationController?.popViewController(animated: true)
                          })
                      } else {
                          self.showAlert(title: "", message: dict["message"] as? String ?? "")
                      }
            
            
        }
    }
    func readForm(){
        Loader.show()
        let dic = ["type":"read_satisfactory_form",
                   "project_id":self.project_id] as [String:Any]
        ServiceHelper.sharedInstance.readSatisfacgtoryForm(parameter: dic) { (result, error) in
            Loader.hide()
            guard let dict = result as? [String:Any] else {
                return
            }
            guard let data = dict["data"] as? [String:Any] else {
                return
            }
           
            guard let company_name = data["company_name"] as? String else {
                return
            }
            guard let contract_title = data["contract_title"] as? String else {
                return
            }
                self.companyName = company_name
                self.contractTitle = contract_title
                self.formTableView.reloadData()
           
        }
    }
    
}
