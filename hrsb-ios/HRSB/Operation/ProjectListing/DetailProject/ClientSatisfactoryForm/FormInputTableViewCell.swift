//
//  FormInputTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 30/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class FormInputTableViewCell: UITableViewCell {

    @IBOutlet weak var textFieldInput: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
