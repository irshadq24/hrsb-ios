//
//  FormStarTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 30/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import Cosmos

class FormStarTableViewCell: UITableViewCell {

    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
