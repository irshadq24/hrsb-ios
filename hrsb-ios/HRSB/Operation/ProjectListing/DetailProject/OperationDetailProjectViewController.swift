//
//  OperationDetailProjectViewController.swift
//  HRSB
//
//  Created by Javed Multani on 28/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class OperationDetailProjectViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
     var chooseImage = UIImage(named: "budgetary-flat")
         var objTask:ObjOperationProjectTask? = nil
    var objProject:ObjOperationProjectDetails? = nil
    var project_id = ""
    var selectedTypeTaskArray:[ObjOperationTypeTask] = [ObjOperationTypeTask]()
    @IBOutlet weak var detailTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailTableView.delegate = self
        detailTableView.dataSource = self
        
        detailTableView.register(UINib(nibName: "DetailProjectTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectTitleTableViewCell")
        detailTableView.register(UINib(nibName: "DetailProjectTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectTableViewCell")
        detailTableView.register(UINib(nibName: "DetailProjectSectionTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectSectionTableViewCell")
        
        self.getProjectDetail(project_id: self.project_id)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 13
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0{
            return 137.0
        }else if indexPath.row == 10 || indexPath.row == 11 || indexPath.row == 12{
            return 55.0
        }else{
            return 70.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectTitleTableViewCell", for: indexPath) as! DetailProjectTitleTableViewCell
            
            cell.titleLabel.text = objProject?.projectTitle ?? ""
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrintDate = DateFormatter()
            dateFormatterPrintDate.dateFormat = "dd-MM-yyyy"
            
            let dateFormatterPrintTime = DateFormatter()
            dateFormatterPrintTime.dateFormat = "HH:mm a"
            
            if let date = dateFormatterGet.date(from: objProject?.dateTime ?? "") {
                print(dateFormatterPrintDate.string(from: date))
                cell.dateLabel.text = dateFormatterPrintDate.string(from: date)
            } else {
                print("There was an error decoding the string")
            }
            
            if let date2 = dateFormatterGet.date(from: objProject?.dateTime ?? "") {
                print(dateFormatterPrintTime.string(from: date2))
                cell.timeLabel.text = dateFormatterPrintTime.string(from: date2)
            } else {
                print("There was an error decoding the string")
            }
            
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.row == 10 || indexPath.row == 11 || indexPath.row == 12{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectSectionTableViewCell", for: indexPath) as! DetailProjectSectionTableViewCell
            if indexPath.row == 10{
                
                cell.iconImage.image = UIImage(named: "executionprocess")
                cell.titleLabel.text = "Project Execution"
            }else if indexPath.row == 11{
                cell.titleLabel.text = "Project Closure"
                cell.iconImage.image = UIImage(named: "approval_icon")
            }else{
                cell.titleLabel.text = "Client Satisfactory Form"
                cell.iconImage.image = UIImage(named: "claim3")
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectTableViewCell", for: indexPath) as! DetailProjectTableViewCell
            if indexPath.row == 1{
                cell.titleLabel.text = "Project Code"
                cell.valueLabel.text = objProject?.projectId ?? ""
            }else if indexPath.row == 2{
                cell.titleLabel.text = "Operation Planner Update (OPU)"
                cell.valueLabel.text = objProject?.opuName ?? ""
            }else if indexPath.row == 3{
                cell.titleLabel.text = "Type of Work"
                cell.valueLabel.text = objProject?.typeOfWorkName ?? ""
            }else if indexPath.row == 4{
                cell.titleLabel.text = "Head of Work"
                cell.valueLabel.text = objProject?.headOfWorkName ?? ""
            }else if indexPath.row == 5{
                cell.titleLabel.text = "Type of Task"
                cell.valueLabel.text = objProject?.typeOfTaskName ?? ""
            }else if indexPath.row == 6{
                cell.titleLabel.text = "Head of Task"
                cell.valueLabel.text = objProject?.headOfTaskName ?? ""
            }else if indexPath.row == 7{
                cell.titleLabel.text = "Status"
                cell.valueLabel.text = objProject?.statusName ?? ""
            }else if indexPath.row == 8{
                cell.titleLabel.text = "Progress"
                cell.valueLabel.text = objProject?.progress ?? ""
            }else if indexPath.row == 9{
                cell.titleLabel.text = "Attachment"
                cell.valueLabel.text = objProject?.attachment ?? ""
                cell.valueLabel.textColor = UIColor(red:0.38, green:0.24, blue:0.56, alpha:1.0)
                let a = UIImageView()
                a.kf.setImage(with: URL(string: self.objProject?.urlPath ?? ""), completionHandler: { image, _, _, _ in
                    if image != nil{
                        self.chooseImage = image!
                                             
                    }
                })
                
                let button = UIButton(frame: cell.contentView.frame)
               button.backgroundColor = .clear
                button.setTitle("", for: .normal)
               button.addTarget(self, action: #selector(buttonHandlerDownload), for: .touchUpInside)
                cell.contentView.bringSubviewToFront(button)
                cell.contentView.addSubview(button)
                
            }
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 10{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "OperationProjectExecutionViewController")as! OperationProjectExecutionViewController
             vc.projectId = self.project_id
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 11{
          let vc = storyboard?.instantiateViewController(withIdentifier: "ProjectExecutionClosureViewController")as! ProjectExecutionClosureViewController
            vc.projectId = self.project_id
            navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 12{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "ClientSatisfactoryFormViewController")as! ClientSatisfactoryFormViewController
            vc.project_id = self.project_id
            vc.objProject = self.objProject
            vc.objTask = self.objTask
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    //MARK: - button action
     @IBAction func buttonHandlerDownload(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(self.chooseImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
      
    }
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonHandlerEdit(_ sender: Any) {
        if self.objProject == nil{
            
        }else{
        let vc = storyboard?.instantiateViewController(withIdentifier: "OperationAddProjectViewController")as! OperationAddProjectViewController
        vc.isEdit = true
        vc.objProject = self.objProject
        vc.objTask = self.objTask
            vc.selectedTypeTaskArray = self.selectedTypeTaskArray
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Image downloaded successfully...", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    //MARK: - API call
    func getProjectDetail(project_id:String){
        Loader.show()
        ServiceHelper.sharedInstance.getOperationProjectDetail(project_id: project_id) { (result, error) in
            Loader.hide()
            
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            let data = responseDic["data"] as! [String : Any]
            let json = JSON(data)
            let obj: ObjOperationProjectDetails = ObjOperationProjectDetails.init(json: json)
            self.objProject = obj
            
            //task
        
            let arrayMain = data["task"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationProjectTask = ObjOperationProjectTask.init(json: json)
                self.objTask = obj
                
                let dic = ["type_code":obj.typeId!,"type_name":obj.typeName!,"type_employee":obj.typeEmployee!,"type_option":"","type_employee_name":obj.employeeName!,"type_id":obj.typeId!] as [String:Any]
                let json2 = JSON(dic)
                    let objType: ObjOperationTypeTask = ObjOperationTypeTask.init(json: json2)
                
                self.selectedTypeTaskArray.append(objType)
            }
            self.detailTableView.reloadData()
        }
        
    }
    
}
private let kObjOperationTypeTaskTypeCodeKey: String = "type_code"
private let kObjOperationTypeTaskTypeNameKey: String = "type_name"
private let kObjOperationTypeTaskTypeEmployeeKey: String = "type_employee"
private let kObjOperationTypeTaskTypeOptionKey: String = "type_option"
private let kObjOperationTypeTaskTypeEmployeeNameKey: String = "type_employee_name"
private let kObjOperationTypeTaskTypeIdKey: String = "type_id"
