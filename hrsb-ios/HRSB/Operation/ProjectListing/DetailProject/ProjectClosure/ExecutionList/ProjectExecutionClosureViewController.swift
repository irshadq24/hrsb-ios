//
//  ProjectExecutionClosureViewController.swift
//  HRSB
//
//  Created by Javed Multani on 30/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ProjectExecutionClosureViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
 @IBOutlet weak var detailTableView: UITableView!
    var projectId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableView.delegate = self
        detailTableView.dataSource = self
            
            detailTableView.register(UINib(nibName: "ExecutionListClosureTableViewCell", bundle: nil), forCellReuseIdentifier: "ExecutionListClosureTableViewCell")
        // Do any additional setup after loading the view.
    }
    

  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExecutionListClosureTableViewCell", for: indexPath) as! ExecutionListClosureTableViewCell
        if indexPath.row == 0{
             cell.titleLabel.text = "JTC"
        }else{
             cell.titleLabel.text = "Client Report"
        }
           
               cell.contentView.backgroundColor = UIColor .clear
               cell.backgroundColor = UIColor .clear
               cell.selectionStyle = .none
        
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
           let vc = storyboard?.instantiateViewController(withIdentifier: "OperationExecutionFileViewController")as! OperationExecutionFileViewController
        vc.isFromClosure = true
        vc.projectId = projectId
        if indexPath.row == 0{
            vc.titletype = "jtc"
               }else{
                    vc.titletype = "client_report"
               }
                     navigationController?.pushViewController(vc, animated: true)
      }
      //MARK: - button action
      @IBAction func buttonHAndlerBack(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
}
