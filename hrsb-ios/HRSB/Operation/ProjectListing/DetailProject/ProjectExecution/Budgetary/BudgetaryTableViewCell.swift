//
//  BudgetaryTableViewCell.swift
//  HRSB
//
//  Created by Javed Multani on 28/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class BudgetaryTableViewCell: UITableViewCell {
    @IBOutlet weak var workLabel: UILabel!
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var OPULabel: UILabel!
    @IBOutlet weak var projectLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
