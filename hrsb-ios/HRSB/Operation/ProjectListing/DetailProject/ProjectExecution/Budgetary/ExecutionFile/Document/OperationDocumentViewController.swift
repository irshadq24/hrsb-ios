//
//  OperationDocumentViewController.swift
//  HRSB
//
//  Created by Javed Multani on 29/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class OperationDocumentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var objProject:ObjProjectClosure? = nil
    var projectId = ""
    var isFromClosure = false
    var isFromExecution = false
    var chooseImage = UIImage(named: "budgetary-flat")
    @IBOutlet weak var detailTableVIew: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableVIew.delegate = self
        detailTableVIew.dataSource = self
        
        detailTableVIew.register(UINib(nibName: "DetailProjectTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailProjectTitleTableViewCell")
        detailTableVIew.register(UINib(nibName: "AddProjectSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "AddProjectSubmitTableViewCell")
        detailTableVIew.register(UINib(nibName: "DocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "DocumentTableViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0{
            return 137.0
        }else if indexPath.row == 1{
            return 270.0
        }else{
            return 65.0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailProjectTitleTableViewCell", for: indexPath) as! DetailProjectTitleTableViewCell
            cell.titleLabel.text = objProject?.attachment!
            cell.dateLabel.text = objProject?.date!
            cell.timeLabel.text = objProject?.time!
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell", for: indexPath) as! DocumentTableViewCell
            cell.documentImage.kf.setImage(with: URL(string: self.objProject?.urlPath ?? ""), completionHandler: { image, _, _, _ in
                 if image != nil{
                self.chooseImage = image!
                }
                
            })
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddProjectSubmitTableViewCell", for: indexPath) as! AddProjectSubmitTableViewCell
            cell.submitButton.setTitle("DELETE", for: .normal)
            cell.submitButton.addTarget(self,action:#selector(buttonClicked), for: .touchUpInside)
            cell.contentView.backgroundColor = UIColor .clear
            cell.backgroundColor = UIColor .clear
            cell.selectionStyle = .none
            
            return cell
        }
        
        
    }
    //MARK: - button action
    
    @IBAction func buttonHandlerDownload(_ sender: Any) {
        
              UIImageWriteToSavedPhotosAlbum(self.chooseImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @objc func buttonClicked(sender:UIButton) {
        if self.isFromClosure{
            self.deleteClosure()
        }else{
            self.deleteExecution()
        }
        
    }
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
       //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Image downloaded successfully...", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
   // MARK: - API
    
    func deleteClosure(){
        Loader.show()
        let dic = ["file_id":self.objProject?.fileId!,
                   "type":"delete_closure_file"] as [String : Any]
        ServiceHelper.sharedInstance.deleteProjectClosure(parameter:dic) { (result, error) in
            Loader.hide()
            guard let dict = result as? [String:Any] else {
                               return
                           }
                           if let status = dict["status"] as? String, status == "1" {
                               self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                                   self.navigationController?.popViewController(animated: true)
                               })
                           } else {
                               self.showAlert(title: "", message: dict["message"] as? String ?? "")
                           }
        }
    }
    func deleteExecution(){
          Loader.show()
          let dic = ["file_id":self.objProject?.fileId!,
                     "type":"delete_execution_file"] as [String : Any]
          ServiceHelper.sharedInstance.deleteProjectExecution(parameter:dic) { (result, error) in
              Loader.hide()
              guard let dict = result as? [String:Any] else {
                                 return
                             }
                             if let status = dict["status"] as? String, status == "1" {
                                 self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                                     self.navigationController?.popViewController(animated: true)
                                 })
                             } else {
                                 self.showAlert(title: "", message: dict["message"] as? String ?? "")
                             }
          }
      }
    
}
