//
//  OperationExecutionFileViewController.swift
//  HRSB
//
//  Created by Javed Multani on 28/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class OperationExecutionFileViewController:  UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
     var projectArray : [ObjProjectClosure] = [ObjProjectClosure]()
   var isFromClosure = false
    var isFromExecution = false
    
    var imagePicker = UIImagePickerController()
      var chooseImage = UIImage(named:"addImage")
    
    var projectId = ""
    var titletype = "client_report"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTableVIew: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableVIew.delegate = self
        detailTableVIew.dataSource = self
        self.titleLabel.text = self.titletype
         detailTableVIew.register(UINib(nibName: "BudgetaryTableViewCell", bundle: nil), forCellReuseIdentifier: "BudgetaryTableViewCell")
        
        if self.isFromClosure{
               self.getProjectClosure()
        }else{
            self.getProjectExecution()
        }
     
        // Do any additional setup after loading the view.
    }
    @IBAction func buttonHandlerAdd(_ sender: Any) {
        self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
            if selected == 1 {
                self.openCamera()
            } else if selected == 2 {
                self.openGallery()
            }
        }
    }
    
    
       // MARK: - UITableViewDataSource
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
        return  self.projectArray.count
         
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
         return 70.0
         
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
         let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetaryTableViewCell", for: indexPath) as! BudgetaryTableViewCell
        
        let obj = self.projectArray[indexPath.row]
        cell.projectLabel.text = obj.attachment!
        cell.codeLabel.isHidden = true
        cell.workLabel.text = "Date: \(obj.date!)"
        cell.OPULabel.text = "Time: \(obj.time!)"
        
                cell.contentView.backgroundColor = UIColor .clear
                cell.backgroundColor = UIColor .clear
                cell.selectionStyle = .none
         
         
         return cell
         
         
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "OperationDocumentViewController")as! OperationDocumentViewController
        vc.projectId = self.projectId
        vc.objProject = self.projectArray[indexPath.row]
        if self.isFromClosure{
            vc.isFromClosure = true
        }else{
            vc.isFromExecution = true
        }
        
               navigationController?.pushViewController(vc, animated: true)
    }
     //MARK: - button action
     @IBAction func buttonHandlerBack(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
     }
   
    //MARK: - API call
       func addClosureFile(image:UIImage){
           Loader.show()
           let endPoint = Constant.ServiceApi.OperationUploadClosure
         print(endPoint)
        var type = ""
        if self.titletype == "Client Report"{
            type = "client_report"
        }else{
            type = "jct"
        }
           var params = [String: String]()
           params["type"] = "project_closure_file_upload"
         params["project_id"] = self.projectId
        params["closure_type"] = type
          print(params)
           ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["execution_file": image], unReachable: {
               print("no internet")
               Loader.hide()
           }) { (result, progress) in
               Loader.hide()
               guard let dict = result else {
                   return
               }
               if let status = dict["status"] as? String, status == "1" {
                   self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                       self.navigationController?.popViewController(animated: true)
                   })
               } else {
                   self.showAlert(title: "", message: dict["message"] as? String ?? "")
               }
               print(result)
           }
       }
    func addExecutionFile(image:UIImage){
              Loader.show()
              let endPoint = Constant.ServiceApi.OperationUploadExecution
            print(endPoint)
            var type = ""
           if self.titletype == "Budgetary"{
               type = "budegetary"
           }else if self.titletype == "QA/QC"{
           type = "qa_and_qc"
            }else if self.titletype == "Safety"{
                      type = "safetly"
            }else if self.titletype == "Logistic"{
                      type = "logistic"
            }else if self.titletype == "Procurement"{
                      type = "procurement"
            }else if self.titletype == "Daily Progress Report"{
                      type = "daily_progress_report"
        }
        //["Budgetary","QA/QC","Safety","Logistic","Procurement","Daily Progress Report"]
        //budegetary','qa_and_qc','safetly','logistic','procureme nt','daily_progress_report',
              var params = [String: String]()
              params["type"] = "project_execution_file_upload"
            params["project_id"] = self.projectId
           params["execution_type"] = type
             print(params)
              ApiManager.hitMultipartForImage(path: endPoint, params, imageInfo: ["execution_file": image], unReachable: {
                  print("no internet")
                  Loader.hide()
              }) { (result, progress) in
                  Loader.hide()
                  guard let dict = result else {
                      return
                  }
                  if let status = dict["status"] as? String, status == "1" {
                      self.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                          self.navigationController?.popViewController(animated: true)
                      })
                  } else {
                      self.showAlert(title: "", message: dict["message"] as? String ?? "")
                  }
                  print(result)
              }
          }
    func getProjectClosure(){
        Loader.show()
        var type = ""
        if self.titletype == "Client Report"{
            type = "client_report"
        }else{
            type = "jct"
        }
        let dic = ["list_type":type,
                   "type":"project_closure_list",
                   "project_id":projectId] as [String : Any]
        ServiceHelper.sharedInstance.getProjectClosureList(parameter: dic) { (result, error) in
            Loader.hide()
            print(result)
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.projectArray.removeAll()
            let arrayMain = responseDic["data"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjProjectClosure = ObjProjectClosure.init(json: json)
                self.projectArray.append(obj)
            }
            self.detailTableVIew.reloadData()
            
        }
    }
    func getProjectExecution(){
           Loader.show()
        var type = ""
                  if self.titletype == "Budgetary"{
                      type = "budegetary"
                  }else if self.titletype == "QA/QC"{
                  type = "qa_and_qc"
                   }else if self.titletype == "Safety"{
                             type = "safetly"
                   }else if self.titletype == "Logistic"{
                             type = "logistic"
                   }else if self.titletype == "Procurement"{
                             type = "procurement"
                   }else if self.titletype == "Daily Progress Report"{
                             type = "daily_progress_report"
               }
           let dic = ["list_type":type,
                      "type":"project_execution_list",
                      "project_id":projectId] as [String : Any]
           ServiceHelper.sharedInstance.getProjectExecutionList(parameter: dic) { (result, error) in
               Loader.hide()
               print(result)
               guard let responseDic =  result as? [String : Any] else {
                   CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                   return
               }
               self.projectArray.removeAll()
               let arrayMain = responseDic["data"] as! [NSArray]
               let swiftArray = arrayMain as AnyObject as! [Any]
               for i in 0..<swiftArray.count {
                   let json = JSON(swiftArray[i])
                   let obj: ObjProjectClosure = ObjProjectClosure.init(json: json)
                   self.projectArray.append(obj)
               }
               self.detailTableVIew.reloadData()
               
           }
       }
    //MARK: - UIImagePicker delegate
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        if let image = info[.editedImage] as? UIImage{
            self.chooseImage = image
           //do something...
            if self.isFromClosure{
                self.addClosureFile(image: self.chooseImage!)
                
            }else{
                self.addExecutionFile(image: self.chooseImage!)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}
