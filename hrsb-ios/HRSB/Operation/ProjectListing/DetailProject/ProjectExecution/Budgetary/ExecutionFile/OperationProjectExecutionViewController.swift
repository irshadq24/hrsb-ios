//
//  OperationProjectExecutionViewController.swift
//  HRSB
//
//  Created by Javed Multani on 28/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class OperationProjectExecutionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var detailTableView: UITableView!
    var isFromProjectClosure = false
    var projectId = ""
    
    var titleArray = ["Budgetary","QA/QC","Safety","Logistic","Procurement","Daily Progress Report"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailTableView.delegate = self
        detailTableView.dataSource = self
        
        detailTableView.register(UINib(nibName: "ProjectExecutionTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectExecutionTableViewCell")
        // Do any additional setup after loading the view.
    }
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.titleArray.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 67.0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectExecutionTableViewCell", for: indexPath) as! ProjectExecutionTableViewCell
        cell.titleLabel.text = titleArray[indexPath.row]
        cell.contentView.backgroundColor = UIColor .clear
        cell.backgroundColor = UIColor .clear
        cell.selectionStyle = .none
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let vc = storyboard?.instantiateViewController(withIdentifier: "OperationExecutionFileViewController")as! OperationExecutionFileViewController
           vc.isFromExecution = true
         vc.projectId = projectId
        vc.titletype = self.titleArray[indexPath.row]
                        navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK: - button action
    @IBAction func buttonHAndlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
