//
//  OperationBudgetaryViewController.swift
//  HRSB
//
//  Created by Javed Multani on 28/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class OperationBudgetaryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var isFromClosure = false
    var isFromExecution = false
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTableVIew: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "Budgetary"
        detailTableVIew.delegate = self
        detailTableVIew.dataSource = self
        
        detailTableVIew.register(UINib(nibName: "BudgetaryTableViewCell", bundle: nil), forCellReuseIdentifier: "BudgetaryTableViewCell")
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 12
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70.0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BudgetaryTableViewCell", for: indexPath) as! BudgetaryTableViewCell
        
      
        cell.contentView.backgroundColor = UIColor .clear
        cell.backgroundColor = UIColor .clear
        cell.selectionStyle = .none
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "OperationExecutionFileViewController")as! OperationExecutionFileViewController
               navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - button action
    @IBAction func buttonHandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
