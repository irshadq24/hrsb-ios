//
//  ProjectListingViewController.swift
//  HRSB
//
//  Created by Javed Multani on 24/10/2019.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProjectListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var projectArray : [ObjOperationProject] = [ObjOperationProject]()
    @IBOutlet weak var projectTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        projectTableView.delegate = self
        projectTableView.dataSource = self
        
        projectTableView.register(UINib(nibName: "ProjectListTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectListTableViewCell")
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getProjectList()
    }
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.projectArray.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 93.0
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectListTableViewCell", for: indexPath) as! ProjectListTableViewCell
        let obj = self.projectArray[indexPath.row]
        cell.titleLabel.text = obj.projectTitle!
        cell.codeLabel.text = "Project Code: " + obj.code!
        cell.dateLabel.text = "Date of Closure: " + obj.dateOfClosure!
        cell.progressLabel.text = "Progress: \(obj.progress!)%"
        cell.statusLabel.text = "Status: " + obj.finalizedStatus!
        cell.contentView.backgroundColor = UIColor .clear
        cell.backgroundColor = UIColor .clear
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OperationDetailProjectViewController")as! OperationDetailProjectViewController
        vc.project_id = projectArray[indexPath.row].projectId!
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - button action
    @IBAction func buttonhandlerBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonHandlerAdd(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OperationAddProjectViewController")as! OperationAddProjectViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - API call
    
    func getProjectList(){
        Loader.show()
        
        ServiceHelper.sharedInstance.getOperationProjectList(type: "all") { (result, error) in
            Loader.hide()
            guard let responseDic =  result as? [String : Any] else {
                CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Something went wrong! Please try after sometime")
                return
            }
            self.projectArray.removeAll()
            let arrayMain = responseDic["data"] as! [NSArray]
            let swiftArray = arrayMain as AnyObject as! [Any]
            for i in 0..<swiftArray.count {
                let json = JSON(swiftArray[i])
                let obj: ObjOperationProject = ObjOperationProject.init(json: json)
                self.projectArray.append(obj)
            }
            self.projectTableView.reloadData()
        }
    }
    //        ServiceHelper.sharedInstance.checkLogins(userName: userName, password: password){ (result, error) in
    //            print(result)
    //            Loader.hide()
    //             if let resultValue = result as? [String : Any], let status = resultValue["status"] as? String {
    //                if let resultdata = resultValue["data"]  as? [String : Any], status != "0" {
    //
    //                }
    //                else {
    //                    let message = resultValue["message"] as! String
    //                    CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: message)
    //
    //                }
    //
    //
    //            }
    //        }
    //
    //    }
    
    
    func getList(){
        let urlstr = Constant.ServiceApi.DomainUrl
        let url = URL(string: "\(urlstr)operation/operation_project_list/")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let parameters: [String: Any] =  ["response_list":"head_of_work","type":"operation_project_list","user_id":"1" ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }
        
        task.resume()
    }
    
    
    
    
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
