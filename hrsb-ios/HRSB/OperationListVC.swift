//
//  OperationListVC.swift
//  HRSB
//
//  Created by Salman on 06/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class OperationListVC: UIViewController {

    
    @IBOutlet weak var tbleview: UITableView!
    var arrImg = ["loa","loa","loa","loa"]
    var arrLbl = ["Project Listing","Edit Project - Head Of Work","Edit Project - Head Of Task", "Finalize Project - Head Of Work"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if getUserDefault(Constant.UserDefaultsKey.userRoleId) != "5" {
//            arrImg.remove(at: 1)
//            arrLbl.remove(at: 1)
//        }
        tbleview.reloadData()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
}


extension OperationListVC: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellOperationMenu", for: indexPath) as! CellOperationMenu
        cell.lblName.text = arrLbl[indexPath.row]
        cell.img.image = UIImage(named: arrImg[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
               let vc = storyboard?.instantiateViewController(withIdentifier: "ProjectListingViewController")as! ProjectListingViewController
                          navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {
               let vc = storyboard?.instantiateViewController(withIdentifier: "EditHeadOfWorkViewController")as! EditHeadOfWorkViewController
                          navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2 {
                 let vc = storyboard?.instantiateViewController(withIdentifier: "EditHeadOfTaskViewController")as! EditHeadOfTaskViewController
                          navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "FinalizeTaskViewController")as! FinalizeTaskViewController
                          navigationController?.pushViewController(vc, animated: true)
        }
            
    }
}
