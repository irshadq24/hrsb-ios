//
//  ObjFinalizeAssign.swift
//
//  Created by Javed Multani on 12/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjFinalizeAssign: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjFinalizeAssignHeadOfTaskNameKey: String = "head_of_task_name"
  private let kObjFinalizeAssignHeadOfTaskIdKey: String = "head_of_task_id"
  private let kObjFinalizeAssignAssignedEmployeeIdKey: String = "assigned_employee_id"
  private let kObjFinalizeAssignTaskNameKey: String = "task_name"
  private let kObjFinalizeAssignAssignedEmployeeNameKey: String = "assigned_employee_name"
  private let kObjFinalizeAssignTaskIdKey: String = "task_id"

  // MARK: Properties
  public var headOfTaskName: String?
  public var headOfTaskId: String?
  public var assignedEmployeeId: String?
  public var taskName: String?
  public var assignedEmployeeName: String?
  public var taskId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    headOfTaskName = json[kObjFinalizeAssignHeadOfTaskNameKey].string
    headOfTaskId = json[kObjFinalizeAssignHeadOfTaskIdKey].string
    assignedEmployeeId = json[kObjFinalizeAssignAssignedEmployeeIdKey].string
    taskName = json[kObjFinalizeAssignTaskNameKey].string
    assignedEmployeeName = json[kObjFinalizeAssignAssignedEmployeeNameKey].string
    taskId = json[kObjFinalizeAssignTaskIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = headOfTaskName { dictionary[kObjFinalizeAssignHeadOfTaskNameKey] = value }
    if let value = headOfTaskId { dictionary[kObjFinalizeAssignHeadOfTaskIdKey] = value }
    if let value = assignedEmployeeId { dictionary[kObjFinalizeAssignAssignedEmployeeIdKey] = value }
    if let value = taskName { dictionary[kObjFinalizeAssignTaskNameKey] = value }
    if let value = assignedEmployeeName { dictionary[kObjFinalizeAssignAssignedEmployeeNameKey] = value }
    if let value = taskId { dictionary[kObjFinalizeAssignTaskIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.headOfTaskName = aDecoder.decodeObject(forKey: kObjFinalizeAssignHeadOfTaskNameKey) as? String
    self.headOfTaskId = aDecoder.decodeObject(forKey: kObjFinalizeAssignHeadOfTaskIdKey) as? String
    self.assignedEmployeeId = aDecoder.decodeObject(forKey: kObjFinalizeAssignAssignedEmployeeIdKey) as? String
    self.taskName = aDecoder.decodeObject(forKey: kObjFinalizeAssignTaskNameKey) as? String
    self.assignedEmployeeName = aDecoder.decodeObject(forKey: kObjFinalizeAssignAssignedEmployeeNameKey) as? String
    self.taskId = aDecoder.decodeObject(forKey: kObjFinalizeAssignTaskIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(headOfTaskName, forKey: kObjFinalizeAssignHeadOfTaskNameKey)
    aCoder.encode(headOfTaskId, forKey: kObjFinalizeAssignHeadOfTaskIdKey)
    aCoder.encode(assignedEmployeeId, forKey: kObjFinalizeAssignAssignedEmployeeIdKey)
    aCoder.encode(taskName, forKey: kObjFinalizeAssignTaskNameKey)
    aCoder.encode(assignedEmployeeName, forKey: kObjFinalizeAssignAssignedEmployeeNameKey)
    aCoder.encode(taskId, forKey: kObjFinalizeAssignTaskIdKey)
  }

}
