//
//  ObjOperationAssignEmp.swift
//
//  Created by Javed Multani on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationAssignEmp: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationAssignEmpEmployeeNameKey: String = "employee_name"
  private let kObjOperationAssignEmpUserIdKey: String = "user_id"
  private let kObjOperationAssignEmpEmployeeIdKey: String = "employee_id"
  private let kObjOperationAssignEmpTypeOfWorkNameKey: String = "type_of_work_name"

  // MARK: Properties
  public var employeeName: String?
  public var userId: String?
  public var employeeId: String?
  public var typeOfWorkName: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    employeeName = json[kObjOperationAssignEmpEmployeeNameKey].string
    userId = json[kObjOperationAssignEmpUserIdKey].string
    employeeId = json[kObjOperationAssignEmpEmployeeIdKey].string
    typeOfWorkName = json[kObjOperationAssignEmpTypeOfWorkNameKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = employeeName { dictionary[kObjOperationAssignEmpEmployeeNameKey] = value }
    if let value = userId { dictionary[kObjOperationAssignEmpUserIdKey] = value }
    if let value = employeeId { dictionary[kObjOperationAssignEmpEmployeeIdKey] = value }
    if let value = typeOfWorkName { dictionary[kObjOperationAssignEmpTypeOfWorkNameKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.employeeName = aDecoder.decodeObject(forKey: kObjOperationAssignEmpEmployeeNameKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kObjOperationAssignEmpUserIdKey) as? String
    self.employeeId = aDecoder.decodeObject(forKey: kObjOperationAssignEmpEmployeeIdKey) as? String
    self.typeOfWorkName = aDecoder.decodeObject(forKey: kObjOperationAssignEmpTypeOfWorkNameKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(employeeName, forKey: kObjOperationAssignEmpEmployeeNameKey)
    aCoder.encode(userId, forKey: kObjOperationAssignEmpUserIdKey)
    aCoder.encode(employeeId, forKey: kObjOperationAssignEmpEmployeeIdKey)
    aCoder.encode(typeOfWorkName, forKey: kObjOperationAssignEmpTypeOfWorkNameKey)
  }

}
