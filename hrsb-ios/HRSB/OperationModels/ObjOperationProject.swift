//
//  ObjOperationProject.swift
//
//  Created by javedmultani16 on 25/10/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationProject: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationProjectCodeKey: String = "code"
  private let kObjOperationProjectProjectTitleKey: String = "project_title"
  private let kObjOperationProjectProjectIdKey: String = "project_id"
  private let kObjOperationProjectProgressKey: String = "progress"
  private let kObjOperationProjectStatusIdKey: String = "status_id"
  private let kObjOperationProjectDateOfClosureKey: String = "date_of_closure"
  private let kObjOperationProjectStatusNameKey: String = "status_name"
  private let kObjOperationProjectPoNumberKey: String = "po_number"
  private let kObjOperationProjectFinalizedStatusKey: String = "finalized_status"

  // MARK: Properties
  public var code: String?
  public var projectTitle: String?
  public var projectId: String?
  public var progress: String?
  public var statusId: String?
  public var dateOfClosure: String?
  public var statusName: String?
  public var poNumber: String?
  public var finalizedStatus: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    code = json[kObjOperationProjectCodeKey].string
    projectTitle = json[kObjOperationProjectProjectTitleKey].string
    projectId = json[kObjOperationProjectProjectIdKey].string
    progress = json[kObjOperationProjectProgressKey].string
    statusId = json[kObjOperationProjectStatusIdKey].string
    dateOfClosure = json[kObjOperationProjectDateOfClosureKey].string
    statusName = json[kObjOperationProjectStatusNameKey].string
    poNumber = json[kObjOperationProjectPoNumberKey].string
    finalizedStatus = json[kObjOperationProjectFinalizedStatusKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = code { dictionary[kObjOperationProjectCodeKey] = value }
    if let value = projectTitle { dictionary[kObjOperationProjectProjectTitleKey] = value }
    if let value = projectId { dictionary[kObjOperationProjectProjectIdKey] = value }
    if let value = progress { dictionary[kObjOperationProjectProgressKey] = value }
    if let value = statusId { dictionary[kObjOperationProjectStatusIdKey] = value }
    if let value = dateOfClosure { dictionary[kObjOperationProjectDateOfClosureKey] = value }
    if let value = statusName { dictionary[kObjOperationProjectStatusNameKey] = value }
    if let value = poNumber { dictionary[kObjOperationProjectPoNumberKey] = value }
    if let value = finalizedStatus { dictionary[kObjOperationProjectFinalizedStatusKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.code = aDecoder.decodeObject(forKey: kObjOperationProjectCodeKey) as? String
    self.projectTitle = aDecoder.decodeObject(forKey: kObjOperationProjectProjectTitleKey) as? String
    self.projectId = aDecoder.decodeObject(forKey: kObjOperationProjectProjectIdKey) as? String
    self.progress = aDecoder.decodeObject(forKey: kObjOperationProjectProgressKey) as? String
    self.statusId = aDecoder.decodeObject(forKey: kObjOperationProjectStatusIdKey) as? String
    self.dateOfClosure = aDecoder.decodeObject(forKey: kObjOperationProjectDateOfClosureKey) as? String
    self.statusName = aDecoder.decodeObject(forKey: kObjOperationProjectStatusNameKey) as? String
    self.poNumber = aDecoder.decodeObject(forKey: kObjOperationProjectPoNumberKey) as? String
    self.finalizedStatus = aDecoder.decodeObject(forKey: kObjOperationProjectFinalizedStatusKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(code, forKey: kObjOperationProjectCodeKey)
    aCoder.encode(projectTitle, forKey: kObjOperationProjectProjectTitleKey)
    aCoder.encode(projectId, forKey: kObjOperationProjectProjectIdKey)
    aCoder.encode(progress, forKey: kObjOperationProjectProgressKey)
    aCoder.encode(statusId, forKey: kObjOperationProjectStatusIdKey)
    aCoder.encode(dateOfClosure, forKey: kObjOperationProjectDateOfClosureKey)
    aCoder.encode(statusName, forKey: kObjOperationProjectStatusNameKey)
    aCoder.encode(poNumber, forKey: kObjOperationProjectPoNumberKey)
    aCoder.encode(finalizedStatus, forKey: kObjOperationProjectFinalizedStatusKey)
  }

}
