//
//  ObjOperationProjectDetails.swift
//
//  Created by Javed Multani on 30/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationProjectDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationProjectDetailsOpuIdKey: String = "opu_id"
  private let kObjOperationProjectDetailsProjectTitleKey: String = "project_title"
  private let kObjOperationProjectDetailsStatusIdKey: String = "status_id"
  private let kObjOperationProjectDetailsStatusNameKey: String = "status_name"
  private let kObjOperationProjectDetailsPoNumberKey: String = "po_number"
  private let kObjOperationProjectDetailsFinalizedStatusKey: String = "finalized_status"
  private let kObjOperationProjectDetailsHeadOfTaskNameKey: String = "head_of_task_name"
  private let kObjOperationProjectDetailsTypeOfWorkIdKey: String = "type_of_work_id"
  private let kObjOperationProjectDetailsHeadOfWorkNameKey: String = "head_of_work_name"
  private let kObjOperationProjectDetailsOpuNameKey: String = "opu_name"
  private let kObjOperationProjectDetailsHeadOfWorkIdKey: String = "head_of_work_id"
  private let kObjOperationProjectDetailsTypeOfWorkNameKey: String = "type_of_work_name"
  private let kObjOperationProjectDetailsProgressKey: String = "progress"
  private let kObjOperationProjectDetailsAttachmentKey: String = "attachment"
  private let kObjOperationProjectDetailsProjectIdKey: String = "project_id"
  private let kObjOperationProjectDetailsDateTimeKey: String = "date_time"
  private let kObjOperationProjectDetailsTypeOfTaskNameKey: String = "type_of_task_name"
  private let kObjOperationProjectDetailsUrlPathKey: String = "url_path"

  // MARK: Properties
  public var opuId: String?
  public var projectTitle: String?
  public var statusId: String?
  public var statusName: String?
  public var poNumber: String?
  public var finalizedStatus: String?
  public var headOfTaskName: String?
  public var typeOfWorkId: String?
  public var headOfWorkName: String?
  public var opuName: String?
  public var headOfWorkId: String?
  public var typeOfWorkName: String?
  public var progress: String?
  public var attachment: String?
  public var projectId: String?
  public var dateTime: String?
  public var typeOfTaskName: String?
  public var urlPath: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    opuId = json[kObjOperationProjectDetailsOpuIdKey].string
    projectTitle = json[kObjOperationProjectDetailsProjectTitleKey].string
    statusId = json[kObjOperationProjectDetailsStatusIdKey].string
    statusName = json[kObjOperationProjectDetailsStatusNameKey].string
    poNumber = json[kObjOperationProjectDetailsPoNumberKey].string
    finalizedStatus = json[kObjOperationProjectDetailsFinalizedStatusKey].string
    headOfTaskName = json[kObjOperationProjectDetailsHeadOfTaskNameKey].string
    typeOfWorkId = json[kObjOperationProjectDetailsTypeOfWorkIdKey].string
    headOfWorkName = json[kObjOperationProjectDetailsHeadOfWorkNameKey].string
    opuName = json[kObjOperationProjectDetailsOpuNameKey].string
    headOfWorkId = json[kObjOperationProjectDetailsHeadOfWorkIdKey].string
    typeOfWorkName = json[kObjOperationProjectDetailsTypeOfWorkNameKey].string
    progress = json[kObjOperationProjectDetailsProgressKey].string
    attachment = json[kObjOperationProjectDetailsAttachmentKey].string
    projectId = json[kObjOperationProjectDetailsProjectIdKey].string
    dateTime = json[kObjOperationProjectDetailsDateTimeKey].string
    typeOfTaskName = json[kObjOperationProjectDetailsTypeOfTaskNameKey].string
    urlPath = json[kObjOperationProjectDetailsUrlPathKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = opuId { dictionary[kObjOperationProjectDetailsOpuIdKey] = value }
    if let value = projectTitle { dictionary[kObjOperationProjectDetailsProjectTitleKey] = value }
    if let value = statusId { dictionary[kObjOperationProjectDetailsStatusIdKey] = value }
    if let value = statusName { dictionary[kObjOperationProjectDetailsStatusNameKey] = value }
    if let value = poNumber { dictionary[kObjOperationProjectDetailsPoNumberKey] = value }
    if let value = finalizedStatus { dictionary[kObjOperationProjectDetailsFinalizedStatusKey] = value }
    if let value = headOfTaskName { dictionary[kObjOperationProjectDetailsHeadOfTaskNameKey] = value }
    if let value = typeOfWorkId { dictionary[kObjOperationProjectDetailsTypeOfWorkIdKey] = value }
    if let value = headOfWorkName { dictionary[kObjOperationProjectDetailsHeadOfWorkNameKey] = value }
    if let value = opuName { dictionary[kObjOperationProjectDetailsOpuNameKey] = value }
    if let value = headOfWorkId { dictionary[kObjOperationProjectDetailsHeadOfWorkIdKey] = value }
    if let value = typeOfWorkName { dictionary[kObjOperationProjectDetailsTypeOfWorkNameKey] = value }
    if let value = progress { dictionary[kObjOperationProjectDetailsProgressKey] = value }
    if let value = attachment { dictionary[kObjOperationProjectDetailsAttachmentKey] = value }
    if let value = projectId { dictionary[kObjOperationProjectDetailsProjectIdKey] = value }
    if let value = dateTime { dictionary[kObjOperationProjectDetailsDateTimeKey] = value }
    if let value = typeOfTaskName { dictionary[kObjOperationProjectDetailsTypeOfTaskNameKey] = value }
    if let value = urlPath { dictionary[kObjOperationProjectDetailsUrlPathKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.opuId = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsOpuIdKey) as? String
    self.projectTitle = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsProjectTitleKey) as? String
    self.statusId = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsStatusIdKey) as? String
    self.statusName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsStatusNameKey) as? String
    self.poNumber = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsPoNumberKey) as? String
    self.finalizedStatus = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsFinalizedStatusKey) as? String
    self.headOfTaskName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsHeadOfTaskNameKey) as? String
    self.typeOfWorkId = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsTypeOfWorkIdKey) as? String
    self.headOfWorkName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsHeadOfWorkNameKey) as? String
    self.opuName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsOpuNameKey) as? String
    self.headOfWorkId = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsHeadOfWorkIdKey) as? String
    self.typeOfWorkName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsTypeOfWorkNameKey) as? String
    self.progress = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsProgressKey) as? String
    self.attachment = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsAttachmentKey) as? String
    self.projectId = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsProjectIdKey) as? String
    self.dateTime = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsDateTimeKey) as? String
    self.typeOfTaskName = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsTypeOfTaskNameKey) as? String
    self.urlPath = aDecoder.decodeObject(forKey: kObjOperationProjectDetailsUrlPathKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(opuId, forKey: kObjOperationProjectDetailsOpuIdKey)
    aCoder.encode(projectTitle, forKey: kObjOperationProjectDetailsProjectTitleKey)
    aCoder.encode(statusId, forKey: kObjOperationProjectDetailsStatusIdKey)
    aCoder.encode(statusName, forKey: kObjOperationProjectDetailsStatusNameKey)
    aCoder.encode(poNumber, forKey: kObjOperationProjectDetailsPoNumberKey)
    aCoder.encode(finalizedStatus, forKey: kObjOperationProjectDetailsFinalizedStatusKey)
    aCoder.encode(headOfTaskName, forKey: kObjOperationProjectDetailsHeadOfTaskNameKey)
    aCoder.encode(typeOfWorkId, forKey: kObjOperationProjectDetailsTypeOfWorkIdKey)
    aCoder.encode(headOfWorkName, forKey: kObjOperationProjectDetailsHeadOfWorkNameKey)
    aCoder.encode(opuName, forKey: kObjOperationProjectDetailsOpuNameKey)
    aCoder.encode(headOfWorkId, forKey: kObjOperationProjectDetailsHeadOfWorkIdKey)
    aCoder.encode(typeOfWorkName, forKey: kObjOperationProjectDetailsTypeOfWorkNameKey)
    aCoder.encode(progress, forKey: kObjOperationProjectDetailsProgressKey)
    aCoder.encode(attachment, forKey: kObjOperationProjectDetailsAttachmentKey)
    aCoder.encode(projectId, forKey: kObjOperationProjectDetailsProjectIdKey)
    aCoder.encode(dateTime, forKey: kObjOperationProjectDetailsDateTimeKey)
    aCoder.encode(typeOfTaskName, forKey: kObjOperationProjectDetailsTypeOfTaskNameKey)
    aCoder.encode(urlPath, forKey: kObjOperationProjectDetailsUrlPathKey)
  }

}
