//
//  ObjOperationProjectTask.swift
//
//  Created by Javed Multani on 30/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationProjectTask: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationProjectTaskEmployeeNameKey: String = "employee_name"
  private let kObjOperationProjectTaskTypeNameKey: String = "type_name"
  private let kObjOperationProjectTaskTypeEmployeeKey: String = "type_employee"
  private let kObjOperationProjectTaskTypeIdKey: String = "type_id"

  // MARK: Properties
  public var employeeName: String?
  public var typeName: String?
  public var typeEmployee: String?
  public var typeId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    employeeName = json[kObjOperationProjectTaskEmployeeNameKey].string
    typeName = json[kObjOperationProjectTaskTypeNameKey].string
    typeEmployee = json[kObjOperationProjectTaskTypeEmployeeKey].string
    typeId = json[kObjOperationProjectTaskTypeIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = employeeName { dictionary[kObjOperationProjectTaskEmployeeNameKey] = value }
    if let value = typeName { dictionary[kObjOperationProjectTaskTypeNameKey] = value }
    if let value = typeEmployee { dictionary[kObjOperationProjectTaskTypeEmployeeKey] = value }
    if let value = typeId { dictionary[kObjOperationProjectTaskTypeIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.employeeName = aDecoder.decodeObject(forKey: kObjOperationProjectTaskEmployeeNameKey) as? String
    self.typeName = aDecoder.decodeObject(forKey: kObjOperationProjectTaskTypeNameKey) as? String
    self.typeEmployee = aDecoder.decodeObject(forKey: kObjOperationProjectTaskTypeEmployeeKey) as? String
    self.typeId = aDecoder.decodeObject(forKey: kObjOperationProjectTaskTypeIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(employeeName, forKey: kObjOperationProjectTaskEmployeeNameKey)
    aCoder.encode(typeName, forKey: kObjOperationProjectTaskTypeNameKey)
    aCoder.encode(typeEmployee, forKey: kObjOperationProjectTaskTypeEmployeeKey)
    aCoder.encode(typeId, forKey: kObjOperationProjectTaskTypeIdKey)
  }

}
