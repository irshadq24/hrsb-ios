//
//  ObjOperationTypeTaskEmp.swift
//
//  Created by Javed Multani on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationTypeTaskEmp: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationTypeTaskEmpEmployeeNameKey: String = "employee_name"
  private let kObjOperationTypeTaskEmpTypeNameKey: String = "type_name"
  private let kObjOperationTypeTaskEmpTypeEmployeeKey: String = "type_employee"
  private let kObjOperationTypeTaskEmpTypeIdKey: String = "type_id"

  // MARK: Properties
  public var employeeName: String?
  public var typeName: String?
  public var typeEmployee: String?
  public var typeId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    employeeName = json[kObjOperationTypeTaskEmpEmployeeNameKey].string
    typeName = json[kObjOperationTypeTaskEmpTypeNameKey].string
    typeEmployee = json[kObjOperationTypeTaskEmpTypeEmployeeKey].string
    typeId = json[kObjOperationTypeTaskEmpTypeIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = employeeName { dictionary[kObjOperationTypeTaskEmpEmployeeNameKey] = value }
    if let value = typeName { dictionary[kObjOperationTypeTaskEmpTypeNameKey] = value }
    if let value = typeEmployee { dictionary[kObjOperationTypeTaskEmpTypeEmployeeKey] = value }
    if let value = typeId { dictionary[kObjOperationTypeTaskEmpTypeIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.employeeName = aDecoder.decodeObject(forKey: kObjOperationTypeTaskEmpEmployeeNameKey) as? String
    self.typeName = aDecoder.decodeObject(forKey: kObjOperationTypeTaskEmpTypeNameKey) as? String
    self.typeEmployee = aDecoder.decodeObject(forKey: kObjOperationTypeTaskEmpTypeEmployeeKey) as? String
    self.typeId = aDecoder.decodeObject(forKey: kObjOperationTypeTaskEmpTypeIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(employeeName, forKey: kObjOperationTypeTaskEmpEmployeeNameKey)
    aCoder.encode(typeName, forKey: kObjOperationTypeTaskEmpTypeNameKey)
    aCoder.encode(typeEmployee, forKey: kObjOperationTypeTaskEmpTypeEmployeeKey)
    aCoder.encode(typeId, forKey: kObjOperationTypeTaskEmpTypeIdKey)
  }

}
