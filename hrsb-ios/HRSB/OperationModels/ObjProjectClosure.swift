//
//  ObjProjectClosure.swift
//
//  Created by Javed Multani on 07/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjProjectClosure: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjProjectClosureFileIdKey: String = "file_id"
  private let kObjProjectClosureProjectIdKey: String = "project_id"
  private let kObjProjectClosureAttachmentKey: String = "attachment"
  private let kObjProjectClosureDateKey: String = "date"
  private let kObjProjectClosureDateTimeKey: String = "date_time"
  private let kObjProjectClosureTypeKey: String = "type"
  private let kObjProjectClosureTimeKey: String = "time"
  private let kObjProjectClosureUrlPathKey: String = "url_path"

  // MARK: Properties
  public var fileId: String?
  public var projectId: String?
  public var attachment: String?
  public var date: String?
  public var dateTime: String?
  public var type: String?
  public var time: String?
  public var urlPath: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    fileId = json[kObjProjectClosureFileIdKey].string
    projectId = json[kObjProjectClosureProjectIdKey].string
    attachment = json[kObjProjectClosureAttachmentKey].string
    date = json[kObjProjectClosureDateKey].string
    dateTime = json[kObjProjectClosureDateTimeKey].string
    type = json[kObjProjectClosureTypeKey].string
    time = json[kObjProjectClosureTimeKey].string
    urlPath = json[kObjProjectClosureUrlPathKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fileId { dictionary[kObjProjectClosureFileIdKey] = value }
    if let value = projectId { dictionary[kObjProjectClosureProjectIdKey] = value }
    if let value = attachment { dictionary[kObjProjectClosureAttachmentKey] = value }
    if let value = date { dictionary[kObjProjectClosureDateKey] = value }
    if let value = dateTime { dictionary[kObjProjectClosureDateTimeKey] = value }
    if let value = type { dictionary[kObjProjectClosureTypeKey] = value }
    if let value = time { dictionary[kObjProjectClosureTimeKey] = value }
    if let value = urlPath { dictionary[kObjProjectClosureUrlPathKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fileId = aDecoder.decodeObject(forKey: kObjProjectClosureFileIdKey) as? String
    self.projectId = aDecoder.decodeObject(forKey: kObjProjectClosureProjectIdKey) as? String
    self.attachment = aDecoder.decodeObject(forKey: kObjProjectClosureAttachmentKey) as? String
    self.date = aDecoder.decodeObject(forKey: kObjProjectClosureDateKey) as? String
    self.dateTime = aDecoder.decodeObject(forKey: kObjProjectClosureDateTimeKey) as? String
    self.type = aDecoder.decodeObject(forKey: kObjProjectClosureTypeKey) as? String
    self.time = aDecoder.decodeObject(forKey: kObjProjectClosureTimeKey) as? String
    self.urlPath = aDecoder.decodeObject(forKey: kObjProjectClosureUrlPathKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fileId, forKey: kObjProjectClosureFileIdKey)
    aCoder.encode(projectId, forKey: kObjProjectClosureProjectIdKey)
    aCoder.encode(attachment, forKey: kObjProjectClosureAttachmentKey)
    aCoder.encode(date, forKey: kObjProjectClosureDateKey)
    aCoder.encode(dateTime, forKey: kObjProjectClosureDateTimeKey)
    aCoder.encode(type, forKey: kObjProjectClosureTypeKey)
    aCoder.encode(time, forKey: kObjProjectClosureTimeKey)
    aCoder.encode(urlPath, forKey: kObjProjectClosureUrlPathKey)
  }

}
