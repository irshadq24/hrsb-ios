//
//  ObjRating.swift
//
//  Created by Javed Multani on 07/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjRating: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjRatingRatingValueKey: String = "rating_value"
  private let kObjRatingRatingIdKey: String = "rating_id"

  // MARK: Properties
  public var ratingValue: String?
  public var ratingId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    ratingValue = json[kObjRatingRatingValueKey].string
    ratingId = json[kObjRatingRatingIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = ratingValue { dictionary[kObjRatingRatingValueKey] = value }
    if let value = ratingId { dictionary[kObjRatingRatingIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.ratingValue = aDecoder.decodeObject(forKey: kObjRatingRatingValueKey) as? String
    self.ratingId = aDecoder.decodeObject(forKey: kObjRatingRatingIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(ratingValue, forKey: kObjRatingRatingValueKey)
    aCoder.encode(ratingId, forKey: kObjRatingRatingIdKey)
  }

}
