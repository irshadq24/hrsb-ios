//
//  ObjOperationEmp.swift
//
//  Created by Javed Multani on 31/10/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationEmp: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationEmpEmployeeNameKey: String = "employee_name"
  private let kObjOperationEmpEmployeeIdKey: String = "employee_id"
  private let kObjOperationEmpUserIdKey: String = "user_id"

  // MARK: Properties
  public var employeeName: String?
  public var employeeId: String?
  public var userId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    employeeName = json[kObjOperationEmpEmployeeNameKey].string
    employeeId = json[kObjOperationEmpEmployeeIdKey].string
    userId = json[kObjOperationEmpUserIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = employeeName { dictionary[kObjOperationEmpEmployeeNameKey] = value }
    if let value = employeeId { dictionary[kObjOperationEmpEmployeeIdKey] = value }
    if let value = userId { dictionary[kObjOperationEmpUserIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.employeeName = aDecoder.decodeObject(forKey: kObjOperationEmpEmployeeNameKey) as? String
    self.employeeId = aDecoder.decodeObject(forKey: kObjOperationEmpEmployeeIdKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kObjOperationEmpUserIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(employeeName, forKey: kObjOperationEmpEmployeeNameKey)
    aCoder.encode(employeeId, forKey: kObjOperationEmpEmployeeIdKey)
    aCoder.encode(userId, forKey: kObjOperationEmpUserIdKey)
  }

}
