//
//  ObjOperationPlanner.swift
//
//  Created by Javed Multani on 26/10/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationPlanner: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationPlannerOpuCodeKey: String = "opu_code"
  private let kObjOperationPlannerOpuIdKey: String = "opu_id"
  private let kObjOperationPlannerOpuNameKey: String = "opu_name"

  // MARK: Properties
  public var opuCode: String?
  public var opuId: String?
  public var opuName: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    opuCode = json[kObjOperationPlannerOpuCodeKey].string
    opuId = json[kObjOperationPlannerOpuIdKey].string
    opuName = json[kObjOperationPlannerOpuNameKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = opuCode { dictionary[kObjOperationPlannerOpuCodeKey] = value }
    if let value = opuId { dictionary[kObjOperationPlannerOpuIdKey] = value }
    if let value = opuName { dictionary[kObjOperationPlannerOpuNameKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.opuCode = aDecoder.decodeObject(forKey: kObjOperationPlannerOpuCodeKey) as? String
    self.opuId = aDecoder.decodeObject(forKey: kObjOperationPlannerOpuIdKey) as? String
    self.opuName = aDecoder.decodeObject(forKey: kObjOperationPlannerOpuNameKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(opuCode, forKey: kObjOperationPlannerOpuCodeKey)
    aCoder.encode(opuId, forKey: kObjOperationPlannerOpuIdKey)
    aCoder.encode(opuName, forKey: kObjOperationPlannerOpuNameKey)
  }

}
