//
//  ObjOperationType.swift
//
//  Created by Javed Multani on 26/10/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationType: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationTypeTypeOptionIdKey: String = "type_option_id"
  private let kObjOperationTypeTypeStatusKey: String = "type_status"

  // MARK: Properties
  public var typeOptionId: String?
  public var typeStatus: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    typeOptionId = json[kObjOperationTypeTypeOptionIdKey].string
    typeStatus = json[kObjOperationTypeTypeStatusKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = typeOptionId { dictionary[kObjOperationTypeTypeOptionIdKey] = value }
    if let value = typeStatus { dictionary[kObjOperationTypeTypeStatusKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.typeOptionId = aDecoder.decodeObject(forKey: kObjOperationTypeTypeOptionIdKey) as? String
    self.typeStatus = aDecoder.decodeObject(forKey: kObjOperationTypeTypeStatusKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(typeOptionId, forKey: kObjOperationTypeTypeOptionIdKey)
    aCoder.encode(typeStatus, forKey: kObjOperationTypeTypeStatusKey)
  }

}
