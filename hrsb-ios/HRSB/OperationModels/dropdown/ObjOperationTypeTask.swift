//
//  ObjOperationTypeTask.swift
//
//  Created by Javed Multani on 26/10/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationTypeTask: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationTypeTaskTypeCodeKey: String = "type_code"
  private let kObjOperationTypeTaskTypeNameKey: String = "type_name"
  private let kObjOperationTypeTaskTypeEmployeeKey: String = "type_employee"
  private let kObjOperationTypeTaskTypeOptionKey: String = "type_option"
  private let kObjOperationTypeTaskTypeEmployeeNameKey: String = "type_employee_name"
  private let kObjOperationTypeTaskTypeIdKey: String = "type_id"

  // MARK: Properties
  public var typeCode: String?
  public var typeName: String?
  public var typeEmployee: String?
  public var typeOption: String?
  public var typeEmployeeName: String?
  public var typeId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    typeCode = json[kObjOperationTypeTaskTypeCodeKey].string
    typeName = json[kObjOperationTypeTaskTypeNameKey].string
    typeEmployee = json[kObjOperationTypeTaskTypeEmployeeKey].string
    typeOption = json[kObjOperationTypeTaskTypeOptionKey].string
    typeEmployeeName = json[kObjOperationTypeTaskTypeEmployeeNameKey].string
    typeId = json[kObjOperationTypeTaskTypeIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = typeCode { dictionary[kObjOperationTypeTaskTypeCodeKey] = value }
    if let value = typeName { dictionary[kObjOperationTypeTaskTypeNameKey] = value }
    if let value = typeEmployee { dictionary[kObjOperationTypeTaskTypeEmployeeKey] = value }
    if let value = typeOption { dictionary[kObjOperationTypeTaskTypeOptionKey] = value }
    if let value = typeEmployeeName { dictionary[kObjOperationTypeTaskTypeEmployeeNameKey] = value }
    if let value = typeId { dictionary[kObjOperationTypeTaskTypeIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.typeCode = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeCodeKey) as? String
    self.typeName = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeNameKey) as? String
    self.typeEmployee = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeEmployeeKey) as? String
    self.typeOption = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeOptionKey) as? String
    self.typeEmployeeName = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeEmployeeNameKey) as? String
    self.typeId = aDecoder.decodeObject(forKey: kObjOperationTypeTaskTypeIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(typeCode, forKey: kObjOperationTypeTaskTypeCodeKey)
    aCoder.encode(typeName, forKey: kObjOperationTypeTaskTypeNameKey)
    aCoder.encode(typeEmployee, forKey: kObjOperationTypeTaskTypeEmployeeKey)
    aCoder.encode(typeOption, forKey: kObjOperationTypeTaskTypeOptionKey)
    aCoder.encode(typeEmployeeName, forKey: kObjOperationTypeTaskTypeEmployeeNameKey)
    aCoder.encode(typeId, forKey: kObjOperationTypeTaskTypeIdKey)
  }

}
