//
//  ObjOperationTypeWork.swift
//
//  Created by Javed Multani on 26/10/2019
//  Copyright (c) Javed Multani. All rights reserved.
//

import Foundation
import SwiftyJSON

public class ObjOperationTypeWork: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kObjOperationTypeWorkTypeCodeKey: String = "type_code"
  private let kObjOperationTypeWorkTypeNameKey: String = "type_name"
  private let kObjOperationTypeWorkTypeEmployeeKey: String = "type_employee"
  private let kObjOperationTypeWorkTypeOptionKey: String = "type_option"
  private let kObjOperationTypeWorkTypeEmployeeNameKey: String = "type_employee_name"
  private let kObjOperationTypeWorkTypeIdKey: String = "type_id"

  // MARK: Properties
  public var typeCode: String?
  public var typeName: String?
  public var typeEmployee: String?
  public var typeOption: String?
  public var typeEmployeeName: String?
  public var typeId: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    typeCode = json[kObjOperationTypeWorkTypeCodeKey].string
    typeName = json[kObjOperationTypeWorkTypeNameKey].string
    typeEmployee = json[kObjOperationTypeWorkTypeEmployeeKey].string
    typeOption = json[kObjOperationTypeWorkTypeOptionKey].string
    typeEmployeeName = json[kObjOperationTypeWorkTypeEmployeeNameKey].string
    typeId = json[kObjOperationTypeWorkTypeIdKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = typeCode { dictionary[kObjOperationTypeWorkTypeCodeKey] = value }
    if let value = typeName { dictionary[kObjOperationTypeWorkTypeNameKey] = value }
    if let value = typeEmployee { dictionary[kObjOperationTypeWorkTypeEmployeeKey] = value }
    if let value = typeOption { dictionary[kObjOperationTypeWorkTypeOptionKey] = value }
    if let value = typeEmployeeName { dictionary[kObjOperationTypeWorkTypeEmployeeNameKey] = value }
    if let value = typeId { dictionary[kObjOperationTypeWorkTypeIdKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.typeCode = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeCodeKey) as? String
    self.typeName = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeNameKey) as? String
    self.typeEmployee = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeEmployeeKey) as? String
    self.typeOption = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeOptionKey) as? String
    self.typeEmployeeName = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeEmployeeNameKey) as? String
    self.typeId = aDecoder.decodeObject(forKey: kObjOperationTypeWorkTypeIdKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(typeCode, forKey: kObjOperationTypeWorkTypeCodeKey)
    aCoder.encode(typeName, forKey: kObjOperationTypeWorkTypeNameKey)
    aCoder.encode(typeEmployee, forKey: kObjOperationTypeWorkTypeEmployeeKey)
    aCoder.encode(typeOption, forKey: kObjOperationTypeWorkTypeOptionKey)
    aCoder.encode(typeEmployeeName, forKey: kObjOperationTypeWorkTypeEmployeeNameKey)
    aCoder.encode(typeId, forKey: kObjOperationTypeWorkTypeIdKey)
  }

}
