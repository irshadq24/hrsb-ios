//
//  ProcurementVC.swift
//  HRSB
//
//  Created by Salman on 01/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ProcurementVC: UIViewController {
    @IBOutlet weak var tbleview: UITableView!
    var arrImg = ["ticket_icon","ticket_icon"]
    var arrLbl = ["Tendor Report","Proposal Tracking"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}

extension ProcurementVC: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ITMenuCell", for: indexPath) as! ITMenuCell
        cell.lblName.text = arrLbl[indexPath.row]
        cell.img.image = UIImage(named: arrImg[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "TenderReportListVC")as! TenderReportListVC
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "ProposalTrackingList")as! ProposalTrackingList
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
