//
//  ChangePasswordViewController.swift
//  HRSB
//
//  Created by BestWeb on 26/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class ChangePasswordViewController: UIViewController,MDCTabBarDelegate,MDCTabBarControllerDelegate,UIScrollViewDelegate,UITextFieldDelegate {

    
    var appBarViewController = MDCAppBarViewController()

    
    @IBOutlet weak var view_white: UIView!
    
    
    
    @IBOutlet weak var txt_old: UITextField!
    
    
    @IBOutlet weak var txt_new: UITextField!
    
    
    @IBOutlet weak var scrol: UIScrollView!
    
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var l3: UILabel!
    
    @IBOutlet weak var txt_confirm: UITextField!
    
    
    @IBOutlet weak var l1: UILabel!
    
    @IBOutlet weak var req: UIButton!
    
    @IBOutlet weak var l2: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        
        
        
        
        scrol.delegate = self
        
        txt_confirm.delegate = self
        txt_old.delegate = self
        txt_new.delegate = self

        
        let menuItemImage = UIImage(named: "back.png")
        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        
        
        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        
        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.BackAction))
        self.navigationItem.leftBarButtonItem = menuItem
        
        self.navigationItem.title = "Change Password"


        req.layer.cornerRadius = 20
        
        scrol.contentSize = CGSize(width: view.frame.width, height: 1500)
        
        
        
        l1.frame = CGRect(x: 20, y: 30, width: view.frame.width - 40, height: 16)
        
        txt_old.frame = CGRect(x: 20, y: l1.frame.origin.y + l1.frame.height + 15, width: view.frame.width - 40, height: 40)

        
        l2.frame = CGRect(x: 20, y: txt_old.frame.origin.y + txt_old.frame.height + 15, width: view.frame.width - 40, height: 16)

        txt_new.frame = CGRect(x: 20, y: l2.frame.origin.y + l2.frame.height + 15, width: view.frame.width - 40, height: 40)

        
        
        l3.frame = CGRect(x: 20, y: txt_new.frame.origin.y + txt_new.frame.height + 15, width: view.frame.width - 40, height: 16)


        
        txt_confirm.frame = CGRect(x: 20, y: l3.frame.origin.y + l3.frame.height + 15, width: view.frame.width - 40, height: 40)
        
        
        req.frame = CGRect(x: view.frame.width/2 - 75, y: txt_confirm.frame.origin.y + txt_confirm.frame.height + 50, width: 150, height: 40)

        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func back_Action(_ sender: Any) {
        
        
        //dismiss(animated: false, completion: nil)
        navigationController?.popViewController(animated: true)

        
        
    }
    
    @objc func BackAction(){
        navigationController?.popViewController(animated: true)
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)
        
    }
    
    
    
    
    
    
    
    @IBAction func Req_action(_ sender: Any) {
        
        
        let oldpwd = CommonFunction.shared.trimString(text:  self.txt_old.text)
        let newpwd = CommonFunction.shared.trimString(text: self.txt_new.text)
        let confnewpwd = CommonFunction.shared.trimString(text: self.txt_confirm.text)

        
        if oldpwd == "" {
            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Enter phone number")
        } else if newpwd == "" {
            CustomAlertController.alert(title: Constant.ErrorMessage.Alert, message: "Enter password")
        } else {
            self.Changepass(oldpwd: oldpwd, newpwd: newpwd, confrmPwd: confnewpwd)
        }

        
        
        
        
    }
    
    
    
    
    
    private func Changepass(oldpwd: String, newpwd: String, confrmPwd: String){

    
        ServiceHelper.sharedInstance.ChangePwd(Newpwd: newpwd, ConfirmNewPwd: confrmPwd, userid: "6", type: "change_password"){(result, error)in
            
            if let resultValue = result as? [String : Any]{
                
                
                print("failed",resultValue)

                
            }
            
            
        }
    
    }
    
    
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        
        txt_new.resignFirstResponder()
        txt_old.resignFirstResponder()
        
        txt_confirm.resignFirstResponder()

        
        return true
    }
    

    

}
