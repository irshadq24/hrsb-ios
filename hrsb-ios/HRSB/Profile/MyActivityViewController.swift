//
//  MyActivityViewController.swift
//  HRSB
//
//  Created by BestWeb on 19/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class MyActivityViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    
    @IBOutlet weak var view_white: UIView!
    
    @IBOutlet var view_activitylist: UIView!
    
    
    
    @IBOutlet weak var tble_activitylist: UITableView!
    
    
    @IBOutlet weak var tilebtn: UIButton!
    
    @IBOutlet var plusbtn: UIView!
    let reuseIdentifier = "MyActivityCell"
    
    
    let reuseIdentifier1 = "ActivityListCell"

    var appBarViewController = MDCAppBarViewController()

    var items = ["Security", "Language","Support"]

    
    @IBOutlet var view_tile: UIView!
    
    @IBOutlet weak var scrol_tilr: UIScrollView!
    
    
    @IBOutlet weak var tble: UITableView!
    
    @IBOutlet weak var type: UIButton!
    
    
    
    @IBOutlet weak var back: UIButton!
    
    
    
    
    @IBOutlet weak var filter: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tble.dataSource = self
        tble.delegate = self
        
        
        tble_activitylist.dataSource = self
        tble_activitylist.delegate = self

        
//        self.view.addSubview(plusbtn)
//        
//        filter.layer.cornerRadius = 25
//        
//        
//        plusbtn.frame = CGRect(x: self.view.frame.width - 120, y: self.view.frame.height - 120, width: 100, height: 100)
//        
//        
//        view_activitylist.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//
//
//        
//        
//        self.addChild(self.appBarViewController)
//        self.view.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//        
//        
//        
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//        
//        
//        
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
//        
//        
//        
//        
//        
//        
//        
//        let menuItemImage = UIImage(named: "back.png")
//        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
//        
//        
//        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
//        
//        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
//                                       style: .plain,
//                                       target: self,
//                                       action: #selector(self.MenuAction))
//        self.navigationItem.leftBarButtonItem = menuItem
//        
//        self.navigationItem.title = "My Activity"
//
//        
//        
//
//        self.setheight()
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    @objc func MenuAction(){
        
        
        self.dismiss(animated: false, completion: nil)

        
        
    }
    
    
    @IBAction func back_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)
                
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//self.dismiss(animated: false, completion: nil)

    }
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = appBarViewController.headerView.frame.origin.y + appBarViewController.headerView.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }

    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tble{
        
        
        
        return self.items.count
        
    }
        else{
            
            
            return self.items.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tble{

        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! MyActivityCell
        
        
        
        
        
        cell.l1.text = items[indexPath.row]
        cell.l2.text = "date"
        cell.l3.text = "time"

        
        return cell
            
        }else{
            
            
            
            let cell = self.tble_activitylist.dequeueReusableCell(withIdentifier: self.reuseIdentifier1, for: indexPath) as! ActivityListCell
            
            
            
            
            
            cell.l1.text = items[indexPath.row]

            
            
            
            return cell
            
        }
            
            
            
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 60
    }
    

    
    
    
    
    
    @IBAction func tilebtn_action(_ sender: Any) {
        
        
        self.view.addSubview(view_tile)
        
        
        
        
    }
    
    
    
    
    @IBAction func type_btn(_ sender: Any) {
        
        self.view.addSubview(view_activitylist)

        
        
    }
    
    
    @IBAction func filter_btn(_ sender: Any) {
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
}
