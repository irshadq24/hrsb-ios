//
//  ProfileViewController.swift
//  HRSB
//
//  Created by BestWeb on 21/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
protocol SlideMenuDelegate1 {
    func slideMenuItemSelectedAtIndex1(_ index : Int32)
    
    
}
class ProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {
    
    
    
    
    @IBOutlet var l3: UILabel!
    @IBOutlet var l1: UILabel!
    
    @IBOutlet var lbl_desig: UILabel!
    
    
    @IBOutlet var lbl_name: UILabel!
    
    
    @IBOutlet var l2: UILabel!
    
    @IBOutlet var lbl_mail: UILabel!
    
    
    @IBOutlet var lbl_mbl: UILabel!
    
    
    @IBOutlet var l4: UILabel!
    
    @IBOutlet weak var tble: UITableView!
    
    
    @IBOutlet weak var vies: UIView!
    
    @IBOutlet var l5: UILabel!
    
    
    let reuseIdentifier = "ProfileCell"
    
    
    
    var items = ["Settings", "My Activity", "Logout"]
    
    
    var Images = ["Setting.png", "Activity.png", "Logout.png"]

    var appBarViewController = MDCAppBarViewController()

    @IBOutlet var lbl_staff: UILabel!
    
    
    var btnMenu1 : UIButton!
    var delegate1 : SlideMenuDelegate1?
    
    @IBOutlet weak var Img_pofile: UIImageView!
    @IBOutlet weak var btnCloseMenuOverlay: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        print("EMAIL", CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.Email))
        let url = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.profilePic)
        ApiManager.getImage(url) { (image) in
            self.Img_pofile.image = image
            self.Img_pofile.makeRounded()
        }
        lbl_mail.text = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.Email)
        lbl_mbl.text = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.MobileNumber)
        lbl_name.text = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.Name)
        lbl_desig.text = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.Designation)
        lbl_staff.text = CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.EmpId)
        tble.dataSource = self
        tble.delegate = self
        
        
        vies.frame = CGRect(x: 75, y: 0, width: view.frame.width, height: view.frame.height)

        
//        self.addChild(self.appBarViewController)
//        self.vies.addSubview(self.appBarViewController.view)
//        self.appBarViewController.didMove(toParent: self)
//
//
//        // Set the tracking scroll view.
//        self.appBarViewController.headerView.trackingScrollView = nil
//        self.appBarViewController.navigationBar.backItem = nil
//
//
//        self.appBarViewController.headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 113/255.0, blue: 103/255.0, alpha: 1.0)
//
//
//        self.appBarViewController.headerView.frame = CGRect(x: view.frame.origin.x, y: 0, width: view.frame.width - 75, height: view.frame.height)


        self.navigationItem.title = "Profile"




        navigationController?.navigationBar.barTintColor = .white

        
        vies.addSubview(Img_pofile)
        
        
        Img_pofile.frame = CGRect(x: self.vies.frame.width/2 - 70, y:  90, width: 70, height: 70)

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                print("iPhone X, XS")
                
                
                Img_pofile.frame = CGRect(x: self.vies.frame.width/2 - 70, y:  130, width: 70, height: 70)

                
            case 1792:
                print("iPhone XR")
                
                Img_pofile.frame = CGRect(x: self.vies.frame.width/2 - 70, y:  130, width: 70, height: 70)

                
            default:
                break
            }
        }
        

        
        
        
        
        l1.frame  = CGRect(x: 25, y: Img_pofile.frame.origin.y + Img_pofile.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        lbl_name.frame  = CGRect(x: 25, y: l1.frame.origin.y + l1.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        l2.frame  = CGRect(x: 25, y: lbl_name.frame.origin.y + lbl_name.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        lbl_mail.frame  = CGRect(x: 25, y: l2.frame.origin.y + l2.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        l3.frame  = CGRect(x: 25, y: lbl_mail.frame.origin.y + lbl_mail.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        lbl_mbl.frame  = CGRect(x: 25, y: l3.frame.origin.y + l3.frame.height + 15, width: vies.frame.width - 30, height: 16)

        l4.frame  = CGRect(x: 25, y: lbl_mbl.frame.origin.y + lbl_mbl.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        lbl_desig.frame  = CGRect(x: 25, y: l4.frame.origin.y + l4.frame.height + 15, width: vies.frame.width - 30, height: 16)


        l5.frame  = CGRect(x: 25, y: lbl_desig.frame.origin.y + lbl_desig.frame.height + 15, width: vies.frame.width - 30, height: 16)
        
        lbl_staff.frame  = CGRect(x: 25, y: l5.frame.origin.y + l5.frame.height + 15, width: vies.frame.width - 30, height: 16)

        
        self.setheight()
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        
        
        
        
        btnMenu1.tag = 0
        
        if (self.delegate1 != nil) {
            var index = Int32(sender.tag)
            if(sender == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate1?.slideMenuItemSelectedAtIndex1(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
        

        
        
        
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! ProfileCell
        
        
        
        
        
        cell.l1.text = items[indexPath.row]
        
        cell.img.image = UIImage(named: Images[indexPath.row])
        
        
        
        
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        if indexPath.row == 0{
            
            
            print(0)
            
//            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.SettingVC)
//            self.present(HrVc, animated: true, completion: nil)
//
//
//            let transition = CATransition()
//            transition.duration = 0.3
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            view.window!.layer.add(transition, forKey: kCATransition)
            

            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.SettingVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)

            
            
        }else if indexPath.row == 1{
            
//            let transition = CATransition()
//            transition.duration = 0.3
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            view.window!.layer.add(transition, forKey: kCATransition)
//
//            let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.MyActivity)
//
//
//            present(presentedVC, animated: false, completion: nil)

//            print(1)
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.MyActivity)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)

            
        }else if indexPath.row == 2{
                kAppDelegate.logout()
        }
    }
    
    
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = lbl_staff.frame.origin.y + lbl_staff.frame.height + 15
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (50 * items.count))
        
        
        
        
        
    }
    


}
