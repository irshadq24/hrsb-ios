//
//  SettingsViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    var appBarViewController = MDCAppBarViewController()

    @IBOutlet weak var back: UIButton!
    
    
    @IBOutlet weak var tble: UITableView!
    
    let reuseIdentifier = "SettingsCell"
    
    var items = [["title":"Change Password", "img":"changePass"],["title":"Language", "img":"language"],["title":"Support", "img":"support"]]
    
    @IBOutlet weak var view_white: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tble.dataSource = self
        tble.delegate = self
        

        
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        
        
        
        
        
        
        let menuItemImage = UIImage(named: "back.png")
        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        
        
        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        
        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.MenuAction))
        self.navigationItem.leftBarButtonItem = menuItem
        
        self.navigationItem.title = "Settings"
        
        
        
        
        self.setheight()


        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = appBarViewController.headerView.frame.origin.y + appBarViewController.headerView.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }

    
    
    
    
    @IBAction func back_Acton(_ sender: Any) {
        navigationController?.popViewController(animated: true)

        
        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        dismiss(animated: false, completion: nil)

    }
    
    
    
    @objc func MenuAction(){
        
        navigationController?.popViewController(animated: true)
        //self.dismiss(animated: false, completion: nil)
        
        
        
    }
    

    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! SettingsCell
        
        
        
        
        
        cell.l1.text = items[indexPath.row]["title"] ?? ""
        cell.img.image = UIImage(named: "\(items[indexPath.row]["img"] ?? "")")
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 60
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        if indexPath.row == 0{
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.ChangePwdVC)
            //self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)

        }
        else if indexPath.row == 0{
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.SettingVC)
           // self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)

        }
        else if indexPath.row == 0{
            
            
            
            let HrVc = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.SettingVC)
         //   self.present(HrVc, animated: true, completion: nil)
            navigationController?.pushViewController(HrVc, animated: true)

        }
        
    }
}
