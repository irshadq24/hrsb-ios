//
//  ProposalDetailsVC.swift
//  HRSB
//
//  Created by Salman on 03/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class DateModel {

    var title = ""
    var reviewDate: Date?
    var submissionDate: Date?
    init(title: String, reviewDate: Date?, submissionDate: Date?) {
        self.title = title
        self.reviewDate = reviewDate
        self.submissionDate = submissionDate
    }
    
}

class ProposalDetailsVC: UIViewController {

    @IBOutlet weak var viewAssignHeight: NSLayoutConstraint!
    @IBOutlet weak var viewFinalDate: UIView!
    @IBOutlet weak var viewAssignEmp: UIView!
    var proposalId = ""
    var arrModel = [ReportModel]()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnReviewDate: UIButton!
    @IBOutlet weak var btnAssignEmployee: UIButton!
    @IBOutlet weak var btnDateFinal: UIButton!
    
    
    var arrEmployee = [[String: Any]]()
    let dropDown = DropDown()
    var selectedEmployee = [[String: Any]]()
    var dictProposalDetail = [String: Any]()
    var arrDateModel = [DateModel]()
    var isForDetail = true
    @IBOutlet weak var btnEmployee: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAssignEmployee.makeRoundCorner(20)
        btnAssignEmployee.makeBorder(1, color: UIColor.lightGray)
        getAllEmployee()
        
        viewAssignEmp.isHidden = false
        viewFinalDate.isHidden = true
        arrModel.removeAll()
        viewAssignHeight.constant = 180
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchDetail()
    }
    
    func getAllEmployee() {
        let endPoint = MethodName.getNormalEmployee
        ApiManager.request(path: endPoint, parameters: ["user_id":getUserDefault(Constant.UserDefaultsKey.UserId)], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let subCate = dict["data"] as? [[String: Any]] {
                    self?.arrEmployee = subCate
                    self?.populateSelectedEmp()
                    
                }
            case .failure(_):
                break
            case .noDataFound(_):
                break
            }
        }
    }
    
    
    
    @IBAction func tapEmployee(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // Action triggered on selection
        var arrSubCat = [String]()
        for sub in arrEmployee {
            let name = sub["employee_name"] as? String ?? ""
            arrSubCat.append(name)
        }
        dropDown.dataSource = arrSubCat
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.selectedEmployee.append(self?.arrEmployee[index] ?? [:])
            //sender.setTitle(item, for: .normal)
            self?.collectionView.reloadData()
        }
        dropDown.show()
    }
    
    @IBAction func tapUpdateEmp(_ sender: Any) {
        let endPoint = MethodName.updateProposal
        var empId = ""
        for emp in selectedEmployee {
            empId = "\(empId),\(emp["user_id"] as? String ?? "")"
        }
        let params = ["type":"update_proposal_assigned_employee", "assigned_employee":"\(empId)", "proposal_id":"\(proposalId)"]
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let status = dict["status"] as? String, status == "1" {
                    self?.showAlert(title: "Success", message: "Successfully assign employee.")
                }
                print(data)
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    
    @IBAction func tapFinalDate(_ sender: UIButton) {
        ViewDatePicker.show { (selectedDate) in
            guard let _ = selectedDate else {
                return
            }
            sender.setTitle(selectedDate!.toString(dateFormat: "yyyy-MM-dd"), for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            print(selectedDate!)
        }
    }
    
    @IBAction func tapUpdateDates(_ sender: Any) {
        for data in arrDateModel {
            if data.reviewDate == nil || data.submissionDate == nil {
                self.showAlert(title: "", message: "Please fill all dates")
                return
            }
        }
        let endPoint = MethodName.updateProposal

        let params = ["date_of_submission": btnDateFinal.titleLabel?.text ?? "",
                      "type":"update_proposal_date",
                      "proposal_id":proposalId,
                      "review_date_one": arrDateModel[0].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_two": arrDateModel[1].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_three": arrDateModel[2].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_four": arrDateModel[3].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_five": arrDateModel[4].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_six": arrDateModel[5].reviewDate?.toString(dateFormat: "yyyy-MM-dd") ,
                      "review_date_seven": arrDateModel[6].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_eight": arrDateModel[7].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_nine": arrDateModel[8].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "review_date_ten": arrDateModel[9].reviewDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_one": arrDateModel[0].submissionDate?.toString(dateFormat: "yyyy-MM-dd") ,
                      "proposal_submission_date_two": arrDateModel[1].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_three": arrDateModel[2].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_four": arrDateModel[3].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_five": arrDateModel[4].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_six": arrDateModel[5].submissionDate?.toString(dateFormat: "yyyy-MM-dd") ,
                      "proposal_submission_date_seven": arrDateModel[6].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_eight": arrDateModel[7].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_nine": arrDateModel[8].submissionDate?.toString(dateFormat: "yyyy-MM-dd"),
                      "proposal_submission_date_ten": arrDateModel[9].submissionDate?.toString(dateFormat: "yyyy-MM-dd")
        ]
        print(params)
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params as [String : Any], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                print(data)
                if let dict = data as? [String: Any], let status = dict["status"] as? String, status != "0" {
                    self?.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
        
        //proposalId
    }
    
    
    @IBAction func tapEdit(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditPurposalVC") as! EditPurposalVC
        vc.dictProposalDetail = dictProposalDetail
        vc.proposalId = proposalId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func fetchDetail() {
        let endPoint = MethodName.getProposalDetail
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "read_proposal", "proposal_id":"\(proposalId)"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let data = dict["data"] as? [String: Any] {
                    self?.setupModel(info: data)
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func populateSelectedEmp() {
        self.selectedEmployee.removeAll()
        if let selectedEmp = dictProposalDetail["assigned_employee"] as? [String] {
            print("ids:",selectedEmp)
            for id in selectedEmp {
                let emp = self.arrEmployee.filter { (data) -> Bool in
                    return (data["user_id"] as? String ?? "") == id
                }
                if emp.count > 0 {
                    self.selectedEmployee.append(emp[0])
                }
            }
            self.collectionView.reloadData()
        }
    }
    
    func setupModel(info: [String: Any], forReviewDate: Bool = false) {
        isForDetail = !forReviewDate
        if !forReviewDate {
            viewAssignEmp.isHidden = false
            viewFinalDate.isHidden = true
            arrModel.removeAll()
            viewAssignHeight.constant = 180
            dictProposalDetail = info
            arrModel.removeAll()
            populateSelectedEmp()
            
            
            arrModel.append(ReportModel(info: ["title": "Company", "description": info["company"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Client", "description": info["client"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Tender Date Received", "description": info["tender_date_receive"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Type", "description": info["tender_type"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Description", "description": info["description"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Closing Date", "description": info["closing_submitted_date"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Time", "description": info["time"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Tender/RFQ/ITQ REF. NO:", "description": info["reference_no"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Method of Sending", "description": info["meathod_of_sending"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Site visit/ Tender Briefing", "description": info["site_visit"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Status", "description": info["status"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Value (RM)", "description": "RM \(info["value"] as? String ?? "")"]))
            arrModel.append(ReportModel(info: ["title": "Remark", "description": info["remark"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "1st extension", "description": info["extension_date_one"] as? String ?? "-"]))
            arrModel.append(ReportModel(info: ["title": "2nd extension", "description": info["extension_date_two"] as? String ?? "-"]))
            arrModel.append(ReportModel(info: ["title": "3rd extension", "description": info["extension_date_three"] as? String ?? "-"]))
            arrModel.append(ReportModel(info: ["title": "Reason for extension", "description": info["reason"] as? String ?? ""]))
            arrModel.append(ReportModel(info: ["title": "Attachment", "description": info["tender_file"] as? String ?? ""]))
        } else {
            viewAssignHeight.constant = 0
            arrDateModel.removeAll()
            viewAssignEmp.isHidden = true
            viewFinalDate.isHidden = false
            print("Received:",info)
            if let strReviewDate = info["review_date_one"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_one"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "1st Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "1st Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_two"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_two"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "2nd Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "2nd Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_three"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_three"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "3rd Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "3rd Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_four"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_four"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "4th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "4th Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_five"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_five"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "5th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "1st Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_six"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_six"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "6th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "6th Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_seven"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_seven"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "7th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "7th Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_eight"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_eight"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "8th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "8th Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_nine"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_nine"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "9th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "9th Review Date", reviewDate: nil, submissionDate: nil))
            }
            if let strReviewDate = info["review_date_ten"] as? String, let reviewDate = strReviewDate.getDateInstance(validFormat: "yyyy-MM-dd"), let strSubmissionDate = info["proposal_submission_date_ten"] as? String, let submissionDate = strSubmissionDate.getDateInstance(validFormat: "yyyy-MM-dd")  {
                arrDateModel.append(DateModel(title: "10th Review Date", reviewDate: reviewDate, submissionDate: submissionDate))
            }else {
                arrDateModel.append(DateModel(title: "10th Review Date", reviewDate: nil, submissionDate: nil))
            }
        }
        
        self.tblView.reloadData()
    }
    
    
    @IBAction func tapDetailReviewDate(_ sender: UIButton) {
        btnDetails.backgroundColor = UIColor.white
        btnReviewDate.backgroundColor = UIColor.white
        btnReviewDate.setTitleColor(UIColor.getRGBColor(104, g: 56, b: 148), for: .normal)
        btnDetails.setTitleColor(UIColor.getRGBColor(104, g: 56, b: 148), for: .normal)
        sender.backgroundColor = UIColor.getRGBColor(104, g: 56, b: 148)
        sender.setTitleColor(.white, for: .normal)
        if sender == btnDetails {
            setupModel(info: dictProposalDetail)
        } else {
            setupModel(info: dictProposalDetail, forReviewDate: true)
        }
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ProposalDetailsVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForDetail {
            return arrModel.count
        }else{
            return arrDateModel.count
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isForDetail {
            return 65
        } else {
            return 125
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isForDetail {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellReportDetail", for: indexPath)
            let lblTitle = cell.viewWithTag(120) as! UILabel
            let lblDescription = cell.viewWithTag(121) as! UILabel
            lblTitle.text = arrModel[indexPath.row].title
            lblDescription.text = arrModel[indexPath.row].description
            if arrModel.count-1 == indexPath.row {
                lblDescription.textColor = .blue
            }else {
                lblDescription.textColor = .black
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellReviewDate", for: indexPath) as! CellReviewDate
            cell.info = arrDateModel[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrModel.count-1 == indexPath.row {
            if let url = URL(string: dictProposalDetail["url_path"] as? String ?? "") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}
extension ProposalDetailsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    @objc func didTapClose(_ sender: UIButton) {
        if let cell = sender.superview?.superview?.superview as? UICollectionViewCell, let indexPath = collectionView.indexPath(for: cell){
            selectedEmployee.remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedEmployee.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCell", for: indexPath)
        let lblName = cell.viewWithTag(121) as! UILabel
        let btnClose = cell.viewWithTag(120)  as! UIButton
        let viewContainer = cell.viewWithTag(123)
        lblName.text = selectedEmployee[indexPath.row]["employee_name"] as? String ?? ""
        viewContainer?.makeRoundCorner(19)
        viewContainer?.makeBorder(1, color: UIColor.lightGray)
        btnClose.addTarget(self, action: #selector(didTapClose(_:)), for: .touchUpInside)
        return cell
    }
}
