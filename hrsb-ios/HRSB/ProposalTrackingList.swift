//
//  ProposalTrackingList.swift
//  HRSB
//
//  Created by Salman on 01/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ProposalTrackingModel {
    var company = ""
    var tender_type = ""
    var tender_date_receive = Date()
    var closing_submitted_date = Date()
    var status = ""
    var proposal_id = ""
    
    
    
    init(info: [String: Any]) {
        self.company = info["company"] as? String ?? ""
        self.tender_type = info["tender_type"] as? String ?? ""
        self.status = info["status"] as? String ?? ""
        self.proposal_id = info["proposal_id"] as? String ?? ""
        if let strDate = info["tender_date_receive"] as? String, let date = strDate.getDateInstance(validFormat: "yyyy-MM-dd") {
            self.tender_date_receive = date
        }
        if let strDate = info["closing_submitted_date"] as? String, let date = strDate.getDateInstance(validFormat: "yyyy-MM-dd") {
            self.closing_submitted_date = date
        }
    }
}

class ProposalTrackingList: UIViewController {
    var arrProposal = [ProposalTrackingModel]()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchAllProposal()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func fetchAllProposal() {
        let endPoint = MethodName.getProposalTrackingList
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "proposal_tracking_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let tickets = dict["data"] as? [[String: Any]] {
                    self?.arrProposal.removeAll()
                    for ticket in tickets {
                        self?.arrProposal.append(ProposalTrackingModel(info: ticket))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }

}
extension ProposalTrackingList: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrProposal.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellProposal", for: indexPath) as! CellProposal
        cell.info = arrProposal[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProposalDetailsVC") as! ProposalDetailsVC
        vc.proposalId = arrProposal[indexPath.row].proposal_id
        navigationController?.pushViewController(vc, animated: true)
    }
}

