//
//  QualityDocViewController+Service.swift
//  HRSB
//
//  Created by Zakir Khan on 17/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension QualityDocViewController {
    func requestServer(param: [String: Any]?) {
        var endPoint = ""
        if qualityDoc == "Quality Documentation" {
            endPoint = "hseq/list_quality"
        } else if enviromentDoc == "Environment Documentation" {
            endPoint = "hseq/list_environment"
        } else if safetyDoc == "Safety Documentation" {
            endPoint = "hseq/list_safety"
        }
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    self.hSEQDocument = try decoder.decode(HSEQDocument.self, from: data)
                    Console.log("HSEQDocument:- \(self.hSEQDocument)")
                    tble.reloadData()
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func requestServerUpload(param: [String: Any]) {
       // let endPoint = MethodName.hrsqUploadDocument
        var endPoint = ""
        if qualityDoc == "Quality Documentation" {
            endPoint = "hseq/add_quality_document"
        } else if enviromentDoc == "Environment Documentation" {
            endPoint = "hseq/add_environment_document"
        } else if safetyDoc == "Safety Documentation" {
            endPoint = "hseq/add_safety_document"
        }
        var params = param
        let imgInfo = ["attachment_file": params["image"] as! UIImage]
        params.removeValue(forKey: "image")
        Loader.show()
        ApiManager.hitMultipartForImage(path: endPoint, params as! Dictionary<String, String>, imageInfo: imgInfo, unReachable: {
            Loader.hide()
            print("unreachable")
        }) { (response, code) in
            Loader.hide()
            let message = response?["message"]as? String ?? ""
            DisplayBanner.show(message: message)
            if code == 200 {
                if self.qualityDoc == "Quality Documentation" {
                    self.requestServer(param: ["document_type_id": "5"])
                } else if self.enviromentDoc == "Environment Documentation" {
                    self.requestServer(param: ["document_type_id": "6"])
                } else if self.safetyDoc == "Safety Documentation" {
                    self.requestServer(param: ["document_type_id": "4"])
                }
            }
        }
    }
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have camera.")
        }
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            kAppDelegate.window?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            DisplayBanner.show(message: "You don't have permission to access gallery.")
        }
    }

}


extension QualityDocViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        if qualityDoc == "Quality Documentation" {
            if let image = info[.editedImage] as? UIImage{
                let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
                requestServerUpload(param: ["add_type": "quality_document", "document_type_id": "5", "employee_id": userID , "image": image])
            }
        } else if enviromentDoc == "Environment Documentation" {
            if let image = info[.editedImage] as? UIImage{
                let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
                requestServerUpload(param: ["add_type": "environment_document", "document_type_id": "6", "employee_id": userID , "image": image])
            }
        } else if safetyDoc == "Safety Documentation" {
            if let image = info[.editedImage] as? UIImage{
                let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
                requestServerUpload(param: ["add_type": "safety_document", "document_type_id": "4", "employee_id": userID , "image": image])
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}


