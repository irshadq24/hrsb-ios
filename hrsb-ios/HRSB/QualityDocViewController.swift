//
//  QualityDocViewController.swift
//  HRSB
//
//  Created by BestWeb on 28/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents

class QualityDocViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    @IBOutlet weak var btnUpload: UIButton!
    let reuseIdentifier = "QualityDocCell"
    
    var appBarViewController = MDCAppBarViewController()
    var qualityDoc = ""
    var enviromentDoc = ""
    var safetyDoc = ""
    
    var items = ["Quality Documentation", "Environment Documentation", "Safety Documentation"]
    var hSEQDocument: HSEQDocument?

    
    
    @IBOutlet weak var tble: UITableView!
    @IBOutlet weak var labelDocName: UILabel!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var view_white: UIView!
    var chooseImage = UIImage()
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userRole = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.userRoleId)as? String ?? ""
        if userRole == "2" {
            btnUpload.isHidden = false
        } else {
            btnUpload.isHidden = true
        }
        
        if qualityDoc == "Quality Documentation" {
            labelDocName.text = qualityDoc
            requestServer(param: ["document_type_id": "5"])
        } else if enviromentDoc == "Environment Documentation" {
            labelDocName.text = enviromentDoc
            requestServer(param: ["document_type_id": "6"])
        } else if safetyDoc == "Safety Documentation" {
            labelDocName.text = safetyDoc
            requestServer(param: ["document_type_id": "4"])
        }
        
        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        
        
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        
        
        
        
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1.png")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)
        tble.dataSource = self
        tble.delegate = self
       // self.setheight()
        addSlideMenuButton()
        addSlideMenuButton1()
    }
    @IBAction func back_ACtion(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc func NotifyAction(){
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        navigationController?.pushViewController(presentedVC, animated: true)
    }
    
    
    func setheight(){
        
        
        var heig = Float()
        
        heig = Float(100 * items.count)
        
        print(heig)
        
        
        
        let displayheigth: CGFloat = view_white.frame.origin.y + view_white.frame.height
        
        
        let Widht = self.view.frame.width
        
        
        tble.frame = CGRect(x: 0, y: Int(displayheigth) , width: Int(Widht), height: (60 * items.count))
        
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hSEQDocument?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! QualityDocCell
        let data = hSEQDocument?.data[indexPath.row]
        cell.l1.text = data?.document_title
        if let date = data?.date_create?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            cell.l3.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            cell.l2.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        return cell
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = hSEQDocument?.data[indexPath.row]
        if qualityDoc == "Quality Documentation" {
            let vc = QualityDocumentDetailVC.instantiate(fromAppStoryboard: .main)
            vc.qualityDoc = qualityDoc
            vc.documentListData = data
            navigationController?.pushViewController(vc, animated: true)
        } else if enviromentDoc == "Environment Documentation" {
            let vc = QualityDocumentDetailVC.instantiate(fromAppStoryboard: .main)
            vc.enviromentDoc = enviromentDoc
            vc.documentListData = data
            navigationController?.pushViewController(vc, animated: true)
        } else if safetyDoc == "Safety Documentation" {
            let vc = QualityDocumentDetailVC.instantiate(fromAppStoryboard: .main)
            vc.safetyDoc = safetyDoc
            vc.documentListData = data
            navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    @IBAction func buttonActionUpload(_ sender: UIButton) {
        Console.log("buttonActionUpload")
        if qualityDoc == "Quality Documentation" {
            self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
                if selected == 1 {
                    self.openCamera()
                } else if selected == 2 {
                    self.openGallery()
                }
            }
        } else if enviromentDoc == "Environment Documentation" {
            self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
                if selected == 1 {
                    self.openCamera()
                } else if selected == 2 {
                    self.openGallery()
                }
            }
        } else if safetyDoc == "Safety Documentation" {
            self.showAlertWithActions(msg: "Choose Image from", titles: ["Camera", "Gallery", "Cancel"]) { (selected) in
                if selected == 1 {
                    self.openCamera()
                } else if selected == 2 {
                    self.openGallery()
                }
            }
        }
    }

    
    
}
