//
//  QualityDocumentDetailVC.swift
//  HRSB
//
//  Created by Zakir Khan on 17/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class QualityDocumentDetailVC: UIViewController {

    @IBOutlet weak var labelDocumentTitle: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var imageHRSQDocument: UIImageView!
    
    var qualityDoc = ""
    var enviromentDoc = ""
    var safetyDoc = ""
    var documentListData: DocumentListData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Console.log(documentListData)
        setupData()
    }
    
    func setupData() {
        labelTitle.text = documentListData?.document_title
        if let date = documentListData?.date_create?.getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            labelTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            labelDate.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        
        if qualityDoc == "Quality Documentation" {
            labelDocumentTitle.text = qualityDoc
            imageHRSQDocument.image = UIImage(named: "quality-documentation-flat")
        } else if enviromentDoc == "Environment Documentation" {
            labelDocumentTitle.text = enviromentDoc
            imageHRSQDocument.image = UIImage(named: "environment-documentation")
        } else if safetyDoc == "Safety Documentation" {
            labelDocumentTitle.text = safetyDoc
            imageHRSQDocument.image = UIImage(named: "safety-documentation-flat")
        }
    }


}


