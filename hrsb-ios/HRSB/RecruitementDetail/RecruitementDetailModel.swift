//
//  RecruitementDetailModel.swift
//  HRSB
//
//  Created by Salman on 19/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct RecruitementDetail: Codable {
    let data: RecruitementDetailData?
}

struct RecruitementDetailData: Codable {
    let job_type_id: String?
    let minimum_experience: String?
    let short_description: String?
    let company_id: String?
    let date_of_closing: String?
    let long_description: String?
    let job_title: String?
    let job_vacancy: String?
    let qualification: String?
    let basic_salary: String?
    let job_id: String?
    let designation_id: String?
    let location: String?
    let responsibility: String?
    let gender: String?
    let status: String?
}
