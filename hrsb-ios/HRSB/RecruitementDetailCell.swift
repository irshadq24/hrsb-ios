//
//  RecruitementDetailCell.swift
//  HRSB
//
//  Created by Salman on 18/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class RecruitementDetailCell: UITableViewCell {

    @IBOutlet weak var viewDot: UIView!

    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    
    func setup() {
        viewDot.makeRoundCorner(5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
