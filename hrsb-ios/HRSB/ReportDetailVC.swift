//
//  ReportDetailVC.swift
//  HRSB
//
//  Created by Salman on 01/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ReportModel {
    var title = ""
    var description = ""
    init(info: [String: Any]) {
        self.title = info["title"] as? String ?? ""
        self.description = info["description"] as? String ?? ""
    }
    
}

class ReportDetailVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    var tenderId = ""
    var arrModel = [ReportModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDetail()
    }
    
    func fetchDetail() {
        let endPoint = MethodName.getTenderDetail
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "read_tender", "tender_id":"\(tenderId)"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let data = dict["data"] as? [String: Any] {
                    self?.setupModel(info: data)

                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    func setupModel(info: [String: Any]) {
        arrModel.append(ReportModel(info: ["title": "Company", "description": info["company"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Client", "description": info["client"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Tender Date Received", "description": info["tender_date_receive"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Type", "description": info["tender_type"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Description", "description": info["description"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Closing Date", "description": info["closing_submitted_date"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Time", "description": info["time"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Tender/RFQ/ITQ REF. NO:", "description": info["reference_no"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Method of Sending", "description": info["meathod_of_sending"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Site visit/ Tender Briefing", "description": info["site_visit"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Status", "description": info["status"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Value (RM)", "description": "RM \(info["value"] as? String ?? "")"]))
        arrModel.append(ReportModel(info: ["title": "Remark", "description": info["remark"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "1st extension", "description": info["extension_date_one"] as? String ?? "-"]))
        arrModel.append(ReportModel(info: ["title": "2nd extension", "description": info["extension_date_two"] as? String ?? "-"]))
        arrModel.append(ReportModel(info: ["title": "3rd extension", "description": info["extension_date_three"] as? String ?? "-"]))
        arrModel.append(ReportModel(info: ["title": "Reason for extension", "description": info["reason"] as? String ?? ""]))
        arrModel.append(ReportModel(info: ["title": "Attachment", "description": info["tender_file"] as? String ?? ""]))
        self.tblView.reloadData()
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ReportDetailVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModel.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellReportDetail", for: indexPath)
        let lblTitle = cell.viewWithTag(120) as! UILabel
        let lblDescription = cell.viewWithTag(121) as! UILabel
        lblTitle.text = arrModel[indexPath.row].title
        lblDescription.text = arrModel[indexPath.row].description
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
