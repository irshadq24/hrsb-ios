//
//  TenderReportListVC.swift
//  HRSB
//
//  Created by Salman on 01/10/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit


class TendorModel {
    var company = ""
    var tender_type = ""
    var tender_date_receive = Date()
    var closing_submitted_date = Date()
    var status = ""
    var priority = ""
    var tender_id = ""
    var converted_to_proposal = ""
    
    
    init(info: [String: Any]) {
        self.company = info["company"] as? String ?? ""
        self.tender_type = info["tender_type"] as? String ?? ""
        self.status = info["status"] as? String ?? ""
        self.priority = info["priority"] as? String ?? ""
        self.tender_id = info["tender_id"] as? String ?? ""
        self.converted_to_proposal = info["converted_to_proposal"] as? String ?? ""
        if let strDate = info["tender_date_receive"] as? String, let date = strDate.getDateInstance(validFormat: "yyyy-MM-dd") {
            self.tender_date_receive = date
        }
        if let strDate = info["closing_submitted_date"] as? String, let date = strDate.getDateInstance(validFormat: "yyyy-MM-dd") {
            self.closing_submitted_date = date
        }
    }
}

class TenderReportListVC: UIViewController {
    var arrTendor = [TendorModel]()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAllTendor()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func fetchAllTendor() {
        let endPoint = MethodName.getTenderList
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: ["type": "tender_list"], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let tickets = dict["data"] as? [[String: Any]] {
                    for ticket in tickets {
                        self?.arrTendor.append(TendorModel(info: ticket))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
}

extension TenderReportListVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTendor.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTendor", for: indexPath) as! CellTendor
        cell.info = arrTendor[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReportDetailVC") as! ReportDetailVC
        vc.tenderId = arrTendor[indexPath.row].tender_id
        navigationController?.pushViewController(vc, animated: true)
    }
}
