//
//  TicketCommentVC.swift
//  HRSB
//
//  Created by Salman on 03/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit



class CommentModel {
    var id = ""
    var ticketId = ""
    var empName = ""
    var empDesig = ""
    var comment = ""
    var date = ""
    var instance: TicketCommentVC!
    
    init(info: [String: Any]) {
        self.id = info["comment_id"] as? String ?? ""
        self.ticketId = info["ticket_id"] as? String ?? ""
        self.empName = info["employee_name"] as? String ?? ""
        self.empDesig = info["employee_designation"] as? String ?? ""
        self.comment = info["ticket_comment"] as? String ?? ""
    }
}


class TicketCommentVC: UIViewController {

    @IBOutlet weak var lblTicketDescription: UILabel!
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    var arrTicketModel = [CommentModel]()
    var ticketModel: TicketModel?
    @IBOutlet weak var lblTitle: UILabel!
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification , object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification , object:nil)
        lblTitle.text = ticketModel?.subject ?? ""
        fetchComments()
    }

    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardHeight = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        UIView.animate(withDuration: 0.25) {
            if self.hasTopNotch {
                self.constBottom.constant = keyboardHeight-32
            }
            self.constBottom.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.25) {
            self.constBottom.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func fetchComments() {
        let endPoint = MethodName.getAllComment
        ApiManager.request(path: endPoint, parameters: ["ticket_id": ticketModel?.ticketId ?? ""], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let tickets = dict["data"] as? [[String: Any]] {
                    for ticket in tickets {
                        self?.arrTicketModel.append(CommentModel(info: ticket))
                    }
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapSendComment(_ sender: Any) {
        if txtViewComment.text! == "" {
            self.showAlert(title: "", message: "Please add comment.")
            return
        }
        let endPoint = MethodName.addComment
        let params = ["ticket_id": ticketModel?.ticketId ?? "", "user_id": getUserDefault(Constant.UserDefaultsKey.UserId), "xin_comment": txtViewComment.text!, "add_type": "set_comment", "employee_name": getUserDefault(Constant.UserDefaultsKey.Name), "ticket_comment": txtViewComment.text!]
        txtViewComment.text = ""
        ApiManager.request(path: endPoint, parameters: params, methodType: .post) { [weak self](result) in
            switch result {
            case .success(_):
                let indexPath = IndexPath(row: self?.arrTicketModel.count ?? 0, section: 0)
                self?.arrTicketModel.append(CommentModel(info: params))
                self?.tblView.insertRows(at: [indexPath], with: .left)
                break
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension TicketCommentVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTicketModel.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellComment", for: indexPath) as! CellComment
        let data = arrTicketModel[indexPath.row]
        data.instance = self
        cell.info = data
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
