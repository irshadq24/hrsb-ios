//
//  TicketDetailsVC.swift
//  HRSB
//
//  Created by Salman on 02/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown
class TicketDetailsVC: UIViewController {
    var arrImg = ["Compensationbenifits","Compensationbenifits","Compensationbenifits", "Compensationbenifits"]
    var arrLbl = ["Details","Comments","Ticket Files", "Note"]
    var ticketModel: TicketModel?
    let dropDownAssigned = DropDown()
    
    @IBOutlet weak var btnAssignedTo: UIButton!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblTicketCode: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblStaffID: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var viewAssign: UIView!
    var ticketId = ""
    var arrEmployee = [[String: String]]()
    var assignedEmp = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        readTicket()
    }
    func getEmployee() {
        let endPoint = MethodName.getEmployee
        ApiManager.request(path: endPoint, parameters: [:], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let employee = dict["data"] as? [[String: String]] {
                    self?.arrEmployee = employee
                    var names = [String]()
                    for emp in self?.arrEmployee ?? [] {
                        names.append(emp["employee_name"] ?? "")
                    }
                    self?.dropDownAssigned.dataSource = names
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func viewSetup() {
        getEmployee()
        dropDownAssigned.anchorView = btnAssignedTo
        dropDownAssigned.bottomOffset = CGPoint(x: 0, y: btnAssignedTo.bounds.height)
        // Action triggered on selection
        dropDownAssigned.selectionAction = { [weak self] (index, item) in
            var emp = self?.arrEmployee[index]
            let names = self?.btnAssignedTo.titleLabel?.text ?? ""
            if names != "" {
                self?.assignedEmp = (emp?["employee_id"] ?? "")
                self?.btnAssignedTo.setTitle("\(names), \(emp?["employee_name"] ?? "")", for: .normal)
            }else {
                self?.assignedEmp = (self?.assignedEmp ?? "")+","+(emp?["employee_id"] ?? "")
                self?.btnAssignedTo.setTitle("\(emp?["employee_name"] ?? "")", for: .normal)
            }
        }
        viewAssign.makeRoundCorner(20)
        viewAssign.makeBorder(1, color: UIColor.lightGray)
        lblEmpName.text = ticketModel?.empName
        lblCompany.text = ticketModel?.company
        //lblStaffID.text = ticketModel?.status
        lblSubject.text = ticketModel?.subject
        lblPriority.text = ticketModel?.priority
        if let date = ticketModel?.date.toString(dateFormat: "dd-MM-yyyy"), let time = ticketModel?.date.toString(dateFormat: "hh-mm a"){
            lblDate.text = "\(date)"
            lblTime.text = "\(time)"
        }
        lblStaffID.text = ticketModel?.empID
        lblCategory.text = ticketModel?.category
        lblSubCategory.text = ticketModel?.subCategory
        lblTicketCode.text = ticketModel?.ticketCode
        btnAssignedTo.setTitle(ticketModel?.assignTo ?? "", for: .normal)
    }
    
    @IBAction func tapEdit(_ sender: Any) {
        let addTicket = self.storyboard?.instantiateViewController(withIdentifier: "AddTicketVC") as! AddTicketVC
        addTicket.ticketModel = self.ticketModel
        addTicket.isForEdit = true
        self.navigationController?.pushViewController(addTicket, animated: true)
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func readTicket() {
        let endPoint = MethodName.readTicket
        ApiManager.request(path: endPoint, parameters: ["ticket_id": ticketModel?.ticketId ?? ""], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let ticketDetail = dict["data"] as? [String: Any] {
                    print("read", ticketDetail)
                    self?.ticketModel?.empID = ticketDetail["employee_id"] as? String ?? ""
                    self?.ticketModel?.companyId = ticketDetail["company_id"] as? String ?? ""
                    self?.ticketModel?.ticketPriority = ticketDetail["ticket_priority"] as? String ?? ""
                    self?.ticketModel?.description = ticketDetail["description"] as? String ?? ""
                    self?.ticketModel?.category = ticketDetail["catetory"] as? String ?? ""
                    self?.ticketModel?.subCategory = ticketDetail["sub_category"] as? String ?? ""
                    self?.ticketModel?.assignTo = ticketDetail["assigned_to"] as? String ?? ""
                    self?.ticketModel?.ticketNote = ticketDetail["ticket_note"] as? String ?? ""
                    self?.ticketModel?.ticketCode = ticketDetail["ticket_code"] as? String ?? ""
                    self?.ticketModel?.attachment = ticketDetail["attachment"] as? String ?? ""
                    self?.viewSetup()
                }
                
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapAssignedTo(_ sender: Any) {
        dropDownAssigned.show()
    }
}

extension TicketDetailsVC: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ITMenuCell", for: indexPath) as! ITMenuCell
        cell.lblName.text = arrLbl[indexPath.row]
        cell.img.image = UIImage(named: arrImg[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let status = storyboard?.instantiateViewController(withIdentifier: "UpdateTicketStatus")as! UpdateTicketStatus
            status.ticketModel = ticketModel
            navigationController?.pushViewController(status, animated: true)
        }
        if indexPath.row == 1 {
            let comment = storyboard?.instantiateViewController(withIdentifier: "TicketCommentVC")as! TicketCommentVC
            comment.ticketModel = ticketModel!
            navigationController?.pushViewController(comment, animated: true)
        }
        if indexPath.row == 2 {
            let files = storyboard?.instantiateViewController(withIdentifier: "TicketFilesVC") as! TicketFilesVC
            files.ticketId = ticketModel?.ticketId ?? ""
            navigationController?.pushViewController(files, animated: true)
        }
        if indexPath.row == 3 {
            let note = storyboard?.instantiateViewController(withIdentifier: "NoteVc") as! NoteVc
            note.ticketModel = ticketModel
            navigationController?.pushViewController(note, animated: true)
        }
    }
}
