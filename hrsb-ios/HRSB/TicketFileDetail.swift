//
//  TicketFileDetail.swift
//  HRSB
//
//  Created by Salman on 21/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TicketFileDetail: UIViewController {

    @IBOutlet weak var txtAttachment: UITextView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    var info = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = info["attachment_title"] ?? ""
        if let date = (info["created_date"] ?? "").getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            lblTime.text = "\(date.toString(dateFormat: "hh:mm a"))"
            lblDate.text = "\(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        btnDelete.makeRoundCorner(25)
        txtAttachment.text = info["attachment_view"] ?? ""
        lblDescription.text = info["attachment_description"] ?? ""
    }
    

    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDelete(_ sender: Any) {
        self.showOkCancelAlertWithAction("Are you sure, you want to delete?") { (isOk) in
            if isOk {
                let endPoint = MethodName.fetchFiles
                ApiManager.request(path: endPoint, parameters: ["ticket_attachment_id": "\(self.info["attachment_id"] ?? "")", "edit_type":"delete_attachment"] , methodType: .post) { [weak self](result) in
                    switch result {
                    case .success(let data):
                        guard let dict = data as? [String: Any] else {
                            return
                        }
                        if let status = dict["status"] as? String, status == "1" {
                            self?.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                                self?.navigationController?.popViewController(animated: true)
                            })
                        } else {
                            self?.showAlert(title: "", message: dict["message"] as? String ?? "")
                        }
                    case .failure(let error):
                        self?.showAlert(title: "", message: error.debugDescription)
                    case .noDataFound(_):
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func tapDownload(_ sender: Any) {
        if let url = URL(string: info["attachment_download"] ?? "")  {
            self.downloadImage(from: url)
        }
    }
    
}
