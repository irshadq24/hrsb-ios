//
//  TicketFilesVC.swift
//  HRSB
//
//  Created by Salman on 04/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TicketFilesVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var ticketId = ""
    var arrTicketFiles = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchTicketFiles()
    }
    
    func fetchTicketFiles() {
        let endPoint = MethodName.fetchFiles
        ApiManager.request(path: endPoint, parameters: ["ticket_id": "\(ticketId)"], methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let tickets = dict["data"] as? [[String: String]] {
                    self?.arrTicketFiles = tickets
                    self?.tblView.reloadData()
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func TapAddTicktFile(_ sender: Any) {
        let addTicketFile = storyboard?.instantiateViewController(withIdentifier: "AddTicketFilesVc") as! AddTicketFilesVc
        addTicketFile.ticketId = ticketId
        navigationController?.pushViewController(addTicketFile, animated: true)
    }
}



extension TicketFilesVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTicketFiles.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTicketFiles", for: indexPath)
        let lblTitle = cell.viewWithTag(120) as! UILabel
        let lblDate = cell.viewWithTag(121) as! UILabel
        let lblTime = cell.viewWithTag(122) as! UILabel
        lblTitle.text = arrTicketFiles[indexPath.row]["attachment_title"] as? String ?? ""
        if let date = (arrTicketFiles[indexPath.row]["created_date"] as? String ?? "").getDateInstance(validFormat: "yyyy-MM-dd HH:mm:ss") {
            lblTime.text = "Time: \(date.toString(dateFormat: "hh:mm a"))"
            lblDate.text = "Date: \(date.toString(dateFormat: "dd-MM-yyyy"))"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = storyboard?.instantiateViewController(withIdentifier: "TicketFileDetail") as! TicketFileDetail
        detail.info = arrTicketFiles[indexPath.row]
        navigationController?.pushViewController(detail, animated: true)
    }
}
