//
//  TimeSlotCelll.swift
//  HRSB
//
//  Created by Air 3 on 10/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TimeSlotCelll: UICollectionViewCell {
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var viewSlot: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSlot.makeRoundCorner(5)
        viewSlot.makeBorder(1, color: .black)
    }
}
