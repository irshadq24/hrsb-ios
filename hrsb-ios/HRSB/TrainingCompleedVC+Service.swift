//
//  TrainingCompleedVC+Service.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

extension TrainingCompleedVC {
    func requestServer(param: [String: Any]?) {
        let endPoint = MethodName.trainingJoin
        ApiManager.request(path:endPoint, parameters: param, methodType: .post) { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(data: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }
    }
    
    func handleSuccess(data: Any) {
        if let data = data as? [String : Any] {
            do{
                let data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                let decoder = JSONDecoder()
                do {
                    DisplayBanner.show(message: "You have join training successfully!")
                    dismiss(animated: true, completion: nil)
//                    self.traningAndDevelopment = try decoder.decode(TraningAndDevelopment.self, from: data)
//                    Console.log("Data:- \(String(describing: self.traningAndDevelopment))")
                   
                    
                } catch {
                    Console.log(error.localizedDescription)
                    DisplayBanner.show(message: error.localizedDescription)
                }
            } catch {
                Console.log(error.localizedDescription)
                DisplayBanner.show(message: error.localizedDescription)
            }
            
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }
}
