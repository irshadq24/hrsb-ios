//
//  TrainingCompleedVC.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TrainingCompleedVC: UIViewController {

    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var buttonJoin: UIButton!
    
    var trainingId: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
       
    }
    
    func setup() {
        viewCard.makeRoundCorner(10)
        buttonJoin.makeRoundCorner(25)
    }

    @IBAction func buttonActionClose(_ sender: UIButton) {
        Console.log("buttonActionClose")
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func buttonActionOkay(_ sender: UIButton) {
        Console.log("buttonActionOkay")
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["user_id": userID, "training_id": trainingId ?? ""])
        
        
    }

}
