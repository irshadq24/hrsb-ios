//
//  TrainingDevelopmentDetailVC+Action.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension TrainingDevelopmentDetailVC {
    
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionJoin(_ sender: UIButton) {
        Console.log("buttonActionJoin")
        let vc = TrainingCompleedVC.instantiate(fromAppStoryboard: .main)
        vc.trainingId = trainingDeatil?.training_id
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
}
