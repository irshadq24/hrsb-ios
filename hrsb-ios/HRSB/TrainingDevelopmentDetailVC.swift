//
//  TrainingDevelopmentDetailVC.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class TrainingDevelopmentDetailVC: UIViewController {

    @IBOutlet weak var buttonBooki: UIButton!
    @IBOutlet weak var labelCompnayName: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelTrainerName: UILabel!
    @IBOutlet weak var labelTrainerCost: UILabel!
    @IBOutlet weak var labelStartDate: UILabel!
    @IBOutlet weak var labelFinishDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var labelTitleName: UILabel!
    
    var trainingDeatil: TraningDevelopment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        Console.log("TraningDevelopment:- \(String(describing: trainingDeatil))")
    }
    
    func setup() {
        Console.log(trainingDeatil)
        labelDate.text = trainingDeatil?.training_date
        labelTitleName.text = trainingDeatil?.title
        textViewDescription.text = trainingDeatil?.description
        labelStatus.text = trainingDeatil?.status
        labelCompnayName.text = trainingDeatil?.company_name
        labelType.text = "Internal"
        labelTrainerCost.text = trainingDeatil?.training_cost
        let date = trainingDeatil?.training_date
        if let startDate = date?.split(separator: " "), startDate.count > 2 {
            labelStartDate.text = String(startDate[0])
            labelFinishDate.text = String(startDate[2])
        }
        buttonBooki.makeRoundCorner(20)
        buttonBooki.makeBorder(1, color: UIColor.init(hexString: "#FF9795"))
    }
}
