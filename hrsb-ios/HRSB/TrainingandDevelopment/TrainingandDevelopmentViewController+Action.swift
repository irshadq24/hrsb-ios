//
//  TrainingandDevelopmentViewController+Action.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension TrainingandDevelopmentViewController {
    
    @objc func buttonActionJoin(_ sender: UIButton) {
        Console.log("buttonActionJoin")
        let trainingId = traningAndDevelopment?.data
        let vc = TrainingCompleedVC.instantiate(fromAppStoryboard: .main)
        vc.trainingId = trainingId?[sender.tag].training_id
        navigationController?.present(vc, animated: true, completion: nil)
    }
    @objc func buttonActionView(_ sender: UIButton) {
        Console.log("buttonActionView")
        let vc = TrainingDevelopmentDetailVC.instantiate(fromAppStoryboard: .main)
        vc.trainingDeatil = traningAndDevelopment?.data?[sender.tag]
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
