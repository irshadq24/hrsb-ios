//
//  TrainingandDevelopmentViewController.swift
//  HRSB
//
//  Created by BestWeb on 19/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import MaterialComponents
class TrainingandDevelopmentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MDCTabBarDelegate,MDCTabBarControllerDelegate {

    
    
    
    
    
    @IBOutlet var view_training: UIView!
    
    
    @IBOutlet weak var closetraining: UIButton!
    
    
    
    
    @IBOutlet weak var view_white: UIView!
    
    
    
    @IBOutlet weak var back: UIButton!
    
    
    let reuseIdentifier = "TraininganddevelopmentCell"
    
    var appBarViewController = MDCAppBarViewController()

    
    var items = ["Recruitment", "Employee", "Staff Relation","Training and Development","Claim","Admin","Calendar"]
    var traningAndDevelopment: TraningAndDevelopment?
    
    @IBOutlet weak var tble: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addChild(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParent: self)
        let userID = Constants.userDefaults.value(forKey: "UserId")as? String ?? ""
        requestServer(param: ["user_id": userID])
        
        
        self.appBarViewController.headerView.trackingScrollView = nil
        
        
        
        self.appBarViewController.headerView.backgroundColor = UIColor(red: 104/255.0, green: 56/255.0, blue: 148/255.0, alpha: 1.0)
        
        
        view_white.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: self.view.frame.width, height: 60)
        
        
        let menuItemImage = UIImage(named: "menu.png")
        /// let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        
        
        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysOriginal)
        
        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.leftBarButtonItem = menuItem
        
        let searchItemImage = UIImage(named: "bell.png")
        let templatedSearchItemImage = searchItemImage?.withRenderingMode(.alwaysOriginal)
        let searchItem = UIBarButtonItem(image: templatedSearchItemImage,
                                         style: .plain,
                                         target: self,
                                         action: #selector(self.NotifyAction))
        
        
        let tuneItemImage = UIImage(named: "Bell1")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysOriginal)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: self,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem, searchItem ]
        
        let imageView = UIImageView(frame: CGRect(x: 80, y: 0, width: 40, height: 10))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "toplogo.png")
        imageView.image = image
        
        self.appBarViewController.headerStackView.addSubview(imageView)
        
        
        
        
        imageView.frame = CGRect(x: view.frame.size.width/2 - 50, y: appBarViewController.headerStackView.frame.width/2, width: 100, height: 50)

        
        
        tble.dataSource = self
        tble.delegate = self

        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @objc func NotifyAction(){
        
        
//
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifiers.NotifyVC)
        //    presentedVC.view.backgroundColor = UIColor.green
        // presentedVC.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(didTapCloseButton(_:)))
        //   let nvc = UINavigationController(rootViewController: presentedVC)
        
        navigationController?.pushViewController(presentedVC, animated: true)

        
        
      //  present(presentedVC, animated: false, completion: nil)
        
        
    }

    
    @IBAction func back_Action(_ sender: Any) {
        navigationController?.popViewController(animated: true)

        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//
//        self.dismiss(animated: false, completion: nil)

        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return traningAndDevelopment?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tble.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! TraininganddevelopmentCell
        let data = traningAndDevelopment?.data?[indexPath.row]
        cell.l1.text = data?.title
        cell.l_date.text = data?.training_date
        cell.l_dtal.text = data?.description
       
        cell.join.tag = indexPath.row
        cell.join.addTarget(self, action: #selector(buttonActionJoin(_:)), for: .touchUpInside)
        cell.buttonView.tag = indexPath.row
        cell.buttonView.addTarget(self, action: #selector(buttonActionView(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return 200
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//
//        view.addSubview(view_training)
//
//
//        view_training.frame = CGRect(x: 0, y: self.appBarViewController.headerView.frame.origin.y + self.appBarViewController.headerView.frame.height, width: view.frame.width, height: view.frame.height)
//
        
    }
    
    
    
    
    
    
    @IBAction func closetraining_Acton(_ sender: Any) {
        
        
        self.view_training.removeFromSuperview()
        
    }
    
    
    
    
    
    
    
    
    
}
