//
//  TraningAndDevelopment.swift
//  HRSB
//
//  Created by Air 3 on 21/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct TraningAndDevelopment: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [TraningDevelopment]?
}

struct TraningDevelopment: Codable {
    let training_id: String?
    let name: String?
    let company_name: String?
    let trainer_name: String?
    let training_date: String?
    let training_cost: String?
    let description: String?
    let status: String?
    let title: String?
    
}

