//
//  UpdateClaimModel.swift
//  HRSB
//
//  Created by Air 3 on 26/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct UpdateClaimModel: Codable {
    let status : String?
    let message: String?
    let data: UpdateClaim?
}

struct UpdateClaim: Codable {
    let employee_id: String?
    let expense_type_id: String?
    let project_code: String?
    let remark: String?
    let amount: String?
    let billcopy_file: String?
    let claim_date: String?
    
}




struct LeaveCategoryModel: Codable {
    let all_leave_types: [LeaveCategory]?
}

struct LeaveCategory: Codable {
    let leave_type_id:  String?
   // let company_id: String?
    let type_name: String?
    let days_per_year: String?
    let status: String?
    //let created_at: String?
    let balance_leave: String?
}
//all_leave_types
//0
//days_per_year : "14"
//leave_type_id : "1"
//type_name : "Annual Leave"
//balance_leave : "14"
//1
