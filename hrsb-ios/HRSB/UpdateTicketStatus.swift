//
//  DetailsViewController.swift
//  HRSB
//
//  Created by Salman on 03/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

class UpdateTicketStatus: UIViewController {

    @IBOutlet weak var txtRemark: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    var ticketModel: TicketModel?
    var dropDownStatus = DropDown()
    var statusId = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = ticketModel?.subject
        submitBtn.makeRoundCorner(10)
        dropDownStatus.anchorView = btnStatus
        dropDownStatus.dataSource = ["Open", "Closed"]
        dropDownStatus.bottomOffset = CGPoint(x: 0, y: btnStatus.bounds.height)
        // Action triggered on selection
        dropDownStatus.selectionAction = { [weak self] (index, item) in
            self?.btnStatus.setTitle(item, for: .normal)
            self?.statusId = "\(index)"
        }
    }
    
    
    @IBAction func tapStatus(_ sender: Any) {
        dropDownStatus.show()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        let endPoint = MethodName.updateTicketStatus
        let params = ["edit_type":"update_status","ticket_id":ticketModel?.ticketId ?? "", "status_id":self.statusId, "remarks": txtRemark.text!]
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                guard let dict = data as? [String: Any] else {
                    return
                }
                if let status = dict["status"] as? String, status == "1" {
                    self?.showOkAlertWithHandler(dict["message"] as? String ?? "", handler: {
                        self?.navigationController?.popViewController(animated: true)
                    })
                } else {
                    self?.showAlert(title: "", message: dict["message"] as? String ?? "")
                }
            case .failure(let error):
                self?.showAlert(title: "", message: error.debugDescription)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
}
