//
//  VehicleBookingApprovalVC+TableView.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation
import UIKit

extension VehicleBookingApprovalVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleApproval?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewVehicleApprovalList.dequeueReusableCell(withIdentifier: "LeaveApprovalCell", for: indexPath)as! LeaveApprovalCell
        let data = vehicleApproval?.data?[indexPath.row]
        //cell.labelStatus.text = "Status: \(data?.status ?? "")"
        cell.labelLeaveType.text = data?.title
        cell.labelDate.text = "Date from: \(data?.date_from ?? "")"
        cell.labelTime.isHidden = true
        cell.labelApplicant.text = "Applicant: \(data?.purpose ?? "")"
        
        if data?.status == "1" {
            cell.labelStatus.text = "Status: Pending"
        } else if data?.status == "2" {
            cell.labelStatus.text = "Status: Approved"
        } else if data?.status == "3" {
            cell.labelStatus.text = "Status: Rejected"
        } else if data?.status == "4" {
            cell.labelStatus.text = "Status: First level approval"
        } else if data?.status == "5" {
            cell.labelStatus.text = "Status: Canceled"
        } else if data?.status == "6" {
            cell.labelStatus.text = "Status: Second level approval"
        } else if data?.status == "7" {
            cell.labelStatus.text = "Status: Submitted"
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = vehicleApproval?.data?[indexPath.row]
        let vc = CorporateVehicleVC.instantiate(fromAppStoryboard: .main)
        vc.bookingId = data?.user_id ?? ""
        vc.project = data?.project ?? ""
        vc.purpose = data?.purpose ?? ""
        vc.date_from = data?.date_from ?? ""
        vc.status = data?.status ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
