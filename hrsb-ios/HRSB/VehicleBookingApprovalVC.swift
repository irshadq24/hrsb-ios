//
//  VehicleBookingApprovalVC.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class VehicleBookingApprovalVC: UIViewController {

    @IBOutlet weak var tableViewVehicleApprovalList: UITableView!
    var vehicleApproval: VehicleApproval?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewVehicleApprovalList.delegate = self
        tableViewVehicleApprovalList.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userID = Constants.userDefaults.value(forKey: Constant.UserDefaultsKey.UserId)as? String ?? ""
        requestServer(param: ["employee_id": userID])
    }
    
    @IBAction func buttonActionBack(_ sender: UIButton) {
        Console.log("buttonActionBack")
        navigationController?.popViewController(animated: true)
        
    }

}
