//
//  VehicleDetailView.swift
//  HRSB
//
//  Created by Air 3 on 09/08/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct VehicleDetailView: Codable {
    let status: String?
    let data: VehicleDetail?
}

struct VehicleDetail: Codable {
    let vehicle_id: String?
    let transport_type: String?
    let plat_no: String?
    let roadtax_date: String?
    let insurans_date: String?
    let puspakom_date: String?
    let location: String?
    let type: String?
    let bank: String?
    let remarks: String?
    let status: String?
}
