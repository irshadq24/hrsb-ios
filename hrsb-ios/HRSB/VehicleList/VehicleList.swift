//
//  VehicleList.swift
//  HRSB
//
//  Created by Air 3 on 31/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct VehicleListModel: Codable {
    let recordsTotal: Int?
    let recordsFiltered: Int?
    let data: [VehicleList]?
}

struct VehicleList: Codable {
    let vehicle_id: String?
    let user: String?
    let transport_type: String?
    let plat_no: String?
    let roadtax_date: String?
    let insurans_date: String?
    let puspakom_date: String?
    let location: String?
    let type: String?
    let bank: String?
    let remarks: String?
}


struct VehicleApproval: Codable {
    let data: [VehicleApprovalData]?
}

struct VehicleApprovalData: Codable {
    let status: String?
    let booking_id: String?
    let vehicle_id: String?
    let title: String?
    let purpose: String?
    let date_from: String?
    let project: String?
    let date_to: String?
    let user_id: String?
}

struct VehicleBooking: Codable {
    let status: String?
    let data: VehicleBookingDetail?
}

struct VehicleBookingDetail: Codable {
    let booking_id: String?
    let transport_type: String?
    let plat_no: String?
    let type: String?
    let title: String?
    let booked_by: String?
    let date_from: String?
    let date_to: String?
    let purpose: String?
    let project: String?
    
}


