//
//  ViewAddLicence.swift
//  HRSB
//
//  Created by Salman on 17/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit
import DropDown

protocol AddLicenceDelegate {
    func didAddLicencence(info: [String: String])
}

class ViewAddLicence: UIView {
    var arrCategory = [[String: String]]()
    var dropdown = DropDown()
    var selectedCategory = [String: String]()
    var delegate: AddLicenceDelegate?
    var assetId = ""
    @IBOutlet weak var btnChooseCategory: UIButton!
    @IBOutlet weak var txtEnterItem: UITextField!
    @IBOutlet weak var viewPopup: UIView!
    
    @IBAction func tapSubmit(_ sender: UIButton) {
        if txtEnterItem.text! == "" || selectedCategory.keys.count == 0 {
            kAppDelegate.window?.rootViewController?.showAlert(title: "", message: "Please enter detail.")
            return
        }
        let endPoint = MethodName.addAssetUpdate
        let params = ["type":"add_asset_update", "asset_id":assetId, "category_id": selectedCategory["license_id"] ?? "", "type_name": txtEnterItem.text!]
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let status = dict["status"] as? String, status == "1" {
                    self?.delegate?.didAddLicencence(info: ["category_name":self?.btnChooseCategory.titleLabel?.text ?? "", "type_name":self?.txtEnterItem.text ?? "", "category_id": self?.selectedCategory["license_id"] ?? ""])
                    self?.close()
                } else {
                    kAppDelegate.window?.rootViewController?.showAlert(title: "", message: "Something went wrong.")
                }
            case .failure(let error):
                break
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
    
    @IBAction func tapChooseCategory(_ sender: UIButton) {
        self.dropdown.selectionAction = { [weak self] (index, item) in
            sender.setTitle(item, for: .normal)
            self?.selectedCategory = self?.arrCategory[index] ?? [:]
        }
        self.dropdown.show()
    }
    
    override func awakeFromNib() {
        fetchCategory()
        let gestre = UITapGestureRecognizer(target: self, action: #selector(self.didTapGesture(_:)))
        self.addGestureRecognizer(gestre)
    }
    
    @objc func didTapGesture(_ sender: UITapGestureRecognizer) {
        let touchPoint = sender.location(in: self)
        if viewPopup.frame.contains(touchPoint) {
            print("touches")
        }else {
            self.close()
            print("not touch")
        }
    }
    
    func fetchCategory() {
        let endPoint = MethodName.assetDropdown
        ApiManager.requestMultipartApiServer(path: endPoint, parameters: [:], methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                if let dict = data as? [String: Any], let detail = dict["data"] as? [[String: String]] {
                    self?.arrCategory = detail
                    self?.dropdown.anchorView = self?.btnChooseCategory
                    var datasrc = [String]()
                    for item in detail {
                        datasrc.append(item["license_name"] as? String ?? "")
                    }
                    self?.dropdown.dataSource = datasrc
                    self?.dropdown.bottomOffset = CGPoint(x: 0, y: self?.btnChooseCategory.bounds.height ?? 0)
                }
            case .failure(let error):
                break
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            print(progress)
        }
    }
}
