//
//  ViewController.swift
//  HRSB
//
//  Created by BestWeb on 18/06/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    
    
    @IBOutlet weak var lftbtns: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
    @IBOutlet weak var ritbtn: UIButton!
    
    @IBOutlet weak var clctn: UICollectionView!
    
    
    @IBOutlet weak var pgcntrl: UIPageControl!
    
    var currentPage = 0

    
    
    var items = ["HUMAN RESOURCE & ADMINISTRATION", "EASY TO AMNAGE", "MULTIPLE LANGUAGE","EFFICIENT","FINANCE & ACCOUNT"]
    
    var Images = ["w1.png", "w2.png", "w3.png","w4.png","w5.png"]

    
    let reuseIdentifier = "SplashviewCell"

    

    
    @IBAction func login_Action(_ sender: Any) {
        let LoginVC = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.Login)
        self.present(LoginVC, animated: true, completion: nil)
    }
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.pgcntrl.numberOfPages = items.count
        clctn.dataSource = self
        clctn.delegate = self
        login.layer.cornerRadius = 20
        clctn.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        if CommonFunction.shared.getStringForKeyFromUserDefaults(key: Constant.UserDefaultsKey.ULogin) == "1" {
            clctn.isHidden = true
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.moveto), userInfo: nil, repeats: false)
        }else{
            clctn.isHidden = false
        }
    }

    
    @objc func moveto(){
        
        let LoginVC = CommonFunction.shared.getViewControllerWithIdentifier(identifier: Constant.ViewControllerIdentifiers.Home)
        self.present(LoginVC, animated: true, completion: nil)

    }
    
    
    
    @IBAction func ritbtn(_ sender: AnyObject) {
        
        
        
        
        
//        let visibleItems: NSArray = self.clctn.indexPathsForVisibleItems as NSArray
//        let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
//        let nextItem: NSIndexPath = NSIndexPath(row: currentItem.row + 1, section: 0)
//        self.clctn.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)

        
        
        let visibleItems: NSArray = self.clctn.indexPathsForVisibleItems as NSArray
        print(visibleItems)
        let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        print(currentItem)

        let nextItem: NSIndexPath = NSIndexPath(row: currentItem.row + 1, section: 0)
        
        
        
        print(nextItem)
        

        
        

        self.clctn.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
        

        
        
    }
    
    
    
    @IBAction func lftbtn(_ sender: Any) {
        
        
        
        let visibleItems: NSArray = self.clctn.indexPathsForVisibleItems as NSArray
        
        print(visibleItems)

        let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        print(currentItem)

        
        let nextItem: NSIndexPath = NSIndexPath(row: currentItem.row - 1, section: 0)
        
        print(nextItem)

        
        self.clctn.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
        
        
        
        

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        
        let count = self.items.count
        
        pgcntrl.numberOfPages = count
        pgcntrl.isHidden = !(count > 1)
        
        return count
        
        
        
        
        //  return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        
        let cell = clctn.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as! SplashviewCell
        
        
        cell.lbl1.text = items[indexPath.row]
        cell.img.image = UIImage(named: Images[indexPath.row])

        
        return cell
        
        
        
        
        
        
    }
    
    
    

    
    private func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    //    let currentIndex:CGFloat = self.clctn.contentOffset.x / self.clctn.frame.size.width
        
        
        let currentIndex:CGFloat = view.frame.width
        
        pgcntrl.currentPage = Int(currentIndex)
    }
    

    
    
    
    
    

    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
       // pgcntrl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        let pageWidth = view.frame.width
        
        pgcntrl?.currentPage = Int(pageWidth)

        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        let pageWidth = view.frame.width

        pgcntrl?.currentPage = Int(pageWidth)

       // pgcntrl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }


    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = view.frame.width
      //  self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        
        
        self.currentPage = Int(pageWidth)

        
        print("gfg", scrollView.frame.width)
        
        self.pgcntrl.currentPage = self.currentPage
    }


    @IBAction func Action_pg(_ sender: Any) {
        
        
        let visibleItems: NSArray = self.clctn.indexPathsForVisibleItems as NSArray
        
        print(visibleItems)
        
        let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        print(currentItem)

        
    }
    var thisWidth:CGFloat = 0

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pgcntrl.currentPage = indexPath.section
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        thisWidth = CGFloat(view.frame.width)
        return CGSize(width: thisWidth, height: view.frame.height)
    }


    
     func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if let collectionView = scrollView as? UIScrollView,
            let section = clctn.indexPathForItem(at: targetContentOffset.pointee)?.section {
            self.pgcntrl.currentPage = section
        }
    }

    
}

