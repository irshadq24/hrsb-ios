//
//  ViewDatePicker.swift
//  HRSB
//
//  Created by Salman on 18/09/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import UIKit

class ViewDatePicker: UIView {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    typealias CompletionBlock = (_ pickedDate: Date?)-> Void
    static var completionBlock: CompletionBlock?
    var selectedDate = Date()
    
    @IBOutlet weak var viewPopup: UIView!
    
    override func awakeFromNib() {
        let gestre = UITapGestureRecognizer(target: self, action: #selector(self.didTapGesture(_:)))
        self.addGestureRecognizer(gestre)
        datePicker.datePickerMode = .date
    }
    
    class func show(completion: @escaping CompletionBlock) {
        let viewAddLic = ViewDatePicker.fromNib(xibName: "ViewDatePicker") as!  ViewDatePicker
        viewAddLic.frame = kAppDelegate.window!.frame
        kAppDelegate.window?.addSubview(viewAddLic)
        completionBlock = completion
    }
    
    @IBAction func didChange(_ sender: UIDatePicker) {
        selectedDate = sender.date
    }
    
    @IBAction func tapDone(_ sender: Any) {
        ViewDatePicker.completionBlock?(selectedDate)
        self.close()
    }
    @objc func didTapGesture(_ sender: UITapGestureRecognizer) {
        let touchPoint = sender.location(in: self)
        if let senderView = sender.view, viewPopup.frame.contains(touchPoint) {
            print("touches")
        }else {
            self.close()
            print("not touch")
        }
    }
    
    
    
}
