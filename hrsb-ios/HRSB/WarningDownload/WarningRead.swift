//
//  WarningRead.swift
//  HRSB
//
//  Created by Air 3 on 29/07/19.
//  Copyright © 2019 BestWeb. All rights reserved.
//

import Foundation

struct WarningReadList: Codable {
    let warning_id: String?
    let company: String?
    let warning_to: String?
    let warning_by: String?
    let warning_date: String?
    let warning_type: String?
    let warning_file_download: String?
    let warning_file_view: String?
    let subject: String?
    let description: String?
    let status: String?
}
